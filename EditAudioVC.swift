//
//  EditVideo.swift
//  Latakoo
//
//  Created by ABC on 28/09/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//

import UIKit
import Photos
import AVKit
import AVFoundation
import DSWaveformImage




protocol EditAudioDelegate:class {
    func NewAudio(_ controller: EditAudioVC, file: URL , delete: Bool  , IsFromLib: Bool)

}

class EditAudioVC: UIViewController,ICGVideoTrimmerDelegate ,RETrimControlDelegate,AVAudioPlayerDelegate,UIActionSheetDelegate{
    func trimControl(_ trimControl: RETrimControl!, didChangeLeftValue leftValue: CGFloat, rightValue: CGFloat) {
      
         
            
        resetaudio = true
        startTime = leftValue
        stopTime = rightValue
          self.seekVideo(toPos: 0)
        audioPlayer?.currentTime = 0
        playnew.setImage(UIImage.init(named: "play"), for: .normal)
        progressslider.isHidden = false
        lblvideotimer.isHidden = false

        player.pause()
        stopPlaybackTimeChecker()
        audioPlayer!.pause()
      
      lblstarttime.text = timeFormatted(startTime, isWithMinutes: false)
      lblendtime.text = timeFormatted(stopTime, isWithMinutes: false)

        
        
        
    }
    
    
    let app = UIApplication.shared.delegate as! AppDelegate
     weak var delegate: EditAudioDelegate?

    @IBOutlet weak var Movieview: UIView!
    @IBOutlet weak var volumeslider: UISlider!
    var restartOnPlay = false
    @IBOutlet weak var trimmerView: ICGVideoTrimmerView!
    @IBOutlet weak var videoslider: UISlider!
    @IBOutlet weak var stack: UIStackView!
    var audioPlayer: AVAudioPlayer?
    var isportrait : Bool?
    var prevTrackerTime: CGFloat = 0.0
    var startTime: CGFloat = 0
    var stopTime: CGFloat = 0
    var trimControl: RETrimControl?
    var  IsFromLib : Bool?
    var player = AVPlayer()
    let controller = AVPlayerViewController()
    var playbackTimeCheckerTimer: Timer?
    var trimmerPositionChangedTimer: Timer?
    var videoPlaybackPosition: CGFloat = 0.0
    let playnew = UIButton(type: .custom)
    var videoUrl :URL?
    var AudioUrl :URL?
    var audioDurationSeconds : CGFloat = 0
    var videoduration :Float = 0

    @IBOutlet weak var volumes: UISlider!
    
    @IBOutlet weak var progressslider: UISlider!

    var finalurl : URL?
    var resetaudio = true
    let App = UIApplication.shared.delegate as! AppDelegate
    var paybackview : UIView?
    var x:CGFloat?
    var point:CGFloat?

    var sliderlabel : UILabel?

    var imageView : UIImageView?
    @IBOutlet var lblvideostartpoint: UILabel!

    @IBOutlet var lblstarttime: UILabel!
    @IBOutlet var lblendtime: UILabel!
    @IBOutlet var lblvideotimer: UILabel!
    @IBOutlet var lblvideotimerReverse: UILabel!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(videoUrl!)
        
        videoslider.setThumbImage(UIImage(named: "trimline"), for: .highlighted)
        videoslider.setThumbImage(UIImage(named: "trimline"), for: .normal)
        
        
        progressslider.setThumbImage(UIImage(named: "trimline_circle"), for: .highlighted)
        progressslider.setThumbImage(UIImage(named: "trimline_circle"), for: .normal)
        
        
        
       sliderlabel = UILabel(frame: CGRect(x: videoslider.frame.origin.x, y: videoslider.frame.origin.y-56, width: 30, height: 30))
      sliderlabel?.textAlignment = NSTextAlignment.center
      sliderlabel?.textColor = UIColor.black
        sliderlabel!.text = "0"
       x = videoslider.frame.origin.x
        sliderlabel?.font = UIFont.systemFont(ofSize: 12)
      self.view.addSubview(sliderlabel!)

        print(x!)
        print(videoslider.frame.origin.x)

        
        volumeslider.setThumbImage(UIImage(named: "trimline_circle"), for: .highlighted)
         volumeslider.setThumbImage(UIImage(named: "trimline_circle"), for: .normal)
        self.perform(#selector(loadvideo), with: nil, afterDelay: 0.5)
        self.perform(#selector(loadTrimmer), with: nil, afterDelay: 0.5)

        
      
        
        
    }
    
    
    @objc func loadTrimmer(){
       
        print(app.TrimThisAudio)
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
               let recDir = paths[0]
               let audioFileName = "\(recDir)/\(app.TrimThisAudio["orginalfile"] ?? "")"

               print(audioFileName)
        
            AudioUrl =  URL.init(fileURLWithPath: audioFileName)

                 try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                 try! AVAudioSession.sharedInstance().setActive(true)
        
        

                 try! audioPlayer = AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: audioFileName))
        
        
                 audioPlayer!.prepareToPlay()
                 audioPlayer!.pause()
                 audioPlayer!.delegate = self

               
               let audioAsset = AVURLAsset(url:URL.init(fileURLWithPath: audioFileName), options: nil)
               let audioDuration = audioAsset.duration
                audioDurationSeconds = CGFloat(CMTimeGetSeconds(audioDuration))
               
               print(audioDurationSeconds)
              stopTime = audioDurationSeconds
            lblendtime.text = timeFormatted(stopTime, isWithMinutes: false)

        
        var valueofy =  stack.frame.origin.y + 140
        var valueofynew =  stack.frame.origin.y + 151

       if UIDevice.current.hasNotch
        {
             valueofy =  stack.frame.origin.y + 175
             valueofynew =  stack.frame.origin.y + 186

        }
       
        
        
              imageView  = UIImageView(frame:CGRect(x: 42, y: valueofy , width: stack.frame.size.width - 44 , height: 50));
          
        imageView?.backgroundColor = UIColor.white
        self.view.addSubview(imageView!)
        
        let waveformImageDrawer = WaveformImageDrawer()
            waveformImageDrawer.waveformImage(fromAudioAt: URL.init(fileURLWithPath: audioFileName),
                                                 size: imageView!.bounds.size,
                                          style: .striped,
                                          position: .middle) { image in
            // need to jump back to main queue
            DispatchQueue.main.async {
                self.imageView!.image = image
            }
        }
        
        
         //  imageView?.image = UIImage(named:"equilizer")
           
        
        
         paybackview = UIView(frame: CGRect (x: 42, y: valueofy, width: 0, height: 50))
        paybackview!.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.addSubview(paybackview!)
        

               trimControl = RETrimControl(frame: CGRect(x: 20, y: valueofynew, width: stack.frame.size.width , height: 50))
                      trimControl?.length = audioDurationSeconds
                      trimControl?.delegate = self
                      if let trimControl = trimControl {
                      self.view.addSubview(trimControl)
                      self.view.bringSubview(toFront: trimControl)
        }
        
     
        
    }
    
    
    
    @objc func loadvideo(){
        self.play(url:videoUrl!, iscut: false)
    }
    
    func seekVideo(toPos pos: CGFloat) {
        videoPlaybackPosition = pos
        let time = CMTimeMakeWithSeconds(Float64(videoPlaybackPosition), (player.currentTime().timescale))
        player.seek(to: time, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
    }
    
   func trimmerView(_ trimmerView: ICGVideoTrimmerView, didChangeLeftPosition startTime: CGFloat, rightPosition endTime: CGFloat) {
         stopPlaybackTimeChecker()
          restartOnPlay = true
          player.pause()
        
         
      }
      
      
      
      func trimmerView(_ trimmerView: ICGVideoTrimmerView, didChangeLeftPosition startTime: CGFloat, middleStart MiddleTime: CGFloat, mIddleEnd MiddleEnd: CGFloat, rightPosition endTime: CGFloat) {
          
          restartOnPlay = true
         stopPlaybackTimeChecker()
         player.pause()
        
        
      
        
      }
    
    
    func timeFormatted(_ interval: CGFloat, isWithMinutes: Bool) -> String? {
    
        var milliseconds = UInt(interval * 1000)
        var seconds = milliseconds / 1000
        milliseconds %= 1000
        let minutes = seconds / 60
        seconds %= 60
        //    unsigned long hours = minutes / 60;
        //    minutes %= 60;

        var strMillisec = NSNumber(value: milliseconds).stringValue
        if strMillisec.count > 2 {
            strMillisec = (strMillisec as NSString).substring(to: 2)
        }

        if isWithMinutes {
            return String(format: "%02ld:%02ld:%02ld", Int(minutes), Int(seconds), Int(strMillisec) ?? 0)
        } else {
            return String(format: "%02ld:%02ld", Int(minutes), Int(seconds))
        }
    }
    
    func startPlaybackTimeChecker() {
        
        stopPlaybackTimeChecker()
        playbackTimeCheckerTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                                        selector:
            #selector(EditAudioVC.onPlaybackTimeChecker), userInfo: nil, repeats: true)
    }
    
    func stopPlaybackTimeChecker() {
        
        playbackTimeCheckerTimer?.invalidate()
        playbackTimeCheckerTimer = nil
    }
    
      @objc func onPlaybackTimeChecker() {
        let curTime = player.currentTime()
        var seconds = CMTimeGetSeconds(curTime)
            if seconds < 0 {
            seconds = Float64(0) // this happens! dont know why.
            }
            videoPlaybackPosition = CGFloat(seconds)
            trimmerView.seek(toTime: CGFloat(seconds))
        
        if Float(videoPlaybackPosition)<videoslider.value {
          self.paybackview!.frame.size.width = 0
        }
        
        let t = CGFloat(videoduration) - CGFloat(seconds)
        
        print(t)
        
        lblvideotimerReverse.text = timeFormatted(t, isWithMinutes: false)

        
        progressslider.value = Float(seconds)
        print("Start time \(startTime)")
        print("Stop time \(stopTime)")
        print("Audio player current time \(audioPlayer!.currentTime)")

        lblvideotimer.text = timeFormatted(CGFloat(seconds), isWithMinutes: false)

        
        if !audioPlayer!.isPlaying {
            if Float(seconds) >= videoslider.value {
                if resetaudio == true{
                    print("Audio player current time \(audioPlayer!.currentTime)")
                    audioPlayer?.currentTime = TimeInterval(startTime)
                    audioPlayer?.play()
                }else{
                    if audioPlayer!.currentTime > TimeInterval(startTime){
                        if  audioPlayer!.currentTime >= Double(stopTime - 0.2){

                              let widhthh = stack.frame.size.width - 44
                              let point = widhthh / CGFloat(audioDurationSeconds)
                              let curTimer = audioPlayer!.currentTime
                              var sec = CGFloat(curTimer)
                               sec = sec - startTime
                              let viewwidth = point*sec
                              print(viewwidth)
                            print("viewwidth value = > \(viewwidth)")
                            self.paybackview?.frame.origin.x = point*startTime + 42
                            print("x value = > \(point*startTime + 44)")
                            
                        UIView.animate(withDuration: 0.1, animations: {
                            self.paybackview!.frame.size.width = point*(self.stopTime - self.startTime
                       ) })
                        
                        }else{
                        audioPlayer?.play()
                        }
                    }
                }
              
            }
        }
      
        if audioPlayer!.isPlaying {
            
                   let widhthh = stack.frame.size.width - 44
                   let point = widhthh / CGFloat(audioDurationSeconds)
                   let curTimer = audioPlayer!.currentTime
                   var sec = CGFloat(curTimer)
                    sec = sec - startTime
                   let viewwidth = point*sec
                   print(viewwidth)
                 print("viewwidth value = > \(viewwidth)")
                 self.paybackview?.frame.origin.x = point*startTime + 42
                 print("x value = > \(point*startTime + 44)")
                   UIView.animate(withDuration: 0.1, animations: {
                       self.paybackview!.frame.size.width = viewwidth
                    })
            
            
            
            if  audioPlayer!.currentTime >= Double(stopTime - 0.2){
              audioPlayer!.pause()
                resetaudio = false
                UIView.animate(withDuration: 0.1, animations: {
                    self.paybackview!.frame.size.width = point*(self.stopTime - self.startTime
               ) })
               }
        }
       
       
        
        
        }
    
    
    
    
    @objc func itemDidFinishPlaying(_ notification: Notification) {
        let startTimer = CMTimeMake(Int64(0), 1)
        player.seek(to: startTimer)
        player.pause()
        audioPlayer?.pause()
        audioPlayer?.currentTime = 0
        playnew.setImage(UIImage.init(named: "play"), for: .normal)

        stopPlaybackTimeChecker()
        audioPlayer!.pause()
       resetaudio = true

        
    }
    
    
    
    
    
   
    
    
    func play(url:URL , iscut:Bool){
        

        if player == nil {
           
        }else{
            player.pause()
            controller.removeFromParentViewController()
            controller.view.removeFromSuperview()
//            trimmerView.removeFromSuperview()
        }
        app.HideProgress()
        
      

        finalurl = url
        trimmerView.asset = AVAsset.init(url: url)
        trimmerView.themeColor = UIColor.init(named:"backgroundcolor")!
        trimmerView.showsRulerView = false
        trimmerView.maxLength = 2000
        trimmerView.rulerLabelInterval = 10
        trimmerView.trackerColor = UIColor.cyan
        trimmerView.delegate = self
        trimmerView.thumbWidth = 10
        trimmerView.isremovethumb = true
        trimmerView.resetSubviews()
        trimmerView.showsRulerView = false
        trimmerView.hideTracker(false)
        
       
        let asset = AVAsset(url: url)
        let duration = asset.duration
         videoduration = Float(CMTimeGetSeconds(duration))
        print(videoduration)
        
        lblvideotimerReverse.text = timeFormatted(CGFloat(videoduration), isWithMinutes: false)

        let widhthh = videoslider.frame.size.width
         point = widhthh / CGFloat(videoduration )
        
        print(point!)
        
        videoslider.maximumValue = videoduration
        progressslider.maximumValue = videoduration
        videoslider.value =  Float(app.TrimThisAudio["playtime"] as? String ?? "0")!
        
         
        let t = Float(app.TrimThisAudio["playtime"] as? String ?? "0")!

        
        lblvideostartpoint.text = timeFormatted(CGFloat(t), isWithMinutes: false)



        print(videoslider.value)

        print(videoduration)
        
        
        let playerItem = AVPlayerItem(asset:AVAsset.init(url: url))
        player = AVPlayer(playerItem: playerItem)
        Movieview.backgroundColor = UIColor.clear
        let width = UIScreen.main.bounds.size.width
        player.volume = 0.4
        controller.showsPlaybackControls = false
        controller.view.frame = CGRect(x: Movieview.frame.origin.x, y: Movieview.frame.origin.y, width: width, height: Movieview.frame.size.height)
        playnew.frame = CGRect(x: Movieview.frame.origin.x, y: Movieview.frame.origin.y, width: width, height:Movieview.frame.size.height)
        addChildViewController(controller)
        view.addSubview(controller.view)
        
        playnew.isHidden = false
        playnew.setImage(UIImage.init(named: "play"), for: .normal)
        playnew.backgroundColor = .clear
        playnew.addTarget(self, action: #selector(playnewmethod), for: .touchUpInside)
        playnew.tintColor = UIColor.clear
        self.view.addSubview(playnew)
        self.view.bringSubview(toFront: playnew)
        view.bringSubview(toFront:progressslider)
        view.bringSubview(toFront:lblvideotimer)
        view.bringSubview(toFront:lblvideotimerReverse)


        controller.player = player
        player.pause()
        NotificationCenter.default.addObserver(self, selector: #selector(EditVideo.itemDidFinishPlaying(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        
        
        
    }
    
    
    

    
    

    
    

    
   
    
    @objc func playnewmethod(sender : UIButton){
      
        
        if player.isPlaying {
                playnew.setImage(UIImage.init(named: "play"), for: .normal)
                  player.pause()
                  stopPlaybackTimeChecker()
                 audioPlayer!.pause()
                  resetaudio = false
            progressslider.isHidden = false
            lblvideotimer.isHidden = false

             self.view.bringSubview(toFront: playnew)
            view.bringSubview(toFront:progressslider)
            view.bringSubview(toFront:lblvideotimer)
            view.bringSubview(toFront:lblvideotimerReverse)

              }
                else {
                
                playnew.setImage(UIImage.init(named: "pause"), for: .normal)
                 self.view.bringSubview(toFront: playnew)
                  player.play()
           // progressslider.isHidden = true
           // lblvideotimer.isHidden = true

            startPlaybackTimeChecker()
                  
                
              }
        
        self.trimmerView.hideTracker(false)
    }
    
    
    @IBAction func btnsave(_ sender: Any) {
        audioPlayer?.currentTime = 0
                      playnew.setImage(UIImage.init(named: "play"), for: .normal)
                      player.pause()
                      stopPlaybackTimeChecker()
                      audioPlayer!.pause()
        self.TrimAudio()
       
    }
    
    
    @IBAction func btndelete(_ sender: Any) {
        playnew.setImage(UIImage.init(named: "play"), for: .normal)
                         player.pause()
                         stopPlaybackTimeChecker()
                        audioPlayer!.pause()
        self.navigationController?.popViewController(animated: false)

        delegate?.NewAudio(self, file: AudioUrl!, delete:true, IsFromLib: IsFromLib!)
       
    }
    
    
    
    

    
    
    

    
    

    

    
    

    
    @IBAction func btnback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        playnew.setImage(UIImage.init(named: "play"), for: .normal)
                         player.pause()
                         stopPlaybackTimeChecker()
                        audioPlayer!.pause()
       
       }
    
    
    
    @IBAction func volumeslider(_ sender: Any) {
        self.audioPlayer!.volume = volumeslider.value
        print(volumes.value)
    
    }
    
    
    @IBAction func Progressslider(_ sender: Any) {
        playnew.setImage(UIImage.init(named: "play"), for: .normal)
        player.pause()
        stopPlaybackTimeChecker()
        audioPlayer!.pause()
        self.view.bringSubview(toFront: playnew)
       view.bringSubview(toFront:progressslider)
        view.bringSubview(toFront:lblvideotimer)
        player.seek(to: CMTimeMakeWithSeconds(Float64(progressslider!.value), 60000))
        lblvideotimer.text = timeFormatted(CGFloat(progressslider!.value), isWithMinutes: false)
        if progressslider.value>videoslider.value {
            var t = progressslider.value - videoslider.value
            if t > Float(stopTime) {
                t =  Float(stopTime) - 0.2
            }
            
            print(videoduration)
            print(progressslider.value)

           let tm = CGFloat(videoduration) - CGFloat(progressslider!.value)
            print(tm)
        lblvideotimerReverse.text = timeFormatted(tm, isWithMinutes: false)
            
        audioPlayer?.currentTime = TimeInterval(t)
        resetaudio = false
        }else{
        resetaudio = true
        }
    }
    
    @IBAction func videoSlider(_ sender: Any) {
      
     // self.seekVideo(toPos: 0)
     // audioPlayer?.currentTime = 0
      playnew.setImage(UIImage.init(named: "play"), for: .normal)
      

      player.pause()
      stopPlaybackTimeChecker()
      audioPlayer!.pause()
        
        if Float(videoPlaybackPosition)>videoslider.value {
                  var t = Float(videoPlaybackPosition) - videoslider.value
                  if t > Float(stopTime) {
                      t =  Float(stopTime) - 0.2
                  }
              audioPlayer?.currentTime = TimeInterval(t)
              resetaudio = false
              }else{
              resetaudio = true
              }
        

        self.paybackview?.frame.size.width = 0
        let t = Int(videoslider.value)
        lblvideostartpoint.text = timeFormatted(CGFloat(t), isWithMinutes: false)
        sliderlabel?.frame.origin.x = videoslider!.thumbCenterX - 14
        sliderlabel!.text = "\(t)"
        


    }
      

    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
           if !IS_IPHONE{
               self.perform(#selector(loadvideo), with: nil, afterDelay: 0.5)
            self.perform(#selector(changeloader), with: nil, afterDelay: 0.5)
              self.perform(#selector(loadTrimmer), with: nil, afterDelay: 0.5)


           }
       }
       
    
    

    @objc func changeloader(){
        app.initializeLoader()
    }
    
    @objc func resetplayer(){
        if finalurl != nil {
            self.play(url: finalurl!, iscut: false)
        }
    }
    
    
    
    func TrimAudio()
        {
        App.ShowProgress()
            let asset =  AVURLAsset(url: AudioUrl!, options: [AVURLAssetPreferPreciseDurationAndTimingKey : true])

            
            let length = Float(asset.duration.value) / Float(asset.duration.timescale)
        print("video length: \(length) seconds")
            
           
            
            let  outputURL = self.getDocumentsDirector().appendingPathComponent("1\(AudioUrl?.lastPathComponent ?? "")")
            
            print(outputURL)
            
            if FileManager.default.fileExists(atPath: outputURL.path) {
                       do {
                           try FileManager.default.removeItem(atPath: outputURL.path)
                       } catch {
                       }
            }
            
            
            print(outputURL)
        
            guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetPassthrough) else { return }
        exportSession.outputURL = outputURL
          
            if AudioUrl?.pathExtension == "wav" {
                
                exportSession.outputFileType = .wav
           
            }else{
               
                exportSession.outputFileType = .m4a

            }
        
            let timeRange = CMTimeRange(start: CMTime(seconds: (Double(startTime)), preferredTimescale: 1000),
                                        end: CMTime(seconds: (Double(stopTime)), preferredTimescale: 1000))
        
        exportSession.timeRange = timeRange
        exportSession.exportAsynchronously {
        switch exportSession.status {
        case .completed:
            DispatchQueue.main.async {
                
                    var Dict: [String: Any] = [:]
                      Dict["leftvalue"] = ""
                      Dict["rightvalue"] = ""
                       Dict["volumekey"] = "\(self.volumeslider.value)"
                      Dict["orginalfile"] = outputURL.lastPathComponent
                      Dict["trimfile"] = ""
                      Dict["playtime"] = "\(self.videoslider.value)"
                Dict["videoduration"] = self.app.TrimThisAudio["seconds"]
                self.app.TrimThisAudio = Dict
                
                print(self.app.TrimThisAudio)
                
              self.navigationController?.popViewController(animated: false)
                self.delegate?.NewAudio(self, file: outputURL, delete:false, IsFromLib: self.IsFromLib!)
                
            }
        case .failed:
        print("failed \(exportSession.error.debugDescription)")
            
            
        case .cancelled:
        print("cancelled \(exportSession.error.debugDescription)")
        default: break
        }
        }
    }

        func getDocumentsDirector() -> URL {
            // Grab CWD and set to paths
    //        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    //        return paths[0]
            
            let documentsURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            
              return documentsURL
        }

}


extension UISlider {
    var thumbCenterX: CGFloat {
        let trackRect = self.trackRect(forBounds: frame)
        let thumbRect = self.thumbRect(forBounds: bounds, trackRect: trackRect, value: value)
        return thumbRect.midX
    }
}


