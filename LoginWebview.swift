//
//  LoginWebview.swift
//  Latakoo
//
//  Created by bharat on 03/03/22.
//  Copyright © 2022 parangat technology. All rights reserved.
//

import UIKit
import WebKit
import Alamofire
import SwiftyJSON








final class WebCacheCleaner {

    class func clear() {
        URLCache.shared.removeAllCachedResponses()

        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        print("[WebCacheCleaner] All cookies deleted")

        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                print("[WebCacheCleaner] Record \(record) deleted")
            }
        }
    }

}


class LoginWebview: UIViewController,WKNavigationDelegate {
    @IBOutlet weak var webview: WKWebView!
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
      if (UserDefaults.standard.value(forKey: "home") != nil){
          var story : UIStoryboard?
                     
                if IS_IPHONE {
                     story = UIStoryboard.init(name: "Main", bundle: nil)
                    }else{
                     story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                }
          let TabbarController = story?.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
          if appdelegate.isfromshortcut == true {
          if appdelegate.isfromshortcutidentifir == "Capture" {
                  TabbarController.selectedIndex = 0

                  
          } else if appdelegate.isfromshortcutidentifir == "Pilot" {
              TabbarController.selectedIndex = 1

                  
          }else if appdelegate.isfromshortcutidentifir == "Library" {
                TabbarController.selectedIndex = 2

                  
          }else{
                  TabbarController.selectedIndex = 3

          }
              
          }else{
              TabbarController.selectedIndex = 1

          }
         // TabbarController.selectedIndex = 1
          self.navigationController?.pushViewController(TabbarController, animated: false)
      }else{
         
          WebCacheCleaner.clear()
          let url = URL(string: "https://dev.login.latakoo.com/?launcher=1")!
          webview.load(URLRequest(url: url))
          webview.allowsBackForwardNavigationGestures = true
          webview.navigationDelegate = self
         timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
          
       //   app.ShowProgress()

          
     }

  if (UserDefaults.standard.value(forKey: "server") == nil){
  
  UserDefaults.standard.set("https://latakoo.com/", forKey: "server")
  UserDefaults.standard.set("https://api2.latakoo.com/", forKey: "serverManifest")
  }
        
        
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
     
      
       

    }
   
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    @objc func timerAction() {
        webview.evaluateJavaScript("document.body.outerHTML", completionHandler: { result, error in
            if let html = result as? String {
            
                if html.contains("<span id=") {
                    print("exists")
                    self.timer.invalidate()
                    print(html)
                    let html = html.replacingOccurrences(of: "\"", with:"")
                    print(html)
                    let Strnew = html.slice(from:"<span id=data>", to: "</span>")
                    print(Strnew!)
                    let userid = Strnew!.slice(from:"{id:", to:",")
                    print(userid!)
                    let encodedstring = Strnew?.slice(from: "data:", to: "}")
                    print(encodedstring!)
                    let decodedData = Data(base64Encoded: encodedstring!)
                    let decodedString = String(data: decodedData!, encoding: .utf8)!
                    print(decodedString)
                    let dict = self.convertToDictionary(text: decodedString)
                    print(dict)
                    
                    let email = dict!["username"] as? String
                    let password = dict!["internal_system_ac"] as? String
                    
                    if password != nil{
                    self.login(LoginEMail: email!, LoginPassword: password!)
                    }
                
                   
                    }
                }
            })
       }
    
    
    func login(LoginEMail:String,LoginPassword:String){
        
            if !Reachability.isConnectedToNetwork() {
                print("Internet is Not available.")
                AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
                return
            }
            
          
            
            let qualityOfServiceClass = DispatchQoS.QoSClass.background
            let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
                
                // 3: Do your networking task or background work here.
                
                DispatchQueue.main.async(execute: { () -> Void in
            
            var tokenVal : String!
            if UserDefaults.standard.value(forKey: "deviceTokenString") == nil{
                tokenVal = "C649C67E66101C370344466BE9B2388C86DEF9641F2F655B5D526653EEF1BBFA"
            }else{
                tokenVal = UserDefaults.standard.value(forKey: "deviceTokenString") as? String
                print("tokenvalue %@",tokenVal)
                
            }
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
       //     appdelegate.ShowProgress()
                    
          let headers = ["User-Agent":"HTTP_USER_AGENT:Android",
                         "os_type": "IOS",
                         "os_version": "IOS14",
                         "app_version": "3.1.3"]
        //https://api2.latakoo.com/auth/sign-in?email=bharat.p%40parangat.com&password=ritu%40123

            let aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)auth/sign-in"
            let aDictParam : [String:Any]!
                    aDictParam = ["email": LoginEMail ,
                          "password": LoginPassword
            ]
                    
            print(aStrApi)
            print(aDictParam)
                    print(headers)


                    
                    let encoding = Alamofire.JSONEncoding.default
                           Alamofire.request(aStrApi, method: .post, parameters: aDictParam, encoding:encoding, headers: headers).responseJSON { (responseObject) -> Void in
                                                   
                                                   print(responseObject)
                                                   
                                                   if responseObject.result.isSuccess {
                                                       let resJson = JSON(responseObject.result.value!)
                                                    let usrDetail = resJson
                                                       print(resJson)
                                                                            
                                                                                  
                                                                                    
                                                                                    
                                                       if usrDetail["result"].exists() {
                                                        
                                                        
                                                        self.view.isUserInteractionEnabled = true
                                                        
                                                                

                                        let usrDetailVal = usrDetail["result"].rawValue as! [String:Any]
                                                                                print(usrDetailVal)
                                                                                    
                                                                                    
                                                                                    UserDefaults.standard.set(usrDetailVal, forKey: "userdetails")

                                                                                let networks  = usrDetailVal["available_networks"] as! [Any]
                                                                                print(networks)
                                                                                    
                                                                    var NoificationStatus  = usrDetailVal["notification_preferences"] as! [Any]
                                                                    
                                                        let dict : [String:Any] = ["name": "All Networks" , "id":"0" , "notify":false]
                                                                          NoificationStatus.insert(dict, at: 0)
                                                                                    
                                                                                    UserDefaults.standard.set(NoificationStatus, forKey: "notification_preferences")
                                                                                    
                                                                                    let boolvalue : Bool = false
                                                                                    UserDefaults.standard.set(boolvalue, forKey: "allnotification")
                                                                                     UserDefaults.standard.set(boolvalue, forKey: "myuploads")

                                                                                   UserDefaults.standard.set(networks, forKey: "AvailableNetworks")
                                                                                    
                                                  
                                                                                            
                                                                                  let defultnetwork = usrDetailVal["defaultNetwork"] as! String
                                                                                  let organizationid = usrDetailVal["organization_id"] as! String
                                                                                   print("default network is => \(defultnetwork)")
                                                                                    print("organization id  is => \(organizationid)")
                                                                                    UserDefaults.standard.set(organizationid, forKey:"organization_id")
                                                                                   UserDefaults.standard.set(defultnetwork, forKey:"defultnetwork")
                                                                                    let parts = (usrDetailVal["authcode"] as AnyObject).components(separatedBy: ":")
                                                                                    let arrhomenetwork : [String] = []
                                                                                    let arrselectednetwork : [String] = []
                                                                                    UserDefaults.standard.set("All", forKey:"filterednetwork")
                                                                                    UserDefaults.standard.set(arrhomenetwork, forKey:"homenetwork")
                                                                                    UserDefaults.standard.set(arrselectednetwork, forKey:"selectedfilterNetwork")
                                                             
                                                                   
                                                                    
                                                          let stremail = usrDetailVal["email"] as! String
                                                                    
                                                             print(stremail)
                                                                    UserDefaults.standard.set(LoginEMail, forKey: "Email")
                                                                    
                                                                    
                                                            UserDefaults.standard.set(LoginPassword, forKey: "Password")
                                                                                    UserDefaults.standard.set(parts[0], forKey: "Authcode")
                                                                                    UserDefaults.standard.set(parts[1], forKey: "userId")
                                                                                    var manifest: String? = nil
                                                                                           if let value = usrDetailVal["manifest"]{
                                                                                               manifest = "\(value)"
                                                                                           }

                                                                                           if (manifest == "0") {
                                                                                               UserDefaults.standard.set("NO", forKey: "isshowmenifest")
                                                                                           } else {
                                                                                               UserDefaults.standard.set("YES", forKey: "isshowmenifest")
                                                                                           }
                                                                                    
                                                                                   UserDefaults.standard.synchronize()
                                                                                  appdelegate.HideProgress()
                                                                    
                                                                    var story : UIStoryboard?
                                                                               
                                                                          if IS_IPHONE {
                                                                               story = UIStoryboard.init(name: "Main", bundle: nil)
                                                                              }else{
                                                                               story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                                                                          }
                                                                    
                                                                                    UserDefaults.standard.set("home", forKey: "home")
                                                                                    UserDefaults.standard.set("0", forKey: "capturetype")
                                                                    
                                            let RefreshToken = usrDetailVal["refreshToken"] as? String
                                                let AuthToken = usrDetailVal["token"] as? String
                                                        UserDefaults.standard.setValue(AuthToken, forKey: "Authtoken")
                                                                        UserDefaults.standard.setValue(RefreshToken, forKey: "RefreshToken")
                                                                                    UserDefaults.standard.synchronize()
                                                                                    DispatchQueue.main.async {
                                                                                   let TabbarController = story?.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
                                                                                  TabbarController.selectedIndex = 1
                                                                                  self.navigationController?.pushViewController(TabbarController, animated: false)

                                                                                        
                                                                                        
                                                                                  
                                                                                  
                                                                                    }
                                                                                    
                                                                                    }else{
                                                                                       
                                                                                        self.view.isUserInteractionEnabled = true
                                                                                                                                                                                                 
                                                                                                                                                                                                 let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                                                                                                                                                                                 appdelegate.HideProgress()
                                                                                                                                                                                           _ = SCLAlertView().showError("Error", subTitle:"A user could not be found which matches the credentials provided", closeButtonTitle:"OK")
                                                                                        
                                                            }
                                                                              
                                                    }

                                                   
                                                   if responseObject.result.isFailure {
                                                                                                
                                                                                                  let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                                                                                  appdelegate.HideProgress()
                                                                                            _ = SCLAlertView().showError("Error", subTitle:"A user could not be found which matches the credentials provided", closeButtonTitle:"OK")



                                                   }
                                               }
                    

            
                })
        
        
    }
    
   
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
      
     //   app.HideProgress()

        
    }
           
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
         
       
        print("message: \(message.body)")
           // and whatever other actions you want to take
    
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        switch navigationAction.navigationType{
        case .linkActivated:
            if navigationAction.targetFrame == nil{
                self.webview.load(navigationAction.request)
            }
        default:
            break
            
            
        }
        
        if let url = navigationAction.request.url{
            print(url.absoluteString)
        }
        
        decisionHandler(.allow)
        
       
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension String {
    
    func slice(from: String, to: String) -> String? {
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
}
