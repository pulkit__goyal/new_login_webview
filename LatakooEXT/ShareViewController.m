//
//  ShareViewController.m
//  latakooEXT
//
//  Created by Bharat Parashar on 06/12/18.
//  Copyright © 2018 latakoo. All rights reserved.
//

#import "ShareViewController.h"


@import MobileCoreServices;
static NSString *const AppGroupId = @"group.com.latakoo.ext";

@interface ShareViewController ()
{
    NSMutableArray *arrSites;
    
    int flag_video;
    int flag_image;

    
    
}
@end

@implementation ShareViewController

- (BOOL)isContentValid {
    // Do validation of contentText and/or NSExtensionContext attachments here
    return YES;
}

-(void)viewDidLoad
{
    
    flag_image = 0;
    flag_video = 0;

    arrSites = [[NSMutableArray alloc] init];
    sharedUserDefaults = [[NSUserDefaults alloc] initWithSuiteName:AppGroupId];
    
    
    for (NSExtensionItem *item in self.extensionContext.inputItems) {
        for (NSItemProvider *itemProvider in item.attachments) {
            // Here kUTTypeAppleProtectedMPEG4Video is one kind of video type you can modified base on your requirement.
            if ([itemProvider hasItemConformingToTypeIdentifier:@"public.jpeg"]) {
                [itemProvider loadItemForTypeIdentifier:@"public.jpeg" options:nil completionHandler:^(NSURL *url, NSError *error)
                {
                    self->flag_image = 1;
                        NSDictionary *dictSite = [NSDictionary dictionaryWithObjectsAndKeys:@"Image", @"Text",url.lastPathComponent, @"URL",nil];
                    [self->arrSites addObject:dictSite];
                    
                  
                }];
            }
            
            
            if ([itemProvider hasItemConformingToTypeIdentifier:@"public.jpg"]) {
                [itemProvider loadItemForTypeIdentifier:@"public.jpg" options:nil completionHandler:^(NSURL *url, NSError *error)
                {
                    self->flag_image = 1;
                        NSDictionary *dictSite = [NSDictionary dictionaryWithObjectsAndKeys:@"Image", @"Text",url.lastPathComponent, @"URL",nil];
                    [self->arrSites addObject:dictSite];
                    
                  
                }];
            }
            
            
            if ([itemProvider hasItemConformingToTypeIdentifier:@"public.png"]) {
                [itemProvider loadItemForTypeIdentifier:@"public.png" options:nil completionHandler:^(NSURL *url, NSError *error) {
                    // You will get Image data here that you can submit to server.
                    self->flag_image = 1;
                    NSDictionary *dictSite = [NSDictionary dictionaryWithObjectsAndKeys:@"Image", @"Text", url.lastPathComponent, @"URL",nil];
                    [self->arrSites addObject:dictSite];
                }];
            }
            
            if ([itemProvider hasItemConformingToTypeIdentifier:@"public.mpeg-4"]) {
                [itemProvider loadItemForTypeIdentifier:@"public.mpeg-4" options:nil completionHandler:^(NSURL *url, NSError *error) {
                    self->flag_video = 1;
                    NSDictionary *dictSite = [NSDictionary dictionaryWithObjectsAndKeys:@"Video", @"Text",url.lastPathComponent, @"URL",nil];
                    [self->arrSites addObject:dictSite];
                }];
            } if ([itemProvider hasItemConformingToTypeIdentifier:@"com.apple.quicktime-movie"]) {
                [itemProvider loadItemForTypeIdentifier:@"com.apple.quicktime-movie" options:nil completionHandler:^(NSURL *url, NSError *error) {
                    self->flag_video = 1;
                    NSDictionary *dictSite = [NSDictionary dictionaryWithObjectsAndKeys:@"Video", @"Text", url.lastPathComponent, @"URL",nil];
                     [self->arrSites addObject:dictSite];
                }];
            }
            
        }
        [self performSelector:@selector(allitem) withObject:nil afterDelay:1.0];
    }
}


-(void)allitem{
    
    
    if (flag_image == 1 && flag_video == 1) {
        flag_image = 0;
        flag_video = 0;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Latakoo" message:@"Kindly select either images or videos" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
            [UIView animateWithDuration:0.20 animations:^
             {
                 self.view.transform = CGAffineTransformMakeTranslation(0, self.view.frame.size.height);
             }
                             completion:^(BOOL finished)
             {
                 [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
             }];
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        flag_image = 0;
        flag_video = 0;
        [self->sharedUserDefaults setObject:arrSites forKey:@"SharedExtension"];
        [self->sharedUserDefaults synchronize];

    [UIView animateWithDuration:0.20 animations:^
     {
         self.view.transform = CGAffineTransformMakeTranslation(0, self.view.frame.size.height);
     }
                     completion:^(BOOL finished)
     {
         [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
     }];
    
    UIResponder* responder = self;
    while ((responder = [responder nextResponder]) != nil) {
        NSLog(@"responder = %@", responder);
        if ([responder respondsToSelector:@selector(openURL:)] == YES) {
            [responder performSelector:@selector(openURL:) withObject:[NSURL URLWithString:@"Latakoo://"]];
        }
        
        // Do any additional setup after loading the view.
    }
        
        
    }
    
}



- (NSArray *)configurationItems {
    // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
    return @[];
}

@end
