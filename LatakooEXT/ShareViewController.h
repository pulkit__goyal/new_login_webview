//
//  ShareViewController.h
//  latakooEXT
//
//  Created by Bharat Parashar on 06/12/18.
//  Copyright © 2018 latakoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface ShareViewController : UIViewController
{
    NSUserDefaults *sharedUserDefaults;
    NSExtensionItem *inputItem;
}
@end
