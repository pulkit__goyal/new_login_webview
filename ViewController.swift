//
//  ViewController.swift
//  NewLogin
//
//  Created by bharat on 19/11/21.
//

import UIKit
import Alamofire
import CoreData
import Photos
import AFNetworking
import SwiftyJSON
import IQKeyboardManagerSwift
import SystemConfiguration
import SDWebImage


public class Reachability {

    class func isConnectedToNetwork() -> Bool {

        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }

        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }

        /* Only Working for WIFI
        let isReachable = flags == .reachable
        let needsConnection = flags == .connectionRequired

        return isReachable && !needsConnection
        */

        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)

        return ret

    }
}

class ViewController: UIViewController {
    @IBOutlet weak var imguser: UIImageView!
    @IBOutlet weak var imgcompanylogo: UIImageView!

    @IBOutlet weak var btnnext: UIButton!
    
    @IBOutlet weak var btnadvanced: UIButton!

    @IBOutlet weak var btnForgetpassword: UIButton!
    @IBOutlet weak var lblwelcome: UILabel!
    @IBOutlet weak var btnlogin: UIButton!
    @IBOutlet weak var btnmoreinfo: UIButton!
    @IBOutlet weak var resendcode: UIButton!

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtpassword: UITextField!
    @IBOutlet weak var btneye: UIButton!
    @IBOutlet weak var otpTextField: AEOTPTextField!
    @IBOutlet weak var btnback: UIButton!
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    var useremail: String = ""
    var userpassword: String = ""

    var flaglogin = 0
    var flag = 0
    var dictcheckuser : [String: Any] = [:]
    

    @IBOutlet weak var viewpassword: UIView!
    @IBOutlet weak var Viewotp: UIView!
    
    
    var strotp = ""
    
    
    typealias speedTestCompletionHandler = (_ megabytesPerSecond: Double? , _ error: Error?) -> Void

      var speedTestCompletionBlock : speedTestCompletionHandler?

      var startTime: CFAbsoluteTime!
      var stopTime: CFAbsoluteTime!
      var bytesReceived: Int!
       var timer: Timer?
    var uploadTask: URLSessionUploadTask!

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
      
        super.viewDidLoad()

        
        

       
        
        
        
                  if #available(iOS 13.0, *) {
                      overrideUserInterfaceStyle = .light
                  }
                  
        
                if (UserDefaults.standard.value(forKey: "home") != nil){
                    var story : UIStoryboard?
                               
                          if IS_IPHONE {
                               story = UIStoryboard.init(name: "Main", bundle: nil)
                              }else{
                               story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                          }
                    let TabbarController = story?.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
                    if appdelegate.isfromshortcut == true {
                    if appdelegate.isfromshortcutidentifir == "Capture" {
                            TabbarController.selectedIndex = 0

                            
                    } else if appdelegate.isfromshortcutidentifir == "Pilot" {
                        TabbarController.selectedIndex = 1

                            
                    }else if appdelegate.isfromshortcutidentifir == "Library" {
                          TabbarController.selectedIndex = 2

                            
                    }else{
                            TabbarController.selectedIndex = 3

                    }
                        
                    }else{
                        TabbarController.selectedIndex = 1

                    }
                   // TabbarController.selectedIndex = 1
                    self.navigationController?.pushViewController(TabbarController, animated: false)
                }else{
                
                    btnnext.layer.cornerRadius = 5
                    btnnext.layer.masksToBounds = true
                    btnForgetpassword.isHidden = true
                    viewpassword.isHidden = true
                    Viewotp.isHidden = true
                    setupOTPTextField()
                    btnForgetpassword.contentHorizontalAlignment = .right
                    resendcode.contentHorizontalAlignment = .left
                    
               }
        
            if (UserDefaults.standard.value(forKey: "server") == nil){
         //   UserDefaults.standard.set("https://latakoo.com/", forKey: "server")
          //  UserDefaults.standard.set("https://api2.latakoo.com/", forKey: "serverManifest")
               // UserDefaults.standard.set("https://dev.latakoo.com/", forKey: "server")
                UserDefaults.standard.set("https://latakoo.com/", forKey: "server")

                UserDefaults.standard.set("https://dev.api2.latakoo.com/", forKey: "serverManifest")
             
            }
      
        
        
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    
    func servicecheckuser(useremail:String) {
        if !Reachability.isConnectedToNetwork() {

            return
        }
        appdelegate.ShowProgress()
        
//        let headers = ["Authorization":
//                "Bearer \(UserDefaults.standard.value(forKey: "Authtoken") ?? "")"]
        let aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)auth/check-user?email=\(useremail)"
        print(aStrApi)
      //  print(headers)

        let encoding = URLEncoding.httpBody
        Alamofire.request(aStrApi, method: .get, parameters: nil, encoding: encoding, headers: nil).responseJSON { [self] (responseObject) -> Void in

            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                let usrDetail = resJson
                print(usrDetail)
                self.appdelegate.HideProgress()
                if usrDetail["result"].exists() {
                    self.dictcheckuser = usrDetail["result"].rawValue as! [String: Any]
                    print(self.dictcheckuser)
                    self.viewpassword.isHidden = false
                    self.flaglogin = 1
                    self.btnForgetpassword.isHidden = false
                    self.lblwelcome.text = "WELCOME \(self.dictcheckuser["name"] as? String ?? "")".uppercased()
                    self.imguser.sd_setImage(with: URL(string: self.dictcheckuser["icon"] as? String ?? ""), placeholderImage: UIImage(named:"latakoobirdNEWW"))
                //    self.imgcompanylogo.isHidden = false
                                if IS_IPHONE {
                    self.imguser.layer.cornerRadius = 30
                                }else{
                    self.imguser.layer.cornerRadius = 60
                                }
                    self.btnnext.setTitle("Log In", for: [])
                    self.btnback.isHidden = false
                    self.imguser.layer.masksToBounds = true
                    self.Viewotp.isHidden = true
                    self.useremail = txtEmail.text!
                    txtEmail.text = ""
                    btnadvanced.isHidden = true


                }else{
                    self.appdelegate.HideProgress()
                    self.txtEmail.text = ""
                    _ = SCLAlertView().showError("Error", subTitle:"User does not exist", closeButtonTitle:"OK")
                }

            }


            if responseObject.result.isFailure {


            }

        }
    }
    
    
    
     func ServiceLogin(password:String){
        
           self.txtEmail.resignFirstResponder()
           self.txtpassword.resignFirstResponder()

               
                    
        if !Reachability.isConnectedToNetwork() {
                        print("Internet is Not available.")
                        AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
                        return
            }
                    
                  
                    
                    
//                    var tokenVal : String!
//                    if UserDefaults.standard.value(forKey: "deviceTokenString") == nil{
//                        tokenVal = "C649C67E66101C370344466BE9B2388C86DEF9641F2F655B5D526653EEF1BBFA"
//                    }else{
//                        tokenVal = UserDefaults.standard.value(forKey: "deviceTokenString") as? String
//                        print("tokenvalue %@",tokenVal)
//
//                    }
                    appdelegate.ShowProgress()
                            
//                  let headers = ["User-Agent":"HTTP_USER_AGENT:Android",
//                                 "os_type": "IOS",
//                                 "os_version": "IOS14",
//                                 "app_version": "3.1.3"]

                    let aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)auth/login"
                    let aDictParam : [String:Any]!
                    aDictParam = ["email":self.useremail,
                                  "password":self.txtpassword.text!
                    ]
                    print(aStrApi)
                    print(aDictParam)
                 //   print(headers)
                            
                    let encoding = Alamofire.JSONEncoding.default
                        Alamofire.request(aStrApi, method: .post, parameters: aDictParam, encoding:encoding, headers: nil).responseJSON { (responseObject) -> Void in
                                                           
                                print(responseObject)
                                                           
                                                           if responseObject.result.isSuccess {
                                                               let resJson = JSON(responseObject.result.value!)
                                                            let usrDetail = resJson
                                                               print(resJson)
                                                               
                                                               let usrDetailVal = usrDetail.rawValue as! [String: Any]


                                                               self.userpassword = self.txtpassword.text ?? ""
                                                               self.txtpassword.text = ""

                                                               if usrDetail["id"].exists() {
                                                                   
                                                                   if usrDetail["mfa"].rawValue as? Bool == true{
                                                                       self.appdelegate.HideProgress()

                                                                       print("MFA IS UNABLE")
                                                                       self.viewpassword.isHidden = true
                                                                       self.flaglogin = 2
                                                                       self.Viewotp.isHidden = false
                                                                       
                                                                   
                                                                   }else{
                                                                    
                                                                let RefreshToken = usrDetailVal["refresh_token"] as? String
                                                                let AuthToken = usrDetailVal["token"] as? String
                                                                       
                                                                    print(RefreshToken)
                                                                    print(AuthToken)

                                                                       
                                                            UserDefaults.standard.setValue(AuthToken, forKey: "Authtoken")
                                                            UserDefaults.standard.setValue(RefreshToken, forKey: "RefreshToken")
                                                            UserDefaults.standard.synchronize()
                                                          print("MFA IS DISABLE")
                                                          //  self.ServiceGetUserData()
                                                                    //will goto home
                                                                self.Servicegetmyprofile()

                                                                       
                                                                   }
                                                             
                                                               
                                                               }else{
                                                                                                                                                    self.txtpassword.text = ""
                                                                    self.appdelegate.HideProgress()
                                                                                                                                                                                            _ = SCLAlertView().showError("Error", subTitle:"A user could not be found which matches the credentials provided", closeButtonTitle:"OK")
                                                                                                
                                                                    }
                                                                                      
                                                            }

                                                           
                                                           if responseObject.result.isFailure {
                                                              self.view.isUserInteractionEnabled = true
                                                            self.txtpassword.text = ""
                                                               self.appdelegate.HideProgress()
                                                        _ = SCLAlertView().showError("Error", subTitle:"A user could not be found which matches the credentials provided", closeButtonTitle:"OK")


                                                           }
                                                       }
                        
                    
                }
    
    
    
    
    
    func ServiceGetUserData(){
       
          self.txtEmail.resignFirstResponder()
          self.txtpassword.resignFirstResponder()

              
                   
       if !Reachability.isConnectedToNetwork() {
                       print("Internet is Not available.")
                       AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
                       return
           }
                   
              

                   let aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)auth/sign-in"
                   let aDictParam : [String:Any]!
                   aDictParam = ["email":self.useremail,
                                 "password":self.userpassword
                   ]
                   print(aStrApi)
                   print(aDictParam)
                           
                   let encoding = Alamofire.JSONEncoding.default
                       Alamofire.request(aStrApi, method: .post, parameters: aDictParam, encoding:encoding, headers: nil).responseJSON { (responseObject) -> Void in
                                                          
                               print(responseObject)
                                                          
                                                          if responseObject.result.isSuccess {
                                                              let resJson = JSON(responseObject.result.value!)
                                                           let usrDetail = resJson
                                                              print(resJson)
                                                                                   
                                                                                         
                                                                                           

                                        if usrDetail["result"].exists() {
                                                               
                                                            
                                                               
                                                                       
                                                                   

                                               let usrDetailVal = usrDetail["result"].rawValue as! [String:Any]
                                        print(usrDetailVal)
                                            UserDefaults.standard.set(usrDetailVal, forKey: "userdetails")
                                            let networks  = usrDetailVal["available_networks"] as! [Any]
                                                print(networks)
                                        var NoificationStatus  = usrDetailVal["notification_preferences"] as! [Any]
                                       let dict : [String:Any] = ["name": "All Networks" , "id":"0" , "notify":false]
                                        NoificationStatus.insert(dict, at: 0)
                                        UserDefaults.standard.set(NoificationStatus, forKey: "notification_preferences")
                                            let boolvalue : Bool = false
                                            UserDefaults.standard.set(boolvalue, forKey: "allnotification")
                                            UserDefaults.standard.set(boolvalue, forKey: "myuploads")

                                                                                          UserDefaults.standard.set(networks, forKey: "AvailableNetworks")
                                                                                           
                                                         
                                                                                                   
                                                                                         let defultnetwork = usrDetailVal["defaultNetwork"] as! String
                                                                                         let organizationid = usrDetailVal["organization_id"] as! String
                                                                                          print("default network is => \(defultnetwork)")
                                                                                           print("organization id  is => \(organizationid)")
                                                                                           UserDefaults.standard.set(organizationid, forKey:"organization_id")
                                                                                          UserDefaults.standard.set(defultnetwork, forKey:"defultnetwork")
                                                                                           let parts = (usrDetailVal["authcode"] as AnyObject).components(separatedBy: ":")
                                                                                           let arrhomenetwork : [String] = []
                                                                                           let arrselectednetwork : [String] = []
                                                                                           UserDefaults.standard.set("All", forKey:"filterednetwork")
                                                                                           UserDefaults.standard.set(arrhomenetwork, forKey:"homenetwork")
                                                                                           UserDefaults.standard.set(arrselectednetwork, forKey:"selectedfilterNetwork")
                                                                    
                                                                          
                                                                           
                                                                 let stremail = usrDetailVal["email"] as! String
                                                                           
                                                                    print(stremail)
                                                                           UserDefaults.standard.set(stremail, forKey: "Email")
                                                                           
                                                                           
                                                                                           UserDefaults.standard.set(self.txtpassword.text, forKey: "Password")
                                                                                           UserDefaults.standard.set(parts[0], forKey: "Authcode")
                                                                                           UserDefaults.standard.set(parts[1], forKey: "userId")
                                                                                           var manifest: String? = nil
                                                                                                  if let value = usrDetailVal["manifest"]{
                                                                                                      manifest = "\(value)"
                                                                                                  }

                                                                                                  if (manifest == "0") {
                                                                                                      UserDefaults.standard.set("NO", forKey: "isshowmenifest")
                                                                                                  } else {
                                                                                                      UserDefaults.standard.set("YES", forKey: "isshowmenifest")
                                                                                                  }
                                                                                           
                                                                                          UserDefaults.standard.synchronize()
                                                                  self.appdelegate.HideProgress()
                                                                           
                                                                           var story : UIStoryboard?
                                                                                      
                                                                                 if IS_IPHONE {
                                                                                      story = UIStoryboard.init(name: "Main", bundle: nil)
                                                                                     }else{
                                                                                      story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                                                                                 }
                                                                           
                                                                                           UserDefaults.standard.set("home", forKey: "home")
                                                                                           UserDefaults.standard.set("0", forKey: "capturetype")
                                                                           
//                                                   let RefreshToken = usrDetailVal["refreshToken"] as? String
//                                                       let AuthToken = usrDetailVal["token"] as? String
//                                                               UserDefaults.standard.setValue(AuthToken, forKey: "Authtoken")
//                                                                               UserDefaults.standard.setValue(RefreshToken, forKey: "RefreshToken")
//                                                                                           UserDefaults.standard.synchronize()
                                                                                         
                                let TabbarController = story?.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
                                        TabbarController.selectedIndex = 1
                                self.navigationController?.pushViewController(TabbarController, animated: false)

                                                                                               
                                                                                               
                                                                                         
                                                                                         
                                                                                           
                                                                                        }else{
                                                                                              
                                                                                                                                                                                                        self.txtEmail.text = ""
                                                                                                                                                                                                        self.txtpassword.text = ""
                                                                                                                                                                                                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                                                                                                                                                                                        appdelegate.HideProgress()
                                                                                                                                                                                                  _ = SCLAlertView().showError("Error", subTitle:"A user could not be found which matches the credentials provided", closeButtonTitle:"OK")
                                                                                               
                                                                   }
                                                                                     

                                                          
                                                          if responseObject.result.isFailure {
                                                            self.appdelegate.HideProgress()
                                                     
                                                              
                                                              
                                                              


                                                          }
                                                      }
                       
                   
               }
    
    
    
    }
    
    
    func Servicegetmyprofile() {
            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }

        appdelegate.ShowProgress()

        let headers = ["Authorization":
                "Bearer \(UserDefaults.standard.value(forKey: "Authtoken") ?? "")"]
        var aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)user/profile"


        print(aStrApi)
        print(headers)

        let encoding = URLEncoding.httpBody
        Alamofire.request(aStrApi, method: .get, parameters: nil, encoding: encoding, headers: headers).responseJSON { (responseObject) -> Void in

            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                let usrDetail = resJson
                print(usrDetail)
                if usrDetail["result"].exists() {
                    
                    
                    
                    let usrDetailVal = usrDetail["result"].rawValue as! [String:Any]
             print(usrDetailVal)
                 UserDefaults.standard.set(usrDetailVal, forKey: "userdetails")
                 let networks  = usrDetailVal["available_networks"] as! [Any]
                     print(networks)
             var NoificationStatus  = usrDetailVal["notification_preferences"] as! [Any]
            let dict : [String:Any] = ["name": "All Networks" , "id":"0" , "notify":false]
             NoificationStatus.insert(dict, at: 0)
             UserDefaults.standard.set(NoificationStatus, forKey: "notification_preferences")
                 let boolvalue : Bool = false
                 UserDefaults.standard.set(boolvalue, forKey: "allnotification")
                 UserDefaults.standard.set(boolvalue, forKey: "myuploads")

                                                               UserDefaults.standard.set(networks, forKey: "AvailableNetworks")
                                                                
                              
                                                                        
                                                              let defultnetwork = usrDetailVal["defaultNetwork"] as! String
                                                              let organizationid = usrDetailVal["organization_id"] as! String
                                                               print("default network is => \(defultnetwork)")
                                                                print("organization id  is => \(organizationid)")
                                                                UserDefaults.standard.set(organizationid, forKey:"organization_id")
                                                               UserDefaults.standard.set(defultnetwork, forKey:"defultnetwork")
                                                                let parts = (usrDetailVal["authcode"] as AnyObject).components(separatedBy: ":")
                                                                let arrhomenetwork : [String] = []
                                                                let arrselectednetwork : [String] = []
                                                                UserDefaults.standard.set("All", forKey:"filterednetwork")
                                                                UserDefaults.standard.set(arrhomenetwork, forKey:"homenetwork")
                                                                UserDefaults.standard.set(arrselectednetwork, forKey:"selectedfilterNetwork")
                                         
                                               
                                                
                                      let stremail = usrDetailVal["email"] as! String
                                                
                                         print(stremail)
                                                UserDefaults.standard.set(stremail, forKey: "Email")
                                                
                                                
                                                                UserDefaults.standard.set(self.txtpassword.text, forKey: "Password")
                                                                UserDefaults.standard.set(parts[0], forKey: "Authcode")
                                                                UserDefaults.standard.set(parts[1], forKey: "userId")
                                                                var manifest: String? = nil
                                                                       if let value = usrDetailVal["manifest"]{
                                                                           manifest = "\(value)"
                                                                       }

                                                                       if (manifest == "0") {
                                                                           UserDefaults.standard.set("NO", forKey: "isshowmenifest")
                                                                       } else {
                                                                           UserDefaults.standard.set("YES", forKey: "isshowmenifest")
                                                                       }
                                                                
                                                               UserDefaults.standard.synchronize()
                                       self.appdelegate.HideProgress()
                                                
                                                var story : UIStoryboard?
                                                           
                                                      if IS_IPHONE {
                                                           story = UIStoryboard.init(name: "Main", bundle: nil)
                                                          }else{
                                                           story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                                                      }
                                                
                                                                UserDefaults.standard.set("home", forKey: "home")
                                                                UserDefaults.standard.set("0", forKey: "capturetype")
                                                
//                                                   let RefreshToken = usrDetailVal["refreshToken"] as? String
//                                                       let AuthToken = usrDetailVal["token"] as? String
//                                                               UserDefaults.standard.setValue(AuthToken, forKey: "Authtoken")
//                                                                               UserDefaults.standard.setValue(RefreshToken, forKey: "RefreshToken")
//                                                                                           UserDefaults.standard.synchronize()
                                                              
     let TabbarController = story?.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
             TabbarController.selectedIndex = 1
     self.navigationController?.pushViewController(TabbarController, animated: false)

                                                                    
                                                                    
                                                              
                                                              
                                                                
                                                             }else{
                                                                 
                                       self.txtEmail.text = ""
                                        self.txtpassword.text = ""
                                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                        appdelegate.HideProgress()
                                    _ = SCLAlertView().showError("Error", subTitle:"A user could not be found which matches the credentials provided", closeButtonTitle:"OK")

}

            }
            if responseObject.result.isFailure {
                self.appdelegate.HideProgress()


            }


        }





    }
    
    
    func ServiceMatchMFA(){
       if !Reachability.isConnectedToNetwork() {
                       print("Internet is Not available.")
                       AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
                       return
           }
                   
             

                   let aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)auth/check-mfa"
                   let aDictParam : [String:Any]!
                   aDictParam = ["mfa_number":strotp,
                                 "id":"\(dictcheckuser["user_id"] ?? "")"
                   ]
        
                 print(aDictParam)
                 
        self.appdelegate.ShowProgress()

                   let encoding = Alamofire.JSONEncoding.default
                       Alamofire.request(aStrApi, method: .post, parameters: aDictParam, encoding:encoding, headers: nil).responseJSON { (responseObject) -> Void in
                                                          
                                             print(responseObject)

                                                          if responseObject.result.isSuccess {
                                                              let resJson = JSON(responseObject.result.value!)
                                                           let usrDetail = resJson
                                                              print(usrDetail)
                                                              let usrDetailVal = usrDetail.rawValue as! [String: Any]

                                                              
                                                              if usrDetail["id"].exists() {
                                                           
                                                                  let RefreshToken = usrDetailVal["refresh_Token"] as? String
                                                                  let AuthToken = usrDetailVal["token"] as? String
                                                                         
                                                                       print(RefreshToken)
                                                                         print(AuthToken)

                                                              UserDefaults.standard.setValue(AuthToken, forKey: "Authtoken")
                                                              UserDefaults.standard.setValue(RefreshToken, forKey: "RefreshToken")
                                                              UserDefaults.standard.synchronize()
                                                            print("MFA IS DISABLE")
                                                                  
                                                             //   self.ServiceGetUserData()
                                                                  self.Servicegetmyprofile()

                                                              }else{
                                                            _ = SCLAlertView().showError("Error", subTitle:"Authentication code not matched", closeButtonTitle:"OK")
                                                                self.appdelegate.HideProgress()

                                                              }
                                                                                     
                                                           }

                                                          
                                                          if responseObject.result.isFailure {
                                                        
                                                              self.appdelegate.HideProgress()
                                                       _ = SCLAlertView().showError("Error", subTitle:"Authentication code not matched", closeButtonTitle:"OK")


                                                          }
                                                      }
                       
                   
               }
    
    
    
    func ResendMFA(mode:String){
       if !Reachability.isConnectedToNetwork() {
                       print("Internet is Not available.")
                       AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
                       return
           }
                   
             

                   let aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)auth/resend-mfa-code"
                   let aDictParam : [String:Any]!
                   aDictParam = ["mode":mode,
                                 "id":"\(dictcheckuser["user_id"] ?? "")"
                   ]
        
                 print(aDictParam)
                 
        self.appdelegate.ShowProgress()

                   let encoding = Alamofire.JSONEncoding.default
                       Alamofire.request(aStrApi, method: .post, parameters: aDictParam, encoding:encoding, headers: nil).responseJSON { (responseObject) -> Void in
                           self.appdelegate.HideProgress()

                                        print(responseObject)
                                                          
                                                          if responseObject.result.isSuccess {
                                                              let resJson = JSON(responseObject.result.value!)
                                                           let usrDetail = resJson
                                                              print(usrDetail)
                                                           //   if usrDetail["id"].exists() {
                                                                  let appearance = SCLAlertView.SCLAppearance(
                                                                      kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                                                                      kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                                                                      kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                                                                      showCloseButton: false,
                                                                      dynamicAnimatorActive: true,
                                                                      buttonsLayout: .horizontal
                                                                  )

                                                                  let alert = SCLAlertView(appearance: appearance)
                                                                  _ = alert.addButton("OK") {

                                                                  }
                                                                  _ = alert.showSuccess("", subTitle:"Authentication code sent successfully.")
                                                            //  }
                                                                                
                                                                                     
                                                           }

                                                          
                                                          if responseObject.result.isFailure {
                                                        
                                                              self.appdelegate.HideProgress()
                                                       _ = SCLAlertView().showError("Error", subTitle:"wrong credential", closeButtonTitle:"OK")


                                                          }
                                                      }
                       
                   
               }
    
    
    func serviceforgotpassword(email:String){
         
           if !Reachability.isConnectedToNetwork() {
                           print("Internet is Not available.")
                           AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
                           return
               }
                       
                 

                       let aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)auth/forgot-password"
                       let aDictParam : [String:Any]!
                       aDictParam = [
                                     "email":email
                                   ]
            
                     print(aDictParam)
                     
            self.appdelegate.ShowProgress()

                       let encoding = Alamofire.JSONEncoding.default
                           Alamofire.request(aStrApi, method: .post, parameters: aDictParam, encoding:encoding, headers: nil).responseJSON { (responseObject) -> Void in
                               self.appdelegate.HideProgress()

                                            print(responseObject)
                                                              
                                                              if responseObject.result.isSuccess {
                                                                  let resJson = JSON(responseObject.result.value!)
                                                               let usrDetail = resJson
                                                                  print(usrDetail)
                                                                //  if usrDetail["id"].exists() {
                                                                      let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                                                      appdelegate.HideProgress()
                                                                      print("Internet is Not available.")
                                                                      let appearance = SCLAlertView.SCLAppearance(dynamicAnimatorActive: true)
                                                                      _ = SCLAlertView(appearance: appearance).showNotice("LATAKOO", subTitle:"New password sent on given email address" , closeButtonTitle:"OK")
                                                                      
//                                                                  }else{
//                                                                      let appearance = SCLAlertView.SCLAppearance(
//                                                                              kTextFieldHeight: 50,
//                                                                              showCloseButton: false,
//                                                                              dynamicAnimatorActive: true,
//                                                                              buttonsLayout: .horizontal
//
//                                                                            )
//                                                                            let alert = SCLAlertView(appearance: appearance)
//
//
//                                                                            _ = alert.addButton("Close") {
//
//
//
//                                                                            }
//                                                                          let icon = UIImage(named: "exclamation.png")
//                                                                         let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
//                                                                            _ = alert.showCustom("Unable to reset password", subTitle:"This email address you entered is associated with an account enable for Single Sign On (SSO) credentials. this mean you will log into latakoo using your corporate credentials.                                                                                 if you do not know your SSO ID or need to reset your password, please contact your it Administrator.", color: color, circleIconImage: icon!)
//
//                                                                  }
                                                                                    
                                                                                         
                                                               }

                                                              
                                                              if responseObject.result.isFailure {
                                                            
                                                                  self.appdelegate.HideProgress()
                                                           _ = SCLAlertView().showError("Error", subTitle:"", closeButtonTitle:"OK")


                                                              }
                                                          }
                           
                       
                   }
        
        
     
        
    
    
   
    @IBAction func backonline(_ sender: Any) {
  
        flaglogin = flaglogin - 1
        if flaglogin == 0 {
       btnback.isHidden = true
       self.txtEmail.text = ""
        viewpassword.isHidden = true
        btnForgetpassword.isHidden = true
        lblwelcome.text = "WELCOME TO LATAKOO"
        imguser.image = UIImage.init(named: "latakoobirdNEWW")
     //   imgcompanylogo.isHidden = true
        btnlogin.setTitle("NEXT", for: [])
        btneye.isHidden = true
        resendcode.isHidden = true
        imguser.layer.masksToBounds = true
        Viewotp.isHidden = true
        resendcode.isHidden = true
        if IS_IPHONE {
        imguser.layer.cornerRadius = 30
        }else{
        imguser.layer.cornerRadius = 60
        }
        
        }
        if flaglogin == 1 {
            
            viewpassword.isHidden = false
            btnForgetpassword.isHidden = false
         //   imgcompanylogo.isHidden = false
            if IS_IPHONE {
            imguser.layer.cornerRadius = 30
            }else{
            imguser.layer.cornerRadius = 60
            }
             btnnext.setTitle("Log In", for: [])
            btnback.isHidden = false
            imguser.layer.masksToBounds = true
            Viewotp.isHidden = true
          
        }
        if flaglogin == 2 {
           // imgcompanylogo.isHidden = true
            viewpassword.isHidden = true
            Viewotp.isHidden = true
        }
    
    
    }
    
    
//    func checkForSpeedTest() {
//
//           testDownloadSpeedWithTimout(timeout: 5.0) { (speed, error) in
//
//               let spd = speed!*1024*1024
//               print("Download speed = > \(Units(bytes: Int64(spd)).getReadableUnit())")
//
//
//           }
//
//       }

//       func testDownloadSpeedWithTimout(timeout: TimeInterval, withCompletionBlock: @escaping speedTestCompletionHandler) {
//
//           guard let url = URL(string: "https://images.apple.com/v/imac-with-retina/a/images/overview/5k_image.jpg") else { return }
//
//           startTime = CFAbsoluteTimeGetCurrent()
//           stopTime = startTime
//           bytesReceived = 0
//
//           speedTestCompletionBlock = withCompletionBlock
//
//           let configuration = URLSessionConfiguration.ephemeral
//           configuration.timeoutIntervalForResource = timeout
//           let session = URLSession.init(configuration: configuration, delegate: self, delegateQueue: nil)
//           session.dataTask(with: url).resume()
//
//       }
    
    
    
    
    

//       func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
//           bytesReceived! += data.count
//           stopTime = CFAbsoluteTimeGetCurrent()
//       }
//
//       func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
//
//           let elapsed = stopTime - startTime
//
//           if let aTempError = error as NSError?, aTempError.domain != NSURLErrorDomain && aTempError.code != NSURLErrorTimedOut && elapsed == 0  {
//               speedTestCompletionBlock?(nil, error)
//               return
//           }
//
//           let speed = elapsed != 0 ? Double(bytesReceived) / elapsed / 1024.0 / 1024.0 : -1
//           speedTestCompletionBlock?(speed, nil)
//
//       }
    
    
    
//    func uploadspeed(){
//
//        let urlString = "https://myserver/upload.php"
//             guard let url = URL(string: urlString) else {
//                return
//             }
//
//
//
//            var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10)
//            urlRequest.httpMethod = "POST"
//            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
//
//            let emptyData = Data.init(capacity: 102454)
//
//            let json = ["file" : emptyData]
//            urlRequest.httpBody = try? JSONEncoder().encode(json)
//
//            let sessionConfiguration = URLSessionConfiguration.ephemeral
//            let session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: nil)
//            uploadTask = session.uploadTask(with: urlRequest, from: emptyData) { (data, response, error) in
//               if let err = error {
//                   //There's an error
//               }
//               else if let response = response {
//                   //check for response status
//               }
//
//               //Stop the timer here
//                self.timer?.invalidate()
//             }
//
//           uploadTask.resume()
//           calculateSpeed()
//       }



        
//    func calculateSpeed() {
//        var previousBytesSent: Int64 = 0
//       timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (_) in
//           let bytesSent = self.uploadTask.countOfBytesSent
//            let speed =  abs(bytesSent-previousBytesSent)
//
//           print("upload speed = > \(Units(bytes: speed).getReadableUnit())")
//
//
//            previousBytesSent = bytesSent
//        })
//    }
    
    public struct Units {
      
      public let bytes: Int64
      
      public var kilobytes: Double {
        return Double(bytes) / 1_024
      }
      
      public var megabytes: Double {
        return kilobytes / 1_024
      }
      
      public var gigabytes: Double {
        return megabytes / 1_024
      }
      
      public init(bytes: Int64) {
        self.bytes = bytes
      }

    public func getReadableUnit() -> String {
        
        switch bytes {
        case 0..<1_024:
          return "\(bytes) bytes"
        case 1_024..<(1_024 * 1_024):
          return "\(String(format: "%.2f", kilobytes)) kb"
        case 1_024..<(1_024 * 1_024 * 1_024):
          return "\(String(format: "%.2f", megabytes)) mb"
        case (1_024 * 1_024 * 1_024)...Int64.max:
          return "\(String(format: "%.2f", gigabytes)) gb"
        default:
          return "\(bytes) bytes"
        }
      }
    }
    
    func setupOTPTextField() {
        otpTextField.otpDelegate = self
        if IS_IPHONE {
        otpTextField.otpFontSize = 18
        }else{
        otpTextField.otpFontSize = 30

        }

        otpTextField.otpTextColor = .black
        otpTextField.otpDefaultBorderColor = UIColor.clear
        otpTextField.otpFilledBorderColor = UIColor.gray

        otpTextField.otpFilledBorderWidth = 1
        otpTextField.otpDefaultBorderWidth = 0

        otpTextField.configure()
    }
    
    
    @IBAction func btneyee(_ sender: Any) {
        if flag == 0 {
            flag = 1
            btneye.setImage(UIImage(named:"eye_open"), for:.normal)
            txtpassword.isSecureTextEntry  = false
            
        }else{
            flag = 0
            btneye.setImage(UIImage(named:"hide_eye"), for: .normal)
            txtpassword.isSecureTextEntry  = true
        }
    }
    @IBAction func btnlogin(_ sender: Any) {
  
        if flaglogin == 0{
            btnadvanced.isHidden = false

            if self.txtEmail.text == ""{
                self.view.makeToast("Please Enter your User ID/SSO")
            }else{
                servicecheckuser(useremail: txtEmail.text ?? "")
            }
        }
       else if flaglogin == 1 {
           btnadvanced.isHidden = true
            if self.txtpassword.text == ""{
            self.view.makeToast("Please Enter your Password")
            }else{

          //  imgcompanylogo.isHidden = false
                self.ServiceLogin(password: txtpassword.text ?? "")
            }

        }
      else if flaglogin == 2 {
        btnadvanced.isHidden = true
        if strotp == ""{
        self.view.makeToast("Please Enter valid OTP")
            
        }else{
              //  imgcompanylogo.isHidden = true
              
            
            self.ServiceMatchMFA()
                
            }
        }
    }
    
    
    @IBAction func btnmoreinfo(_ sender: Any) {
        
        var story : UIStoryboard?
        story = UIStoryboard.init(name: "Main", bundle: nil)
        let MoreInfoVC = story?.instantiateViewController(withIdentifier: "MoreInfoVC") as! MoreInfoVC
        self.navigationController?.pushViewController(MoreInfoVC, animated: false)
   
    }
    
    @IBAction func btnresendcode(_ sender: UIButton) {
 
        let alert = UIAlertController(title: nil , message:nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Send to mobile phone", style: .default , handler:{ (UIAlertAction)in
       
            self.ResendMFA(mode: "sms")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Send to email address", style: .default , handler:{ (UIAlertAction)in
      
            self.ResendMFA(mode: "email")

        }))

        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
       
       
        }))

        
        if IS_IPHONE {
            alert.popoverPresentationController?.sourceView = view

        } else {
            alert.popoverPresentationController?.sourceView = btnmoreinfo
        }

        present(alert, animated: true) {
       
        
        }
        
       
    }
    
    
    @IBAction func Forgotpassword(_ sender: Any) {

        
        let appearance = SCLAlertView.SCLAppearance(
                kTextFieldHeight: 50,
                showCloseButton: false,
                dynamicAnimatorActive: true,
                buttonsLayout: .horizontal
                
              )
              let alert = SCLAlertView(appearance: appearance)
              let txt = alert.addTextField("Enter your name")
        
              _ = alert.addButton("Cancel") {

                     }
        
              _ = alert.addButton("Done") {
                  print("Text value: \(txt.text ?? "NA")")
                self.serviceforgotpassword(email: txt.text ?? "")

              }
              _ = alert.showEdit("Email", subTitle:"Please enter your email address")
    
        
       
    }
    
    @IBAction func Advanced(_ sender: Any) {
       
               let appearance = SCLAlertView.SCLAppearance(
                        kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                        kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                        kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                        showCloseButton: false,
                        dynamicAnimatorActive: true,
                        buttonsLayout: .horizontal
                        
                    )
                     let alert = SCLAlertView(appearance: appearance)
            
        _ = alert.addButton("DEV"){
                      
                //    UserDefaults.standard.set("https://dev.latakoo.com/", forKey: "server")
                  UserDefaults.standard.set("https://latakoo.com/", forKey: "server")
                   UserDefaults.standard.set("https://dev.api2.latakoo.com/", forKey: "serverManifest")

                        
                    }
            _ = alert.addButton("LIVE") {
                    
                UserDefaults.standard.set("https://latakoo.com/", forKey: "server")
                UserDefaults.standard.set("https://api2.latakoo.com/", forKey: "serverManifest")



                    }
                    
               UserDefaults.standard.synchronize()
                    let icon = UIImage(named:"server.png")
                    let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
                    _ = alert.showCustom("Server", subTitle: "Select api server", color: color, circleIconImage: icon!)
            }
    
    
}

extension ViewController: AEOTPTextFieldDelegate {
    func didUserFinishEnter(the code: String) {
        strotp = code
        
    }
}


