//  SelectedSDcardVC.swift
//  Latakoo
//  Created by ABC on 22/10/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//
import Foundation
import MobileCoreServices
import Photos
import UIKit

class SelectedSDcardVC: UIViewController, UITextFieldDelegate, SendAssignmentDelegate {
    func fromsend(_ controller: SendAssignment) {
        if App.Assignmentname != "" {
            txttitle.text = App.Assignmentname
            txttitle.text = removeSpecialCharsFromString(text: txttitle.text!)
            print(txttitle.text!)

        }
    }

    @IBOutlet var groupicon: UIImageView!
    @IBOutlet var imageview: UIImageView!
    @IBOutlet var myscrollview: UIScrollView!
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var txttitle: UITextField!
    @IBOutlet var lblquality: UILabel!
    @IBOutlet var txtitemcode: UITextField!
    @IBOutlet var lblitemcode: UILabel!
    @IBOutlet var lbluseremail: UILabel!
    @IBOutlet var lblkeywords: UILabel!
    @IBOutlet var lblcontact: UILabel!
    @IBOutlet var lblnetwork: UILabel!
    @IBOutlet var lblfilecount: UILabel!
    @IBOutlet var viewitemcode: UIImageView!

    @IBOutlet var imgarrow: UIImageView!
    @IBOutlet var btnselectassignment: UIButton!

    var isfromview: String = ""
    var ismultipleupload: Bool = false
    var assestarray: [PHAsset] = []
    var assestarraySD: [URL] = []

    var assestarrayaudio: [String] = []
    var maxcharecter: Int = 0
    
    @IBOutlet var sliderview: StepSlider!
    var dictuser: [String: Any] = [:]
    var bitratearry: [Any] = []
    let App = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        lblfilecount.layer.cornerRadius = lblfilecount.frame.size.width / 2
        lblfilecount.layer.borderWidth = 2
        lblfilecount.layer.borderColor = UIColor.white.cgColor
        lblfilecount.layer.masksToBounds = true
        lblfilecount.isHidden = true

        lbluseremail.text = UserDefaults.standard.value(forKey: "Email") as? String

        App.ArrKeywords = []
        App.ArrEmail = []
        App.ArrNetworks = []

        var height: CGFloat = 1100

        if IS_IPHONE {
            if isfromview == "video" {
                height = 1100
            } else {
                height = 900
            }
            myscrollview.contentSize = CGSize(width: view.frame.size.width, height: height)
        }

        if isfromview == "image" {
            setupimage()

        } else if isfromview == "video" {
            
            setupvideo()
      
        
        } else if isfromview == "Group" {
            setupgroup()
            
        } else {
            setupaudio()
        }

        let str = UserDefaults.standard.value(forKey: "isshowmenifest") as? String
        if str == "NO" {
            btnselectassignment.isHidden = true
            imgarrow.isHidden = true

        } else {
            btnselectassignment.isHidden = false
            imgarrow.isHidden = false
            lbltitle.text = "Title & Assignments:"
            
            if App.Assignmentname != "" {
                txttitle.text = App.Assignmentname
                txttitle.text = removeSpecialCharsFromString(text: txttitle.text!)
                print(txttitle.text!)

            }
        }
        if isfromview == "video" {
            setupslider()
        }
    }

    func setupgroup() {
        let url = assestarraySD[0]

        let extensionn = url.pathExtension
        print("extensionn: \(extensionn)")
        let uti = UTTypeCreatePreferredIdentifierForTag(
            kUTTagClassFilenameExtension,
            extensionn as CFString,
            nil)

        let retainedValue = uti!.takeRetainedValue()
        print("retainedValue: \(retainedValue)")

        if UTTypeConformsTo(retainedValue, kUTTypeImage) {
            ismultipleupload = true
            lblfilecount.isHidden = false
            lblfilecount.text = "\(assestarraySD.count)"

            let url = assestarraySD[0]
            lblfilecount.isHidden = true
            let filename = url.lastPathComponent
            txttitle.text = filename.fileName()
            txttitle.text = removeSpecialCharsFromString(text: txttitle.text!)
            print(txttitle.text!)

            self.imageview.sd_setImage(with: url, completed: nil)

        }
        else if UTTypeConformsTo(retainedValue, kUTTypeVideo) {
            ismultipleupload = true
            lblfilecount.isHidden = false
            lblfilecount.text = "\(assestarraySD.count)"

            let url = assestarraySD[0]

            let filename = url.lastPathComponent
            txttitle.text = filename.fileName()
            txttitle.text = removeSpecialCharsFromString(text: txttitle.text!)
            print(txttitle.text!)


            let asset = AVAsset(url: url)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            let time: CMTime = CMTimeMake(1, 1)
            let imageRef = try? imageGenerator.copyCGImage(at: time, actualTime: nil)
            var thumbnail: UIImage?
            if let imageRef = imageRef {
                thumbnail = UIImage(cgImage: imageRef)
            }
            self.imageview.image = thumbnail
          //  setupslider()
        }
        else if UTTypeConformsTo(retainedValue, kUTTypeMovie) {
            ismultipleupload = true
            lblfilecount.isHidden = false
            lblfilecount.text = "\(assestarraySD.count)"

            let url = assestarraySD[0]

            let filename = url.lastPathComponent
            txttitle.text = filename.fileName()
            txttitle.text = removeSpecialCharsFromString(text: txttitle.text!)
            print(txttitle.text!)


            let asset = AVAsset(url: url)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            let time: CMTime = CMTimeMake(1, 1)
            let imageRef = try? imageGenerator.copyCGImage(at: time, actualTime: nil)
            var thumbnail: UIImage?
            if let imageRef = imageRef {
                thumbnail = UIImage(cgImage: imageRef)
            }
            self.imageview.image = thumbnail
          //  setupslider()
        }
        else if UTTypeConformsTo(retainedValue, kUTTypeAudio) {
            ismultipleupload = true
            lblfilecount.isHidden = false
            lblfilecount.text = "\(assestarraySD.count)"
            txttitle.text = assestarraySD[0].lastPathComponent.fileName()
            txttitle.text = removeSpecialCharsFromString(text: txttitle.text!)
            print(txttitle.text!)

            self.imageview.image = UIImage(named: "placeaudio")
        }
    }

    @IBAction func selectassignment(_ sender: Any) {
        var story: UIStoryboard?

        if IS_IPHONE {
            story = UIStoryboard(name: "Manifest", bundle: nil)
        } else {
            story = UIStoryboard(name: "ManifestIPAD", bundle: nil)
        }

        let SendAssignment = story?.instantiateViewController(withIdentifier: "SendAssignment") as! SendAssignment
        SendAssignment.delegate = self
        navigationController?.pushViewController(SendAssignment, animated: false)
    }

    func setupimage() {
        if assestarraySD.count > 1 {
            ismultipleupload = true
            lblfilecount.isHidden = false
            lblfilecount.text = "\(assestarraySD.count)"

            let url = assestarraySD[0]
            lblfilecount.isHidden = true
            let filename = url.lastPathComponent
            txttitle.text = filename.fileName()
            txttitle.text = removeSpecialCharsFromString(text: txttitle.text!)
            print(txttitle.text!)

            self.imageview.sd_setImage(with: url, completed: nil)

        } else {
            let url = assestarraySD[0]
            lblfilecount.isHidden = true
            imageview.isHidden = true
            ismultipleupload = false
            let filename = url.lastPathComponent
            txttitle.text = filename.fileName()
            txttitle.text = removeSpecialCharsFromString(text: txttitle.text!)
            print(txttitle.text!)


            self.groupicon.sd_setImage(with: url, completed: nil)
        }
    }

    func setupvideo() {
        if assestarraySD.count > 1 {
            ismultipleupload = true
            lblfilecount.isHidden = false
            lblfilecount.text = "\(assestarraySD.count)"

            let url = assestarraySD[0]

            let filename = url.lastPathComponent
            txttitle.text = filename.fileName()
            txttitle.text = removeSpecialCharsFromString(text: txttitle.text!)
            print(txttitle.text!)


            let asset = AVAsset(url: url)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            let time: CMTime = CMTimeMake(1, 1)
            let imageRef = try? imageGenerator.copyCGImage(at: time, actualTime: nil)
            var thumbnail: UIImage?
            if let imageRef = imageRef {
                thumbnail = UIImage(cgImage: imageRef)
            }
            self.imageview.image = thumbnail

        } else {
            ismultipleupload = false
            lblfilecount.isHidden = true
            imageview.isHidden = true

            let url = assestarraySD[0]

            let filename = url.lastPathComponent
            txttitle.text = filename.fileName()
            txttitle.text = removeSpecialCharsFromString(text: txttitle.text!)
            print(txttitle.text!)

            
            let asset = AVAsset(url: url)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            let time: CMTime = CMTimeMake(1, 1)
            let imageRef = try? imageGenerator.copyCGImage(at: time, actualTime: nil)
            var thumbnail: UIImage?
            if let imageRef = imageRef {
                thumbnail = UIImage(cgImage: imageRef)
            }
            self.groupicon.image = thumbnail
        }
    }

    func setupaudio() {
        if assestarrayaudio.count > 1 {
            ismultipleupload = true
            lblfilecount.isHidden = false
            lblfilecount.text = "\(assestarrayaudio.count)"
            txttitle.text = assestarraySD[0].lastPathComponent.fileName()
            txttitle.text = removeSpecialCharsFromString(text: txttitle.text!)
            print(txttitle.text!)


        } else {
            txttitle.text = assestarraySD[0].lastPathComponent.fileName()
            ismultipleupload = false
            txttitle.text = removeSpecialCharsFromString(text: txttitle.text!)
            print(txttitle.text!)

        }
    }
    
    func setupslider() {
        dictuser = UserDefaults.standard.value(forKey: "userdetails") as! [String: Any]
        print(dictuser)
        bitratearry = dictuser["bitrates_details"] as! [Any]
        print(bitratearry)

        sliderview.maxCount = UInt(bitratearry.count)

        if UserDefaults.standard.value(forKey: "selectedbitrate") != nil {
            var count = 0
            for value in bitratearry {
                print(value)
                let dict = value as! [String: Any]
                print(dict)

                if dict["label"] as! String == UserDefaults.standard.value(forKey: "selectedbitrate") as! String {
                    sliderview.index = UInt(count)
                }

                count = count + 1
            }

        } else {
            sliderview.index = 0
            let dict = bitratearry[0] as! [String: Any]
            lblquality.text = dict["label"] as? String
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        checkifkeywordselected()
        checkifcontactselected()
        cheifitecodeenable()

        if App.ArrNetworks.count > 0 {
            if App.ArrNetworks.count == 1 {
                let networks = UserDefaults.standard.value(forKey: "AvailableNetworks") as? [Any] ?? []
                let defaultnetwork = App.ArrNetworks[0]
                for dict in networks {
                    let dictnetwork = dict as! [String: Any]
                    print(dictnetwork)
                    let networkid = dictnetwork["company_id"] as! String
                    if networkid == defaultnetwork {
                        lblnetwork.text = dictnetwork["company_name"] as? String
                    }
                }
            } else {
                lblnetwork.text = "\(App.ArrNetworks.count) Selected"
            }

        } else {
            let networks = UserDefaults.standard.value(forKey: "AvailableNetworks") as? [Any] ?? []
            let defaultnetwork = UserDefaults.standard.value(forKey: "defultnetwork") as! String
            for dict in networks {
                let dictnetwork = dict as! [String: Any]
                print(dictnetwork)
                let networkid = dictnetwork["company_id"] as! String

                if networkid == defaultnetwork {
                    lblnetwork.text = dictnetwork["company_name"] as? String
                }
            }
        }
    }

    func cheifitecodeenable() {
        if App.ArrNetworks.count > 0 {
            hideshowitemcode(ishide: App.hideshowitemcode)

        } else {
            let networks = UserDefaults.standard.value(forKey: "AvailableNetworks") as? [Any] ?? []
            let defaultnetwork = UserDefaults.standard.value(forKey: "defultnetwork") as! String
            for dict in networks {
                let dictnetwork = dict as! [String: Any]
                print(dictnetwork)
                let networkid = dictnetwork["company_id"] as! String
                if networkid == defaultnetwork {
                    let enableitemcode = dictnetwork["enable_itemcode"] as! String

                    if enableitemcode == "1" {
                        hideshowitemcode(ishide: false)
                        lblitemcode.text = "\(dictnetwork["company_name"] as? String ?? "") Item Code:"

                        var maxchar = dictnetwork["maxchar"] as? String ?? "1000"

                        if maxchar == "" || maxchar == "0" {
                            maxchar = "1000"
                        }

                        maxcharecter = Int(maxchar)!
                        print(maxcharecter)

                    } else {
                        hideshowitemcode(ishide: true)
                    }

                    lblnetwork.text = dictnetwork["company_name"] as? String
                }
            }
        }
    }

    func hideshowitemcode(ishide: Bool) {
        if ishide == true {
            lblitemcode.isHidden = true
            txtitemcode.isHidden = true
            viewitemcode.isHidden = true
        } else {
            let networks = UserDefaults.standard.value(forKey: "AvailableNetworks") as? [Any] ?? []
            for dict in networks {
                let dictnetwork = dict as! [String: Any]
                print(dictnetwork)
                let networkid = dictnetwork["company_id"] as! String

                if App.ArrNetworks.contains(networkid) {
                    let enableitemcode = dictnetwork["enable_itemcode"] as! String
                    if enableitemcode == "1" {
                        lblitemcode.text = "\(dictnetwork["company_name"] as? String ?? "") Item Code:"
                        var maxchar = dictnetwork["maxchar"] as? String ?? "1000"

                        if maxchar == "" || maxchar == "0" {
                            maxchar = "1000"
                        }
                        maxcharecter = Int(maxchar)!
                        print(maxcharecter)
                    }
                }
            }

            lblitemcode.isHidden = false
            txtitemcode.isHidden = false
            viewitemcode.isHidden = false
        }
    }

    @IBAction func send(_ sender: Any) {
        if txttitle.text == "" {
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please enter title", hideAfter: 1.0)
        } else {
            
            txttitle.text = removeSpecialCharsFromString(text: txttitle.text!)
            print(txttitle.text!)

            
            let story = UIStoryboard(name: "Main", bundle: nil)
            let UploadSDCardVC = story.instantiateViewController(withIdentifier: "UploadSDCardVC") as! UploadSDCardVC
            if isfromview == "video" {
                UploadSDCardVC.chosenBitrateindex = Int(sliderview.index)
                UploadSDCardVC.FilesArraySD = assestarraySD
                appdelegate.originalFileName = "\(txttitle.text!)"

                UploadSDCardVC.isimage = isfromview
                UploadSDCardVC.filename = txttitle.text
                UploadSDCardVC.X_LATAKOO_ITEM_CODE = txtitemcode.text ?? ""
                UploadSDCardVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(UploadSDCardVC, animated: true)

            } else {
                UploadSDCardVC.FilesArraySD = assestarraySD
                appdelegate.originalFileName = "\(txttitle.text!)"
                UploadSDCardVC.isimage = isfromview
                UploadSDCardVC.filename = txttitle.text
                UploadSDCardVC.X_LATAKOO_ITEM_CODE = txtitemcode.text ?? ""
                UploadSDCardVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(UploadSDCardVC, animated: true)
            }

//             let story = UIStoryboard(name: "Main", bundle: nil)
//             let UploadVC = story.instantiateViewController(withIdentifier: "UploadVC") as! UploadVC
//             if isfromview == "audio" {
//                 UploadVC.filearrayaudio = assestarrayaudio
//                 appdelegate.originalFileName = "\(txttitle.text!).m4a"
//             } else if isfromview == "video" {
//                 UploadVC.chosenBitrateindex = Int(sliderview.index)
//                 UploadVC.FilesArray = assestarray
//                 appdelegate.originalFileName = "\(txttitle.text!).mp4"
//             } else {
//                 UploadVC.FilesArray = assestarray
//                 appdelegate.originalFileName = "\(txttitle.text!).jpg"
//             }
//             UploadVC.isimage = isfromview
//             UploadVC.filename = txttitle.text
//             UploadVC.X_LATAKOO_ITEM_CODE = txtitemcode.text ?? ""
//             UploadVC.hidesBottomBarWhenPushed = true
//             navigationController?.pushViewController(UploadVC, animated: true)
        }
    }

    @IBAction func bakc(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func btnkeyword(_ sender: Any) {
        var story: UIStoryboard?

        if IS_IPHONE {
            story = UIStoryboard(name: "Main", bundle: nil)
        } else {
            story = UIStoryboard(name: "MainIPAD", bundle: nil)
        }
        let KeywordVC = story?.instantiateViewController(withIdentifier: "KeywordVC") as! KeywordVC
        navigationController?.pushViewController(KeywordVC, animated: true)
    }

    @IBAction func slder(_ sender: Any) {}

    @IBAction func btnsharing(_ sender: Any) {
        var story: UIStoryboard?

        if IS_IPHONE {
            story = UIStoryboard(name: "Main", bundle: nil)
        } else {
            story = UIStoryboard(name: "MainIPAD", bundle: nil)
        }
        let MultipleNetwork = story?.instantiateViewController(withIdentifier: "MultipleNetwork") as! MultipleNetwork
        navigationController?.pushViewController(MultipleNetwork, animated: true)
    }

    @IBAction func contacts(_ sender: Any) {
        var story: UIStoryboard?

        if IS_IPHONE {
            story = UIStoryboard(name: "Main", bundle: nil)
        } else {
            story = UIStoryboard(name: "MainIPAD", bundle: nil)
        }
        let EmailVC = story?.instantiateViewController(withIdentifier: "EmailVC") as! EmailVC
        navigationController?.pushViewController(EmailVC, animated: true)
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         // Get the new view controller using segue.destination.
         // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func sliderquality(_ sender: Any) {
        let newindex = Int(sliderview.index)
        let dict = bitratearry[newindex] as! [String: Any]
        lblquality.text = dict["label"] as? String
        appdelegate.fileurl = nil

    }
    
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars = Set("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-& ")
        return text.filter {okayChars.contains($0) }
    }
    
    

    func checkifkeywordselected() {
        if App.ArrKeywords.count > 0 {
            lblkeywords.text = "\(App.ArrKeywords.count) Selected"
        }
    }

    func checkifcontactselected() {
        if App.ArrEmail.count > 0 {
            lblcontact.text = "\(App.ArrEmail.count) Selected"
        }
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }

    let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 20 {
            if txtitemcode.text!.count >= maxcharecter, range.length == 0 {
                return false // return NO to not change text
            } else {
                return true
            }
        }

        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")

        return (string == filtered)
    }
}
