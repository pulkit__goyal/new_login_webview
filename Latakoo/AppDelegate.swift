//
//  AppDelegate.swift
//  Latakoo
//
//  Created by Bharat Parashar on 14/11/18.
//  Copyright © 2018 parangat technology. All rights reserved.
//

import AWSSNS
import CoreData
import Firebase
import IQKeyboardManagerSwift
import Photos
import UIKit
import UserNotifications
import CoreTelephony
import AWSMobileClient
import AWSS3

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    let bgView = UIView()
    let bgView1 = UIView()
    let backgroundbg = UIView()

    var isfilter: Bool = false
    var isUploadingPause: Bool = false

    var isgotomanifest: Bool = false

    var isfromshortcut: Bool = false
    var isfromshortcutidentifir: String = ""

    var ArrEmail: [Any] = []
    var ArrKeywords: [Any] = []
    var ArrNetworks: [String] = []
    @objc var TrimThisAudio: [String: Any] = [:]
    @objc var isitunes: String?
    @objc var videoduration: String?
    @objc var posToMove: CGFloat = 0

    @objc var isdarmode: Bool = false

    var jeremyGif = UIImage.gifImageWithName("latakoo_loder")
    var jeremy = UIImage.gifImageWithName("loader")

    var imageView: UIImageView?
    var imageView1: UIImageView?

    @objc var arrrequestids: [Any] = []
    @objc var arrrnames: [String] = []

    @objc var arrrequest: [Any] = []

    var image: [PHAsset] = []
    var all: [PHAsset] = []
    var video: [PHAsset] = []
    var AssignmentID: String = ""
    var TopicID: String = ""
    var OrgID: String = ""
    var Assignmentname: String = ""

    var ifgotocapture: String = ""
    var isStitch: String = ""
      
    var uploadas: String = ""
    var checkdownload: String = ""

    var wrongstitch: Bool = false

    var fileurl: URL?
    var appTID: String = ""

    var restrictRotation: UIInterfaceOrientationMask = .all
    var launchedShortcutItem: UIApplicationShortcutItem?

    var hideshowitemcode: Bool = true
    var isfromshare: Bool = false
    var imagebackground = UIImageView()
    let SNSPlatformApplicationArn = "arn:aws:sns:us-east-1:184321411418:app/APNS/LatakooProd"

 //  let SNSPlatformApplicationArn = "arn:aws:sns:us-east-1:184321411418:app/APNS_SANDBOX/LatakooDev"

    ////AWS S3////////AWS S3////////AWS S3////////AWS S3////
    var accessKey: String = ""
    var secretKey: String = ""
    var bucketname: String = ""
    var bucketpath: String = ""
    var filename: String = ""
    var sessionToken: String = ""
    var expiration: String = ""
    var fileNameFromServer: String = ""
    var thumbnailBucketpath: String = ""
    var filenameStr: String = ""
    var originalFileName: String = ""
    var bucketpTest: String = ""
    
    var newtimer: Timer?

    //////////////////////////////////////////////////////////////////////////////
    
//    var awsTransferUtility = AWSS3TransferUtility()


    func application(_ application: UIApplication,
                     handleEventsForBackgroundURLSession identifier: String,
                     completionHandler: @escaping () -> Void) {
//        AWSMobileClient.sharedInstance().initialize { _, error in
//            guard error == nil else {
//                print("Error initializing AWSMobileClient. Error: \(error!.localizedDescription)")
//                return
//            }
//            print("AWSMobileClient initialized.")
//        }

        // provide the completionHandler to the TransferUtility to support background transfers.
        AWSS3TransferUtility.interceptApplication(application,
                                                  handleEventsForBackgroundURLSession: identifier,
                                                  completionHandler: completionHandler)
    }
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
       
        
        let modelName = UIDevice.modelName
       
        print(modelName)
          
        let systemVersion = UIDevice.current.systemVersion
      
        print(systemVersion)

        
        let login = LoginVC()
        if #available(iOS 12.0, *) {
            if login.traitCollection.userInterfaceStyle == .dark {
                jeremy = UIImage.gifImageWithName("loader")
            }
        }

        configDynamicShortcutItems()

        if let shortcutItem = launchOptions?[UIApplicationLaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
            launchedShortcutItem = shortcutItem
        }

        FirebaseApp.configure()

        imageView = UIImageView(image: jeremyGif)
        imageView1 = UIImageView(image: jeremyGif)

        application.isStatusBarHidden = true
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 100
        if #available(iOS 13.0, *) {
            IQKeyboardManager.shared.toolbarTintColor = UIColor.label
        } else {
            IQKeyboardManager.shared.toolbarTintColor = UIColor.black
        }

        // Override point for customization after application launch.

        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)

            UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: { _, _ in })

        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()

        var storyboard: UIStoryboard?

//        if IS_IPHONE {
//            storyboard = UIStoryboard(name: "Main", bundle: nil)
//        } else {
//            storyboard = UIStoryboard(name: "MainIPAD", bundle: nil)
//        }
        
        
                if IS_IPHONE {
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                } else {
                    storyboard = UIStoryboard(name: "MainIPAD", bundle: nil)
                }
                

        window = UIWindow(frame: UIScreen.main.bounds)
        
               let ViewController = storyboard?.instantiateViewController(withIdentifier: "LoginWebview") as! LoginWebview
                let navigationController = UINavigationController(rootViewController: ViewController)

//       let ViewController = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//        let navigationController = UINavigationController(rootViewController: ViewController)
//
        
     //  let ViewController = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
    //  let navigationController = UINavigationController(rootViewController: ViewController)
        
        navigationController.setNavigationBarHidden(true, animated: false)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()

        perform(#selector(loader), with: nil, afterDelay: 0.3)

        return true
    }

    func initializeS3() {
        let credentialsProvider = AWSBasicSessionCredentialsProvider(accessKey: accessKey, secretKey: secretKey, sessionToken: sessionToken)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.USEast1, credentialsProvider: credentialsProvider)
        //        configuration?.isAccessibilityElement = false
//        configuration?.timeoutIntervalForRequest = 1800 // in seconds
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }

    @objc func loader() {
        initializeLoader()
    }

    func initializeLoader() {
        bgView.frame = (window?.frame)!
        bgView1.frame = (window?.frame)!
        bgView1.backgroundColor = UIColor.black
        bgView1.alpha = 0.3
        imageView1?.contentMode = .scaleAspectFit
        imageView1?.frame = CGRect(x: 0, y: 0, width: 120, height: 120)
        imageView1?.backgroundColor = UIColor(named: "loaderbackground")
        imageView1?.layer.cornerRadius = 60
        imageView1?.layer.masksToBounds = true
        imageView1?.center = (window?.center)!

        bgView.isHidden = true
        bgView1.isHidden = true
    }

    func ShowProgress() {
        DispatchQueue.main.async {
            self.bgView.addSubview(self.imageView1!)
            self.window!.addSubview(self.bgView1)
            self.window!.addSubview(self.bgView)
            self.window?.bringSubview(toFront: self.bgView1)
            self.window?.bringSubview(toFront: self.bgView)
            self.bgView.isHidden = false
            self.bgView1.isHidden = false
        }
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token = ""
        for i in 0 ..< deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }

        print(token)

        UserDefaults.standard.set(token, forKey: "DeviceToken")
        UserDefaults.standard.synchronize()
    }

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if IS_IPHONE {
            return UIInterfaceOrientationMask.portrait

        } else {
            return restrictRotation
        }
    }

    func HideProgress() {
        DispatchQueue.main.async {
            self.bgView.removeFromSuperview()
            self.imageView1!.removeFromSuperview()
            self.bgView1.removeFromSuperview()

            self.bgView.isHidden = true
            self.bgView1.isHidden = true
        }
    }

    func getAPIEncodedHeader(withParameters params: String?) -> String? {
        var params = params

        params = params?.replacingOccurrences(of: "%20", with: "+") // some issue with token using %20 encoded space causing auth fail

        let email = UserDefaults.standard.object(forKey: "Email") as? String
        let timestamp = String(format: "%i", Int(Date().timeIntervalSince1970))
        let authcode = UserDefaults.standard.object(forKey: "Authcode") as? String
        let pwd = UserDefaults.standard.object(forKey: "Password") as? String
        let pwd_authPart = "\(String(describing: pwd))\(authcode ?? "")".toMD5()
        let suffix = "\(pwd_authPart)\(params ?? "")\(timestamp)".toMD5()
        let API_Token = "\(email ?? ""):\(timestamp):\(suffix)"
        return API_Token
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }



    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any] = [:]) -> Bool {
        if UserDefaults.standard.value(forKey: "home") != nil {
            var sharedUserDefaults: UserDefaults?
            sharedUserDefaults = UserDefaults(suiteName: "group.com.latakoo.ext")
            let shareimages = sharedUserDefaults?.array(forKey: "SharedExtension")
            print(shareimages as Any)

            isfromshare = true

            let navigat = UINavigationController()

            var story: UIStoryboard?

            if IS_IPHONE {
                story = UIStoryboard(name: "Main", bundle: nil)
            } else {
                story = UIStoryboard(name: "MainIPAD", bundle: nil)
            }
            let TabbarController = story?.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
            TabbarController.selectedIndex = 2
            navigat.pushViewController(TabbarController, animated: false)
            window!.rootViewController = navigat
            TabbarController.navigationController?.navigationBar.isHidden = true
            window!.makeKeyAndVisible()
        }

        return true
    }

    @objc func showview() {
        imagebackground.frame = (window!.frame)
        imagebackground.image = UIImage(named: "window.png")
        // self.window?.addSubview(imagebackground)
        UIApplication.shared.keyWindow?.addSubview(imagebackground)
        UIApplication.shared.keyWindow?.bringSubview(toFront: imagebackground)
    }

    func hideview() {
        imagebackground.isHidden = true
        imagebackground.removeFromSuperview()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
           NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)

           UIApplication.shared.applicationIconBadgeNumber = 0
           newtimer?.invalidate()

       }
       
      
       func applicationDidEnterBackground(_ application: UIApplication) {


//           let application1 = UIApplication.shared
//
//           if UIDevice.current.responds(to: #selector(getter: UIDevice.isMultitaskingSupported)) {
//               print("Multitasking Supported")
//
//
//           newtimer =  Timer.scheduledTimer(
//                   timeInterval: 0.1,
//                   target: self,
//                   selector: #selector(countUp),
//                   userInfo: nil,
//                   repeats: true)
//               var background_task: UIBackgroundTaskIdentifier
//               background_task = application1.beginBackgroundTask(expirationHandler: {
//
//                   print("expirationHandler")
//
//
//
//               })
//
//               DispatchQueue.global(qos: .default).async(execute: {
//
//                   while true {
//                       Thread.sleep(forTimeInterval: 1.0)
//
//                   }
//
//               })
//           } else {
//               print("Multitasking Not Supported")
//           }
//
//
//           NotificationCenter.default.post(name: NSNotification.Name("applicationWentInbackground"), object: nil)
       
       }
           
       @objc func countUp(){
           let currentDateTime = Date()
            print(currentDateTime)
           
       }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        

        guard let shortcutItem = launchedShortcutItem else { return }

        // If there is any shortcutItem,that will be handled upon the app becomes active
        _ = handleShortcutItem(item: shortcutItem)
        
        
        // We make it nil after perfom/handle method call for that shortcutItem action
        launchedShortcutItem = nil
    }
    
    
    func gotoscreen(){
        
         var storyboard: UIStoryboard?

                if IS_IPHONE {
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                } else {
                    storyboard = UIStoryboard(name: "MainIPAD", bundle: nil)
                }

                window = UIWindow(frame: UIScreen.main.bounds)

                let viewController = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                let navigationController = UINavigationController(rootViewController: viewController)
                navigationController.setNavigationBarHidden(true, animated: false)
                window?.rootViewController = navigationController
                window?.makeKeyAndVisible()

                perform(#selector(loader), with: nil, afterDelay: 0.3)
        
    }
    

    func handleShortcutItem(item: UIApplicationShortcutItem) -> Bool {
        var handled: Bool = false

        if UserDefaults.standard.value(forKey: "home") != nil {
            isfromshortcut = true

            if item.type == "com.Latakoo.Capture" {
                handled = true
                isfromshortcutidentifir = "Capture"
                self.gotoscreen()
                

            } else if item.type == "com.Latakoo.Pilot" {
                handled = true
                isfromshortcutidentifir = "Pilot"
                self.gotoscreen()

            } else if item.type == "com.Latakoo.Library" {
                handled = true
                isfromshortcutidentifir = "Library"
                self.gotoscreen()

            } else if item.type == "com.Latakoo.Edit" {
                handled = true
                isfromshortcutidentifir = "Edit"
                self.gotoscreen()

            }
        }

        return handled
    }

    func configDynamicShortcutItems() {
        let captureIcon = UIApplicationShortcutIcon(templateImageName: "CaptureIcon_iPad")
        let pilotIcon = UIApplicationShortcutIcon(templateImageName: "PilotIcon_iPad")
        let libraryIcon = UIApplicationShortcutIcon(templateImageName: "IOS7LibraryIcon")
        let editicon = UIApplicationShortcutIcon(templateImageName: "IOS7EditIcon_iPad")

        let shortcutCapture = UIApplicationShortcutItem(
            type: "com.Latakoo.Capture",
            localizedTitle: "Capture",
            localizedSubtitle: nil,
            icon: captureIcon,
            userInfo: nil)

        let shortcutPilot = UIApplicationShortcutItem(
            type: "com.Latakoo.Pilot",
            localizedTitle: "Pilot",
            localizedSubtitle: nil,
            icon: pilotIcon,
            userInfo: nil)

        let shortcutLibrary = UIApplicationShortcutItem(
            type: "com.Latakoo.Library",
            localizedTitle: "Library",
            localizedSubtitle: nil,
            icon: libraryIcon,
            userInfo: nil)

        let shortcutSettings = UIApplicationShortcutItem(
            type: "com.Latakoo.Edit",
            localizedTitle: "Edit",
            localizedSubtitle: nil,
            icon: editicon,
            userInfo: nil)

        let items = [shortcutCapture, shortcutPilot, shortcutLibrary, shortcutSettings]

        UIApplication.shared.shortcutItems = items
    }

    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        completionHandler(handleShortcutItem(item: shortcutItem))
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
          The persistent container for the application. This implementation
          creates and returns a container, having loaded the store for the
          application to it. This property is optional since there are legitimate
          error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Latakoo")
        container.loadPersistentStores(completionHandler: { _, error in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension String {
    func toMD5() -> String {
        if let messageData = data(using: String.Encoding.utf8) {
            var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
            _ = digestData.withUnsafeMutableBytes { digestBytes in
                messageData.withUnsafeBytes { messageBytes in
                    CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
                }
            }
            return digestData.hexString()
        }

        return self
    }
}

extension Data {
    func hexString() -> String {
        let string = map { String($0, radix: 16) }.joined()
        return string
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    // MARK: - Delegates for Notifications

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        let dict = userInfo["aps"] as? [String: Any]
        
        if dict != nil {
        if (dict!["notificationType"] as? String) != nil {
            UserDefaults.standard.set(dict, forKey: "userpush")
            UserDefaults.standard.set("redirecttonext", forKey: "redirecttonext")

            var story: UIStoryboard?

            if IS_IPHONE {
                story = UIStoryboard(name: "Main", bundle: nil)
            } else {
                story = UIStoryboard(name: "MainIPAD", bundle: nil)
            }
            let LoginVC = story?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav = window?.rootViewController as! UINavigationController
            nav.pushViewController(LoginVC, animated: true)
        }
        }else{
            print("Dict is nil")
        }
        completionHandler()
    }

    // called if app is running in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification,
                                withCompletionHandler completionHandler:
                                @escaping (UNNotificationPresentationOptions) -> Void) {
//        let userInfo = notification.request.content.userInfo
//        print(userInfo)
//
//               print(userInfo)
//               let dict = userInfo as? [String:Any]
//               if dict!["notificationType"] != nil {
//              UserDefaults.standard.set(dict, forKey: "userpush")
//              UserDefaults.standard.set("redirecttonext", forKey: "redirecttonext")

        //   }

        return completionHandler([UNNotificationPresentationOptions.alert,
                                  UNNotificationPresentationOptions.sound,
                                  UNNotificationPresentationOptions.badge])
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Entire message: didReceiveRemoteNotification: \(userInfo)")

        print(userInfo)
//               let dict = userInfo as? [String:Any]
//               if dict!["notificationType"] != nil {
//              UserDefaults.standard.set(dict, forKey: "userpush")
//              UserDefaults.standard.set("redirecttonext", forKey: "redirecttonext")

        //    }

        let state: UIApplication.State = application.applicationState
        switch state {
        case UIApplication.State.active:
            print("If needed notify user about the message")
        default:
            print("Run code to download content")
        }

        completionHandler(UIBackgroundFetchResult.newData)
    }

    func startLocalNotificationMessage(message: String, title: String) {
        // Bind this method to UIButton action
        print("startLocalNotification")
        let notification = UILocalNotification()
        notification.alertTitle = title
        notification.fireDate = Date(timeIntervalSinceNow: 1)
        notification.alertBody = message
        notification.timeZone = NSTimeZone.default
        notification.soundName = UILocalNotificationDefaultSoundName
        //   notification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        UIApplication.shared.scheduleLocalNotification(notification)
    }

    func getvideo() {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.video, options: fetchOptions)
        let lastPHAsset = assets.lastObject

        if lastPHAsset != nil {
            all.insert(lastPHAsset!, at: 0)
            video.insert(lastPHAsset!, at: 0)
        }
    }

    func getimage() {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
        let lastPHAsset = assets.lastObject

        if lastPHAsset != nil {
            all.insert(lastPHAsset!, at: 0)
            image.insert(lastPHAsset!, at: 0)
        }
    }
}

