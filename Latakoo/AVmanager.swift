//
//  AVmanager.swift
//  Btv-iOS
//
//  Created by suresh on 12/11/19.
//  Copyright © 2019 leewayhertz. All rights reserved.
//

import Foundation
import AVKit

class AVmanager {
    
    static let AVSharedManager = AVmanager()
    var controller:AVPlayerViewController?
    var controllerForEdit:AVPlayerViewController?
    init() {
        controller  = AVPlayerViewController()
        controllerForEdit = AVPlayerViewController()
        controller!.showsPlaybackControls = true
        controllerForEdit!.showsPlaybackControls = false
             do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryMultiRoute)
               }
             catch {
        }
    }
    
    
    var avControllerPlayer:AVPlayerViewController{
        get{
            return controller!
        }
    }
    
    var avControllerForEdit:AVPlayerViewController{
        get{
            
            return controllerForEdit!
        }
    }
}
