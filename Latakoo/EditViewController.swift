//
//  EditViewController.swift
//  Btv-iOS
//
//  Created by Suresh on 27/08/19.
//  Copyright © 2019 leewayhertz. All rights reserved.


import UIKit
import MobileCoreServices
import AVKit
import Photos
import Foundation


protocol EditViewControllerDelegate:class {
    func newvideo(_ controller: EditVideo, file: PHAsset , delete: Bool)
}


class EditViewController: UIViewController,ICGVideoTrimmerDelegate {
    
    var isFromPublishing = false
    weak var delegate: EditViewControllerDelegate?

    @IBOutlet weak var playBackSlider: UISlider!
    @IBOutlet weak var proTips: UIButton!
    @IBOutlet weak var trimmerView: ICGVideoTrimmerView!
    @IBOutlet weak var videoLayer: UIView!
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var startTimeText: UILabel!
    @IBOutlet weak var durationTime: UILabel!
    @IBOutlet weak var endTimeText: UILabel!
    @IBOutlet weak var middle1: UILabel!
    @IBOutlet weak var middle2: UILabel!
    @IBOutlet weak var trimlabel: UILabel!
    @IBOutlet weak var cutlabel: UILabel!
    
    //Slider
    @IBOutlet weak var waterMarkLbl: UILabel!
    @IBOutlet weak var sliderplayBtn: UIButton!
    @IBOutlet weak var sliderstartTimeLbl: UILabel!
    @IBOutlet weak var sliderendTimeLbl: UILabel!
    @IBOutlet weak var _playheadSlider: UISlider!
    
    var mytimer: Timer?

    typealias CompletionHandler = (_ success:Bool, _ mergedVideoUrl: URL) -> Void
    var editVideoUrl:URL? = nil
    
    var isPlaying = true
    var isSliderEnd = true
    var isSliderVisible:Bool = true
    var isOkPressed = false
    
    var firstpartasset : AVAsset?
    var secondpartasset : AVAsset?
    
    var startTime: CGFloat = 0.0
    var stopTime: CGFloat = 0.0
    var Middle1 : CGFloat = 0
    var Middle2  : CGFloat = 0
    
    let playerObserver: Any? = nil
    var videoPlaybackPosition: CGFloat = 0.0
    var timeObserver: AnyObject!
    var getVideoListS3:[URL]?
    var player:AVPlayer?
    var checkDuration:Float?
    
    var videoUrl:URL!
    var generator:AVAssetImageGenerator!
    let exportSession: AVAssetExportSession! = nil
    var playerItem: AVPlayerItem!
    var playerLayer: AVPlayerLayer!
    var asset: AVAsset!
    var buttonSwitched : Bool = false
    var thumbTime: CMTime!
    
    var observedValue:Double = 0
    var start:Float?
    var end:Float?
    var aVcontroller:AVPlayerViewController?
    
    var videoMaximumDuration: Float = 0
    var currentDuration:Float = 0.0
    
    var isSliderValueChanged: Bool = false
    var isVideoPlayerActive: Bool = false
    var isFromEditedVideo = false
    
    var iscut: Bool = false
    var VIDEO_ID = ""
    var videoasset : PHAsset?
    @IBOutlet weak var volumes: UISlider!

    
    @IBOutlet var playBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        //Originl Code
        trimlabel.isHidden = false
        cutlabel.isHidden = true
        middle1.isHidden = true
        middle2.isHidden = true
         iscut = false
        //For proTips Button
        proTips.layer.borderWidth = 1
        proTips.layer.borderColor = UIColor(red: 86/255, green: 172/255, blue: 80/255, alpha:1).cgColor
        proTips.layer.cornerRadius = 6
        proTips.layer.masksToBounds = true
        
       
      
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        videoUrl = editVideoUrl
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
           self.addtrimmer()

        })

        let option1 = PHVideoRequestOptions()
        option1.isNetworkAccessAllowed = true
        option1.deliveryMode = .automatic
        option1.version = .original
        option1.isNetworkAccessAllowed = true
        option1.progressHandler = {  (progress, error, stop, info) in
            
            
            
            
        }
        
        
        var resultAsset: AVAsset?
        PHCachingImageManager.default().requestAVAsset(forVideo: videoasset!, options: option1, resultHandler: { avasset, audioMix, info in
            DispatchQueue.main.async{
                resultAsset = avasset
                let duration = resultAsset?.duration
                let durationTime = CMTimeGetSeconds(duration ??  CMTimeMake(1, 1))
                print(durationTime)
                let urlAsset = avasset as? AVURLAsset
                let localVideoUrl = urlAsset?.url
                self.videoUrl = localVideoUrl
                self.asset   = urlAsset
                let videoDuration = durationTime
                self.addPlayer(duration : Float(videoDuration), lowerHandler: 0.0)
                     self.aVcontroller?.showsPlaybackControls = false
                
                
                self.playBackSlider.isHidden = false
                self.playBackSlider.value = 0.0
                self.playBackSlider.minimumValue = 0.0
                self.playBackSlider.isContinuous = true
                self.playBackSlider.tintColor = UIColor.clear
                self.playBackSlider.setThumbImage(UIImage(named: "trimline"), for: .normal)
                
                //Custom slider code
                self.sliderplayBtn.isSelected = true
                self.sliderplayBtn.setImage(UIImage(named: "pause"), for: .normal)
                
                let timeInterval: CMTime = CMTimeMakeWithSeconds((1/12), CMTimeScale(USEC_PER_SEC))
                self.timeObserver = self.player!.addPeriodicTimeObserver(forInterval: timeInterval,
                queue: DispatchQueue.main) { (elapsedTime: CMTime) -> Void in
                 self.observeSliderTime() } as AnyObject?

                
                let duration1 : CMTime = (self.player?.currentItem!.asset.duration)!
                let seconds : Float64 = CMTimeGetSeconds(duration1)
                self.sliderendTimeLbl.text = self.stringFromTimeInterval(interval: seconds)
                self._playheadSlider.setThumbImage(UIImage(named: "smallThumb"), for: .normal)
                
            }
        })
        
        
        
        
        
     

   

    }
    
    deinit {
        aVcontroller = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        isSliderVisible = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        
        //self.player?.pause()
    }
    
    // updated code for trim
    func addPlayer(duration : Float , lowerHandler: Float) {

        videoMaximumDuration = duration
        print("videoMaximumDuration===>", videoMaximumDuration)
        
        asset   = AVURLAsset.init(url: videoUrl)
        let item:AVPlayerItem = AVPlayerItem(asset: asset)

        player   = AVPlayer(playerItem: item)

        aVcontroller = AVmanager.AVSharedManager.avControllerForEdit
        aVcontroller!.player = player
        aVcontroller!.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
        self.addChildViewController(aVcontroller!)
        videoLayer.addSubview(aVcontroller!.view)
        aVcontroller!.view.frame = videoLayer.bounds
        videoLayer.bringSubview(toFront: playBtn)
        playBackSlider.maximumValue = videoMaximumDuration
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapOnVideoLayer))
        self.videoLayer.addGestureRecognizer(tap)
                
        self.tapOnVideoLayer(tap: tap)
        
        mytimer?.invalidate()
                      mytimer = nil
                      mytimer = Timer.scheduledTimer(
                          timeInterval: 0.1,
                          target: self,
                          selector: #selector(seekToTime),
                          userInfo: nil,
                          repeats: true)

                       Timer.scheduledTimer(
                          timeInterval: 0.1,
                          target: self,
                          selector: #selector(checkifplaying),
                          userInfo: nil,
                          repeats: true)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(EditViewController.itemDidFinishPlaying(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        
        
        let timeInterval: CMTime = CMTimeMakeWithSeconds((1/12), CMTimeScale(USEC_PER_SEC))
        timeObserver = player!.addPeriodicTimeObserver(forInterval: timeInterval,
                                                       queue: DispatchQueue.main) { (elapsedTime: CMTime) -> Void in
                                                        self.observeTime() } as AnyObject?
        
        if(isOkPressed){
            seekVideo(toPos: lowerHandler)
        }
    }
    
    
    @objc func itemDidFinishPlaying(_ notification: Notification) {
        
        self.playBackSlider.setValue(Float(startTime), animated: false)
        self.player?.pause()

        
    }
    
    
    @objc func seekToTime() {
       
          let second = CGFloat(player!.currentTime().seconds)
        
        if second >= stopTime {
            self.playBackSlider.setValue(Float(startTime), animated: true)
           self.seekVideo(toPos: Float(startTime))
            self.player?.pause()
            return
        }
             
        
        
         UIView.animate(withDuration: TimeInterval(1), delay: 0, options: [.curveLinear, .beginFromCurrentState], animations: { [self] in
            
            if self.iscut == false{
                if second > self.startTime && second < self.stopTime {
                    if (self.player?.rate != 0) && (self.player?.error == nil) {
                        self.playBackSlider.setValue(Float(second), animated: false)

                        }
                   }
            }else{
                
                if second > self.startTime && second < self.stopTime {

                    if second > self.Middle1 && second < self.Middle2 {
                        self.seekVideo(toPos: Float(self.Middle2)+0.5)
                        self.playBackSlider.setValue(Float(self.Middle2)+0.5, animated: false)

                    
                    }else{
                        if (self.player?.rate != 0) && (self.player?.error == nil) {
                    self.playBackSlider.setValue(Float(second), animated: false)
                          }

                    }
                   
                
                }
                
                
               }
            })
           
    }
    
    
    
    @objc  func checkifplaying() {

        if (player?.rate != 0) && (player?.error == nil) {
            if mytimer?.isValid ?? true {
              
            
         
            
            } else {
                mytimer?.invalidate()
                   mytimer = nil
                   mytimer = Timer.scheduledTimer(
                       timeInterval: 0.1,
                       target: self,
                       selector: #selector(seekToTime),
                       userInfo: nil,
                       repeats: true)
               }
           }
       
    }
    
    
    func addtrimmer(){
 //   trimmerView.leftThumbImage = UIImage.init(named: "Left_Arrow_Video")
 // trimmerView.rightThumbImage = UIImage.init(named: "Right_Arrow_Video")
        trimmerView.themeColor = UIColor.init(red:126, green:211, blue:32)
         trimmerView.iscut = iscut
         trimmerView.asset = AVAsset.init(url: videoUrl)
         trimmerView.showsRulerView = false
         trimmerView.maxLength = 2000
         trimmerView.rulerLabelInterval = 10
         trimmerView.trackerColor = UIColor.cyan
         trimmerView.delegate = self
         trimmerView.resetSubviews()
         trimmerView.showsRulerView = false
         trimmerView.hideTracker(true)
          
    }
    
    func trimmerView(_ trimmerView: ICGVideoTrimmerView, didChangeLeftPosition startTime: CGFloat, rightPosition endTime: CGFloat) {
        isSliderValueChanged = true
        isVideoPlayerActive = false
        playBtn.isHidden = false
        player?.pause()
          if (startTime != self.startTime || startTime == 0) {
            self.seekVideo(toPos: Float(startTime))
          }
         
        
          self.startTime = startTime
          self.stopTime = endTime
        
        playBackSlider.setValue(Float(startTime), animated: false)

          startTimeText.text = timeFormatted(startTime, isWithMinutes: false)
          endTimeText.text = timeFormatted(endTime, isWithMinutes: false)
         
      }
      
      func trimmerView(_ trimmerView: ICGVideoTrimmerView, didChangeLeftPosition startTime: CGFloat, middleStart MiddleTime: CGFloat, mIddleEnd MiddleEnd: CGFloat, rightPosition endTime: CGFloat) {
          
        isSliderValueChanged = true
        isVideoPlayerActive = false
        playBtn.isHidden = false
        player!.pause()
         if (startTime != self.startTime || startTime == 0) {
            self.seekVideo(toPos: Float(startTime))
         }
         
         self.startTime = startTime
         self.stopTime = endTime
         self.Middle1 = MiddleTime
         self.Middle2 = MiddleEnd
         playBackSlider.setValue(Float(startTime), animated: false)
         middle1.text = timeFormatted(Middle1, isWithMinutes: false)
          middle2.text = timeFormatted( self.Middle2, isWithMinutes: false)
         startTimeText.text = timeFormatted(startTime, isWithMinutes: false)
         endTimeText.text = timeFormatted(endTime, isWithMinutes: false)
        
      }
      
      func timeFormatted(_ interval: CGFloat, isWithMinutes: Bool) -> String? {
      
          var milliseconds = UInt(interval * 1000)
          var seconds = milliseconds / 1000
          milliseconds %= 1000
          let minutes = seconds / 60
          seconds %= 60
          //    unsigned long hours = minutes / 60;
          //    minutes %= 60;

          var strMillisec = NSNumber(value: milliseconds).stringValue
          if strMillisec.count > 2 {
              strMillisec = (strMillisec as NSString).substring(to: 2)
          }

          if isWithMinutes {
              return String(format: "%02ld:%02ld:%02ld", Int(minutes), Int(seconds), Int(strMillisec) ?? 0)
          } else {
              return String(format: "%02ld:%02ld", Int(minutes), Int(seconds))
          }
      }
      
    @IBAction func btntrim(_ sender: Any) {
        middle1.isHidden = true
        middle2.isHidden = true
        trimlabel.isHidden = false
        cutlabel.isHidden = true
        iscut = false
        self.addtrimmer()
    }
    
    @IBAction func btncut(_ sender: Any) {
        middle1.isHidden = false
        middle2.isHidden = false
        trimlabel.isHidden = true
        cutlabel.isHidden = false
        iscut = true
        self.addtrimmer()
        
    }
    
    
    
    private func observeTime() {
        
        let second = Int(player!.currentTime().seconds)
        let minutes = second/60
        let sec = second - (minutes*60)
        let minStr = String(format:"%02d", minutes)
        let secStr = String(format:"%02d", sec)
        durationTime.text   = "\(minStr):\(secStr)"
       // playBackSlider.maximumValue = videoMaximumDuration
      //  playBackSlider.setValue(Float(sec), animated: true)

    }
    
    
    @IBAction func playBtn(_ sender: Any) {
        isVideoPlayerActive = true
        playBtn.isHidden = true

        self.player?.play()
    }
    
    @objc func tapOnVideoLayer(tap: UITapGestureRecognizer)
    {
        isVideoPlayerActive = false
        self.player?.pause()
        playBtn.isHidden = false
        
    }
    
    func seekVideo(toPos position: Float){
        //self.videoPlaybackPosition = pos
        
        let time: CMTime = CMTimeMakeWithSeconds(Float64(position), CMTimeScale(USEC_PER_SEC))
        self.player?.seek(to: time, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
    }
    

    

    
    @IBAction func slider(_ sender: UISlider) {
        player?.pause()
        player?.seek(to: CMTimeMakeWithSeconds(Float64(sender.value), 60000))
 
    }
    

      

      
    
    
    
    func orientationFromTransform(transform: CGAffineTransform) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
      
        
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
            assetOrientation = .right
            isPortrait = true
        } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
            assetOrientation = .left
            isPortrait = true
      
        } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
           
            assetOrientation = .up
      
        
        } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
            assetOrientation = .down
       
        }
        return (assetOrientation, isPortrait)
    }
      
      
      func deleteFile(_ filePath:URL) {
          guard FileManager.default.fileExists(atPath: filePath.path) else {
              return
          }
          do {
              try FileManager.default.removeItem(atPath: filePath.path)
          }catch{
              fatalError("Unable to delete file: \(error) : \(#function).")
          }
      }
    
   
        
    

    

    
    //MARK:- Custom Controls
    
    @IBAction func handlePlayPauseButtonPressed(_ sender: UIButton) {
           //  sender.isSelected ?  currentPlayer.pause() :   currentPlayer.play()
            if sender.isSelected {
                self.sliderplayBtn.isSelected = false
                player?.pause()
                self.sliderplayBtn.setImage(UIImage(named: "whitePlay"), for: .normal)
            }
            else {
                self.sliderplayBtn.isSelected = true
                player?.play()
                self.sliderplayBtn.setImage(UIImage(named: "pause"), for: .normal)
                
            }
        }
        
        @IBAction func handlePlayheadSliderTouchBegin(_ sender: UISlider) {
            self.sliderplayBtn.isSelected = false
            player?.pause()
            self.sliderplayBtn.setImage(UIImage(named: "whitePlay"), for: .normal)
            player?.pause()
        }
        
        @IBAction func handlePlayheadSliderValueChanged(_ sender: UISlider) {

            let duration : CMTime = (player?.currentItem!.asset.duration)!
            let seconds : Float64 = CMTimeGetSeconds(duration) * Double(sender.value)
         //   var newCurrentTime: TimeInterval = sender.value * CMTimeGetSeconds(currentPlayer.currentItem.duration)
            self.sliderstartTimeLbl.text = self.stringFromTimeInterval(interval: seconds)
           }
        
        @IBAction func handlePlayheadSliderTouchEnd(_ sender: UISlider) {

            let duration : CMTime = (player?.currentItem!.asset.duration)!
            let newCurrentTime: TimeInterval = Double(sender.value) * CMTimeGetSeconds(duration)
            let seekToTime: CMTime = CMTimeMakeWithSeconds(newCurrentTime, 600)
            player?.seek(to: seekToTime)
        }
        
        func stringFromTimeInterval(interval: TimeInterval) -> String {

            let interval = Int(interval)
            let seconds = interval % 60
            let minutes = (interval / 60) % 60
            //let hours = (interval / 3600)
    //        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
            return String(format: "%02d:%02d", minutes, seconds)
        }
    
        private func observeSliderTime() {
            
            let totalSec = (player?.currentItem!.asset.duration)!
            
               
            let second = Int(player!.currentTime().seconds)
            let dur = Float(player!.currentTime().seconds)
            let requiredSec = Float(dur)/Float(CMTimeGetSeconds(totalSec))
    //        let duration : CMTime = (player?.currentItem!.asset.duration)!
    //        let seconds : Float64 = CMTimeGetSeconds(duration)
            //let miliec
               let minutes = second/60
               let sec = second - (minutes*60)
                //let hours = (second / 3600)
               let minStr = String(format:"%02d", minutes)
               let secStr = String(format:"%02d", sec)
               self.sliderstartTimeLbl.text   = "\(minStr):\(secStr)"
                print("Seconds: \(requiredSec)")
              // playBackSlider.maximumValue = videoMaximumDuration
                _playheadSlider.setValue(Float(requiredSec), animated: true)
            
            if dur == Float(CMTimeGetSeconds(totalSec)) {
                self.sliderplayBtn.isSelected = false
                self.sliderplayBtn.setImage(UIImage(named: "whitePlay"), for: .normal)
                self.player?.pause()
                self.player?.seek(to: kCMTimeZero)
                _playheadSlider.setValue(Float(0.0), animated: true)
            }

           }
    
}

