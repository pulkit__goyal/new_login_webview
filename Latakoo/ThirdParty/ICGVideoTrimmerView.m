//
//  ICGVideoTrimmerView.m
//  ICGVideoTrimmer
//
//  Created by Huong Do on 1/18/15.
//  Copyright (c) 2015 ichigo. All rights reserved.
//

#import "ICGVideoTrimmerView.h"
#import "ICGThumbView.h"
#import "Latakoo-Swift.h"




@interface HitTestView : UIView
@property (assign, nonatomic) UIEdgeInsets hitTestEdgeInsets;
- (BOOL)pointInside:(CGPoint)point;

@end

@implementation HitTestView

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    return [self pointInside:point];
}

- (BOOL)pointInside:(CGPoint)point
{
    CGRect relativeFrame = self.bounds;
    CGRect hitFrame = UIEdgeInsetsInsetRect(relativeFrame, _hitTestEdgeInsets);
    return CGRectContainsPoint(hitFrame, point);
}

@end


@interface ICGVideoTrimmerView() <UIScrollViewDelegate>

@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) UIView *frameView;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) AVAssetImageGenerator *imageGenerator;

@property (strong, nonatomic) HitTestView *leftOverlayView;
@property (strong, nonatomic) HitTestView *rightOverlayView;


@property (strong, nonatomic) HitTestView *leftOverlayViewNew;
@property (strong, nonatomic) HitTestView *rightOverlayViewNew;

@property (strong, nonatomic) HitTestView *middlelayer;




@property (assign, nonatomic) BOOL isDraggingRightOverlayView;
@property (assign, nonatomic) BOOL isDraggingLeftOverlayView;
@property (assign, nonatomic) BOOL isDraggingRightOverlayViewNew;
@property (assign, nonatomic) BOOL isDraggingLeftOverlayViewNew;


@property (strong, nonatomic) UIView *topBorder;
@property (strong, nonatomic) UIView *bottomBorder;

@property (nonatomic) CGFloat startTime;
@property (nonatomic) CGFloat startMiddle;

@property (nonatomic) CGFloat endTime;
@property (nonatomic) CGFloat endMiddle;

@property (nonatomic) CGFloat widthPerSecond;

@property (nonatomic) CGPoint leftStartPoint;
@property (nonatomic) CGPoint rightStartPoint;

@property (nonatomic) CGPoint leftStartPointnew;
@property (nonatomic) CGPoint rightStartPointnew;

@property (nonatomic) CGFloat overlayWidth;

@property (nonatomic) CGFloat prevTrackerTime;

@property (nonatomic) CGFloat leftwidth;
@property (nonatomic) CGFloat rightwidht;


@end

@implementation ICGVideoTrimmerView

#pragma mark - Initiation

- (instancetype)initWithFrame:(CGRect)frame
{
    NSAssert(NO, nil);
    return nil;
}

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder
{
    _themeColor = [UIColor lightGrayColor];
    return [super initWithCoder:aDecoder];
}

- (instancetype)initWithAsset:(AVAsset *)asset
{
    return [self initWithFrame:CGRectZero asset:asset];
}

- (instancetype)initWithFrame:(CGRect)frame asset:(AVAsset *)asset
{
    self = [super initWithFrame:frame];
    if (self) {
        _asset = asset;
        [self resetSubviews];
    }
    return self;
}

- (void)setThemeColor:(UIColor *)themeColor {
    _themeColor = themeColor;
    
    [self.bottomBorder setBackgroundColor:_themeColor];
    [self.topBorder setBackgroundColor:_themeColor];
    self.leftThumbView.color = _themeColor;
    self.rightThumbView.color = _themeColor;
    
    self.leftThumbViewNew.color = _themeColor;
    self.rightThumbViewNew.color = _themeColor;
}


#pragma mark - Private methods

//- (UIColor *)themeColor
//{
//    return _themeColor ?: [UIColor lightGrayColor];
//}

- (CGFloat)maxLength
{
    return _maxLength ?: 1000;
}

- (CGFloat)minLength
{
    return _minLength ?: 0;
}

- (UIColor *)trackerColor
{
    return _trackerColor ?: [UIColor clearColor];
}

- (CGFloat)borderWidth
{
    return _borderWidth ?: 1;
}

- (CGFloat)thumbWidth
{
        return _thumbWidth ?: 15;

}

- (NSInteger) rulerLabelInterval
{
    return _rulerLabelInterval ?: 5;
}

#define EDGE_EXTENSION_FOR_THUMB 30
- (void)resetSubviews
{
    CALayer *sideMaskingLayer = [CALayer new];
    sideMaskingLayer.backgroundColor = [UIColor blackColor].CGColor;
    sideMaskingLayer.frame = CGRectMake(0, -10, self.frame.size.width, self.frame.size.height + 20);
    self.layer.mask = sideMaskingLayer;
    
   


    
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
    [self addSubview:self.scrollView];
    [self.scrollView setDelegate:self];
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.scrollView.frame), CGRectGetHeight(self.scrollView.frame))];
    [self.scrollView setContentSize:self.contentView.frame.size];
    [self.scrollView addSubview:self.contentView];
    
    CGFloat ratio = self.showsRulerView ? 0.7 : 1.0;
    self.frameView = [[UIView alloc] initWithFrame:CGRectMake(self.thumbWidth, 0, CGRectGetWidth(self.contentView.frame)-(2*self.thumbWidth), CGRectGetHeight(self.contentView.frame)*ratio)];
    [self.frameView.layer setMasksToBounds:YES];
    [self.contentView addSubview:self.frameView];
    
    [self addFrames];
    
    
    
   
    
    // add borders
    self.topBorder = [[UIView alloc] init];
    [self.topBorder setBackgroundColor:self.themeColor];
    [self addSubview:self.topBorder];
    
    self.bottomBorder = [[UIView alloc] init];
    [self.bottomBorder setBackgroundColor:self.themeColor];
    [self addSubview:self.bottomBorder];
    
    // width for left and right overlay views
    _overlayWidth =  CGRectGetWidth(self.frame) - (self.minLength * self.widthPerSecond);
    
    
    if (_iscut) {
     self.leftOverlayView = [[HitTestView alloc] initWithFrame:CGRectMake(self.thumbWidth - self.overlayWidth+self.overlayWidth/2-50, 0, self.overlayWidth, CGRectGetHeight(self.frameView.frame))];
    }else{
    self.leftOverlayView = [[HitTestView alloc] initWithFrame:CGRectMake(self.thumbWidth - self.overlayWidth, 0, self.overlayWidth, CGRectGetHeight(self.frameView.frame))];
    }
    
  

    CGRect leftThumbFrame = CGRectMake(self.overlayWidth-self.thumbWidth, 0, self.thumbWidth, CGRectGetHeight(self.frameView.frame));

    
    
    
       self.leftOverlayView.hitTestEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -(EDGE_EXTENSION_FOR_THUMB));
    
    
       if (self.leftThumbImage) {
           self.leftThumbView = [[ICGThumbView alloc] initWithFrame:leftThumbFrame thumbImage:self.leftThumbImage];
       } else {
           self.leftThumbView = [[ICGThumbView alloc] initWithFrame:leftThumbFrame color:self.themeColor right:NO removethumb:_isremovethumb];
       }
       

       
       [self.leftThumbView.layer setMasksToBounds:YES];
       [self.leftOverlayView addSubview:self.leftThumbView];
       [self.leftOverlayView setUserInteractionEnabled:YES];
    
    if (_iscut || _isremovethumb) {
   
        [self.leftOverlayView setBackgroundColor:[UIColor clearColor]];
   
    }else{
     
        [self.leftOverlayView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.8]];

    }
       [self addSubview:self.leftOverlayView];
    
    
    
    
    self.trackerView = [[UIView alloc] initWithFrame:CGRectMake(self.thumbWidth, -5, 3, CGRectGetHeight(self.frameView.frame) + 10)];
      self.trackerView.backgroundColor = [UIColor whiteColor];
      self.trackerView.layer.masksToBounds = true;
      self.trackerView.layer.cornerRadius = 2;
      [self addSubview:self.trackerView];
    
    
    /////////////////////////////////new overlay//////////////////////////////////////////////////////////
    
    if (_iscut) {

      self.leftOverlayViewNew = [[HitTestView alloc] initWithFrame:CGRectMake(self.thumbWidth - self.overlayWidth, 0, self.overlayWidth, CGRectGetHeight(self.frameView.frame))];
      self.leftOverlayViewNew.hitTestEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -(EDGE_EXTENSION_FOR_THUMB));
      
      
      
      
            if (self.leftThumbImage) {
                self.leftThumbViewNew = [[ICGThumbView alloc] initWithFrame:leftThumbFrame thumbImage:self.leftThumbImage];
            } else {
                self.leftThumbViewNew = [[ICGThumbView alloc] initWithFrame:leftThumbFrame color:self.themeColor right:NO removethumb:_isremovethumb];
            }
            
            [self.leftOverlayViewNew setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.8]];
            [self.leftThumbViewNew.layer setMasksToBounds:YES];
            [self.leftOverlayViewNew addSubview:self.leftThumbViewNew];
            [self.leftOverlayViewNew setUserInteractionEnabled:YES];
            [self addSubview:self.leftOverlayViewNew];

    }
      
      
      /////////////////////////////////new overlay//////////////////////////////////////////////////////////
    
    

    
    // add right overlay view
    CGFloat rightViewFrameX = CGRectGetWidth(self.frameView.frame) < CGRectGetWidth(self.frame) ? CGRectGetMaxX(self.frameView.frame) : CGRectGetWidth(self.frame) - self.thumbWidth;
    
    
    
    
    
    if (_iscut) {
        self.rightOverlayView = [[HitTestView alloc] initWithFrame:CGRectMake(rightViewFrameX-rightViewFrameX/2+50, 0, self.overlayWidth, CGRectGetHeight(self.frameView.frame))];

    }else{
    self.rightOverlayView = [[HitTestView alloc] initWithFrame:CGRectMake(rightViewFrameX, 0, self.overlayWidth, CGRectGetHeight(self.frameView.frame))];
       }
    
    
    self.rightOverlayView.hitTestEdgeInsets = UIEdgeInsetsMake(0, -(EDGE_EXTENSION_FOR_THUMB), 0, 0);
    
    if (self.rightThumbImage) {
        self.rightThumbView = [[ICGThumbView alloc] initWithFrame:CGRectMake(0, 0, self.thumbWidth, CGRectGetHeight(self.frameView.frame)) thumbImage:self.rightThumbImage];
    } else {
        self.rightThumbView = [[ICGThumbView alloc] initWithFrame:CGRectMake(0, 0, self.thumbWidth, CGRectGetHeight(self.frameView.frame)) color:self.themeColor right:YES removethumb:_isremovethumb];
    }
    [self.rightThumbView.layer setMasksToBounds:YES];
    [self.rightOverlayView addSubview:self.rightThumbView];
    [self.rightOverlayView setUserInteractionEnabled:YES];
    
       if (_iscut || _isremovethumb) {
    
           [self.rightOverlayView setBackgroundColor:[UIColor clearColor]];
    
       }else{
        [self.rightOverlayView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.8]];

       }
    
    [self addSubview:self.rightOverlayView];
    
    _rightwidht = _rightOverlayView.frame.origin.x ;
    
    
    
    /////////////////////////////////new right overlay//////////////////////////////////////////////////////////
    
    if (_iscut) {


     _rightOverlayViewNew = [[HitTestView alloc] initWithFrame:CGRectMake(rightViewFrameX, 0, self.overlayWidth, CGRectGetHeight(self.frameView.frame))];
     
     self.rightOverlayViewNew.hitTestEdgeInsets = UIEdgeInsetsMake(0, -(EDGE_EXTENSION_FOR_THUMB), 0, 0);
     if (self.rightThumbImage) {
            self.rightThumbViewNew = [[ICGThumbView alloc] initWithFrame:CGRectMake(0, 0, self.thumbWidth, CGRectGetHeight(self.frameView.frame)) thumbImage:self.rightThumbImage];
        } else {
            self.rightThumbViewNew = [[ICGThumbView alloc] initWithFrame:CGRectMake(0, 0, self.thumbWidth, CGRectGetHeight(self.frameView.frame)) color:self.themeColor right:YES removethumb:_isremovethumb];
        }
        [self.rightThumbViewNew.layer setMasksToBounds:YES];
        [self.rightOverlayViewNew addSubview:self.rightThumbViewNew];
        [self.rightOverlayViewNew setUserInteractionEnabled:YES];
        [self.rightOverlayViewNew setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.8]];
        [self addSubview:self.rightOverlayViewNew];
        
    }
    
     ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    

    
    CGFloat newwidth = _leftOverlayView.frame.origin.x+_leftOverlayView.frame.size.width;
                   

                   _middlelayer = [[HitTestView alloc] initWithFrame:CGRectMake(_leftOverlayView.frame.origin.x+_leftOverlayView.frame.size.width,0,_rightOverlayView.frame.origin.x-newwidth, CGRectGetHeight(self.frameView.frame))];
                       [_middlelayer.layer setMasksToBounds:YES];
                          if (_iscut) {
                          [_middlelayer setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.8]];
                          }else{
                              [_middlelayer setBackgroundColor:[UIColor clearColor]];

                          }
                   [self addSubview:_middlelayer];
                   [self bringSubviewToFront:_rightOverlayView];
                   [self bringSubviewToFront:_leftOverlayView];
                   [self bringSubviewToFront:_rightOverlayViewNew];
                   [self bringSubviewToFront:_leftOverlayViewNew];
    
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveOverlayView:)];
    [self addGestureRecognizer:panGestureRecognizer];
    [self updateBorderFrames];
    [self notifyDelegateOfDidChange];

}

- (void)updateBorderFrames
{
    CGFloat height = self.borderWidth;
    [self.topBorder setFrame:CGRectMake(CGRectGetMaxX(self.leftOverlayView.frame), 0, CGRectGetMinX(self.rightOverlayView.frame)-CGRectGetMaxX(self.leftOverlayView.frame), height)];
    [self.bottomBorder setFrame:CGRectMake(CGRectGetMaxX(self.leftOverlayView.frame), CGRectGetHeight(self.frameView.frame)-height, CGRectGetMinX(self.rightOverlayView.frame)-CGRectGetMaxX(self.leftOverlayView.frame), height)];
}


- (void)moveOverlayView:(UIPanGestureRecognizer *)gesture
{
    
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
        {
            BOOL isRight =  [_rightOverlayView pointInside:[gesture locationInView:_rightOverlayView]];
            
            BOOL isLeft  =  [_leftOverlayView pointInside:[gesture locationInView:_leftOverlayView]];
            
            
            
            BOOL isRightnew =  [_rightOverlayViewNew pointInside:[gesture locationInView:_rightOverlayViewNew]];
            
            
            BOOL isLeftnew =  [_leftOverlayViewNew pointInside:[gesture locationInView:_leftOverlayViewNew]];


            
            if (isRight){
                self.rightStartPoint = [gesture locationInView:self];
                _isDraggingRightOverlayView = YES;
                _isDraggingLeftOverlayView = NO;
                _isDraggingLeftOverlayViewNew = NO;
                _isDraggingRightOverlayViewNew = NO;
            }
           if (isLeft){
                self.leftStartPoint = [gesture locationInView:self];
                _isDraggingRightOverlayView = NO;
                _isDraggingLeftOverlayViewNew = NO;
                _isDraggingRightOverlayViewNew = NO;
                _isDraggingLeftOverlayView = YES;
            } if (isRightnew){
                self.rightStartPointnew = [gesture locationInView:self];
                _isDraggingRightOverlayView = NO;
                _isDraggingLeftOverlayViewNew = NO;
                _isDraggingRightOverlayViewNew = YES;
                _isDraggingLeftOverlayView = NO;
            } if (isLeftnew){
                self.leftStartPointnew = [gesture locationInView:self];
                _isDraggingRightOverlayView = NO;
                _isDraggingLeftOverlayViewNew = YES;
                _isDraggingRightOverlayViewNew = NO;
                _isDraggingLeftOverlayView = NO;
            }
            
            
            
            
            
            
            
        }    break;
        case UIGestureRecognizerStateChanged:
        {
            CGPoint point = [gesture locationInView:self];
            //------------------------------------------------------------------------------------------------------------
            // Right
            if (_isDraggingRightOverlayView){
                
                CGFloat deltaX = point.x - self.rightStartPoint.x;
                
                CGPoint center = self.rightOverlayView.center;
                center.x += deltaX;
                CGFloat newRightViewMidX = center.x;
                CGFloat minX = CGRectGetMaxX(self.leftOverlayView.frame) + self.minLength * self.widthPerSecond;
                CGFloat maxX = CMTimeGetSeconds([self.asset duration]) <= self.maxLength + 0.5 ? CGRectGetMaxX(self.frameView.frame) : CGRectGetWidth(self.frame) - self.thumbWidth;
                if (newRightViewMidX - self.overlayWidth/2 < minX) {
                    newRightViewMidX = minX + self.overlayWidth/2;
                } else if (newRightViewMidX - self.overlayWidth/2 > maxX) {
                    newRightViewMidX = maxX + self.overlayWidth/2;
                }
                
                self.rightOverlayView.center = CGPointMake(newRightViewMidX, self.rightOverlayView.center.y);
                self.rightStartPoint = point;
                
                [_middlelayer removeFromSuperview];
                
                NSLog(@"right x value %f",
                      _rightOverlayView.frame.origin.x);
                
                CGFloat newwidth = _leftOverlayView.frame.origin.x+_leftOverlayView.frame.size.width;
                

                _middlelayer = [[HitTestView alloc] initWithFrame:CGRectMake(_middlelayer.frame.origin.x,0,_rightOverlayView.frame.origin.x-newwidth, CGRectGetHeight(self.frameView.frame))];
                    [_middlelayer.layer setMasksToBounds:YES];
                       if (_iscut) {
                       [_middlelayer setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.8]];
                       }else{
                           [_middlelayer setBackgroundColor:[UIColor clearColor]];

                       }
                [self addSubview:_middlelayer];
                [self bringSubviewToFront:_rightOverlayView];
                [self bringSubviewToFront:_leftOverlayView];
                [self bringSubviewToFront:_rightOverlayViewNew];
                [self bringSubviewToFront:_leftOverlayViewNew];
                
                [self updateBorderFrames];
                [self notifyDelegateOfDidChange];

            }
            else if (_isDraggingLeftOverlayView){
                
                //------------------------------------------------------------------------------------------------------------
                // Left
                CGFloat deltaX = point.x - self.leftStartPoint.x;


                
                CGPoint center = self.leftOverlayView.center;
                center.x += deltaX;
                CGFloat newLeftViewMidX = center.x;
                CGFloat maxWidth = CGRectGetMinX(self.rightOverlayView.frame) - (self.minLength * self.widthPerSecond);
                CGFloat newLeftViewMinX = newLeftViewMidX - self.overlayWidth/2;
                if (newLeftViewMinX < self.thumbWidth - self.overlayWidth) {
                    newLeftViewMidX = self.thumbWidth - self.overlayWidth + self.overlayWidth/2;
                } else if (newLeftViewMinX + self.overlayWidth > maxWidth) {
                    newLeftViewMidX = maxWidth - self.overlayWidth / 2;
                }
                
                self.leftOverlayView.center = CGPointMake(newLeftViewMidX, self.leftOverlayView.center.y);
                self.leftStartPoint = point;
                NSLog(@"width value %f",                _leftOverlayView.frame.origin.x+_leftOverlayView.frame.size.width);
            
                CGFloat newwidth = _leftOverlayView.frame.origin.x+_leftOverlayView.frame.size.width;
                
                [_middlelayer removeFromSuperview];
                
                _middlelayer = [[HitTestView alloc] initWithFrame:CGRectMake(_leftOverlayView.frame.origin.x+_leftOverlayView.frame.size.width,0,_rightOverlayView.frame.origin.x-newwidth, CGRectGetHeight(self.frameView.frame))];
                      [_middlelayer.layer setMasksToBounds:YES];
                         if (_iscut) {
                         [_middlelayer setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.8]];
                         }else{
                             [_middlelayer setBackgroundColor:[UIColor clearColor]];

                         }
                     [self addSubview:_middlelayer];
                    [self bringSubviewToFront:_rightOverlayView];
                    [self bringSubviewToFront:_leftOverlayView];
                    [self bringSubviewToFront:_rightOverlayViewNew];
                    [self bringSubviewToFront:_leftOverlayViewNew];
                
                    [self updateBorderFrames];
                    [self notifyDelegateOfDidChange];
                
            }    else if (_isDraggingLeftOverlayViewNew){
                CGFloat deltaX = point.x - self.leftStartPointnew.x;
                CGPoint center = self.leftOverlayViewNew.center;
                center.x += deltaX;
                CGFloat newLeftViewMidX = center.x;
                CGFloat maxWidth = CGRectGetMinX(self.rightOverlayViewNew.frame) - (self.minLength * self.widthPerSecond);
                CGFloat newLeftViewMinX = newLeftViewMidX - self.overlayWidth/2;
                if (newLeftViewMinX < self.thumbWidth - self.overlayWidth) {
                    newLeftViewMidX = self.thumbWidth - self.overlayWidth + self.overlayWidth/2;
                } else if (newLeftViewMinX + self.overlayWidth > maxWidth) {
                    newLeftViewMidX = maxWidth - self.overlayWidth / 2;
                }
                
                self.leftOverlayViewNew.center = CGPointMake(newLeftViewMidX, self.leftOverlayViewNew.center.y);
                self.leftStartPointnew = point;
                
                [self updateBorderFrames];
                [self notifyDelegateOfDidChange];
               
            }else if (_isDraggingRightOverlayViewNew){
                
                CGFloat deltaX = point.x - self.rightStartPointnew.x;
                
                CGPoint center = self.rightOverlayViewNew.center;
                center.x += deltaX;
                CGFloat newRightViewMidX = center.x;
                CGFloat minX = CGRectGetMaxX(self.leftOverlayViewNew.frame) + self.minLength * self.widthPerSecond;
                CGFloat maxX = CMTimeGetSeconds([self.asset duration]) <= self.maxLength + 0.5 ? CGRectGetMaxX(self.frameView.frame) : CGRectGetWidth(self.frame) - self.thumbWidth;
                if (newRightViewMidX - self.overlayWidth/2 < minX) {
                    newRightViewMidX = minX + self.overlayWidth/2;
                } else if (newRightViewMidX - self.overlayWidth/2 > maxX) {
                    newRightViewMidX = maxX + self.overlayWidth/2;
                }
                
                self.rightOverlayViewNew.center = CGPointMake(newRightViewMidX, self.rightOverlayViewNew.center.y);
                self.rightStartPointnew = point;
                
                [self updateBorderFrames];
                [self notifyDelegateOfDidChange];
            }
            //------------------------------------------------------------------------------------------------------------
            
            
            
            
            
            break;
        }
        case UIGestureRecognizerStateEnded:
        {
            [self notifyDelegateOfEndEditing];
        }
            
        default:
            break;
    }
}



- (void)seekToTime:(CGFloat) time
{
    CGFloat duration = fabs(_prevTrackerTime - time);
    BOOL animate = (duration>1) ?  NO : YES;
    _prevTrackerTime = time;
    
    
    CGFloat posToMove = time * self.widthPerSecond + self.thumbWidth - self.scrollView.contentOffset.x;
    
    
   AppDelegate *App  =  (AppDelegate*)[[UIApplication sharedApplication] delegate];

    App.posToMove = posToMove;
    
    

    CGRect trackerFrame = self.trackerView.frame;
    trackerFrame.origin.x = posToMove;
    if (animate){
        [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionBeginFromCurrentState animations:^{
            self.trackerView.frame = trackerFrame;
        } completion:nil ];
    }
    else{
        self.trackerView.frame = trackerFrame;
    }
    
}

- (void)hideTracker:(BOOL)flag
{
    if ( flag == YES ){
        self.trackerView.hidden = YES;
    }
    else{
        self.trackerView.alpha = 0;
        self.trackerView.hidden = NO;
        [UIView animateWithDuration:.3 animations:^{
            self.trackerView.alpha = 1;
        }];
    }
}


- (void)notifyDelegateOfDidChange
{
    NSLog(@"leftOverlayView:%f , rightOverlayView:%f contentOffset.x:%@", CGRectGetMaxX(self.leftOverlayView.frame) , CGRectGetMaxX(self.rightOverlayView.frame) , @(self.scrollView.contentOffset.x));
    
    NSLog(@"%f",self.widthPerSecond);
    CGFloat start = 0;
    CGFloat end = 0;
    
    if (_iscut) {

     start = CGRectGetMaxX(self.leftOverlayView.frame) / self.widthPerSecond + (self.scrollView.contentOffset.x - (self.thumbWidth*2)) / self.widthPerSecond;
     end = CGRectGetMinX(self.rightOverlayView.frame) / self.widthPerSecond + (self.scrollView.contentOffset.x) / self.widthPerSecond;
    
    }else{
        
         start = CGRectGetMaxX(self.leftOverlayView.frame) / self.widthPerSecond + (self.scrollView.contentOffset.x -self.thumbWidth) / self.widthPerSecond;
           end = CGRectGetMinX(self.rightOverlayView.frame) / self.widthPerSecond + (self.scrollView.contentOffset.x - self.thumbWidth) / self.widthPerSecond;
    }
    
    
    CGFloat startnew = CGRectGetMaxX(self.leftOverlayViewNew.frame) / self.widthPerSecond + (self.scrollView.contentOffset.x -self.thumbWidth) / self.widthPerSecond;
       CGFloat endnew = CGRectGetMinX(self.rightOverlayViewNew.frame) / self.widthPerSecond + (self.scrollView.contentOffset.x - self.thumbWidth) / self.widthPerSecond;
    
    
    NSLog(@"Left  value:%f",start);
    NSLog(@"Left new value%f",startnew);
    
    NSLog(@"Right  value:%f",end);
    NSLog(@"Right new value%f",endnew);
    
    
    if (!self.trackerView.hidden && start != self.startTime) {
        [self seekToTime:start];
    }
    
    
    if (start==self.startTime && end==self.endTime){
        // thumb events may fire multiple times with the same value, so we detect them and ignore them.
    }
    
    if (_iscut) {
    if (startnew==self.startMiddle && endnew==self.endMiddle){
    }
        self.startMiddle = startnew;
        self.endMiddle = endnew;
    }
    
    
    self.startTime = start;
    self.endTime = end;
   
    
    if([self.delegate respondsToSelector:@selector(trimmerView:didChangeLeftPosition:rightPosition:)])
    {
        
        if (_iscut) {
            [self.delegate trimmerView:self didChangeLeftPosition:self.startMiddle MiddleStart:self.startTime MIddleEnd:self.endTime rightPosition:self.endMiddle];
        }else{
            [self.delegate trimmerView:self didChangeLeftPosition:self.startTime rightPosition:self.endTime];
        }
        
        
   
    }
}

-(void) notifyDelegateOfEndEditing
{
    if([self.delegate respondsToSelector:@selector(trimmerViewDidEndEditing:)])
    {
        [self.delegate trimmerViewDidEndEditing:self];
    }
}

- (void)addFrames
{
    self.imageGenerator = [AVAssetImageGenerator assetImageGeneratorWithAsset:self.asset];
    self.imageGenerator.appliesPreferredTrackTransform = YES;
    if ([self isRetina]){
        self.imageGenerator.maximumSize = CGSizeMake(CGRectGetWidth(self.frameView.frame)*2, CGRectGetHeight(self.frameView.frame)*2);
    } else {
        self.imageGenerator.maximumSize = CGSizeMake(CGRectGetWidth(self.frameView.frame), CGRectGetHeight(self.frameView.frame));
    }
    
    CGFloat picWidth = 20;
    
    // First image
    NSError *error;
    CMTime actualTime;
    CGImageRef halfWayImage = [self.imageGenerator copyCGImageAtTime:kCMTimeZero actualTime:&actualTime error:&error];
    UIImage *videoScreen;
    if ([self isRetina]){
        videoScreen = [[UIImage alloc] initWithCGImage:halfWayImage scale:2.0 orientation:UIImageOrientationUp];
    } else {
        videoScreen = [[UIImage alloc] initWithCGImage:halfWayImage];
    }
    if (halfWayImage != NULL) {
        UIImageView *tmp = [[UIImageView alloc] initWithImage:videoScreen];
        CGRect rect = tmp.frame;
        rect.size.width = videoScreen.size.width;
        tmp.frame = rect;
        [self.frameView addSubview:tmp];
        picWidth = tmp.frame.size.width;
        CGImageRelease(halfWayImage);
    }
    
    Float64 duration = CMTimeGetSeconds([self.asset duration]);
    CGFloat screenWidth = CGRectGetWidth(self.frame) - 2*self.thumbWidth; // quick fix to make up for the width of thumb views
    NSInteger actualFramesNeeded;
    
    CGFloat factor = (duration / _maxLength);
    factor = (factor < 1 ? 1 : factor);
    
    
    CGFloat frameViewFrameWidth = factor * screenWidth;
    
    
    [self.frameView setFrame:CGRectMake(self.thumbWidth, 0, frameViewFrameWidth, CGRectGetHeight(self.frameView.frame))];
    CGFloat contentViewFrameWidth = CMTimeGetSeconds([self.asset duration]) <= self.maxLength + 0.5 ? self.bounds.size.width : frameViewFrameWidth + 2*self.thumbWidth;
    [self.contentView setFrame:CGRectMake(0, 0, contentViewFrameWidth, CGRectGetHeight(self.contentView.frame))];
    [self.scrollView setContentSize:self.contentView.frame.size];
    NSInteger minFramesNeeded = screenWidth / picWidth + 1;
    actualFramesNeeded =  factor * minFramesNeeded + 1;
    
    Float64 durationPerFrame = duration / (actualFramesNeeded*1.0);
    self.widthPerSecond = frameViewFrameWidth / duration;
    
    int preferredWidth = 0;
    NSMutableArray *times = [[NSMutableArray alloc] init];
    for (int i=1; i<actualFramesNeeded; i++){
        
        CMTime time = CMTimeMakeWithSeconds(i*durationPerFrame, 600);
        [times addObject:[NSValue valueWithCMTime:time]];
        
        UIImageView *tmp = [[UIImageView alloc] initWithImage:videoScreen];
        tmp.tag = i;
        
        CGRect currentFrame = tmp.frame;
        currentFrame.origin.x = i*picWidth;
        
        currentFrame.size.width = picWidth;
        preferredWidth += currentFrame.size.width;
        
        if( i == actualFramesNeeded-1){
            currentFrame.size.width-=6;
        }
        tmp.frame = currentFrame;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.frameView addSubview:tmp];
        });
        
        
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (int i=1; i<=[times count]; i++) {
            CMTime time = [((NSValue *)[times objectAtIndex:i-1]) CMTimeValue];
            
            CGImageRef halfWayImage = [self.imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
            
            UIImage *videoScreen;
            if ([self isRetina]){
                videoScreen = [[UIImage alloc] initWithCGImage:halfWayImage scale:2.0 orientation:UIImageOrientationUp];
            } else {
                videoScreen = [[UIImage alloc] initWithCGImage:halfWayImage];
            }
            
            CGImageRelease(halfWayImage);
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImageView *imageView = (UIImageView *)[self.frameView viewWithTag:i];
                [imageView setImage:videoScreen];
                
            });
        }
    });
}

- (BOOL)isRetina
{
    return ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
            ([UIScreen mainScreen].scale > 1.0));
}




@end
