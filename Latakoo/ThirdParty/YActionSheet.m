//
//  YSortActionSheet.m
//
//  Created by yuvraj on 12/01/15.
//  Copyright (c) 2015 yuvrajsinh. All rights reserved.
//

#import "YActionSheet.h"
#import "Latakoo-Swift.h"

#define CELL_HEIGHT 45.0
#define TITLE_VIEW_HEIGHT 44.0
#define DISMISS_BUTTON_HEIGHT 60.0
#define kYActionTitleFontSize 17.0f
#define kYActionTitleColor [UIColor blackColor]
#define kYActionButtonTitleColor [UIColor darkGrayColor]
#define kYOptionTextColor [UIColor colorWithRed:68.0/255.0 green:68.0/255.0 blue:68.0/255.0 alpha:1.0]
#define kYActionSeparatorColor [UIColor clearColor]
#define kYActionDoneButtonColor [UIColor colorWithRed:0.0/255.0 green:153.0/255.0 blue:255.0/255.0 alpha:1.0]

#define IS_IPHONEE [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone


@implementation YActionSheet{
    NSString *title;
    NSString *dismissButtonTitle;
    NSArray *otherTitles;
    UITableView *tableViewOptions;
    UIButton *btnDismiss;
    BOOL dismissOnSelect;
    BOOL selectmultiple;
    NSMutableArray * myArray;
    NSMutableArray * myArrayName;


}
@synthesize selectedIndex = _selectedIndex ;



- (id)initWithTitle:(NSString *)titleText dismissButtonTitle:(NSString *)dismissTitle otherButtonTitles:(NSArray *)buttonTitles dismissOnSelect:(BOOL)dismiss ismenifest:(BOOL)pilot{
    if (self = [super init]){
        myArray =  [[NSMutableArray alloc] init];
        myArrayName =  [[NSMutableArray alloc] init];

        selectmultiple = pilot;
       
        _isfrompilot = false;
        title = [NSString stringWithString:titleText];
        dismissButtonTitle = [NSString stringWithString:dismissTitle];
        otherTitles = [NSArray arrayWithArray:buttonTitles];
        dismissOnSelect = dismiss;
        _selectedIndex = 0;
        self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        self.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        // Setup Dissmiss button
        [self setupDismissView];
        
        // Setup TableView
        [self setupTableView];
    }
    return self;
}


- (id)initWithTitle1:(NSString *)titleText dismissButtonTitle:(NSString *)dismissTitle otherButtonTitles:(NSArray *)buttonTitles dismissOnSelect:(BOOL)dismiss ispilot:(BOOL)pilot{
    if (self = [super init]){
        
        _isfrompilot = true;
        _isfrommenifest = pilot;

        title = [NSString stringWithString:titleText];
        dismissButtonTitle = [NSString stringWithString:dismissTitle];
        otherTitles = [NSArray arrayWithArray:buttonTitles];
        dismissOnSelect = dismiss;
        _selectedIndex = 0;
        self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        self.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        // Setup Dissmiss button
      //  [self setupDismissView];
        
        // Setup TableView
        [self setupTableView];
    }
    return self;
}

#pragma mark - View Setup Methods
- (void)setupDismissView{
    
    btnDismiss = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDismiss.backgroundColor = [UIColor clearColor];
    btnDismiss.frame = self.frame;
    btnDismiss.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [btnDismiss addTarget:self action:@selector(btnCancelActionSheetClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnDismiss];
}

- (void)setupTableView{
    // Add TableView to show options
    tableViewOptions = [[UITableView alloc] initWithFrame:self.frame];
    
     AppDelegate *app =(AppDelegate*)[[UIApplication sharedApplication]delegate];
      if (app.isdarmode) {
      tableViewOptions.backgroundColor = [UIColor colorWithRed:27.0/255.0 green:27.0/255.0 blue:27.0/255.0 alpha:1.0];
      }else{
      tableViewOptions.backgroundColor = [UIColor whiteColor];
      }
    
        
    tableViewOptions.dataSource = self;
    tableViewOptions.delegate = self;
    tableViewOptions.bounces = NO;
    tableViewOptions.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableViewOptions.showsVerticalScrollIndicator = NO;
    tableViewOptions.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    [self addSubview:tableViewOptions];
}

#pragma mark - Show/Hide ActionSheet
- (void)showInViewController:(UIViewController *)inController withYActionSheetBlock:(YActionBlock)handler{
    
    if (_isfrompilot) {
    
    blockHandler = handler;
    CGRect frame = [UIApplication sharedApplication].keyWindow.frame;
    self.frame = frame;
    btnDismiss.frame = self.frame;
    CGFloat optionsHeight = ((otherTitles.count>4) ? 4 : otherTitles.count) * CELL_HEIGHT;
    CGFloat extraHeight = (otherTitles.count>4) ? 500 : 0.0;
    CGFloat totalHeight = TITLE_VIEW_HEIGHT  + optionsHeight + extraHeight+65;
    CGRect tableFrame = CGRectMake(10.0, frame.size.height, frame.size.width-20, totalHeight);
    tableViewOptions.frame = tableFrame;
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
        CGRect newFrame = tableViewOptions.frame;
        newFrame.origin.y = frame.size.height-totalHeight-10;
        tableViewOptions.frame = newFrame;
        tableViewOptions.layer.cornerRadius = 10;
        tableViewOptions.layer.masksToBounds = YES;
    } completion:^(BOOL finished) {
        
    }];
   
    tableViewOptions.scrollEnabled = NO;
    [tableViewOptions reloadData];
        
    
    }else{
    
    blockHandler = handler;
    CGRect frame = [UIApplication sharedApplication].keyWindow.frame;
    self.frame = frame;
    btnDismiss.frame = self.frame;
    CGFloat optionsHeight = ((otherTitles.count>4) ? 4 : otherTitles.count) * CELL_HEIGHT;
    CGFloat extraHeight = (otherTitles.count>4) ? 20.0 : 0.0;
    CGFloat totalHeight = TITLE_VIEW_HEIGHT  + optionsHeight + extraHeight;
        
        int h = 50;
        if (otherTitles.count==2) {
            h = 0;
        }
        
    CGRect tableFrame = CGRectMake(10.0, frame.size.height, frame.size.width-20, totalHeight+h);
    tableViewOptions.frame = tableFrame;
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
        
        CGRect newFrame = self->tableViewOptions.frame;
        
        
        if (self->otherTitles.count==2) {
             newFrame.origin.y = frame.size.height-totalHeight-115;

               }else{
             newFrame.origin.y = frame.size.height-totalHeight-165;

               }
        
        
        
        tableViewOptions.frame = newFrame;
        tableViewOptions.layer.cornerRadius = 10;
        tableViewOptions.layer.masksToBounds = YES;
    } completion:^(BOOL finished) {
        
    }];
    
    CGRect newFrame1 = tableViewOptions.frame;
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    AppDelegate *app =(AppDelegate*)[[UIApplication sharedApplication]delegate];
          if (app.isdarmode) {
              btnDone.backgroundColor = [UIColor colorWithRed:27.0/255.0 green:27.0/255.0 blue:27.0/255.0 alpha:1.0];
              [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];


          }else{
              btnDone.backgroundColor = [UIColor whiteColor];
              [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
          }
    newFrame1.origin.y = frame.size.height-100;
    newFrame1.size.height = 60;
    btnDone.frame =newFrame1;
    btnDone.layer.cornerRadius = 10;
    btnDone.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20.0];
    [btnDone setTitle:dismissButtonTitle forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(btnDismissActionSheetClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnDone];
    [tableViewOptions reloadData];
    }
}

- (void)dissmissActionSheet{
    
    [UIView animateWithDuration:0.3 animations:^{
    self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    CGRect newFrame = tableViewOptions.frame;
    newFrame.origin.y = self.frame.size.height;
    tableViewOptions.frame = newFrame;
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - Button Clicks
- (void)btnCancelActionSheetClick:(id)sender{
    if (blockHandler) {
        blockHandler(self.selectedIndex, YES);
    }
    
    [self dissmissActionSheet];
}

- (void)btnDismissActionSheetClick:(id)sender{
    
    if (blockHandler) {
        blockHandler(-1, YES);
    }
    
    [self dissmissActionSheet];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_isfrompilot) {
        return 100;

    }else{
        return 45;

    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (_isfrompilot) {
        return 0;
    }else{
    return TITLE_VIEW_HEIGHT;
    }
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *mainview = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, TITLE_VIEW_HEIGHT)];

    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, TITLE_VIEW_HEIGHT)];
    
   AppDelegate *app =(AppDelegate*)[[UIApplication sharedApplication]delegate];

    if (app.isdarmode) {
       lblTitle.backgroundColor = [UIColor colorWithRed:27.0/255.0 green:27.0/255.0 blue:27.0/255.0 alpha:1.0];
        lblTitle.textColor = [UIColor whiteColor];
    }else{
        lblTitle.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0];
        lblTitle.textColor = kYActionTitleColor;
    }
    
    lblTitle.font = [UIFont fontWithName:@"HelveticaNeue" size:kYActionTitleFontSize];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    lblTitle.text = title;
    
    [mainview addSubview:lblTitle];

    UIView *viewSeperator = [[UIView alloc] initWithFrame:CGRectMake(0.0, lblTitle.frame.size.height-1, lblTitle.frame.size.width, 1.0)];
      viewSeperator.backgroundColor = kYActionSeparatorColor;
      viewSeperator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
      [mainview addSubview:viewSeperator];
    
    if (selectmultiple) {
    
        
        UIButton *but= [UIButton buttonWithType:UIButtonTypeRoundedRect];
           [but addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
               

            [but setFrame:CGRectMake(self.frame.size.width - 80 , 1, 50, TITLE_VIEW_HEIGHT- 2)];
           [but setTitle:@"Done" forState:UIControlStateNormal];
           if (@available(iOS 13.0, *)) {
               [but setTitleColor:[UIColor labelColor] forState:UIControlStateNormal];
           } else {
               [but setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
           }
           
           [mainview addSubview:but];
           [mainview bringSubviewToFront:but];
           }
    
    

    return mainview;
}

-(void) buttonClicked:(UIButton*)sender
 {
     AppDelegate *app =(AppDelegate*)[[UIApplication sharedApplication]delegate];
     app.arrrequestids = myArray;
     app.arrrnames = myArrayName;

     
     
    NSLog(@"%@",myArrayName);

     
     
     if(myArray.count>0){
         blockHandler(self.selectedIndex, NO);
         [self dissmissActionSheet];

     }else{
         blockHandler(self.selectedIndex, YES);
         [self dissmissActionSheet];
     }
     
 }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return otherTitles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellId"];
    
    
    if (_isfrompilot) {

       
        
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellId"];;
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 30, 48, 48)]; // your cell's height should be greater than 48 for this.
            imgView.tag = 1;
            [cell.contentView addSubview:imgView];
            imgView = nil;
        
        
             
         UIImageView *imgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width-60, 35, 30, 30)]; // your cell's height should be greater than 48 for this.
                   imgView1.tag = 123456;
                   [cell.contentView addSubview:imgView1];
                   imgView1 = nil;
        
           UIImageView *_imgView1 = (UIImageView *)[cell.contentView viewWithTag:123456];
                _imgView1.image = [UIImage imageNamed:@"checkpilot.png"];
        
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(75, 35, 200, 20)];
            lbl.backgroundColor = [UIColor clearColor];
            [lbl setTag:2];
           if (IS_IPHONEE){
           lbl.font = [UIFont systemFontOfSize:16];
           }

            [cell.contentView addSubview:lbl];
            lbl = nil;
        
  ///////////////////////////////////////////////asignment label /////////////////////////////////////////////////////////////////////////////////////////////
        
        UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(75, 55, 200, 20)];
        lbl1.backgroundColor = [UIColor clearColor];
        [lbl1 setTag:786];
        lbl1.textColor = [UIColor lightGrayColor];
        lbl1.font = [UIFont systemFontOfSize:12];
        [cell.contentView addSubview:lbl1];
        
        lbl1 = nil;

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
     NSArray  * imgArray = [NSArray arrayWithObjects: @"latakoo123.png", @"menifest123.png", nil];

          UIImageView *_imgView = (UIImageView *)[cell.contentView viewWithTag:1];
             _imgView.image = [UIImage imageNamed:[imgArray objectAtIndex:indexPath.row]];

             UILabel *_lbl = (UILabel *)[cell.contentView viewWithTag:2];
             _lbl.text =  [otherTitles objectAtIndex:indexPath.row]; //
        

          UILabel *_lbl1 = (UILabel *)[cell.contentView viewWithTag:786];
        
        
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"labelcount"] isEqual:@"0"]) {
            
                         _lbl1.text =  @"";
               
                     }else{
                        // _lbl1.text =  [NSString stringWithFormat:@"%@ New Assignment",[[NSUserDefaults standardUserDefaults]valueForKey:@"labelcount"]];
                         _lbl1.text =  @"";

                }
        
        
        UIView *viewSeperator = [cell.contentView viewWithTag:1111];
           if (!viewSeperator) {
               viewSeperator = [[UIView alloc] initWithFrame:CGRectMake(0.0, 44.0, self.frame.size.width, 1.0)];
               viewSeperator.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0];
               viewSeperator.tag = 1111;
               viewSeperator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
               [cell.contentView addSubview:viewSeperator];
           }
        
        if (indexPath.row == 0) {
            viewSeperator.hidden = NO;
            _lbl1.hidden = YES;
            if (_isfrommenifest) {
                _imgView1.hidden = NO;
            }else{
                _imgView1.hidden = YES;
            }
            
        }else{
            
         viewSeperator.hidden = YES;
        _lbl1.hidden = NO;
          if (_isfrommenifest) {
          _imgView1.hidden = YES;
            }else{
            _imgView1.hidden = NO;
            }

        }
        
    }

     
  else{

    UIView *viewSeperator = [cell.contentView viewWithTag:1111];
    if (!viewSeperator) {
        viewSeperator = [[UIView alloc] initWithFrame:CGRectMake(0.0, 44.0, self.frame.size.width, 1.0)];
        viewSeperator.backgroundColor = [UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1.0];
        viewSeperator.tag = 1111;
        viewSeperator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
        [cell.contentView addSubview:viewSeperator];
    }
    
    UIImageView *imgView = (UIImageView *)[cell.contentView viewWithTag:1234];
    if (!imgView) {
        imgView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - 60, 11.0, 30, 30)];
        imgView.backgroundColor = [UIColor clearColor];
        imgView.tag = 1234;
        [cell.contentView addSubview:imgView];
    }
    
    // Check for selected/ non-selected row
      AppDelegate *app =(AppDelegate*)[[UIApplication sharedApplication]delegate];

      
      if (selectmultiple == false) {
     
      
      }else{
          if ([myArray containsObject:[app.arrrequest objectAtIndex:indexPath.row]]) {
              [imgView setImage:[UIImage imageNamed:@"CheckMark"]];
          }else{
              [imgView setImage:[UIImage imageNamed:@"uncheck"]];
          }
      }
      
      

 
    
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    

    if (app.isdarmode) {
        cell.textLabel.textColor = [UIColor whiteColor];
    }else{
        cell.textLabel.textColor = kYOptionTextColor;
    }
      
      
      if (selectmultiple) {
          cell.textLabel.textAlignment = NSTextAlignmentLeft;
      }else{
          cell.textLabel.textAlignment = NSTextAlignmentCenter;
      }
     
      
      
    cell.textLabel.text = [otherTitles objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
      if (app.isdarmode) {
          imgView.tintColor = [UIColor whiteColor];
      }else{
          imgView.tintColor =  [UIColor colorWithRed:70.0/255 green:16.0/255 blue:83.0/255 alpha:1.0];
      }
    
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedIndex = indexPath.row;
   
    AppDelegate *app =(AppDelegate*)[[UIApplication sharedApplication]delegate];

    
    if (selectmultiple) {
        
        if (![myArray containsObject:[app.arrrequest objectAtIndex:indexPath.row]]) {
            NSMutableArray *Arr=[myArray mutableCopy];
             [Arr addObject:[app.arrrequest objectAtIndex:indexPath.row]];
             myArray=Arr;
            
            NSMutableArray *ArrNew=[myArrayName mutableCopy];
            [ArrNew addObject:[otherTitles objectAtIndex:indexPath.row]];
            myArrayName=ArrNew;
            
            
            
            
          }else{
              NSMutableArray *Arr=[myArray mutableCopy];
              [Arr removeObject:[app.arrrequest objectAtIndex:indexPath.row]];
              myArray=Arr;
              
              NSMutableArray *Arrnew=[myArrayName mutableCopy];
                           [Arrnew removeObject:[otherTitles objectAtIndex:indexPath.row]];
                           myArrayName=Arrnew;
          }
          [tableView reloadData];
        
        
        
    }else{
        
            if(dismissOnSelect==YES)
            {
                if (blockHandler) {
                    blockHandler(self.selectedIndex, NO);
                }
        
                [self dissmissActionSheet];
            }
    }

    

}




@end

