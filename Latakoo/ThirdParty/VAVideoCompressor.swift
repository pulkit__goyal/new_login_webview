//
//  VAVideoCompressor.swift
//  VAVideoCompressor
//
//  Created by Anton Vodolazkyi on 29.07.2018.
//  Copyright © 2018 Anton Vodolazkyi. All rights reserved.
//

import AVFoundation


public enum VAVideoConverterError: Error {
    case emptyTracks
    case fileAlreadyExist
    case failed
}

