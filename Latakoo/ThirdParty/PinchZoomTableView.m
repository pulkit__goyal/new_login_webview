
// By Bharat Parashar Don't try to  copy write your own code   ////


#import "PinchZoomTableView.h"
#import <QuartzCore/QuartzCore.h>
#define IS_IPHONEE [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone

@implementation PinchZoomTableView
@synthesize originalFrame;
@synthesize screenWidth;
@synthesize screenHeight;
@synthesize deltaY;

-(void)awakeFromNib {
    [super awakeFromNib];
    
	[self addGesture];
	CGRect wh =  [[UIScreen mainScreen]bounds];
	screenWidth = wh.size.width;
	screenHeight = wh.size.height;
    NSLog(@"table view x == %f",self.frame.origin.x);
    NSLog(@"table view y == %f",self.frame.origin.y);
    NSLog(@"table view width == %f",self.frame.size.width);
    NSLog(@"table view height == %f",self.frame.size.height);
    tableframe = self.bounds;
	if(screenWidth > 1000)
         {
		deltaY = 120;
         }
	else {
		deltaY = 50;
	     }
    [[NSNotificationCenter defaultCenter] addObserver:self
    selector:@selector(receiveTestNotification:)
    name:@"reloadtableframe"
    object:nil];
}


- (void) receiveTestNotification:(NSNotification *) notification
{
    [self resetTable];
}
	
- (void)addGesture {
	self.clipsToBounds = YES;
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scalePiece:)];
    [pinchGesture setDelegate:self];
    [self addGestureRecognizer:pinchGesture];
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
      [panRecognizer setMinimumNumberOfTouches:1];
      [panRecognizer setMaximumNumberOfTouches:1];
      [self addGestureRecognizer:panRecognizer];
}


-(void)move:(UIPanGestureRecognizer*)sender {
    
    
    [self bringSubviewToFront:sender.view];
    CGPoint translatedPoint = [sender translationInView:sender.view.superview];
    CGPoint velocity = [sender velocityInView:self];
    int flag = 0;
    if(velocity.x > 0)
    {
           NSLog(@"gesture moving right");
           flag = 0;
    }
    else
    {
           NSLog(@"gesture moving left");
           flag = 1;
        
    }
    
    
    NSLog(@"table y position is => %f",self.frame.origin.y);
    NSLog(@"valoctiy y position is => %f",velocity.y);
    if (self.frame.origin.y>40) {
        return;
    }else{
        NSLog(@"valoctiy y position is => %f",self.frame.origin.y);

    }
    
    if (sender.state == UIGestureRecognizerStateBegan) {
  
    }
    
    CGRect wh =  [[UIScreen mainScreen]bounds];
    
    if (self.frame.size.width>wh.size.width+1) {
    if (self.frame.origin.x>1 && flag == 0){
                                  return;
                                           }
        
    translatedPoint = CGPointMake(sender.view.center.x+translatedPoint.x,     sender.view.center.y+translatedPoint.y);
    [sender.view setCenter:translatedPoint];
    [sender setTranslation:CGPointZero inView:sender.view];
    if (sender.state == UIGestureRecognizerStateEnded) {
        CGFloat velocityX = (0.2*[sender velocityInView:self].x);
        CGFloat velocityY = (0.2*[sender velocityInView:self].y);
        CGFloat finalX = translatedPoint.x + velocityX;
        CGFloat finalY = translatedPoint.y + velocityY;
        // translatedPoint.y + (.35*[(UIPanGestureRecognizer*)sender velocityInView:self.view].y);
        if (finalX < 0) {
            finalX = 0;
        } else if (finalX > self.frame.size.width) {
            finalX = self.frame.size.width;
        }
        if (finalY < 72) { // to avoid status bar
            finalY = 72;
        } else if (finalY > self.frame.size.height) {
            finalY = self.frame.size.height;
        }
        
        CGFloat animationDuration = (ABS(velocityX)*.0002)+.2;
        NSLog(@"the duration is: %f", animationDuration);
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:animationDuration];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        [[sender view] setCenter:CGPointMake(finalX, finalY)];
        [UIView commitAnimations];
       }
     }
}

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    
	if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
		UIView *piece = gestureRecognizer.view;
		originalFrame.size.height = self.frame.size.height;
		self.originalFrame = self.frame;
        CGPoint locationInView = [gestureRecognizer locationInView:nil];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];		
		UIInterfaceOrientation orientaion = [[UIDevice currentDevice]orientation];
		if( orientaion != UIInterfaceOrientationPortrait  && orientaion != UIInterfaceOrientationPortraitUpsideDown &&orientaion != UIInterfaceOrientationLandscapeLeft && orientaion != UIInterfaceOrientationLandscapeRight) {
			orientaion = [[UIApplication sharedApplication] statusBarOrientation];
		}
		if( orientaion == UIInterfaceOrientationPortrait ) {
		}
		else if ( orientaion == UIInterfaceOrientationPortraitUpsideDown ) {
			locationInView.x = screenWidth - locationInView.x;
			locationInView.y = (screenHeight -20) - locationInView.y;
		}
		else if ( orientaion == UIInterfaceOrientationLandscapeLeft ) {
			float x =  screenHeight - locationInView.y;
			float y = locationInView.x;
			locationInView.x = x;
			locationInView.y = y;
		}
		else if ( orientaion == UIInterfaceOrientationLandscapeRight ) {
			float y =  locationInView.y;
			float x = screenWidth - locationInView.x;
			locationInView.x = y;
			locationInView.y = x;
		}		
		//piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, (locationInView.y -deltaY) / piece.bounds.size.height);
        piece.center = self.window.center;
    }
	else if(gestureRecognizer.state == UIGestureRecognizerStateEnded) {
       // [self resetTable];
	}
}

- (void)resetTable {
	[UIView beginAnimations:nil context:nil];
	[self setTransform:CGAffineTransformIdentity];	
    self.frame = [[UIScreen mainScreen] bounds];
     
  
    if (IS_IPHONEE) {
        self.frame = CGRectMake(self.frame.origin.x, 72, self.frame.size.width, self.frame.size.height);
        [UIView commitAnimations];
   }else{
       self.frame = CGRectMake(self.frame.origin.x, 95, self.frame.size.width, self.frame.size.height);
       [UIView commitAnimations];
   }
}



//- (void)scalePiece:(UIPinchGestureRecognizer *)gestureRecognizer {
//    [self adjustAnchorPointForGestureRecognizer:gestureRecognizer];
//    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
//        [gestureRecognizer view].transform = CGAffineTransformScale([[gestureRecognizer view] transform], [gestureRecognizer scale], [gestureRecognizer scale]);
//        [gestureRecognizer setScale:1];
//        NSLog(@"%f",self.frame.origin.x);
//        if (self.frame.origin.x>0 ) {
//            [self resetTable];
//        }
//    }
//}



- (void)scalePiece:(UIPinchGestureRecognizer *)gestureRecognizer {
    [self adjustAnchorPointForGestureRecognizer:gestureRecognizer];
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        [gestureRecognizer view].transform = CGAffineTransformScale([[gestureRecognizer view] transform], [gestureRecognizer scale], [gestureRecognizer scale]);
        [gestureRecognizer setScale:1];
        NSLog(@"self.frame.origin.x: %f", self.frame.origin.x);
                
        if (self.frame.origin.x > 0) {
            [self resetTable];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"hidePullToRefresh" object:nil userInfo:nil];
        [self performSelector:@selector(changeValueOfIsZooming) withObject:nil afterDelay:2.0f];
    }
}


-(void)changeValueOfIsZooming {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showPullToRefresh" object:nil userInfo:nil];
    NSLog(@"Value of iszooming changed to NO");
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
        return YES;
}
 




@end



