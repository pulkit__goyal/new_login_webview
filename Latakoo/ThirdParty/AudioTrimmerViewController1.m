

#import "NSTimer+Blocks.h"
#import "AudioTrimmerViewController1.h"
#import "RETrimControl.h"
#import "AZSoundManager.h"
#import "Latakoo-Swift.h"
#define IS_IPHONE [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone


@interface AudioTrimmerViewController1 ()

{
    AppDelegate *App;
    RETrimControl *trimControl;
    int flagaudio;
    int count;
    NSMutableDictionary *Dict ;
    NSDictionary *dict;
}
@property (weak, nonatomic) IBOutlet UIView *viewtute;
@property (weak, nonatomic) IBOutlet UIImageView *imgtute;
@property (strong, nonatomic) NSString *strTrimmedAudioURL;
@property (strong, nonatomic) AVAssetExportSession *exportSession;
@property (assign, nonatomic) BOOL isPushAudioTrimmer;
@property (nonatomic, strong) AZSoundManager *manager;
@property (nonatomic, strong) AZSoundItem *item;
@property (weak, nonatomic) IBOutlet UISlider *playSlider;
@property (weak, nonatomic) IBOutlet UISlider *videoslider;
@property (weak, nonatomic) IBOutlet UILabel *videostart;
@property (weak, nonatomic) IBOutlet UILabel *videoend;
@property (weak, nonatomic) IBOutlet UILabel *remainingLabel;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UISlider *volumeSlider;
@property (weak, nonatomic) IBOutlet UISlider *videovolumeslider;
@end

@implementation AudioTrimmerViewController1
@synthesize strTrimmedAudioURL;

+ (void) presentAudioTrimmerController:(UIViewController *)controller completion:(CompleteAudioEditing)complete {
    AudioTrimmerViewController1 *audioEditorController = [[AudioTrimmerViewController1 alloc] initWithNibName:@"AudioTrimmerViewController1" bundle:nil];
    audioEditorController.completionBlock=complete;
    audioEditorController.isPushAudioTrimmer = false;
    audioEditorController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.5];
    [controller presentViewController:audioEditorController animated:true completion:nil];
}

+ (void) pushAudioTrimmerController:(UIViewController *)controller completion:(CompleteAudioEditing)complete {
    AudioTrimmerViewController1 *audioEditorController = [[AudioTrimmerViewController1 alloc] initWithNibName:@"AudioTrimmerViewController1" bundle:nil];
    audioEditorController.completionBlock=complete;
    audioEditorController.isPushAudioTrimmer = true;
    audioEditorController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.5];
    [controller.navigationController pushViewController:audioEditorController animated:true];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    count = 0;
    Dict = [[NSMutableDictionary alloc ] init];

    

    App  =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    dict = [App TrimThisAudio];
    Dict = [App.TrimThisAudio mutableCopy];
    audioTrimStartTime=0;
     if ([App.isitunes isEqualToString:@"lib"]){

        _volumeSlider.value = [[dict objectForKey:@"volumekey"] floatValue];
        _volumeSlider.maximumValue = 0.5;
  
    }else{
        
       _volumeSlider.value = [[dict objectForKey:@"volumekey"] floatValue];
       _volumeSlider.maximumValue = 1.0;
        
    }
   

    
    flagaudio = 0 ;
    
    
         [viewEditRecording setHidden:NO];
        [btnUpload setHidden:NO];
    
  
    
            NSString *str = [App.TrimThisAudio objectForKey:@"orginalfile"];
    
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *recDir = [paths objectAtIndex:0];
            NSString *audioFileName  = [NSString stringWithFormat:@"%@/%@" , recDir, str];
    
         strOriginalAudioURL =  audioFileName;
         strAudioURL = audioFileName;
    
        [Dict setObject:str forKey:@"orginalfile"];

    
    
    
    
 
    _videoslider.minimumValue = 0;
    _videoslider.value =  [[dict objectForKey:@"playtime"] intValue];
    _videostart.text = [self convertTime:[[dict objectForKey:@"playtime"] intValue]];
       _videoend.text = [self convertTime:[App.videoduration intValue]];
       _videoslider.maximumValue =[App.videoduration intValue];
       _videoslider.minimumValue = 0;
       _videoslider.value =  [[dict objectForKey:@"playtime"] intValue];
       _videostart.text = [self convertTime:[[dict objectForKey:@"playtime"] intValue]];
   
    
    
    NSString *filePath = strAudioURL;
    self.item = [AZSoundItem soundItemWithContentsOfFile:filePath];
    // Sound item properties
    self.remainingLabel.text = [NSString stringWithFormat:@"-%@", [self convertTime:self.item.duration]];
    // Sound manager
    self.manager = [AZSoundManager sharedManager];
    [self.manager preloadSoundItem:self.item];
    // Get sound item info
    [self.manager getItemInfoWithProgressBlock:^(AZSoundItem *item) {
        NSTimeInterval remainingTime = item.duration - item.currentTime;
        self.progressLabel.text = [self convertTime:item.currentTime];
        self.remainingLabel.text = [NSString stringWithFormat:@"-%@", [self convertTime:remainingTime]];
        self.playSlider.value = item.currentTime / item.duration;
    } finishBlock:^(AZSoundItem *item) {
        NSLog(@"finish playing");
        
        [self->btnPlayPause setSelected:false];
    }];
    
    

    
}



- (IBAction)tap:(id)sender {
    if (count==0) {
        _imgtute.image = [UIImage imageNamed:@"tuteedit1"];
        count = 1;
        
    }else if (count==1){
        
        _imgtute.image = [UIImage imageNamed:@"tutevideoslider"];
        count = 2;
        
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:@"tute" forKey:@"tute"];
        _viewtute.hidden = YES;
    }
    
}


- (NSString*)convertTime:(NSInteger)time
{
    NSInteger minutes = time / 60;
    NSInteger seconds = time % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}




- (IBAction)playSliderChanged:(id)sender
{
    AZSoundItem *item = self.manager.currentItem;
    NSInteger second = self.playSlider.value * item.duration;
    [self.manager playAtSecond:second];
}



#pragma mark - Remote Control

- (void)remoteControlReceivedWithEvent:(UIEvent *)event
{
    [self.manager remoteControlReceivedWithEvent:event];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



#pragma mark - Buttons
- (void) dismissAudioTrimmer:(NSString *)strTrimedFilePath {
    if (self.completionBlock) {
        if (strTrimedFilePath != nil) {
            NSLog(@"%@",Dict);
            App.TrimThisAudio = Dict;
            
            NSURL *fileUrl = [NSURL fileURLWithPath:strTrimedFilePath];
            self.completionBlock(true, fileUrl);
        }
        else {
            self.completionBlock(true, nil);
        }
    }

    if (self.isPushAudioTrimmer == true) {
        [self.navigationController popViewControllerAnimated:true];
    }
    else {
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

- (IBAction)btnCancelTapped:(id)sender {
    if (flagaudio == 1) {
        flagaudio=0;
        [btnPlayPause setSelected:FALSE];
        [self.manager pause];
    }
    
    [self.manager pause];
    strTrimmedAudioURL = nil;
    [self dismissAudioTrimmer:strTrimmedAudioURL];
}

- (IBAction)btnUploadTapped:(id)sender {
    
    [self.manager pause];

    
    
    if (flagaudio == 1) {
        flagaudio=0;
        [btnPlayPause setSelected:FALSE];
        [self.manager pause];
    }
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL]) {
        if(isAudioTrimmed) {
             [self dismissAudioTrimmer:strTrimmedAudioURL];
        }
        else {
            [self trimAudioWithCompletion:^{
                isAudioTrimmed = TRUE;
                [self hideUnhideControls:TRUE];
                [self dismissAudioTrimmer:strTrimmedAudioURL];
            }];
        }
    }
}



- (IBAction)btnPlayPauseTapped:(id)sender {
    
    
    if (flagaudio == 0 ) {
        flagaudio=1;
        [btnPlayPause setSelected:TRUE];
        [self trimtempaudio];
    }else{
        [btnPlayPause setSelected:FALSE];
        [self.manager pause];
    }
}


- (void)btnTrimFinalAudio {
    [self trimAudioWithCompletion:^{
        self->isAudioTrimmed = TRUE;
        [self hideUnhideControls:TRUE];
    }];
}

#pragma mark - Gesture




#pragma mark - Other Methods

- (NSString *)timeFormatted:(CGFloat)interval isWithMinutes:(BOOL)isWithMinutes {
    unsigned long milliseconds = interval * 1000;
    unsigned long seconds = milliseconds / 1000;
    milliseconds %= 1000;
    unsigned long minutes = seconds / 60;
    seconds %= 60;
//    unsigned long hours = minutes / 60;
//    minutes %= 60;
    NSString *strMillisec = @(milliseconds).stringValue;
    if (strMillisec.length > 2) {
        strMillisec = [strMillisec substringToIndex:2];
     }
    
    if(isWithMinutes) {
        return [NSString stringWithFormat:@"%02ld:%02ld",(long)minutes, (long)seconds];
     }
else {
    
    return [NSString stringWithFormat:@"%02ld:%02ld",(long)seconds, (long)[strMillisec integerValue]];

     }
}



- (void)trimAudioWithCompletion:(void(^)(void))completion {
    if(!strTrimmedAudioURL) {
        strTrimmedAudioURL = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"1%@",[strAudioURL lastPathComponent]]];
    }
    if([[NSFileManager defaultManager] fileExistsAtPath:strTrimmedAudioURL]) {
        [[NSFileManager defaultManager] removeItemAtPath:strTrimmedAudioURL error:nil];
    }
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:strAudioURL]];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:asset];
    if ([compatiblePresets containsObject:AVAssetExportPresetHighestQuality]) {
        self.exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetPassthrough];
        
        self.exportSession.outputURL = [NSURL fileURLWithPath:strTrimmedAudioURL];
        
        NSString *EXT  = [strTrimmedAudioURL pathExtension];
        
        
        if ([EXT isEqualToString:@"wav"]) {
            
            self.exportSession.outputFileType = AVFileTypeWAVE;

        }else{
      
            self.exportSession.outputFileType = AVFileTypeAppleM4A;

        
        }
       
      

        CMTime start = CMTimeMakeWithSeconds(audioTrimStartTime, asset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(audioTrimEndTime - audioTrimStartTime, asset.duration.timescale);
        CMTimeRange range = CMTimeRangeMake(start, duration);
        self.exportSession.timeRange = range;
        [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
            switch ([self.exportSession status]) {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"Export failed: %@", [[self.exportSession error] localizedDescription]);
                    break;
                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"Export cancelled");
                    break;
                case AVAssetExportSessionStatusCompleted: {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSError *error;

                        
                        [Dict setObject:strTrimmedAudioURL forKey:@"trimfile"];


                        audioTrimStartTime = 0;
                        audioTrimEndTime = audioDuration;
                        lblAudioStartTime.hidden=YES;
                        lblAudioEndTime.hidden=YES;
                        if(completion) {
                            completion();
                        }
                    });
                    break;
                }
                default:
                    NSLog(@"NONE");
                    break;
            }
        }];
    }
}


- (void)calculateAudioTrimTime {
    [lblAudioStartTime setText:[self timeFormatted:audioTrimStartTime isWithMinutes:FALSE]];
    [lblAudioEndTime setText:[self timeFormatted:audioTrimEndTime isWithMinutes:FALSE]];
    lblAudioStartTime.hidden=NO;
    lblAudioEndTime.hidden=NO;
  }

- (void)hideUnhideControls:(BOOL)isHidden {
    [lblAudioStartTime setHidden:isHidden];
    [lblAudioEndTime setHidden:isHidden];
}

- (void)resetAudioPlayer {
   
    int y = 0;
    int x = 0;
    int w = 0;
    if (IS_IPHONE) {
        y = 360;
        x = 20;
        w = 40;
        
    }else{
        y = 500;
        x = 30;
        w = 60;
        
    }
    
    
//    CGRect frame = CGRectMake(x,y,viewEditRecording.frame.size.width-w, 30);
//       UISlider *slider = [[UISlider alloc] initWithFrame:frame];
//       [slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
//       [slider setBackgroundColor:[UIColor clearColor]];
//       slider.minimumValue = 0.0;
//       slider.maximumValue = 50.0;
//       slider.continuous = YES;
//       slider.value = 25.0;
//    slider.tintColor = [UIColor clearColor];
//    [slider setThumbImage:[UIImage imageNamed:@"trimline"] forState:UIControlStateNormal];

    if ([[dict objectForKey:@"rightvalue"]isEqualToString:@""]) {
        
            audioDuration =  self.manager.currentItem.duration;
            audioTrimEndTime = audioDuration;
         trimControl = [[RETrimControl alloc] initWithFrame:CGRectMake(x,y,viewEditRecording.frame.size.width-w, 30)];
         trimControl.length = audioDuration;
        trimControl.delegate = self;
        [viewEditRecording addSubview:trimControl];
        [viewEditRecording bringSubviewToFront:trimControl];
       

        [lblAudioEndTime setText:[self timeFormatted:audioDuration isWithMinutes:FALSE]];
        [self calculateAudioTrimTime];
        

    }else{
        audioDuration =  self.manager.currentItem.duration;
        trimControl = [[RETrimControl alloc] initWithFrame:CGRectMake(x,y,viewEditRecording.frame.size.width-w, 30)];
        trimControl.length = audioDuration;
        trimControl.delegate = self;
        audioTrimEndTime = [[dict objectForKey:@"rightvalue"] floatValue];
        audioTrimStartTime  = [[dict objectForKey:@"leftvalue"] floatValue];
        [self calculateAudioTrimTime];
        float multiplyvalue = 100/audioDuration;
        float leftvalue = audioTrimStartTime*multiplyvalue;
        trimControl.leftValue = leftvalue;
        float rightvalue = audioTrimEndTime*multiplyvalue;
        if (rightvalue>100) {
            rightvalue= 100;
        }
        
        trimControl.rightValue = rightvalue;
        [viewEditRecording addSubview:trimControl];
        [viewEditRecording bringSubviewToFront:trimControl];

    }
    
  // [viewEditRecording addSubview:slider];


}

-(void)sliderAction:(id)sender
{
    UISlider *slider = (UISlider*)sender;
    float value = slider.value;
    //-- Do further actions
}


- (void)trimControl:(RETrimControl *)trimControl didChangeLeftValue:(CGFloat)leftValue rightValue:(CGFloat)rightValue
{
    audioTrimEndTime =  rightValue;
    
    audioTrimStartTime  = leftValue;
    [self calculateAudioTrimTime];
    NSString * strrightvalue = [NSString stringWithFormat:@"%f",audioTrimEndTime];
    NSString * strleftvalue = [NSString stringWithFormat:@"%f",audioTrimStartTime];
    [Dict setObject:strrightvalue forKey:@"rightvalue"];
    [Dict setObject:strleftvalue forKey:@"leftvalue"];
    NSLog(@"Left = %f, right = %f", leftValue, rightValue);
    [btnPlayPause setSelected:false];
    [self.manager pause];

    
}



- (IBAction)volume:(id)sender {
    UISlider *slider = (UISlider*)sender;
    NSLog(@"%f",slider.value);
    self.manager.volume = slider.value;
    if([App.isitunes isEqualToString:@"itunes"] ){
    }else if ([App.isitunes isEqualToString:@"lib"]){
        
    }else{
        
    }
    NSString * strvolume = [NSString stringWithFormat:@"%.1f",slider.value];
    [Dict setObject:strvolume forKey:@"volumekey"];
  //  App.appvolume = slider.value;
}

- (IBAction)video:(id)sender {
   UISlider *slider = (UISlider*)sender;
    NSString * strafter = [NSString stringWithFormat:@"%.0f",(slider.value)];

    _videostart.text = [self convertTime:(slider.value)];

    [Dict setObject:strafter forKey:@"playtime"];
}




- (IBAction)deleteaudio:(id)sender {
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Are you sure you want to delete this audio" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
    }];
    UIAlertAction *otherAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
       
        
        if (flagaudio == 1) {
            flagaudio=0;
            [btnPlayPause setSelected:FALSE];
            [self.manager pause];
        }
        
        [self.manager pause];
        strTrimmedAudioURL = nil;
        self.completionBlock(false, nil);
        
        
        
        if (self.isPushAudioTrimmer == true) {
            [self.navigationController popViewControllerAnimated:true];
        }
        else {
            [self dismissViewControllerAnimated:true completion:nil];
        }
        
        
        
    }];
    [alert addAction:okAction];
    [alert addAction:otherAction];
    [self presentViewController:alert animated:YES completion:nil];
    

   
}



-(void)trimtempaudio{
    
    
    NSString *strTrimmedAudioURL1 ;

        
        if(!strTrimmedAudioURL1) {
            strTrimmedAudioURL1 = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"trimmerRecording.%@",[strAudioURL pathExtension]]];
            
        }
        if([[NSFileManager defaultManager] fileExistsAtPath:strTrimmedAudioURL1]) {
            [[NSFileManager defaultManager] removeItemAtPath:strTrimmedAudioURL1 error:nil];
        }
        AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:strAudioURL]];
        NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:asset];
        if ([compatiblePresets containsObject:AVAssetExportPresetHighestQuality]) {
            self.exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetPassthrough];
            self.exportSession.outputURL = [NSURL fileURLWithPath:strTrimmedAudioURL1];
        //    self.exportSession.outputFileType = AVFileTypeWAVE;
            NSString *EXT  = [strTrimmedAudioURL1 pathExtension];
                  
                  
                  if ([EXT isEqualToString:@"wav"]) {
                      
                      self.exportSession.outputFileType = AVFileTypeWAVE;

                  }else{
                
                      self.exportSession.outputFileType = AVFileTypeAppleM4A;

                  
                  }
            
            
            CMTime start = CMTimeMakeWithSeconds(audioTrimStartTime, asset.duration.timescale);
            CMTime duration = CMTimeMakeWithSeconds(audioTrimEndTime - audioTrimStartTime, asset.duration.timescale);
            CMTimeRange range = CMTimeRangeMake(start, duration);
            self.exportSession.timeRange = range;
            
            [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
                switch ([self.exportSession status]) {
                    case AVAssetExportSessionStatusFailed:
                        NSLog(@"Export failed: %@", [[self.exportSession error] localizedDescription]);
                        break;
                    case AVAssetExportSessionStatusCancelled:
                        NSLog(@"Export cancelled");
                        break;
                    case AVAssetExportSessionStatusCompleted: {
                        dispatch_async(dispatch_get_main_queue(), ^{
        
                            
                            [NSThread sleepForTimeInterval:0.5];
                            [self Playaudioatpath:strTrimmedAudioURL1];
                            
                            
                        });
                        break;
                    }
                    default:
                        NSLog(@"NONE");
                        break;
                }
            }];
        }else{
            
            NSLog(@"audio file not supported");

        }
    
}


-(void)Playaudioatpath:(NSString*)audiopath{
    
    _item = [AZSoundItem soundItemWithContentsOfFile:audiopath];
    
    self.manager = [AZSoundManager sharedManager];
    [self.manager preloadSoundItem:self.item];
    [self.manager getItemInfoWithProgressBlock:^(AZSoundItem *item) {
      
    } finishBlock:^(AZSoundItem *item) {
        NSLog(@"finish playing");
        [btnPlayPause setSelected:false];

    }];
    
    
    [self performSelector:@selector(Playaudio) withObject:nil afterDelay:0.5];
    
  
    
}

-(void)Playaudio{
    
    [_manager play];
}

-(void)viewDidAppear:(BOOL)animated {
    
   [self performSelector:@selector(changetoportrait) withObject:nil afterDelay:0.1];

}

-(void)changetoportrait{
    
    [[UIDevice currentDevice] setValue:
    [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
                                forKey:@"orientation"];
    [self resetAudioPlayer];
    
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    NSLog(@"%ld",(long)toInterfaceOrientation);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (BOOL)shouldAutorotate {
    UIInterfaceOrientation interfaceOrientation = [[UIDevice currentDevice]orientation];
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}



-(void)resetview{
    
    
    
    
    
}


@end
