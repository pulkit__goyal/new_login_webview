/*

 
 */


#import "NSTimer+Blocks.h"
#import "AudioTrimmerViewController.h"
#import "RETrimControl.h"
#import "Latakoo-Swift.h"
#define IS_IPHONE [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone

CGFloat const minimumAudioDuration = 1;
NSString *letters ;


@interface AudioTrimmerViewController (){
    AppDelegate *App;
    RETrimControl *trimControl;
    int flagaudio;
    int count;
}
@property (strong, nonatomic) NSString *strTrimmedAudioURL;
@property (strong, nonatomic) AVAssetExportSession *exportSession;
@property (assign, nonatomic) BOOL isPushAudioTrimmer;
@property (assign, nonatomic) BOOL iscanceltaped;

@end

@implementation AudioTrimmerViewController
@synthesize strTrimmedAudioURL;

+ (void) presentAudioTrimmerController:(UIViewController *)controller completion:(CompleteAudioEditing)complete {
    AudioTrimmerViewController *audioEditorController = [[AudioTrimmerViewController alloc] initWithNibName:@"AudioTrimmerViewController" bundle:nil];
    audioEditorController.completionBlock=complete;
    audioEditorController.isPushAudioTrimmer = false;
    audioEditorController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.3];
    [controller presentViewController:audioEditorController animated:true completion:nil];
}

+ (void) pushAudioTrimmerController:(UIViewController *)controller completion:(CompleteAudioEditing)complete {
    AudioTrimmerViewController *audioEditorController = [[AudioTrimmerViewController alloc] initWithNibName:@"AudioTrimmerViewController" bundle:nil];
    audioEditorController.completionBlock=complete;
    audioEditorController.isPushAudioTrimmer = true;
    audioEditorController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.3];
    [controller.navigationController pushViewController:audioEditorController animated:true];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    letters =  @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    audioTrimStartTime = 0;

    // Do any additional setup after loading the view.
    [viewStartRecord setHidden:FALSE];
    [viewRecording setHidden:TRUE];
    [viewEditRecording setHidden:TRUE];
    [imgViewAudioIndicatorLine setHidden:TRUE];
    [btnUpload setHidden:YES];
    App  =  (AppDelegate*)[[UIApplication sharedApplication] delegate];

    strOriginalAudioURL = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"originalRecording.wav"];
    strAudioURL = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%s.wav","Recording"]];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL]) {
        [[NSFileManager defaultManager] removeItemAtPath:strAudioURL error:nil];
    }
    
    
    [self prepareToRecordAudio];
    
    UIPanGestureRecognizer *panGestureLeftThumb = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [panGestureLeftThumb setDelegate:self];
    [imgViewLeftThumb addGestureRecognizer:panGestureLeftThumb];
    
    UIPanGestureRecognizer *panGestureRightThumb = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [panGestureRightThumb setDelegate:self];
    [imgViewRightThumb addGestureRecognizer:panGestureRightThumb];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)trimControl:(RETrimControl *)trimControl didChangeLeftValue:(CGFloat)leftValue rightValue:(CGFloat)rightValue
{
    audioTrimEndTime = rightValue;
    audioTrimStartTime  = leftValue;
    [self calculateAudioTrimTime];
    
   
}

#pragma mark - Buttons

- (void) dismissAudioTrimmer:(NSString *)strTrimedFilePath {
    
    // Call Completion Block.
    if (self.completionBlock) {
        if (strTrimedFilePath != nil) {
            NSURL *fileUrl = [NSURL fileURLWithPath:strTrimedFilePath];
          
            self.completionBlock(true, fileUrl);
        }
        else {
            
            self.completionBlock(false, nil);
        }
    }

    if (self.isPushAudioTrimmer == true) {
        [self.navigationController popViewControllerAnimated:true];
    }
    else {
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

- (IBAction)btnCancelTapped:(id)sender {
    _iscanceltaped = YES;
    [self stopRecording];
    strTrimmedAudioURL = nil;
    [self dismissAudioTrimmer:nil];
}

- (IBAction)btnUploadTapped:(id)sender {
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL]) {
        [self stopAudioPlayer];
        if(isAudioTrimmed) {
             [self dismissAudioTrimmer:strTrimmedAudioURL];
        }
        else {
            [self trimAudioWithCompletion:^{
                isAudioTrimmed = TRUE;
                [self resetAudioPlayer];
                [self hideUnhideControls:TRUE];
                [self dismissAudioTrimmer:strTrimmedAudioURL];
            }];
        }
    }
}

- (IBAction)btnStartRecordingTapped:(id)sender {
    [self startRecording];
}

- (IBAction)btnStopRecordingTapped:(id)sender {
    [self stopRecording];
}

- (IBAction)btnResetTapped:(id)sender {

     isAudioTrimmed = FALSE;
    if([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL]) {
        [[NSFileManager defaultManager] removeItemAtPath:strAudioURL error:nil];
    }
    [[NSFileManager defaultManager] copyItemAtPath:strOriginalAudioURL toPath:strAudioURL error:nil];
    [self resetAudioPlayer];
}

- (IBAction)btnPlayPauseTapped:(id)sender {
    if(isPlaying) {
        [self pauseAudioPlayer];
    }
    else {
        [self playAudioPlayer];
        [audioPlayer setCurrentTime:audioTrimStartTime];
    }
}

- (IBAction)btnResetAudioTapped:(id)sender {
//    [self resetAudioPlayer];
//    
//    
//    [self hideUnhideControls:FALSE];
//    [self startRecording];
//    
    [self viewDidLoad];
}

- (IBAction)btnEditTrimmedVideoTapped:(id)sender {
    if (((UIButton *) sender).selected) {
        isAudioTrimmed = FALSE;
        [self resetAudioPlayer];
        
        [self hideUnhideControls:FALSE];
    }
    else {
        [self btnTrimFinalAudio];
    }
    
}

- (void)btnTrimFinalAudio {
    [self stopAudioPlayer];
    [self trimAudioWithCompletion:^{
        self->isAudioTrimmed = TRUE;
        [self resetAudioPlayer];
        [self hideUnhideControls:TRUE];
    }];
}

#pragma mark - Gesture

- (void)handlePan:(UIPanGestureRecognizer *)sender {
    [self stopAudioPlayer];
  //  if(sender.state == UIGestureRecognizerStateChanged) {
        UIView *view = sender.view;
        [view.superview bringSubviewToFront:view];
        CGPoint translation = [sender translationInView:view];
        CGPoint viewPosition = view.center;
        viewPosition.x += translation.x;
        
        [self calculateAudioTrimTime];

        //NSLog(@"audioTrimStartTime: %f audioTrimEndTime : %f", audioTrimStartTime, audioTrimEndTime);
        if(audioTrimEndTime - audioTrimStartTime > minimumAudioDuration ||
           ([view isEqual:imgViewLeftThumb] && translation.x <= 0) ||
           ([view isEqual:imgViewRightThumb] && translation.x >= 0)) {
            view.center = viewPosition;
            
            CGRect frame = view.frame;
            if(view.frame.origin.x <= (-view.frame.size.width/2)) {
                frame.origin.x = -view.frame.size.width/2;
                view.frame = frame;
            }
            else if(view.center.x >= viewAudioPlotContainer.frame.size.width){
                frame.origin.x = viewAudioPlotContainer.frame.size.width - (view.frame.size.width/2);
                view.frame = frame;
            }
        }
        else {
            [self calculateAudioTrimTime];
        }
        [sender setTranslation:CGPointZero inView:imgViewLeftThumb];
    }


#pragma mark - Other Methods

- (NSString *)timeFormatted:(CGFloat)interval isWithMinutes:(BOOL)isWithMinutes {
    unsigned long milliseconds = interval * 1000;
    unsigned long seconds = milliseconds / 1000;
    milliseconds %= 1000;
    unsigned long minutes = seconds / 60;
    seconds %= 60;
//    unsigned long hours = minutes / 60;
//    minutes %= 60;
    
    NSString *strMillisec = @(milliseconds).stringValue;
    if (strMillisec.length > 2) {
        strMillisec = [strMillisec substringToIndex:2];
    }
    
    if(isWithMinutes) {
        return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",(long)minutes, (long)seconds, (long)[strMillisec integerValue]];
    }
    else {
        return [NSString stringWithFormat:@"%02ld:%02ld",(long)seconds, (long)[strMillisec integerValue]];
    }
}



- (void)trimAudioWithCompletion:(void(^)(void))completion {
    
    if(!strTrimmedAudioURL) {
        strTrimmedAudioURL = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"trimmertemp.wav"];
    }
    if([[NSFileManager defaultManager] fileExistsAtPath:strTrimmedAudioURL]) {
        [[NSFileManager defaultManager] removeItemAtPath:strTrimmedAudioURL error:nil];
    }
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:strAudioURL]];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:asset];
    if ([compatiblePresets containsObject:AVAssetExportPresetHighestQuality]) {
        self.exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetPassthrough];
        self.exportSession.outputURL = [NSURL fileURLWithPath:strTrimmedAudioURL];
        self.exportSession.outputFileType = AVFileTypeWAVE;
        
        CMTime start = CMTimeMakeWithSeconds(audioTrimStartTime, asset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(audioTrimEndTime - audioTrimStartTime, asset.duration.timescale);
        CMTimeRange range = CMTimeRangeMake(start, duration);
        self.exportSession.timeRange = range;
        
        [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
            switch ([self.exportSession status]) {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"Export failed: %@", [[self.exportSession error] localizedDescription]);
                    break;
                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"Export cancelled");
                    break;
                case AVAssetExportSessionStatusCompleted: {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSError *error;
                        
                        if([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL]) {
                            [[NSFileManager defaultManager] removeItemAtPath:strAudioURL error:nil];
                        }
                        [[NSFileManager defaultManager] copyItemAtPath:strTrimmedAudioURL toPath:strAudioURL error:&error];
                        if(completion) {
                            completion();
                        }

                      
                    });
                    break;
                }
                default:
                    NSLog(@"NONE");
                    break;
            }
        }];
    }
}

- (void)calculateAudioTrimTime {
    
   
    [lblAudioStartTime setText:[self timeFormatted:audioTrimStartTime isWithMinutes:FALSE]];
    [lblAudioEndTime setText:[self timeFormatted:audioTrimEndTime isWithMinutes:FALSE]];
    
  
}

- (void)hideUnhideControls:(BOOL)isHidden {
    [btnEditTrimmedAudio setSelected:isHidden];
   [lblAudioStartTime setHidden:isHidden];
   [lblAudioEndTime setHidden:isHidden];
}

#pragma mark - Audio Recorder

- (void)prepareToRecordAudio {
    NSURL *outputFileURL = [NSURL fileURLWithPath:strOriginalAudioURL];
    if([[NSFileManager defaultManager] fileExistsAtPath:outputFileURL.path]) {
        [[NSFileManager defaultManager] removeItemAtURL:outputFileURL error:nil];
    }
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session setActive:YES error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    [recordSetting setValue :[NSNumber  numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:11025.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey];
    [recordSetting setValue:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
    
    
    // Initiate and prepare the recorder
    NSError *error = nil;
    audioRecorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:&error];
    if (error) {
        NSLog(@"Audio Session Error: %@", error.localizedDescription);
    }
    else {
        audioRecorder.delegate = self;
        audioRecorder.meteringEnabled = YES;
        [audioRecorder prepareToRecord];
    }
}

- (void)startRecording {
    [btnUpload setHidden:YES];
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:YES error:nil];
    [audioRecorder record];
    [self startRcorderTimer];
    
    [viewStartRecord setHidden:TRUE];
    [viewRecording setHidden:FALSE];
    [viewEditRecording setHidden:TRUE];
}

- (void)stopRecording {
    [audioRecorder stop];
   // [btnUpload setHidden:NO];
    [self stopRecorderTimer];
}

- (void)startRcorderTimer {
    __block NSDate *date = [NSDate date];
    timerRecording = [NSTimer scheduledTimerWithTimeInterval:0.1 block:^{
        audioDuration = [[NSDate date] timeIntervalSinceDate:date];
        [lblRecordingTime setText:[self timeFormatted:audioDuration isWithMinutes:TRUE]];
    } repeats:YES];
}

- (void)stopRecorderTimer {
    if (timerRecording) {
        [timerRecording invalidate];
        timerRecording = nil;
    }
}

#pragma mark - Audio Player

- (void)resetAudioPlayer {
    [self stopAudioPlayer];
    audioPlayer = nil;
    [self prepareToPlayAudio];
    int y = 0;
    int x = 0;
    int w = 0;
    if (IS_IPHONE) {
        y = 120;
        x = 0;
        w = 40;
    }else{
        y = 200;
        x = 0;
        w = 60;
    }
    
 
        [trimControl removeFromSuperview];
        
        trimControl = [[RETrimControl alloc] initWithFrame:CGRectMake(x,y,viewAudioPlotContainer.frame.size.width, 30)];
        trimControl.length = audioDuration;
        trimControl.delegate = self;
        [viewAudioPlotContainer addSubview:trimControl];
        [viewAudioPlotContainer bringSubviewToFront:trimControl];
        
        audioTrimEndTime = audioDuration;
        [lblAudioEndTime setText:[self timeFormatted:audioDuration isWithMinutes:FALSE]];
        [self calculateAudioTrimTime];
        
   
}

- (void)prepareToPlayAudio {
    if (strAudioURL) {
        NSURL *url = [NSURL fileURLWithPath:strAudioURL];
        NSError *error;
        if (!audioPlayer) {
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            if (error) {
                NSLog(@"Error in audioPlayer: %@",[error localizedDescription]);
            } else {
                audioPlayer.delegate = self;
                [audioPlayer prepareToPlay];
                audioDuration = audioPlayer.duration;
                [lblAudioDuration setText:[self timeFormatted:audioDuration isWithMinutes:TRUE]];
            }
        }
    }
}

- (void)playAudioPlayer {
    if (!audioPlayer) {
      //  [self prepareToPlayAudio];
        [self trimtempaudio];
    }
    
    if(audioPlayer) {
        isPlaying = YES;
        [audioPlayer play];
        [btnPlayPause setSelected:TRUE];
        [self startAudioTimerToDisplay];
    }
}

- (void)pauseAudioPlayer {
    if (isPlaying) {
        isPlaying = NO;
        [audioPlayer pause];
        [btnPlayPause setSelected:FALSE];
        [imgViewAudioIndicatorLine setHidden:TRUE];
        [self stopAudioTimerToDisplay];
    }
}

- (void)stopAudioPlayer {
    if (isPlaying) {
        isPlaying = NO;
        [audioPlayer stop];
        [btnPlayPause setSelected:FALSE];
        [imgViewAudioIndicatorLine setHidden:TRUE];
        [self stopAudioTimerToDisplay];
    }
}

- (void)startAudioTimerToDisplay {
    if (timerAudioPlayer) {
        [timerAudioPlayer invalidate];
        timerAudioPlayer = nil;
    }
    
    timerAudioPlayer = [NSTimer scheduledTimerWithTimeInterval:.1 block:^{
//        [imgViewAudioIndicatorLine setCenter:CGPointMake((viewAudioPlotContainer.frame.size.width * audioPlayer.currentTime / audioDuration), imgViewAudioIndicatorLine.center.y)];
//        [imgViewAudioIndicatorLine setHidden:FALSE];
        if(audioPlayer.currentTime >= audioTrimEndTime) {
            [self stopAudioPlayer];
        }
    } repeats:YES];
}

- (void)stopAudioTimerToDisplay {
    if (timerAudioPlayer) {
        [timerAudioPlayer invalidate];
        timerAudioPlayer = nil;
    }
}


#pragma mark - Audio Recorder Delegate

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag {
    // Stop recording
    [audioRecorder stop];
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:strOriginalAudioURL]];
    audioDuration = CMTimeGetSeconds(asset.duration);
    [self calculateAudioTrimTime];
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [audioSession setActive:NO error:nil];

   // [viewStartRecord setHidden:TRUE];
  //  [viewRecording setHidden:TRUE];
  //  [viewEditRecording setHidden:FALSE];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL]) {
        [[NSFileManager defaultManager] removeItemAtPath:strAudioURL error:nil];
    }
    NSError *error;
    [[NSFileManager defaultManager] copyItemAtPath:strOriginalAudioURL toPath:strAudioURL error:&error];
   
    
    
   // [self prepareToPlayAudio];
   // [self resetAudioPlayer];
    
    /////bharat changed this ///////////
    
    
  //  dispatch_async(dispatch_get_main_queue(), ^{
        
       /// [self trimAudioWithCompletion:^{
               self->isAudioTrimmed = TRUE;
               [self resetAudioPlayer];
               [self hideUnhideControls:TRUE];
               if (_iscanceltaped) {
             [self dismissAudioTrimmer:nil];

               }else{
        [self dismissAudioTrimmer:self->strAudioURL];

               }
         //  }];
        
        
   // });
    
    
   
    
    
    
    ///////////////////////////////////////////////



}

- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError * __nullable)error {
    
}

#pragma mark - Audio Player Delegate

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    [self stopAudioPlayer];
}

-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
    [self stopAudioPlayer];
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player {
    [self pauseAudioPlayer];
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player {
    [self playAudioPlayer];
}

#pragma mark - Gesture Recognizer Delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint velocity = [(UIPanGestureRecognizer *)gestureRecognizer velocityInView:gestureRecognizer.view];
    return fabs(velocity.x) > fabs(velocity.y);
}


-(NSString *) randomStringWithLength: (int) len {
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    return randomString;
}

-(void)trimtempaudio{
    
    
    NSString *strTrimmedAudioURL1 ;

        
        if(!strTrimmedAudioURL1) {
            strTrimmedAudioURL1 = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"trimmerRecording.wav"];
        }
        if([[NSFileManager defaultManager] fileExistsAtPath:strTrimmedAudioURL1]) {
            [[NSFileManager defaultManager] removeItemAtPath:strTrimmedAudioURL1 error:nil];
        }
        AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:strAudioURL]];
        NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:asset];
        if ([compatiblePresets containsObject:AVAssetExportPresetHighestQuality]) {
            self.exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetPassthrough];
            self.exportSession.outputURL = [NSURL fileURLWithPath:strTrimmedAudioURL1];
            self.exportSession.outputFileType = AVFileTypeWAVE;
            
            CMTime start = CMTimeMakeWithSeconds(audioTrimStartTime, asset.duration.timescale);
            CMTime duration = CMTimeMakeWithSeconds(audioTrimEndTime - audioTrimStartTime, asset.duration.timescale);
            CMTimeRange range = CMTimeRangeMake(start, duration);
            self.exportSession.timeRange = range;
            
            [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
                switch ([self.exportSession status]) {
                    case AVAssetExportSessionStatusFailed:
                        NSLog(@"Export failed: %@", [[self.exportSession error] localizedDescription]);
                        break;
                    case AVAssetExportSessionStatusCancelled:
                        NSLog(@"Export cancelled");
                        break;
                    case AVAssetExportSessionStatusCompleted: {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSError *error;
        
                                NSURL *url = [NSURL fileURLWithPath:strTrimmedAudioURL1];
                                if (!audioPlayer) {
                                    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
                                    if (error) {
                                        NSLog(@"Error in audioPlayer: %@",[error localizedDescription]);
                                    } else {
                                        audioPlayer.delegate = self;
                                        [audioPlayer prepareToPlay];
                                        audioDuration = audioPlayer.duration;
                                        [lblAudioDuration setText:[self timeFormatted:audioDuration isWithMinutes:TRUE]];
                                    }
                                }
                            
                            
                        });
                        break;
                    }
                    default:
                        NSLog(@"NONE");
                        break;
                }
            }];
        }
    
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    NSLog(@"%ld",(long)toInterfaceOrientation);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (BOOL)shouldAutorotate {
    UIInterfaceOrientation interfaceOrientation = [[UIDevice currentDevice]orientation];
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

-(void)viewDidAppear:(BOOL)animated {

   [self performSelector:@selector(changetoportrait) withObject:nil afterDelay:0.1];

}

-(void)changetoportrait{

    [[UIDevice currentDevice] setValue:
    [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
                                forKey:@"orientation"];

}
@end
