//
//  ApiManager.swift
//  InTouch
//
//  Created by Shruti Aggarwal on 20/03/18.
//  Copyright © 2018 parangat technology. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD



class APIManager: NSObject {

    
    class func requestGETURL(_ strURL: String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
      
        Alamofire.request(strURL).responseJSON { (responseObject) -> Void in
            print(responseObject)
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
             //   SVProgressHUD.dismiss()

            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
               // SVProgressHUD.dismiss()


            }
        }
    }
    
    class func requestPOSTURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        Alamofire.request(strURL, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: headers).responseJSON {
            
            
            (responseObject) -> Void in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)

            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                
                failure(error)

            }
        }
    }
    
    
    
    
    
    class func requestGETURLNEW(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        
        
        
      
        Alamofire.request(strURL, method: .get, parameters: params,  encoding: URLEncoding.httpBody, headers: headers ).responseJSON { (responseObject) -> Void in
            

            print(headers!)

            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)

            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                
                failure(error)

            }
        }
    }
    
    
    //    class func requestDeleteURL
    
    class func requestDeleteURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        Alamofire.request(strURL, method: .delete, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                
                failure(error)
            }
        }
    }
    
    class func requestImage(path: String, completionHandler: @escaping (_ img : UIImage? ) -> Void){
        
        Alamofire.request(path).responseData { (response) in
            if response.error == nil {
                SVProgressHUD.show(withStatus: "Please Wait")

                
                print(response.result)
                
                if let data = response.data {
                    SVProgressHUD.dismiss()

                    guard   let aImg = UIImage(data: data) else{
                        print("bhag dk boos")
                        return
                        
                    }
                    completionHandler(aImg)
                    SVProgressHUD.dismiss()

                }else{
                    completionHandler(nil)
                    SVProgressHUD.dismiss()

                }
                
            }
        }
    }
}
