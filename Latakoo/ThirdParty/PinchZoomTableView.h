

#import <UIKit/UIKit.h>

@interface PinchZoomTableView : UITableView <UIGestureRecognizerDelegate>{
	
	CGRect originalFrame;
	CGFloat screenWidth;
	CGFloat screenHeight;
	CGFloat deltaY;
    
    CGRect tableframe;

}

@property (nonatomic, readwrite) CGRect originalFrame;
@property (nonatomic, readwrite) CGFloat screenWidth;
@property (nonatomic, readwrite) CGFloat screenHeight;
@property (nonatomic, readwrite) CGFloat deltaY;
- (void) resetTable;
- (void) addGesture;


@end
