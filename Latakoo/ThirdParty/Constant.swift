//
//  Constant.swift
//  InTouch
//
//  Created by Shruti Aggarwal on 20/03/18.
//  Copyright © 2018 parangat technology. All rights reserved.
//


import Foundation
import UIKit

class Constant: NSObject {
    
    
    struct urldata {
        static var baseurl = UserDefaults.standard.value(forKey: "server") as! String
        static var baseManifestUrl = UserDefaults.standard.value(forKey: "serverManifest") as! String

        
    }
    
    struct API {
        
    }
    
    struct DeviceType {
        
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH <= 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6_7          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P_7P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
        static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
    }
    
    
    
    struct ScreenSize {
        
        static let kHeight  = UIScreen.main.bounds.height
        static let kWidth   = UIScreen.main.bounds.width
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.kWidth, ScreenSize.kHeight)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.kWidth, ScreenSize.kHeight)
        
    }
    
    struct Storyboard {
        static let kMain            =   "Main"
    }
    
    struct NavControllerID {
        static let kLoginNC        =   "LoginNC"
        
    }
    
    struct ViewControllerID {
        static let LoginVC           =   "LoginVC"
     
    }
    
    struct Font {
        
    }
    
}
