import AWSS3 // 1
import Foundation
import UIKit

typealias progressBlock = (_ progress: Double) -> Void // 2
typealias completionBlock = (_ response: Any?, _ error: Error?) -> Void // 3
let appdelegate = UIApplication.shared.delegate as! AppDelegate

class AWSS3Manager {
    static let shared = AWSS3Manager() // 4

    private init() { }
    let bucketName = appdelegate.bucketpath // 5

    // Upload image using UIImage object
    func uploadImage(imagePath: String, progress: progressBlock?, completion: completionBlock?) {
        let fileUrl = URL(fileURLWithPath: imagePath)
        let filenamestr = "\(appdelegate.fileNameFromServer).jpg"
        print("IMAGE FILE NAME: \(filenamestr)")
        uploadImagefile(fileUrl: fileUrl, fileName: filenamestr, contenType: "image/jpeg", progress: progress, completion: completion)
    }

    // Upload image using UIImage object
    func uploadMediaThumbnail(imagePath: String, progress: progressBlock?, completion: completionBlock?) {
        let fileUrl = URL(fileURLWithPath: imagePath)
        let filenamestr = "\(appdelegate.fileNameFromServer).jpg"
        uploadMediaThumbnailfile(fileUrl: fileUrl, fileName: filenamestr, contenType: "image/jpeg", progress: progress, completion: completion)
    }

    // Upload video from local path url
    func uploadVideo(videoUrl: URL, progress: progressBlock?, completion: completionBlock?) {
        // let fileName = self.getUniqueFileName(fileUrl: videoUrl)
        uploadfileinMultipart(fileUrl: videoUrl, fileName: appdelegate.fileNameFromServer, contenType: "video/mp4", progress: progress, completion: completion)
    }

    // Upload auido from local path url
    func uploadAudio(audioUrl: URL, progress: progressBlock?, completion: completionBlock?) {
//        let fileName = getUniqueFileName(fileUrl: audioUrl)
        uploadAudiofile(fileUrl: audioUrl, fileName: appdelegate.fileNameFromServer, contenType: "audio", progress: progress, completion: completion)
    }

    // Upload files like Text, Zip, etc from local path url
    func uploadOtherFile(fileUrl: URL, conentType: String, progress: progressBlock?, completion: completionBlock?) {
        let fileName = getUniqueFileName(fileUrl: fileUrl)
        uploadfile(fileUrl: fileUrl, fileName: fileName, contenType: conentType, progress: progress, completion: completion)
    }

    // Get unique file name
    func getUniqueFileName(fileUrl: URL) -> String {
        let strExt: String = "." + URL(fileURLWithPath: fileUrl.absoluteString).pathExtension
        return (ProcessInfo.processInfo.globallyUniqueString + strExt)
    }

    // MARK: - AWS file upload

    // fileUrl :  file local path url
    // fileName : name of file, like "myimage.jpeg" "video.mov"
    // contenType: file MIME type
    // progress: file upload progress, value from 0 to 1, 1 for 100% complete
    // completion: completion block when uplaoding is finish, you will get S3 url of upload file here
    private func uploadfile(fileUrl: URL, fileName: String, contenType: String, progress: progressBlock?, completion: completionBlock?) {
        DispatchQueue.main.async {
            // Upload progress block
            let expression = AWSS3TransferUtilityUploadExpression()
            expression.progressBlock = { _, awsProgress in
                guard let uploadProgress = progress else { return }
                DispatchQueue.main.async {
                    uploadProgress(awsProgress.fractionCompleted)
                }
            }
            // Completion block
            var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
            completionHandler = { (_, error) -> Void in
                DispatchQueue.main.async(execute: {
                    if error == nil {
                        let url = AWSS3.default().configuration.endpoint.url
//                    let publicURL = url?.appendingPathComponent(appdelegate.bucketpath).appendingPathComponent(fileName)
                        let publicURL = url?.appendingPathComponent(appdelegate.bucketpath)
                        print("Uploaded to: \(String(describing: publicURL))")
                        if let completionBlock = completion {
                            completionBlock(publicURL?.absoluteString, nil)
                        }
                    } else {
                        if let completionBlock = completion {
                            completionBlock(nil, error)
                        }
                    }
                })
            }
            // Start uploading using AWSS3TransferUtility
            let awsTransferUtility = AWSS3TransferUtility.default()
            print("appdelegate.bucketpath: \(appdelegate.bucketpath)")

            awsTransferUtility.uploadFile(fileUrl, bucket: appdelegate.bucketname, key: appdelegate.bucketpath, contentType: "video/mp4", expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
                if let error = task.error {
                    print("error is: \(error.localizedDescription)")
                }
                if let _ = task.result {
                    // your uploadTask
                    print("task.result is: \(String(describing: task.result))")
                }
                return nil
            }
        }
    }

    // Multi-part Upload

    // Thumbnail
    private func uploadMediaThumbnailfile(fileUrl: URL, fileName: String, contenType: String, progress: progressBlock?, completion: completionBlock?) {
        DispatchQueue.main.async {
            // Upload progress block
            let expression = AWSS3TransferUtilityMultiPartUploadExpression()
            // Set metadata here
            let startuploadtime = Date.getCurrentDate()
            print("start-upload-time: \(startuploadtime)")
//        expression.setValue(startuploadtime, forRequestParameter: "start-upload-time")
//        expression.setValue("attachment; filename = \(fileName)", forRequestParameter: "Content-Disposition")

            expression.progressBlock = { _, awsProgress in
                guard let uploadProgress = progress else { return }
                DispatchQueue.main.async {
                    uploadProgress(awsProgress.fractionCompleted)
                }
            }

            // Completion block
            var completionHandler: AWSS3TransferUtilityMultiPartUploadCompletionHandlerBlock?
            completionHandler = { (_, error) -> Void in
                DispatchQueue.main.async(execute: {
                    if error == nil {
                        let url = AWSS3.default().configuration.endpoint.url
                        let publicURL = url?.appendingPathComponent(appdelegate.thumbnailBucketpath)
                        print("Media Thumbnail Uploaded to: \(String(describing: publicURL))")
                        if let completionBlock = completion {
                            completionBlock(publicURL?.absoluteString, nil)
                        }
                    } else {
                        if let completionBlock = completion {
                            completionBlock(nil, error)
                        }
                    }
                })
            }

            // Start uploading using AWSS3TransferUtility
            let awsTransferUtility = AWSS3TransferUtility.s3TransferUtility(forKey: appdelegate.bucketpTest)
            print("appdelegate.thumbnailBucketpath: \(appdelegate.thumbnailBucketpath)")

            awsTransferUtility?.uploadUsingMultiPart(fileURL: fileUrl, bucket: appdelegate.bucketname, key: appdelegate.thumbnailBucketpath, contentType: "image/jpeg", expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
                if let error = task.error {
                    print("error is: \(error.localizedDescription)")
                }
                if let _ = task.result {
                    // your uploadTask
                    print("task.result is: \(String(describing: task.result))")
                }
                return nil
            }
        }
    }

    // Image
    private func uploadImagefile(fileUrl: URL, fileName: String, contenType: String, progress: progressBlock?, completion: completionBlock?) {
        DispatchQueue.main.async {
            // Upload progress block
            let expression = AWSS3TransferUtilityMultiPartUploadExpression()
            // Set metadata here
            let startuploadtime = Date.getCurrentDate()
            print("start-upload-time: \(startuploadtime)")
            expression.setValue(startuploadtime, forRequestHeader: "start-upload-time")
            print("fileName is: \(fileName)")
            expression.setValue("attachment; filename = \(appdelegate.originalFileName)", forRequestHeader: "Content-Disposition")

            expression.progressBlock = { _, awsProgress in
                guard let uploadProgress = progress else { return }
                DispatchQueue.main.async {
                    uploadProgress(awsProgress.fractionCompleted)
                }
            }

            // Completion block
            var completionHandler: AWSS3TransferUtilityMultiPartUploadCompletionHandlerBlock?
            completionHandler = { (_, error) -> Void in
                DispatchQueue.main.async(execute: {
                    if error == nil {
                        let url = AWSS3.default().configuration.endpoint.url
                        let publicURL = url?.appendingPathComponent(appdelegate.bucketpath)
                        print("Image File Uploaded to: \(String(describing: publicURL))")
                        if let completionBlock = completion {
                            completionBlock(publicURL?.absoluteString, nil)
                        }
                    } else {
                        if let completionBlock = completion {
                            completionBlock(nil, error)
                        }
                    }
                })
            }

            // Start uploading using AWSS3TransferUtility
            let awsTransferUtility = AWSS3TransferUtility.s3TransferUtility(forKey: appdelegate.bucketpTest)
            print("appdelegate.bucketpath: \(appdelegate.bucketpath)")

            awsTransferUtility?.uploadUsingMultiPart(fileURL: fileUrl, bucket: appdelegate.bucketname, key: appdelegate.bucketpath, contentType: "image/jpeg", expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
                if let error = task.error {
                    print("error is: \(error.localizedDescription)")
                }
                if let _ = task.result {
                    // your uploadTask
                    print("task.result is: \(String(describing: task.result))")
                }
                return nil
            }
        }
    }

    // Audio
    private func uploadAudiofile(fileUrl: URL, fileName: String, contenType: String, progress: progressBlock?, completion: completionBlock?) {
        DispatchQueue.main.async {
            // Upload progress block
            let expression = AWSS3TransferUtilityMultiPartUploadExpression()
            // Set metadata here
            let startuploadtime = Date.getCurrentDate()
            print("start-upload-time: \(startuploadtime)")
            expression.setValue(startuploadtime, forRequestHeader: "start-upload-time")
            print("fileName is: \(fileName)")
            expression.setValue("attachment; filename = \(appdelegate.originalFileName)", forRequestHeader: "Content-Disposition")

            expression.progressBlock = { _, awsProgress in
                guard let uploadProgress = progress else { return }
                DispatchQueue.main.async {
                    uploadProgress(awsProgress.fractionCompleted)
                }
            }

            // Completion block
            var completionHandler: AWSS3TransferUtilityMultiPartUploadCompletionHandlerBlock?
            completionHandler = { (_, error) -> Void in
                DispatchQueue.main.async(execute: {
                    if error == nil {
                        let url = AWSS3.default().configuration.endpoint.url
                        let publicURL = url?.appendingPathComponent(appdelegate.bucketpath)
                        print("Audio File Uploaded to: \(String(describing: publicURL))")
                        if let completionBlock = completion {
                            completionBlock(publicURL?.absoluteString, nil)
                        }
                    } else {
                        if let completionBlock = completion {
                            completionBlock(nil, error)
                        }
                    }
                })
            }

            // Start uploading using AWSS3TransferUtility
            let awsTransferUtility = AWSS3TransferUtility.s3TransferUtility(forKey: appdelegate.bucketpTest)
            print("appdelegate.thumbnailBucketpath: \(appdelegate.bucketpath)")

            awsTransferUtility?.uploadUsingMultiPart(fileURL: fileUrl, bucket: appdelegate.bucketname, key: appdelegate.bucketpath, contentType: "audio", expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
                if let error = task.error {
                    print("error is: \(error.localizedDescription)")
                }
                if let _ = task.result {
                    // your uploadTask
                    print("task.result is: \(String(describing: task.result))")
                }
                return nil
            }
        }
    }

    // Video
    private func uploadfileinMultipart(fileUrl: URL, fileName: String, contenType: String, progress: progressBlock?, completion: completionBlock?) {
        DispatchQueue.main.async {
            // Upload progress block
            let expression = AWSS3TransferUtilityMultiPartUploadExpression()
            // Set metadata here
            let startuploadtime = Date.getCurrentDate()
            print("start-upload-time: \(startuploadtime)")
            expression.setValue(startuploadtime, forRequestHeader: "start-upload-time")
            print("fileName is: \(fileName)")
            expression.setValue("attachment; filename = \(appdelegate.originalFileName)", forRequestHeader: "Content-Disposition")

            expression.progressBlock = { _, awsProgress in
                guard let uploadProgress = progress else { return }
                DispatchQueue.main.async {
                    uploadProgress(awsProgress.fractionCompleted)
                }
            }
            // Completion block
            var completionHandler: AWSS3TransferUtilityMultiPartUploadCompletionHandlerBlock?
            completionHandler = { (_, error) -> Void in
                DispatchQueue.main.async(execute: {
                    if error == nil {
                        let url = AWSS3.default().configuration.endpoint.url
                        let publicURL = url?.appendingPathComponent(appdelegate.bucketpath)
                        print("Video FIle Uploaded to: \(String(describing: publicURL))")
                        if let completionBlock = completion {
                            completionBlock(publicURL?.absoluteString, nil)
                        }
                    } else {
                        if let completionBlock = completion {
                            completionBlock(nil, error)
                        }
                    }
                })
            }

            // Start uploading using AWSS3TransferUtility
            let awsTransferUtility = AWSS3TransferUtility.s3TransferUtility(forKey: appdelegate.bucketpTest)
            print("appdelegate.bucketpath: \(appdelegate.bucketpath)")

            awsTransferUtility?.uploadUsingMultiPart(fileURL: fileUrl, bucket: appdelegate.bucketname, key: appdelegate.bucketpath, contentType: "video/mp4", expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
                if let error = task.error {
                    print("error is: \(error.localizedDescription)")
                }
                if let _ = task.result {
                    // your uploadTask
                    print("task.result is: \(String(describing: task.result))")
                }
                return nil
            }
        }
    }

    /////////////////////DOWNLOAD////////////////////DOWNLOAD/////////////////////////DOWNLOAD//////////////////
    // Download FIle
    func downloadFile(fileID: String, downloadURL: URL, fileName: String, fileType: String, progress: progressBlock?, completion: completionBlock?) {
        print("In AWSS3Manager: fileID: \(fileID) & downloadURL: \(downloadURL)")

        DispatchQueue.main.async {
            // Start downloading using AWSS3TransferUtility
            let awsTransferUtility = AWSS3TransferUtility.s3TransferUtility(forKey: appdelegate.bucketpTest)
            print("appdelegate.bucketpTest: \(appdelegate.bucketpTest)")
            print("appdelegate.bucketpath: \(appdelegate.bucketpath)")

            let expression = AWSS3TransferUtilityDownloadExpression()
            expression.progressBlock = { _, awsProgress in
                guard let downloadProgress = progress else { return }
                DispatchQueue.main.async {
                    downloadProgress(awsProgress.fractionCompleted)
                }
            }

            var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
            completionHandler = { (_, _, _, error) -> Void in
                DispatchQueue.main.async(execute: {
                    if error == nil {
                        let url = AWSS3.default().configuration.endpoint.url
                        let publicURL = url?.appendingPathComponent(appdelegate.bucketpath)
                        print("Downloaded file from: \(String(describing: publicURL))")
                        if let completionBlock = completion {
                            completionBlock(publicURL?.absoluteString, nil)
                        }
                    } else {
                        print("Failed with error: \(error)")
                        if let completionBlock = completion {
                            completionBlock(nil, error)
                        }
                    }
                })
            }

//        awsTransferUtility?.download(to: downloadURL, bucket: appdelegate.bucketname, key: appdelegate.bucketpath, expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
//            if let error = task.error {
//                print("error is: \(error.localizedDescription)")
//            }
//            if let _ = task.result {
//                // your downloadTask
//                print("task.result is: \(String(describing: task.result))")
//            }
//            return nil
//        }

            print("appdelegate.bucketname: appdelegate.bucketname: appdelegate.bucketname:  \n  \(appdelegate.bucketname)")
            awsTransferUtility?.download(to: downloadURL, bucket: appdelegate.bucketname, key: appdelegate.bucketpath, expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
                if let error = task.error {
                    print("error is: \(error.localizedDescription)")
                }
                if let _ = task.result {
                    // your downloadTask
                    print("task.result is: \(String(describing: task.result))")
                }
                return nil
            }
        }
    }
}
