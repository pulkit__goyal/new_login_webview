//
//  FilterVC.swift
//  Latakoo
//
//  Created by Shruti Aggarwal on 21/08/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import UIKit
class filtercell: UITableViewCell {
    @IBOutlet weak var lblnetwokname: UILabel!
    @IBOutlet weak var imgtick: UIImageView!
    
    
}

class FilterVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return networks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! filtercell
        let dict = networks[indexPath.row]
        cell.lblnetwokname.text = dict["company_name"] as? String
        
        if ishomeselected == "All"{
            if indexPath.row == 0{
            cell.imgtick.isHidden = false
            }else{
            cell.imgtick.isHidden = true
            }
            
            
        }else if ishomeselected == "HomeNetworks"{
            
            if indexPath.row == 1{
                cell.imgtick.isHidden = false
            }else{
                cell.imgtick.isHidden = true
            }
            
        } else if ishomeselected == "youruploads" {

        if indexPath.row == 2{
                       cell.imgtick.isHidden = false
                   }else{
                       cell.imgtick.isHidden = true
                   }
        }
        
        
        else{
            
           
            let companyid = dict["company_id"] as? String
            
            if indexPath.row>2{
            if selectednetwork .contains(companyid!){
                cell.imgtick.isHidden = false
               }else{
                cell.imgtick.isHidden = true
                   }
            }else{
                cell.imgtick.isHidden = true
                }
            
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = networks[indexPath.row]
        if indexPath.row == 0{
           ishomeselected = "All"
            selectednetwork.removeAll()
            
        }else if indexPath.row == 1{
            if homenetwork.count > 0{
                selectednetwork.removeAll()
                ishomeselected = "HomeNetworks"
                
            }
                
            else{
                let alertController = UIAlertController(title: "Home Network", message: "No Home network selected in settings", preferredStyle: .alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                    
                    print("Ok button tapped");
                    
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            }
        }else if indexPath.row == 2{
           ishomeselected = "youruploads"
            selectednetwork.removeAll()
            
        }
        
        else{
            ishomeselected = "filterednetwork"
            let companyid = "\(dict["company_id"] as? String ?? "")"
            if selectednetwork .contains(companyid){
                if let index = selectednetwork.index(of:companyid) {
                    if selectednetwork.count>1{
                    selectednetwork.remove(at: index)
                                            }
                }
                }else{
                selectednetwork.insert(companyid, at:0)
            }
        }
        tblfilter.reloadData()
    }
    
    @IBOutlet weak var tblfilter: UITableView!
    let appdelegate = UIApplication.shared.delegate as! AppDelegate

    var networks : [[String:Any]] = []
    var selectednetwork : [String] = []
    var filerednetwork : [String] = []
    var homenetwork : [String] = []
    var ishomeselected : String?

    override func viewDidLoad() {
        super.viewDidLoad()

            
        networks  = UserDefaults.standard.value(forKey:"AvailableNetworks") as! [[String : Any]]
            print(networks)
        
            networks = networks.sorted { ($0["company_name"] as! String).localizedCaseInsensitiveCompare($1["company_name"] as! String) == ComparisonResult.orderedAscending }
            let dict2 : [String:Any] = ["company_name":"Your Uploads"]
            let dict : [String:Any] = ["company_name":"All Networks"]
            let dict1 : [String:Any] = ["company_name":"Home Networks"]
            networks.insert(dict2, at: 0)
            networks.insert(dict1, at: 0)
            networks.insert(dict, at: 0)

            filerednetwork = UserDefaults.standard.value(forKey: "selectedfilterNetwork") as? [String] ?? []
            homenetwork = UserDefaults.standard.value(forKey:"homenetwork") as? [String] ?? []
        ishomeselected = UserDefaults.standard.value(forKey:"filterednetwork") as? String
            
            
            if ishomeselected  == "All"{
                
             //   selectednetwork.insert("0", at: 0)
                
            }else if ishomeselected == "HomeNetworks" {
              //  selectednetwork = homenetwork

                
            }else if ishomeselected == "youruploads" {
              //  selectednetwork = homenetwork

                
            }
            else{
                selectednetwork = filerednetwork
            }
            
            
            tblfilter.reloadData()
         
    //        UserDefaults.standard.set("All", forKey:"filterednetwork")
    //        UserDefaults.standard.set(arrhomenetwork, forKey:"homenetwork")
    //        UserDefaults.standard.set(arrselectednetwork, forKey:"selectedfilterNetwork")

        }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        networks  = UserDefaults.standard.value(forKey:"AvailableNetworks") as! [Any] as! [[String : Any]]
        print(networks)
         networks = networks.sorted { ($0["company_name"] as! String).localizedCaseInsensitiveCompare($1["company_name"] as! String) == ComparisonResult.orderedAscending }
        
        let dict2 : [String:Any] = ["company_name":"Your Uploads"]
        let dict : [String:Any] = ["company_name":"All Networks"]
        let dict1 : [String:Any] = ["company_name":"Home Networks"]
       networks.insert(dict2, at: 0)
        networks.insert(dict1, at: 0)
        networks.insert(dict, at: 0)

        filerednetwork = UserDefaults.standard.value(forKey: "selectedfilterNetwork") as? [String] ?? []
        homenetwork = UserDefaults.standard.value(forKey:"homenetwork") as? [String] ?? []
        ishomeselected = UserDefaults.standard.value(forKey:"filterednetwork") as? String
        
        print(ishomeselected!)
        
       if ishomeselected  == "All"{
           
        //   selectednetwork.insert("0", at: 0)
           
       }else if ishomeselected == "HomeNetworks" {
         //  selectednetwork = homenetwork

           
       }else if ishomeselected == "youruploads" {
         //  selectednetwork = homenetwork

           
       }
       else{
           selectednetwork = filerednetwork
       }
        
        
        tblfilter.reloadData()
     

    }
    
    
    
    @IBAction func btnback(_ sender: Any) {
        
        
        UserDefaults.standard.set(ishomeselected, forKey: "filterednetwork")
        
      
        if ishomeselected  == "All"{
            
            
        }else if ishomeselected == "HomeNetworks" {
            
            
        }
        else if ishomeselected == "youruploads" {
          //  selectednetwork = homenetwork

            
        }else{
        
            UserDefaults.standard.set(selectednetwork, forKey:"selectedfilterNetwork")
        
        }
        
        UserDefaults.standard.synchronize()
    
        
//        let story =  UIStoryboard(name: "Main", bundle: nil)
//        let TabbarController = story.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
//        TabbarController.selectedIndex = 1
//        self.navigationController?.pushViewController(TabbarController, animated: false)
        
        appdelegate.isfilter = true
        self.navigationController?.popViewController(animated: true)
    
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
   
    

}
