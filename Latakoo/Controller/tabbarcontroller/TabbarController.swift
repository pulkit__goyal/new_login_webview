//
//  TabbarController.swift
//  Latakoo
//
//  Created by Shruti Aggarwal on 15/11/18.
//  Copyright © 2018 parangat technology. All rights reserved.
//

import UIKit

class TabbarController: UITabBarController {

    @IBOutlet weak var mytabbar: UITabBar!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        if IS_IPHONE {
                    
       let numberOfItems = CGFloat(tabBar.items!.count)
        let tabBarItemSize = CGSize(width: tabBar.frame.width/numberOfItems, height: tabBar.frame.height-1)
        tabBar.selectionIndicatorImage = UIImage.imageWithColor(color:UIColor(named: "pilotcolor-1")!, size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0), resizingMode: .stretch)
                               
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .selected)
            
           

    UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor(named: "pilotcolor")!], for: .normal)
        
        } else {
            
          
           
          let numberOfItems = CGFloat(tabBar.items!.count)
        let tabBarItemSize = CGSize(width: tabBar.frame.width/numberOfItems, height:80)
                                      
        tabBar.selectionIndicatorImage = UIImage.imageWithColor(color:UIColor(named: "pilotcolor-1")!, size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0), resizingMode: .stretch)
                                      
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .selected)
            
            let fontBold:UIFont = UIFont.boldSystemFont(ofSize: 16)
    UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor(named: "pilotcolor")!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font:fontBold], for: .normal)

            
        
        }
        
        
        
       
    
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
       
    
  
    }
    
    
    override var traitCollection: UITraitCollection {
          let curr = super.traitCollection
          let compact = UITraitCollection(horizontalSizeClass: .compact)
          return UITraitCollection(traitsFrom: [curr, compact])
      }
  
    
    
   
       override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      }

      override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
      }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
     
        
               
    }
    
    override func viewWillLayoutSubviews() {

        
        
        if IS_IPHONE {
               tabBar.frame.size.width = self.view.frame.width + 4
               tabBar.frame.origin.x = -3
                  } else {
                   tabBar.frame.size.width = self.view.frame.width + 4
                    tabBar.frame.origin.x = -3

                   }

  
    }
    
    override func viewDidLayoutSubviews() {
           super.viewDidLayoutSubviews()

           if IS_IPHONE {
          
            
           
            
           } else {
            let fontBold:UIFont = UIFont.boldSystemFont(ofSize: 16)
                      UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: fontBold], for: .normal)
               tabBar.invalidateIntrinsicContentSize()
               var tabFrame = tabBar.frame
               tabFrame.size.height = 80.0
               tabFrame.origin.y = view.frame.size.height - 80
               tabBar.frame = tabFrame
               tabBar.isTranslucent = true
           }
       }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
}

extension UIImage {
    
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0.0, y: 0.0, width: Double(size.width), height: Double(size.height))
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}


