//
//  LoginVC.swift
//  Latakoo
//
//  Created by Shruti Aggarwal on 14/11/18.
//  Copyright © 2018 parangat technology. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import TransitionButton  // 1: First import the TransitionButton library
import Photos
import AFNetworking
import SwiftyJSON
import IQKeyboardManagerSwift
import SystemConfiguration

//class Connectivity {
//    class var isConnectedToInternet:Bool {
//        return NetworkReachabilityManager()!.isReachable
//    }
//}




class LoginVC: UIViewController {
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtpassword: UITextField!
    @IBOutlet weak var btnEye: UIButton!
    @IBOutlet weak var btnsignin: UIButton!
    var flag : Int = 0
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var imgback: UIImageView!
    @IBOutlet weak var logoback: UIImageView!

   
    
    override func viewDidLoad() {
        
        
        

       
        
        txtEmail.textContentType = .username
        txtpassword.textContentType = .password
        super.viewDidLoad()
        
                  if #available(iOS 13.0, *) {
                      overrideUserInterfaceStyle = .light
                  }
                  
        
                if (UserDefaults.standard.value(forKey: "home") != nil){
                    var story : UIStoryboard?
                               
                          if IS_IPHONE {
                               story = UIStoryboard.init(name: "Main", bundle: nil)
                              }else{
                               story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                          }
                    let TabbarController = story?.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
                    if appdelegate.isfromshortcut == true {
                    if appdelegate.isfromshortcutidentifir == "Capture" {
                            TabbarController.selectedIndex = 0

                            
                    } else if appdelegate.isfromshortcutidentifir == "Pilot" {
                        TabbarController.selectedIndex = 1

                            
                    }else if appdelegate.isfromshortcutidentifir == "Library" {
                          TabbarController.selectedIndex = 2

                            
                    }else{
                            TabbarController.selectedIndex = 3

                    }
                        
                    }else{
                        TabbarController.selectedIndex = 1

                    }
                   // TabbarController.selectedIndex = 1
                    self.navigationController?.pushViewController(TabbarController, animated: false)
                }else{
                    imgback.isHidden = true
                    logoback.isHidden = true
               }
        
            if (UserDefaults.standard.value(forKey: "server") == nil){
              //  Editor.saveaudio()
                
               
                         
                         



                     
//                     do {
//                let bundleDirectory = Bundle.main.bundlePath
//                       let audio_inputFilePath = URL(fileURLWithPath: bundleDirectory).appendingPathComponent("newaudio.mp3").path
//                       let audio_inputFileUrl = URL(fileURLWithPath: audio_inputFilePath)
//
//                         let audioData = NSData(contentsOf: audio_inputFileUrl)
//
//               let newURL = self.getDocumentsDirector().appendingPathComponent("newaudio.m4a")
//                        try audioData?.write(to: newURL, options: .atomic)
//
//                }catch {
//                    print(error)
//
//                }
                
                
            UserDefaults.standard.set("https://latakoo.com/", forKey: "server")
            UserDefaults.standard.set("https://api2.latakoo.com/", forKey: "serverManifest")
            }
     
        self.roundcorner()
        
      
    
        
        
        txtEmail.attributedPlaceholder = NSAttributedString(string: "User ID/SSO",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        txtpassword.attributedPlaceholder = NSAttributedString(string: "Password",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])

        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
      func getDocumentsDirector() -> URL {
       
            let documentsURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            
              return documentsURL
        }
    
//    func devlogin(_ button: TransitionButton){
//        
//                 self.txtEmail.resignFirstResponder()
//                 self.txtpassword.resignFirstResponder()
//
//                      if self.txtEmail.text == "" {
//                                      self.view.makeToast("Please Enter your Email")
//                      }else if !(self.txtEmail.text? .isValidEmail())! {
//                                      self.view.makeToast("Please Enter valid Email")
//                      }else if self.txtpassword.text == ""{
//                                      self.view.makeToast("Please Enter your Password")
//                      }else{
//                          
//                          
//                                              if !Reachability.isConnectedToNetwork() {
//                              print("Internet is Not available.")
//                              AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
//                              return
//                          }
//                          
//                          
//                          button.startAnimation() // 2: Then start the animation when the user tap the button
//                          self.view.isUserInteractionEnabled = false
//                          let qualityOfServiceClass = DispatchQoS.QoSClass.background
//                          let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
//                          backgroundQueue.async(execute: {
//                              
//                              // 3: Do your networking task or background work here.
//                              
//                              DispatchQueue.main.async(execute: { () -> Void in
//                          
//                          var tokenVal : String!
//                          if UserDefaults.standard.value(forKey: "deviceTokenString") == nil{
//                              tokenVal = "C649C67E66101C370344466BE9B2388C86DEF9641F2F655B5D526653EEF1BBFA"
//                          }else{
//                              tokenVal = UserDefaults.standard.value(forKey: "deviceTokenString") as? String
//                              print("tokenvalue %@",tokenVal)
//                              
//                          }
//                          let appdelegate = UIApplication.shared.delegate as! AppDelegate
//                     //     appdelegate.ShowProgress()
//                                  
//                                  
//                      let header = ["User-Agent":"HTTP_USER_AGENT:Android"]
//                                  
//
//                          
//                          let aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=user.authenticate.x"
//                          let aDictParam : [String:Any]!
//                          aDictParam = ["email":self.txtEmail.text ?? "",
//                                        "password":self.txtpassword.text!
//                          ]
//                             print(aStrApi)
//                            print(aDictParam)
//
//                                  
//                          APIManager.requestPOSTURL(aStrApi, params: aDictParam as [String : AnyObject]?, headers: header, success: {(usrDetail) in
//                              print(usrDetail)
//                              
//                              
//                              
//                              self.view.isUserInteractionEnabled = true
//
//                             
//                              
//                              if usrDetail["status_code"].rawString() == "0"{
//                                  button.backgroundColor = UIColor.white
//                                  
//                                  button.stopAnimation(animationStyle: .expand, completion: {
//                                      
//                                  
//                                  let usrDetailVal = usrDetail["result"].rawValue as! [String:Any]
//                                  print(usrDetailVal)
//                                      
//                                      
//                                      UserDefaults.standard.set(usrDetailVal, forKey: "userdetails")
//
//                                  let networks  = usrDetailVal["available_networks"] as! [Any]
//                                  print(networks)
//                                      
//                                      let NoificationStatus  = usrDetailVal["notification_preferences"] as! [Any]
//                                      
//                                      UserDefaults.standard.set(NoificationStatus, forKey: "notification_preferences")
//                                      
//                                      let boolvalue : Bool = false
//                                      UserDefaults.standard.set(boolvalue, forKey: "allnotification")
//                                       UserDefaults.standard.set(boolvalue, forKey: "myuploads")
//
//                                     UserDefaults.standard.set(networks, forKey: "AvailableNetworks")
//                                      
//                                      DispatchQueue.global(qos: .background).async {
//                                             var theJSONTextvalue :String = ""
//                                                                       if let theJSONData = try?  JSONSerialization.data(
//                                                                           withJSONObject: networks,
//                                                                           options: .prettyPrinted
//                                                                           ),
//                                                                           let theJSONText  = String(data: theJSONData,
//                                                                                                     encoding: String.Encoding.ascii) {
//                                                                           print(theJSONText)
//                                                                           theJSONTextvalue = theJSONText
//                                                                           print(theJSONTextvalue)
//                                                                           self.insetincoredata(theJSONTextvalue)
//                                                          }
//                                          
//                                              }
//                                              
//                                    let defultnetwork = usrDetailVal["defaultNetwork"] as! String
//                                    let organizationid = usrDetailVal["organization_id"] as! String
//                                     print("default network is => \(defultnetwork)")
//                                      print("organization id  is => \(organizationid)")
//                                      UserDefaults.standard.set(organizationid, forKey:"organization_id")
//                                     UserDefaults.standard.set(defultnetwork, forKey:"defultnetwork")
//                                      let parts = (usrDetailVal["authcode"] as AnyObject).components(separatedBy: ":")
//                                      let arrhomenetwork : [String] = []
//                                      let arrselectednetwork : [String] = []
//                                      UserDefaults.standard.set("All", forKey:"filterednetwork")
//                                      UserDefaults.standard.set(arrhomenetwork, forKey:"homenetwork")
//                                      UserDefaults.standard.set(arrselectednetwork, forKey:"selectedfilterNetwork")
//                                      UserDefaults.standard.set(self.txtEmail.text, forKey: "Email")
//                                      UserDefaults.standard.set(self.txtpassword.text, forKey: "Password")
//                                      UserDefaults.standard.set(parts[0], forKey: "Authcode")
//                                      UserDefaults.standard.set(parts[1], forKey: "userId")
//                                     UserDefaults.standard.synchronize()
//                                     let appdelegate = UIApplication.shared.delegate as! AppDelegate
//                                    appdelegate.HideProgress()
//                                    let story =  UIStoryboard(name: "Main", bundle: nil)
//                                      UserDefaults.standard.set("home", forKey: "home")
//                                      UserDefaults.standard.set("0", forKey: "capturetype")
//                                      UserDefaults.standard.synchronize()
//                                     let TabbarController = story.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
//                                    TabbarController.selectedIndex = 1
//                                    self.navigationController?.pushViewController(TabbarController, animated: false)
//                                  
//                                  })
//                              }else{
//                                  
//                                  button.stopAnimation()
//                                  self.txtEmail.text = ""
//                                  self.txtpassword.text = ""
//                                  self.view.makeToast(usrDetail["message"].rawString())
//                                  let appdelegate = UIApplication.shared.delegate as! AppDelegate
//                                  appdelegate.HideProgress()
//                              }
//                              
//                          },
//                                                    
//                                                    failure: {(error) in
//                                                      self.view.isUserInteractionEnabled = true
//                                                   self.view.makeToast("The request timed out.")
//
//                                                      print(error)
//                                                      appdelegate.HideProgress()
//                                                      button.stopAnimation()
//                                                      
//                                   })
//                          
//                              })
//                          })
//                      }
//              
//          
//        
//        
//    }
    
    
    @IBAction func Login(_ button: TransitionButton){
        
        
        
           self.txtEmail.resignFirstResponder()
           self.txtpassword.resignFirstResponder()

                if self.txtEmail.text == ""{
                                self.view.makeToast("Please Enter your User ID/SSO")
                }
             //       else if !(self.txtEmail.text? .isValidEmail())! {
//                                self.view.makeToast("Please Enter valid Email")
//                }
                    else if self.txtpassword.text == ""{
                                self.view.makeToast("Please Enter your Password")
                }else{
                    
                    
                    if !Reachability.isConnectedToNetwork() {
                        print("Internet is Not available.")
                        AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
                        return
                    }
                    
                  
                    
                    button.startAnimation() // 2: Then start the animation when the user tap the button
                    self.view.isUserInteractionEnabled = false
                    let qualityOfServiceClass = DispatchQoS.QoSClass.background
                    let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
                    backgroundQueue.async(execute: {
                        
                        // 3: Do your networking task or background work here.
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                    
                    var tokenVal : String!
                    if UserDefaults.standard.value(forKey: "deviceTokenString") == nil{
                        tokenVal = "C649C67E66101C370344466BE9B2388C86DEF9641F2F655B5D526653EEF1BBFA"
                    }else{
                        tokenVal = UserDefaults.standard.value(forKey: "deviceTokenString") as? String
                        print("tokenvalue %@",tokenVal)
                        
                    }
                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
               //     appdelegate.ShowProgress()
                            
                  let headers = ["User-Agent":"HTTP_USER_AGENT:Android",
                                 "os_type": "IOS",
                                 "os_version": "IOS14",
                                 "app_version": "3.1.3"]
                //https://api2.latakoo.com/auth/sign-in?email=bharat.p%40parangat.com&password=ritu%40123

                    let aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)auth/sign-in"
                    let aDictParam : [String:Any]!
                    aDictParam = ["email":self.txtEmail.text ?? "",
                                  "password":self.txtpassword.text!
                    ]
                            
                    print(aStrApi)
                    print(aDictParam)
                            print(headers)


                            
                            let encoding = Alamofire.JSONEncoding.default
                                   Alamofire.request(aStrApi, method: .post, parameters: aDictParam, encoding:encoding, headers: headers).responseJSON { (responseObject) -> Void in
                                                           
                                                           print(responseObject)
                                                           
                                                           if responseObject.result.isSuccess {
                                                               let resJson = JSON(responseObject.result.value!)
                                                            let usrDetail = resJson
                                                               print(resJson)
                                                                                    
                                                                                          
                                                                                            
                                                                                            
                                                               if usrDetail["result"].exists() {
                                                                
                                                                
                                                                self.view.isUserInteractionEnabled = true
                                                                
                                                                        button.backgroundColor = UIColor.white
                                                                        
                                                                        button.stopAnimation(animationStyle: .expand, completion: {

                                                let usrDetailVal = usrDetail["result"].rawValue as! [String:Any]
                                                                                        print(usrDetailVal)
                                                                                            
                                                                                            
                                                                                            UserDefaults.standard.set(usrDetailVal, forKey: "userdetails")

                                                                                        let networks  = usrDetailVal["available_networks"] as! [Any]
                                                                                        print(networks)
                                                                                            
                                                                            var NoificationStatus  = usrDetailVal["notification_preferences"] as! [Any]
                                                                            
                                                                let dict : [String:Any] = ["name": "All Networks" , "id":"0" , "notify":false]
                                                                                  NoificationStatus.insert(dict, at: 0)
                                                                                            
                                                                                            UserDefaults.standard.set(NoificationStatus, forKey: "notification_preferences")
                                                                                            
                                                                                            let boolvalue : Bool = false
                                                                                            UserDefaults.standard.set(boolvalue, forKey: "allnotification")
                                                                                             UserDefaults.standard.set(boolvalue, forKey: "myuploads")

                                                                                           UserDefaults.standard.set(networks, forKey: "AvailableNetworks")
                                                                                            
                                                          
                                                                                                    
                                                                                          let defultnetwork = usrDetailVal["defaultNetwork"] as! String
                                                                                          let organizationid = usrDetailVal["organization_id"] as! String
                                                                                           print("default network is => \(defultnetwork)")
                                                                                            print("organization id  is => \(organizationid)")
                                                                                            UserDefaults.standard.set(organizationid, forKey:"organization_id")
                                                                                           UserDefaults.standard.set(defultnetwork, forKey:"defultnetwork")
                                                                                            let parts = (usrDetailVal["authcode"] as AnyObject).components(separatedBy: ":")
                                                                                            let arrhomenetwork : [String] = []
                                                                                            let arrselectednetwork : [String] = []
                                                                                            UserDefaults.standard.set("All", forKey:"filterednetwork")
                                                                                            UserDefaults.standard.set(arrhomenetwork, forKey:"homenetwork")
                                                                                            UserDefaults.standard.set(arrselectednetwork, forKey:"selectedfilterNetwork")
                                                                     
                                                                           
                                                                            
                                                                  let stremail = usrDetailVal["email"] as! String
                                                                            
                                                                     print(stremail)
                                                                            UserDefaults.standard.set(stremail, forKey: "Email")
                                                                            
                                                                            
                                                                                            UserDefaults.standard.set(self.txtpassword.text, forKey: "Password")
                                                                                            UserDefaults.standard.set(parts[0], forKey: "Authcode")
                                                                                            UserDefaults.standard.set(parts[1], forKey: "userId")
                                                                                            var manifest: String? = nil
                                                                                                   if let value = usrDetailVal["manifest"]{
                                                                                                       manifest = "\(value)"
                                                                                                   }

                                                                                                   if (manifest == "0") {
                                                                                                       UserDefaults.standard.set("NO", forKey: "isshowmenifest")
                                                                                                   } else {
                                                                                                       UserDefaults.standard.set("YES", forKey: "isshowmenifest")
                                                                                                   }
                                                                                            
                                                                                           UserDefaults.standard.synchronize()
                                                                                          appdelegate.HideProgress()
                                                                            
                                                                            var story : UIStoryboard?
                                                                                       
                                                                                  if IS_IPHONE {
                                                                                       story = UIStoryboard.init(name: "Main", bundle: nil)
                                                                                      }else{
                                                                                       story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                                                                                  }
                                                                            
                                                                                            UserDefaults.standard.set("home", forKey: "home")
                                                                                            UserDefaults.standard.set("0", forKey: "capturetype")
                                                                            
                                                    let RefreshToken = usrDetailVal["refreshToken"] as? String
                                                        let AuthToken = usrDetailVal["token"] as? String
                                                                UserDefaults.standard.setValue(AuthToken, forKey: "Authtoken")
                                                                                UserDefaults.standard.setValue(RefreshToken, forKey: "RefreshToken")
                                                                                            UserDefaults.standard.synchronize()
                                                                                            DispatchQueue.main.async {
                                                                                           let TabbarController = story?.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
                                                                                          TabbarController.selectedIndex = 1
                                                                                          self.navigationController?.pushViewController(TabbarController, animated: false)

                                                                                                
                                                                                                
                                                                                          
                                                                                          
                                                                                            }
                                                                                            
                                                                      })                        }else{
                                                                                               
                                                                                                self.view.isUserInteractionEnabled = true
                                                                                                                                                                                                         button.stopAnimation()
                                                                                                                                                                                                         self.txtEmail.text = ""
                                                                                                                                                                                                         self.txtpassword.text = ""
                                                                                                                                                                                                         let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                                                                                                                                                                                         appdelegate.HideProgress()
                                                                                                                                                                                                   _ = SCLAlertView().showError("Error", subTitle:"A user could not be found which matches the credentials provided", closeButtonTitle:"OK")
                                                                                                
                                                                    }
                                                                                      
                                                            }

                                                           
                                                           if responseObject.result.isFailure {
                                                              self.view.isUserInteractionEnabled = true
                                                                                                          button.stopAnimation()
                                                                                                          self.txtEmail.text = ""
                                                                                                          self.txtpassword.text = ""
                                                                                                          let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                                                                                          appdelegate.HideProgress()
                                                                                                    _ = SCLAlertView().showError("Error", subTitle:"A user could not be found which matches the credentials provided", closeButtonTitle:"OK")



                                                           }
                                                       }
                            

                    
                        })
                    })
                }
        
    }
    
    
    
//    func newauth() {
//
//        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
//        manager.responseSerializer = AFHTTPResponseSerializer()
//        manager.requestSerializer = AFJSONRequestSerializer()
//
//        let aStrApi = "https://api2.latakoo.com/auth/sign-in"
//                         let aDictParam : [String:Any]!
//                         aDictParam = ["email":self.txtEmail.text ?? "",
//                                       "password":self.txtpassword.text!
//                         ]
//
//                         print(aStrApi)
//
//
//        manager.post(aStrApi, parameters: aDictParam, headers:nil, progress: nil, success: { task, responseObject in
//
//
//
//        }, failure: { operation, error in
//
//            print("\(error.localizedDescription ?? "")")
//
//        })
//    }

    
    @IBAction func moreinfo(_ sender: Any) {
        
        var story : UIStoryboard?
                   
              if IS_IPHONE {
                   story = UIStoryboard.init(name: "Main", bundle: nil)
                  }else{
                   story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
              }
        let MoreInfoVC = story?.instantiateViewController(withIdentifier: "MoreInfoVC") as! MoreInfoVC
        self.navigationController?.pushViewController(MoreInfoVC, animated: false)
   
    }
    
    @IBAction func Forogotpassword(_ sender: Any) {
        
        let appearance = SCLAlertView.SCLAppearance(
                kTextFieldHeight: 50,
                showCloseButton: false,
                dynamicAnimatorActive: true,
                buttonsLayout: .horizontal
                
              )
              let alert = SCLAlertView(appearance: appearance)
              let txt = alert.addTextField("Enter your email")
        
              _ = alert.addButton("Cancel") {

                     }
        
              _ = alert.addButton("Done") {
                  print("Text value: \(txt.text ?? "NA")")
                self.serviceforgotpassword(email: txt.text ?? "")

              }
              _ = alert.showEdit("Email", subTitle:"Please enter your email address")
    }
    
    
    
    @IBAction func Advanced(_ sender: Any) {
       
               let appearance = SCLAlertView.SCLAppearance(
                        kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                        kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                        kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                        showCloseButton: false,
                        dynamicAnimatorActive: true,
                        buttonsLayout: .horizontal
                        
                    )
                     let alert = SCLAlertView(appearance: appearance)
            
        _ = alert.addButton("DEV"){
                      
                        UserDefaults.standard.set("https://dev.latakoo.com/", forKey: "server")
                  UserDefaults.standard.set("https://dev.api2.latakoo.com/", forKey: "serverManifest")

                        
                    }
                    _ = alert.addButton("LIVE") {
                       
                        UserDefaults.standard.set("https://latakoo.com/", forKey: "server")
                        UserDefaults.standard.set("https://api2.latakoo.com/", forKey: "serverManifest")



                    }
                    
               UserDefaults.standard.synchronize()
                    let icon = UIImage(named:"server.png")
                    let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
                    _ = alert.showCustom("Server", subTitle: "Select api server", color: color, circleIconImage: icon!)
            }
       
        
    
    
    
    
    
    
    
    func roundcorner(){
        
        btnsignin.layer.cornerRadius = 5
        btnsignin.layer.masksToBounds = true
//        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email",
//                                                            attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
//
//        txtpassword.attributedPlaceholder = NSAttributedString(string: "Password",
//                                                               attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
    }
    
    @IBAction func btneye(_ sender: Any) {
        if flag == 0 {
            flag = 1
            btnEye.setImage(UIImage(named:"eye_open"), for: UIControlState.normal)
            txtpassword.isSecureTextEntry  = false
            
        }else{
            flag = 0
            btnEye.setImage(UIImage(named:"hide_eye"), for: UIControlState.normal)
            txtpassword.isSecureTextEntry  = true
        }
    }
    
    
    func serviceforgotpassword(email:String){
        
        if email == ""{
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Email can't be blank", hideAfter: 2.0)
            return
        }
        
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 2.0)
            return
        }
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.ShowProgress()
        var aStrApi = ""
       
            aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=user.forgotPassword&email=\(email)"
        
        
        APIManager.requestGETURL(aStrApi, success: {(usrDetail) in
            print(usrDetail)
            if usrDetail["status_code"].rawString() == "0"{
               
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.HideProgress()
                print("Internet is Not available.")
                let appearance = SCLAlertView.SCLAppearance(dynamicAnimatorActive: true)
                _ = SCLAlertView(appearance: appearance).showNotice("LATAKOO", subTitle:"New Password sent on given email address" , closeButtonTitle:"OK")
                
            }else{
                
                _ = SCLAlertView().showError("LATAKOO", subTitle:"User email is not registered", closeButtonTitle:"OK")
                   appdelegate.HideProgress()
            }
            
        }, failure: {(error) in
            print(error)
            appdelegate.HideProgress()
            _ = SCLAlertView().showError("LATAKOO", subTitle:error.localizedDescription, closeButtonTitle:"OK")
            
            
        })
        
    }
    
    
    
    
    
    func insetincoredata(_ networklist : String){
        
        DispatchQueue.main.async{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Network", in: context)
        let network = NSManagedObject(entity: entity!, insertInto: context)
        network.setValue(networklist, forKey: "network_list")
        
        do {
            try context.save()
            print("successfully save")

        } catch {
            print("Failed saving")
        }
        
        }
    }
    

}

extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}

