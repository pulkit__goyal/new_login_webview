//
//  MoreInfoVC.swift
//  Latakoo
//
//  Created by Shruti Aggarwal on 21/08/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import UIKit

class MoreInfoVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func moreinfoVC(_ sender: Any) {
    self.navigationController?.popViewController(animated: false)
    
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    
    @IBAction func emailbutton(_ sender: Any) {
  
        guard let url = URL(string: "https://latakoo.com/-/support/") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
