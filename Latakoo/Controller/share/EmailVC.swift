//  EmailVC.swift
//  Latakoo
//  Created by Shruti Aggarwal on 03/10/19.
//  Copyright © 2019 parangat technology. All rights reserved.



import UIKit
import Contacts

class EmailCell: UITableViewCell {
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblcheck: UIImageView!
}

class EmailVC: UIViewController,UITableViewDataSource,UITableViewDelegate,ContactsPickerDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Arremail.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! EmailCell
        
        cell.lblname.text = Arremail[indexPath.row]
        
        if !myArray.contains(Arremail[indexPath.row] ){
            cell.lblcheck.isHidden = true
        }else{
            cell.lblcheck.isHidden = false
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !myArray.contains(Arremail[indexPath.row] ){
            myArray.append(Arremail[indexPath.row])
        }else{
            if let index = myArray.index(of:Arremail[indexPath.row]) {
            myArray.remove(at: index)
        }
        }
        tableView.reloadData()
    }
    
    
    @IBOutlet weak var tableview: UITableView!
    var isfromshare: NSString!
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var txtemaill: UITextView!
    @IBOutlet weak var lblemail: UILabel!
    @IBOutlet weak var btntaptoadd: UIButton!
    @IBOutlet weak var viewaddemail: UIView!
    
    var contactRecords : [String] = []
    var myArray : [String] = []
    var Arremail : [String] = []
    let App = UIApplication.shared.delegate as! AppDelegate
    var kNameKey: NSString = "personName"
    var kEmailKey: NSString = "personEmail"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UserDefaults.standard.value(forKey: "emailarray") != nil){
           
            Arremail = UserDefaults.standard.value(forKey: "emailarray") as! [Any] as! [String]

        }
        
        if App.ArrEmail.count>0{
            myArray = App.ArrEmail as! [String]
        }
        
        
        tableview.reloadData()
        
        txtemail.attributedPlaceholder = NSAttributedString(string: "Email",
               attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        // Do any additional setup after loading the view.
    }
    

    @IBAction func emailaction(_ sender: Any) {
        txtemaill.text = txtemail.text
        if (txtemail.text == "") {
            tableview.isHidden = false
            viewaddemail.isHidden = true
        } else if !(self.txtemail.text? .isValidEmail())!{
            lblemail.isHidden = false
            btntaptoadd.isHidden = false
            lblemail.text = "Please enter a valid email address"
            lblemail.textColor = UIColor.red
            tableview.isHidden = true
            viewaddemail.isHidden = false
            btntaptoadd.isHidden = true
        } else {
            lblemail.isHidden = false
            btntaptoadd.isHidden = false
            lblemail.text = "Tap to add email address"
            lblemail.textColor = UIColor.darkGray
            tableview.isHidden = true
            viewaddemail.isHidden = false
            btntaptoadd.isHidden = false
        }
        
    }
    
    
    @IBAction func taptoadd(_ sender: Any) {
        
        
        if Arremail.contains(txtemail.text!) {
            
            if myArray.contains(txtemail.text!) {
                
                txtemail.resignFirstResponder()
                txtemail.text = ""
                txtemaill.text = ""
                viewaddemail.isHidden = true
                tableview.isHidden = false
                txtemail.resignFirstResponder()
            } else {
                myArray.append(txtemail.text!)
                tableview.reloadData()
                txtemail.resignFirstResponder()
                txtemail.text = ""
                txtemaill.text = ""
                viewaddemail.isHidden = true
                tableview.isHidden = false
                txtemail.resignFirstResponder()
            }
        } else {
            Arremail.append(txtemail.text!)
            myArray = Arremail
            //  [myArray addObject:_txtemail.text];
            tableview.reloadData()
            txtemail.resignFirstResponder()
            UserDefaults.standard.set(Arremail, forKey: "emailarray")
            txtemail.text = ""
            txtemaill.text = ""
            viewaddemail.isHidden = true
            tableview.isHidden = false
            txtemail.resignFirstResponder()
        }
        
     
    }
    
    
    @IBAction func btnback(_ sender: Any) {
        App.ArrEmail = myArray
      self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnaddcontact(_ sender: Any) {
        
        
        let contactPickerScene = ContactsPicker(delegate: self, multiSelection: true, subtitleCellType: SubtitleCellValue.email)
           let navigationController = UINavigationController(rootViewController: contactPickerScene)
          navigationController.modalPresentationStyle = UIModalPresentationStyle.fullScreen
           self.present(navigationController, animated: true, completion: nil)
        
    }
    
    
    
    
    
  
//MARK: EPContactsPicker delegates
    func contactPicker(_: ContactsPicker, didContactFetchFailed error: NSError) {
        print("Failed with error \(error.description)")
    }
    
    func contactPicker(_: ContactsPicker, didSelectContact contact: Contact) {
        print("Contact \(contact.displayName) has been selected")
    }
    
    func contactPickerDidCancel(_ picker: ContactsPicker) {
        picker.dismiss(animated: true, completion: nil)
        print("User canceled the selection");
    }
    
    func contactPicker(_ picker: ContactsPicker, didSelectMultipleContacts contacts: [Contact]) {
        defer { picker.dismiss(animated: true, completion: nil) }
        guard !contacts.isEmpty else { return }
        for contact in contacts {
            if contact.emails.count>0 {
            print("contact email is => \(contact.emails[0].email)")
                  Arremail.append(contact.emails[0].email)
                         myArray = Arremail
                         tableview.reloadData()
                         UserDefaults.standard.set(Arremail, forKey: "emailarray")
                         tableview.isHidden = false
                
            }
        }
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
}
