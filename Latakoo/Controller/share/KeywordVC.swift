//
//  KeywordVC.swift
//  Latakoo
//
//  Created by Mayur Sardana on 21/01/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//

import UIKit

class KeywordVC: UIViewController,UITableViewDataSource,UITableViewDelegate,ContactsPickerDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Arrkeyword.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! EmailCell
        
        cell.lblname.text = Arrkeyword[indexPath.row]
        
        if !myArray.contains(Arrkeyword[indexPath.row] ){
            cell.lblcheck.isHidden = true
        }else{
            cell.lblcheck.isHidden = false
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !myArray.contains(Arrkeyword[indexPath.row] ){
            myArray.append(Arrkeyword[indexPath.row])
        }else{
            myArray.remove(at: indexPath.row)
        }
        tableView.reloadData()
    }
    
    
    @IBOutlet weak var tableview: UITableView!
    var isfromshare: NSString!
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var txtemaill: UITextView!
    @IBOutlet weak var lblemail: UILabel!
    @IBOutlet weak var btntaptoadd: UIButton!
    @IBOutlet weak var viewaddemail: UIView!
    var contactRecords : [String] = []
    var myArray : [String] = []
    var Arrkeyword : [String] = []
    let App = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UserDefaults.standard.value(forKey: "keywordarray") != nil){
            Arrkeyword = UserDefaults.standard.value(forKey: "keywordarray") as! [Any] as! [String]
        }
        
       if App.ArrKeywords.count>0{
            myArray = App.ArrEmail as! [String]
        }
        
        txtemail.attributedPlaceholder = NSAttributedString(string: "Enter Tag",
               attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        tableview.reloadData()
    }
    

    @IBAction func emailaction(_ sender: Any) {
        txtemaill.text = txtemail.text
        if (txtemail.text == "") {
            tableview.isHidden = false
            viewaddemail.isHidden = true
        }
         else {
            lblemail.isHidden = false
            btntaptoadd.isHidden = false
            lblemail.text = "Tap to add tag"
            lblemail.textColor = UIColor.darkGray
            tableview.isHidden = true
            viewaddemail.isHidden = false
            btntaptoadd.isHidden = false
        }
        
    }
    
    
    @IBAction func taptoadd(_ sender: Any) {
        
        if Arrkeyword.contains(txtemail.text!) {
            if myArray.contains(txtemail.text!) {
                txtemail.resignFirstResponder()
                txtemail.text = ""
                txtemaill.text = ""
                viewaddemail.isHidden = true
                tableview.isHidden = false
                txtemail.resignFirstResponder()
            } else {
                myArray.append(txtemail.text!)
                tableview.reloadData()
                txtemail.resignFirstResponder()
                txtemail.text = ""
                txtemaill.text = ""
                viewaddemail.isHidden = true
                tableview.isHidden = false
                txtemail.resignFirstResponder()
            }
        } else {
            Arrkeyword.append(txtemail.text!)
            myArray = Arrkeyword
            //  [myArray addObject:_txtemail.text];
            tableview.reloadData()
            txtemail.resignFirstResponder()
            UserDefaults.standard.set(Arrkeyword, forKey: "keywordarray")
            txtemail.text = ""
            txtemaill.text = ""
            viewaddemail.isHidden = true
            tableview.isHidden = false
            txtemail.resignFirstResponder()
        }
        
     
    }
    
    
    @IBAction func btnback(_ sender: Any) {
        App.ArrKeywords = myArray
      self.navigationController?.popViewController(animated: true)
    }
    
    
   
    override var prefersStatusBarHidden: Bool {
        return false
    }
}
