//
//  ShareVC.swift
//  Latakoo
//
//  Created by Shruti Aggarwal on 03/10/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import UIKit

class ShareVC: UIViewController {
    @IBOutlet weak var txtmsg: UITextView!
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var authenticate: UISwitch!
    @IBOutlet weak var reshare: UISwitch!
    var videoid: String!
    var authcode : String!
    var Reshare : String!
    var ArrEmail : [String] = []
    let App = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        authcode = "1"
        Reshare = "0"
        App.ArrKeywords = []
        App.ArrEmail = []
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if App.ArrEmail.count == 1 {
            txtemail.text = "\(App.ArrEmail[0])"
        } else if App.ArrEmail.count > 1 {
            txtemail.text = String(format: "%@ + %lu", App.ArrEmail[0] as! String, App.ArrEmail.count - 1)
        } else {
            txtemail.text = ""
        }
    }

    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func NextButton(_ sender: Any) {
        if App.ArrEmail.count>0 {
            self.ServiceShareemail()
        }else{
            self.view.makeToast("Please add at least one email")
        }
        
    }
    
    @IBAction func addemail(_ sender: Any) {
        
        var story : UIStoryboard?
        if IS_IPHONE {
             story = UIStoryboard.init(name: "Main", bundle: nil)
            }else{
             story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
        }
        let vc = story?.instantiateViewController(withIdentifier: "EmailVC") as! EmailVC
        self.navigationController?.pushViewController(vc, animated: true)
    
    }
    
    @IBAction func authswitch(_ sender: Any) {
        if authcode == "1" {
            authcode = "0"
        }else{
            authcode = "1"
        }
    }
    
    
    @IBAction func reshareswitch(_ sender: Any) {
        if Reshare == "0" {
           Reshare = "1"
        }else{
           Reshare = "0"
        }
    }
    
    
    func ServiceShareemail(){
        
        var jsonData: Data? = nil
        do {
            jsonData = try JSONSerialization.data(withJSONObject: App.ArrEmail, options: .prettyPrinted)
        } catch {
        }
        var jsonString: String? = nil
        if let jsonData = jsonData {
            jsonString = String(data: jsonData, encoding: .utf8)
        }
        
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.ShowProgress()
        let aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=cloudvideo.fileShareNotify"
        

        var aDictParam : [String:Any] = [:]
        aDictParam = [
            "params[user_id]": UserDefaults.standard.value(forKey:"userId") ?? "",
            "params[video_id]": videoid,
            "params[message]":  txtmsg.text,
            "params[public]":  authcode,
            "params[reshare]":  Reshare,
            "params[recipient_emails]": jsonString!,
        ]
        print(aDictParam)
        let heder =  appdelegate.getAPIEncodedHeader(withParameters: aStrApi)
        let headervalue = ["X_PFTP_API_TOKEN":heder]
        APIManager.requestPOSTURL(aStrApi, params: aDictParam as [String : AnyObject]?, headers: headervalue as? [String : String], success: {(usrDetail) in
            print(usrDetail)
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.HideProgress()
            let appearance = SCLAlertView.SCLAppearance(
                                                         kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                                                         kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                                                         kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                                                         showCloseButton: false,
                                                         dynamicAnimatorActive: true,
                                                         buttonsLayout: .horizontal
                               )

                               let alert = SCLAlertView(appearance: appearance)
                               _ = alert.addButton("OK"){
                                   
                               self.navigationController?.popViewController(animated: true)
                               }
                               _ = alert.showSuccess("LATAKOO", subTitle:"Shared successfully.")
            
        },
                                  
                    failure: {(error) in
                     print(error)
                    appdelegate.HideProgress()
        })
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
}
