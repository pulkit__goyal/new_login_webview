//
//  LibraryAudioVC.swift
//  Latakoo
//
//  Created by Mac on 30/07/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import UIKit
import MobileCoreServices
import CoreData
import AVFoundation
import AVKit

class LibraryAudioVC: UIViewController {
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var lblaudiotime: UILabel!
    @IBOutlet weak var btnplay: UIButton!
    @IBOutlet weak var lblaudio: UILabel!
     var flagforAudio = 0
    @IBOutlet weak var btnplayaudio: UIButton!
    @IBOutlet weak var audioslider: UISlider!
    @IBOutlet weak var lblheader: UILabel!
    @IBOutlet weak var viewaudio: UIView!
    @IBOutlet weak var viewsavedelete: UIView!

    var Audiopath : URL!
    var playbackTimer1: Timer?
    var audioPlayer: AVAudioPlayer?
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    var headerstring: String = ""
    var isfromcapture: String = ""

    var audio:[String] = []
    var currentindex : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = false
        lblheader.text = headerstring

        if isfromcapture == "NO"{
        self.viewsavedelete.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnback(_ sender: Any) {
   
   self.navigationController?.popViewController(animated: true)
    
    
//        let story =  UIStoryboard(name: "Main", bundle: nil)
//              let captureVC = story.instantiateViewController(withIdentifier: "captureVC") as! captureVC
//              captureVC.hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(captureVC, animated: false)
    }
    
    
   func randomString(length: Int) -> String {
      let letters = "1234567890"
      return String((0..<length).map{ _ in letters.randomElement()! })
   }
    
    @IBAction func sendbutton(_ sender: Any) {
             var  arr : [String] = []
             var newURL : URL?
             newURL =  Audiopath
         
        if headerstring == "" {
                do{
                     newURL = self.getDocumentsDirector().appendingPathComponent("\(self.randomString(length: 10)).m4a")

            
                    try FileManager.default.moveItem(at: self.Audiopath, to: newURL!)
                    }
                    catch let error{
                        // Handle any errors here
                        dump(error)
                    }

        }
        
        
        
           arr.append(newURL!.path)
              var story : UIStoryboard?
                         
                    if IS_IPHONE {
                         story = UIStoryboard.init(name: "Main", bundle: nil)
                        }else{
                         story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                    }
        appdelegate.isUploadingPause = false
               let SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedAudio") as! SelectedVC
               SelectedVideo.isfromview = "audio"
               SelectedVideo.assestarrayaudio = arr
              self.navigationController?.pushViewController(SelectedVideo, animated: true)
    }
    
    
    
    
    @IBAction func btncancel(_ sender: Any) {
            player?.pause()

        
        let alertController = UIAlertController(title:"Alert", message:"Are you sure you want to delete this audio", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Delete", style: .default, handler: { alert -> Void in
        
            self.navigationController?.popViewController(animated: true)
            
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        self.present(alertController, animated: true, completion: nil)
        
       
        
    }
    
    
    
    @IBAction func btnsave(_ sender: Any) {
        
        
        let appearance = SCLAlertView.SCLAppearance(
                                  kTextFieldHeight: 50,
                                  showCloseButton: false,
                                  dynamicAnimatorActive: true,
                                  buttonsLayout: .horizontal
                                  
                                )
                   
                   let alert = SCLAlertView(appearance: appearance)
                               let txt = alert.addTextField("Audio Name")
                         
                               _ = alert.addButton("Cancel") {

                                      }
                         
                               _ = alert.addButton("Save") {
                                  
                             DispatchQueue.main.async {

                                 
                                 self.viewsavedelete.isHidden = true
                                     self.lblheader.text = txt.text
                                self.headerstring = txt.text!
                               
                                 let newURL = self.getDocumentsDirector().appendingPathComponent("\(txt.text ?? "Untitled").m4a")
                                       do{
                                          
                                   
                                           try FileManager.default.moveItem(at: self.Audiopath, to: newURL)
                                           }
                                           catch let error{
                                               // Handle any errors here
                                               dump(error)
                                           }
                                 
                                
                                self.Audiopath = newURL
                                }
                               }
                               _ = alert.showEdit("Audio name", subTitle:"Save new audio recording as:")
             
             
             
        
        
        
      

    
    }
    
    @IBAction func slideraudio(_ sender: Any) {
    
    
    }
    
        func getDocumentsDirector() -> URL {
            // Grab CWD and set to paths
    //        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    //        return paths[0]
            
            let documentsURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            
              return documentsURL
        }
    
    func playAudio() {
        
        if (player==nil) {
            print(Audiopath!)
            
            let fileURL = Audiopath!
            player = AVPlayer(url: fileURL)
            playerLayer = AVPlayerLayer()
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.frame = view.bounds
            playerLayer?.videoGravity = .resizeAspectFill
            view.layer.addSublayer(playerLayer!)
            do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            }
            catch {
            // report for an error
            }
            
            let audioAsset = AVURLAsset(url:Audiopath!, options: nil)
            let audioDuration = audioAsset.duration
             let audioDurationSeconds = CGFloat(CMTimeGetSeconds(audioDuration))
            audioslider.maximumValue = Float(audioDurationSeconds)
            
            player!.actionAtItemEnd = .none
            NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: .AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
            
            playbackTimer1 = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(myMethod1(_:)), userInfo: nil, repeats: true)
            
        }
        if flagforAudio == 0 {
            btnplayaudio.setImage(UIImage.init(named:"pausebutton.png"), for:.normal)
            flagforAudio = 1;
            player?.play()
            
            
        }else{
            btnplayaudio.setImage(UIImage.init(named:"playbutton.png"), for:.normal)
            flagforAudio = 0;
            player?.pause()
            
        }
        
        if playbackTimer1 != nil {
            playbackTimer1?.invalidate()
        }
        playbackTimer1 = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(myMethod1(_:)), userInfo: nil, repeats: true)
        
    }
    
    
    @objc func myMethod1(_ timer: Timer?) {
        let currentItem = player!.currentItem
        let currentTime = CMTimeGetSeconds((currentItem?.currentTime())!)
        let f = Float(currentTime)
        var seconds = CGFloat(Int(CMTimeGetSeconds((currentItem?.currentTime())!)))
        let hour = seconds / 3600
        seconds = CGFloat(Int(seconds) % 3600)
        let min = seconds / 60
        seconds = CGFloat(Int(seconds) % 60)
        lblaudio.text = String(format:"%02d: %02d: %02d", Int(hour), Int(min), Int(seconds))
      //  audioslider.value = f
        audioslider.setValue(f, animated: true)
    }
    
    
    @objc func playerItemDidReachEnd(){
        player?.pause()
        flagforAudio = 0
      //  audioslider.value = 0
        audioslider.setValue(0, animated: true)
        lblaudio.text = "00:00:00"
        player=nil;
        playbackTimer1?.invalidate()
        btnplayaudio.setImage(UIImage.init(named:"playbutton.png"), for:.normal)
    }
    
    @IBAction func slider(_ sender: Any) {
        if player != nil {
               player!.seek(to: CMTimeMakeWithSeconds(Float64(audioslider!.value), 60000))
                     let curTime = player?.currentTime()
                     let seconds = CMTimeGetSeconds(curTime!)
                     print(seconds)
                   print(audioslider!.value)

               }
        
    }
    
    
    
    @IBAction func playaudio(_ sender: Any) {
        do {
           try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        }
        catch {
           // report for an error
        }
        
        
        
        self.playAudio()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        player?.pause()
        if IS_IPHONE {
            UIAppDelegate?.restrictRotation = .portrait
             }else{
            UIAppDelegate?.restrictRotation = .all
             }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if IS_IPHONE {
                   UIAppDelegate?.restrictRotation = .portrait
                    }else{
                   UIAppDelegate?.restrictRotation = .all
                    }
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    
}
