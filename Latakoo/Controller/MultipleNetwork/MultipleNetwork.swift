//  MultipleNetwork.swift
//  Latakoo
//
//  Created by Mayur Sardana on 26/12/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import UIKit


class MultipleNetwork: UIViewController,UITableViewDataSource,UITableViewDelegate {
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return networks.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
          
          let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! filtercell
        let dict = networks[indexPath.row]
          cell.lblnetwokname.text = dict["company_name"] as? String
          let companyid = "\(dict["company_id"] as? String ?? "")"
        if myArray.contains(companyid) {
            cell.imgtick.isHidden = false
            
        }else{
            cell.imgtick.isHidden = true
        }
          return cell
          
          
          
      }
      
      
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
           let dict = networks[indexPath.row] 
            let companyid = "\(dict["company_id"] as? String ?? "")"
        
            if let index = myArray.index(of:companyid ) {
                myArray.remove(at: index)
            }else{
                myArray.append(companyid)
            }
           tableView.reloadData()
    }
      
      @IBOutlet weak var tblnetwork: UITableView!
      var networks : [[String : Any]] = []
      var defaultnetwork : String = ""
      let App = UIApplication.shared.delegate as! AppDelegate
      var myArray : [String] = []
    
    var hideshowitemcode : Bool = false
    var itemcodecount = 0
   

    override func viewDidLoad() {
        super.viewDidLoad()
        
        defaultnetwork =  UserDefaults.standard.value(forKey:"defultnetwork") as! String
        
        
        if (App.ArrNetworks.count>0) {
               myArray = App.ArrNetworks;
           }else{
            myArray.append(defaultnetwork)
            
           }
                     
            
        
        


        // Do any additional setup after loading the view.
    }
    
     override func viewWillAppear(_ animated: Bool) {
            
           
        
        networks  = UserDefaults.standard.value(forKey:"AvailableNetworks") as! [[String : Any]]
                  print(networks)
              
                  networks = networks.sorted { ($0["company_name"] as! String).localizedCaseInsensitiveCompare($1["company_name"] as! String) == ComparisonResult.orderedAscending }
            tblnetwork.reloadData()
            
            
        }
        
        
        
        @IBAction func btnback(_ sender: Any) {
            
                                let networks = UserDefaults.standard.value(forKey: "AvailableNetworks") as? [Any] ?? []
            
            
                                     let  defaultnetwork =  UserDefaults.standard.value(forKey:"defultnetwork") as! String
                                           for dict in networks{
                                               let dictnetwork = dict as! [String:Any]
                                               print(dictnetwork)
                                               let networkid = dictnetwork["company_id"] as! String
                                                
                                               if myArray.contains(networkid) {
                                                let enableitemcode = dictnetwork["enable_itemcode"] as! String
                                                                         
                                                        if enableitemcode == "1" {
                                                        itemcodecount = itemcodecount+1
                                                         print(itemcodecount)
                                                        }
                                                 }
                                 }
            
            if itemcodecount == 1 {
                App.hideshowitemcode = false

            }else{
                App.hideshowitemcode = true
            }
          
            if (self.myArray.count>0) {
                    self.App.ArrNetworks = self.myArray
                }else{
                    myArray.append(defaultnetwork)
                   self.App.ArrNetworks = self.myArray
                }
                self.navigationController?.popViewController(animated: true)
        }
        
        
        
   override var prefersStatusBarHidden: Bool {
       return true
   }
        

}
