//
//  LibraryImagedetailsVC.swift
//  Latakoo
//
//  Created by Mac on 30/07/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import UIKit
import Photos
class smallcell : UICollectionViewCell{
    
    
    @IBOutlet weak var imgsmall: UIImageView!
}


class LibraryImagedetailsVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,EFImageViewZoomDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return image.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"cell", for: indexPath) as! smallcell
        
        let asset = image[indexPath.item] as! PHAsset
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        option.isNetworkAccessAllowed = true

        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
            cell.imgsmall.image = thumbnail
            
        })
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       // let asset = image[indexPath.item] as! PHAsset
      
        currentindex = indexPath.item
        App.ShowProgress()
        self.perform(#selector(loadimage), with: nil, afterDelay: 0.2)
        
     
        
    }
    
    @IBOutlet weak var mycollectionview: UICollectionView!
    @IBOutlet weak var bigimageview: EFImageViewZoom!
    @IBOutlet weak var imgsmaller: UIImageView!

    @IBOutlet weak var lblheader: UILabel!
    var image : [PHAsset]  = []
    var currentindex : Int!
    let App = UIApplication.shared.delegate as! AppDelegate

    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(image.count)
        mycollectionview.reloadData()
        bigimageview.contentMode = .left
        bigimageview._delegate = self
        App.ShowProgress()

        self.perform(#selector(loadimage), with: nil, afterDelay: 0.2)

       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(true)
       if IS_IPHONE {
        UIAppDelegate?.restrictRotation = .portrait
        }else{
        UIAppDelegate?.restrictRotation = .all
            }
    }
    
    @objc func loadimage(){
        
        let asset = image[currentindex] 
               let manager = PHImageManager.default()
               let option = PHImageRequestOptions()
               option.isNetworkAccessAllowed = true

               var thumbnail = UIImage()
               option.isSynchronous = true
               manager.requestImage(for: asset, targetSize:PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
                   let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                   self.bigimageview.image = thumbnail
                //  self.imgsmaller.image = thumbnail

                
                   self.App.HideProgress()

                   
               })
               let resources = PHAssetResource.assetResources(for: asset)
               let orgFilename = (resources[0]).originalFilename
               lblheader.text = orgFilename
               
    }
    
    @IBAction func backbutton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
//        let story =  UIStoryboard(name: "Main", bundle: nil)
//              let captureVC = story.instantiateViewController(withIdentifier: "captureVC") as! captureVC
//              captureVC.hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(captureVC, animated: false)
    }
    
    @IBAction func Send(_ sender: Any) {
    
        var  arr : [PHAsset] = []
        arr.append(image[currentindex] )
        var story : UIStoryboard?
                   
              if IS_IPHONE {
                   story = UIStoryboard.init(name: "Main", bundle: nil)
                  }else{
                   story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
              }
        appdelegate.isUploadingPause = false
        let SelectedImage = story?.instantiateViewController(withIdentifier: "SelectedImage") as! SelectedVC
        SelectedImage.isfromview = "image"
        SelectedImage.assestarray = arr
        self.navigationController?.pushViewController(SelectedImage, animated: true)
    
    
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return false
    }

}
