

import Photos
import UIKit
import SVProgressHUD

import Foundation
// import ImageCaptureCore
import MobileCoreServices

enum Upload_As: Int {
    case _individual = 0
    case _group = 1
    case _stitch = 2
}

class librarycell: UICollectionViewCell {
    @IBOutlet var imgthumb: UIImageView!
    @IBOutlet var imgvideo: UIImageView!
    @IBOutlet var lblduration: UILabel!
    @IBOutlet var lblvideoname: UILabel!
    @IBOutlet var lblvideoorintation: UILabel!
    
    @IBOutlet var overlayview: UIView!
    @IBOutlet var lblaudioname: UILabel!
    @IBOutlet var deletebutton: UIButton!
    @IBOutlet var imgcloud: UIImageView!
    @IBOutlet var imgbigcheck: UIImageView!
    @IBOutlet var lblcount: UILabel!
    @IBOutlet var viewselection: UIView!
}

@available(iOS 13.0, *)
class LibararyVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    private let refreshControl = UIRefreshControl()
    @IBOutlet var viewCircularProgressBar: CircularProgressBar!

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if flagmedia == 0 {
            return video.count
        } else if flagmedia == 1 {
            return image.count
        } else {
            return audio.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! librarycell
        cell.lblcount.layer.cornerRadius = cell.lblcount.frame.size.width / 2
        cell.lblcount.layer.borderColor = UIColor.white.cgColor
        cell.lblcount.layer.borderWidth = 1
        cell.lblcount.layer.masksToBounds = true
        cell.imgthumb.contentMode = .scaleToFill
        
        if flagmedia == 0 {
            cell.imgthumb.clipsToBounds = true
            cell.imgthumb.contentMode = .scaleAspectFill
            
            cell.overlayview.isHidden = false
            cell.lblduration.isHidden = false
            cell.lblvideoname.isHidden = false
            cell.imgvideo.isHidden = false
            cell.lblvideoname.textColor = UIColor.white
            cell.lblaudioname.isHidden = true
            cell.deletebutton.isHidden = true
            let asset = video[indexPath.item]
            
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            var thumbnail = UIImage()
            option.isSynchronous = true
            option.isNetworkAccessAllowed = true
            let width: CGFloat = collectionView.frame.size.width / 3
            DispatchQueue.global(qos: .background).async {
                manager.requestImage(for: asset, targetSize: CGSize(width: width, height: 110), contentMode: .aspectFill, options: option, resultHandler: { (result, _) -> Void in
                    
                    let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                    DispatchQueue.main.async {
                        cell.imgthumb.image = nil
                        cell.imgthumb.image = thumbnail
                    }
                })
            }
            
            cell.lblvideoname.text = ""
            DispatchQueue.global(qos: .background).async {
                let resources = PHAssetResource.assetResources(for: asset)
                let orgFilename = resources[0].originalFilename
                DispatchQueue.main.async {
                    cell.lblvideoname.text = orgFilename
                    if UserDefaults.standard.value(forKey: "renamedict") != nil {
                        let dict = UserDefaults.standard.value(forKey: "renamedict") as! [String: Any]
                        
                        if dict[asset.localIdentifier] != nil {
                            cell.lblvideoname.text = (dict[asset.localIdentifier] as! String)
                        }
                    }
                }
            }
            
            cell.lblduration.text = ""
            cell.lblvideoorintation.isHidden = false
            
            DispatchQueue.global(qos: .background).async {
                let option1 = PHVideoRequestOptions()
                option1.isNetworkAccessAllowed = true
                option1.deliveryMode = .highQualityFormat
                option1.version = .original
                option1.progressHandler = {  (progress, error, stop, info) in
                }
                
                var resultAsset: AVAsset?
                PHCachingImageManager.default().requestAVAsset(forVideo: asset, options: option1, resultHandler: { avasset, audioMix, info in
                    resultAsset = avasset
                    let duration = resultAsset?.duration
                    let durationTime = CMTimeGetSeconds(duration ??  CMTimeMake(1, 1))
                    
                    let urlAsset = avasset as? AVURLAsset
                  
                    let localVideoUrl = urlAsset?.url
                    
                                if localVideoUrl != nil {
                                            if Editor.ifvideoportrait(localVideoUrl!){
                                                DispatchQueue.main.async {
                                                    cell.lblvideoorintation.text = "VERTICAL"
                                                }
                                            } else {
                                                DispatchQueue.main.async {
                                                    cell.lblvideoorintation.text = "HORIZONTAL"
                                                }
                                            }
                                        }
                    
                    
                    DispatchQueue.main.async{
                        cell.lblduration.text = self.convertTime(Int(durationTime))
                    }
                })
            }
            

            
            let resourceArray = PHAssetResource.assetResources(for: asset)
            let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false // If this returns NO, then the asset is in iCloud and not saved locally yet
            if bIsLocallayAvailable {
                cell.imgcloud.isHidden = true
            } else {
                cell.imgcloud.isHidden = false
            }
            
            if FilesArray.contains(video[indexPath.item]) {
                cell.viewselection.isHidden = false
                cell.lblcount.isHidden = false
                cell.imgbigcheck.isHidden = false
                cell.lblcount.text = "\(FilesArray.index(of: video[indexPath.item])! + 1)"
            } else {
                cell.viewselection.isHidden = true
                cell.lblcount.isHidden = true
                cell.imgbigcheck.isHidden = true
            }
            
        } else if flagmedia == 1 {
            cell.imgthumb.clipsToBounds = true
            cell.imgthumb.contentMode = .scaleAspectFill
            cell.lblvideoorintation.isHidden = true
            
            cell.overlayview.isHidden = true
            cell.lblduration.isHidden = true
            cell.lblvideoname.isHidden = true
            cell.imgvideo.isHidden = true
            cell.lblaudioname.isHidden = true
            cell.deletebutton.isHidden = true
            
            let asset = image[indexPath.item]
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            var thumbnail = UIImage()
            
            option.isSynchronous = true
            option.isNetworkAccessAllowed = true
            
            let width: CGFloat = collectionView.frame.size.width / 3
            
            DispatchQueue.global(qos: .background).async {
                manager.requestImage(for: asset, targetSize: CGSize(width: width, height: 110), contentMode: .aspectFill, options: option, resultHandler: { (result, _) -> Void in
                    let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                    DispatchQueue.main.async {
                        cell.imgthumb.image = nil
                        cell.imgthumb.image = thumbnail
                    }
                })
            }
            
            let resourceArray = PHAssetResource.assetResources(for: asset)
            let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false // If this returns NO, then the asset is in iCloud and not saved locally yet
            if bIsLocallayAvailable {
                cell.imgcloud.isHidden = true
            } else {
                cell.imgcloud.isHidden = false
            }
            
            if FilesArray.contains(image[indexPath.item]) {
                cell.viewselection.isHidden = false
                cell.lblcount.isHidden = false
                cell.imgbigcheck.isHidden = false
                cell.lblcount.text = "\(FilesArray.index(of: image[indexPath.item])! + 1)"
            } else {
                cell.viewselection.isHidden = true
                cell.lblcount.isHidden = true
                cell.imgbigcheck.isHidden = true
            }
            
        } else {
            cell.imgthumb.clipsToBounds = true
            cell.imgthumb.contentMode = .scaleAspectFit
            
            print(audio)
            cell.lblvideoorintation.isHidden = true
            cell.overlayview.isHidden = true
            cell.lblduration.isHidden = true
            cell.lblvideoname.isHidden = true
            cell.imgvideo.isHidden = true
            cell.deletebutton.isHidden = false
            cell.imgcloud.isHidden = true
            cell.imgthumb.contentMode = .scaleAspectFit
            cell.deletebutton.tag = indexPath.item
            cell.deletebutton.addTarget(self, action: #selector(deletebtn), for: .touchUpInside)
            cell.imgthumb.image = UIImage(named: "placeaudio")
            let theFileName = (audio[indexPath.item] as NSString).lastPathComponent
            cell.lblaudioname.text = theFileName
            cell.lblaudioname.isHidden = false
            if FilesArrayAudio.contains(audio[indexPath.item]) {
                cell.viewselection.isHidden = false
                cell.lblcount.isHidden = false
                cell.imgbigcheck.isHidden = false
                cell.lblcount.text = "\(FilesArrayAudio.index(of: audio[indexPath.item])! + 1)"
            } else {
                cell.viewselection.isHidden = true
                cell.lblcount.isHidden = true
                cell.imgbigcheck.isHidden = true
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if IS_IPHONE {
            return CGSize(width: collectionView.frame.size.width / 3, height: 110)
        } else {
            return CGSize(width: collectionView.frame.size.width / 3, height: 200)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if flagismultiple == true {
            if flagmedia == 0 {
                if let index = FilesArray.index(of: video[indexPath.item]) {
                    FilesArray.remove(at: index)
               
                
                } else {
                    FilesArray.append(video[indexPath.item])
                }
                
            } else if flagmedia == 1 {
                if let index = FilesArray.index(of: image[indexPath.item]) {
                    FilesArray.remove(at: index)
                } else {
                    FilesArray.append(image[indexPath.item])
                }
                
            } else {
                if let index = FilesArrayAudio.index(of: audio[indexPath.item]) {
                    FilesArrayAudio.remove(at: index)
                } else {
                    FilesArrayAudio.append(audio[indexPath.item])
                }
            }
            
        } else {
            if flagmedia == 1 {
                if image.count > 0 {
                    var story: UIStoryboard?
                    
                    if IS_IPHONE {
                        story = UIStoryboard(name: "Main", bundle: nil)
                    } else {
                        story = UIStoryboard(name: "MainIPAD", bundle: nil)
                    }
                    let LibraryImagedetailsVC = story?.instantiateViewController(withIdentifier: "LibraryImagedetailsVC") as! LibraryImagedetailsVC
                    LibraryImagedetailsVC.image = image
                    LibraryImagedetailsVC.currentindex = indexPath.item
                    navigationController?.pushViewController(LibraryImagedetailsVC, animated: true)
                }
            } else if flagmedia == 0 {
                if video.count > 0 {
                    var story: UIStoryboard?
                    
                    if IS_IPHONE {
                        story = UIStoryboard(name: "Main", bundle: nil)
                    } else {
                        story = UIStoryboard(name: "MainIPAD", bundle: nil)
                    }
                    let MovieplayerVC = story?.instantiateViewController(withIdentifier: "MovieplayerVC") as! MovieplayerVC
                    MovieplayerVC.video = video
                    MovieplayerVC.currentindex = indexPath.item
                    navigationController?.pushViewController(MovieplayerVC, animated: true)
                }
                
            } else {
                if audio.count > 0 {
                    let theFileName = (audio[indexPath.item] as NSString).lastPathComponent
                    var story: UIStoryboard?
                    
                    if IS_IPHONE {
                        story = UIStoryboard(name: "Main", bundle: nil)
                    } else {
                        story = UIStoryboard(name: "MainIPAD", bundle: nil)
                    }
                    let LibraryAudioVC = story?.instantiateViewController(withIdentifier: "LibraryAudioVC") as! LibraryAudioVC
                    LibraryAudioVC.Audiopath = NSURL(fileURLWithPath: audio[indexPath.item]) as URL
                    LibraryAudioVC.headerstring = theFileName
                    LibraryAudioVC.currentindex = indexPath.item
                    LibraryAudioVC.audio = audio
                    LibraryAudioVC.isfromcapture = "NO"
                    LibraryAudioVC.hidesBottomBarWhenPushed = false
                    navigationController?.pushViewController(LibraryAudioVC, animated: true)
                }
            }
        }
        
        
       
        
      //  let index = IndexPath(row: indexPath.item, section: 0)
       // mycollectionview.reloadItems(at: [index])
     //mycollectionview.reloadData()
        self.mycollectionview.reloadItems(at: self.mycollectionview.indexPathsForVisibleItems)

    }
    
    @objc func deletebtn(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Are you sure you want to delete this audio?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { _ in
            
        }))
        alert.addAction(UIAlertAction(title: "Yes",
                                      style: UIAlertAction.Style.destructive,
                                      handler: { (_: UIAlertAction!) in
                                        
                                        let audiopath = self.audio[(sender as AnyObject).tag]
                                        let filemanager = FileManager.default
                                        print(audiopath)
                                        
                                        if filemanager.fileExists(atPath: audiopath) {
                                            try! filemanager.removeItem(atPath: audiopath)
                                        }
                                        
                                        self.getaudio()
                                        
                                      }))
        present(alert, animated: true, completion: nil)
    }
    
    @IBOutlet var mycollectionview: UICollectionView!
    
    //  var deviceFinder = DeviceFinder()
    
    let documentInteractionController = UIDocumentInteractionController()
    
    @IBOutlet var btnvideo: UIButton!
    @IBOutlet var btnimage: UIButton!
    @IBOutlet var btnaudio: UIButton!
    var possible = true
    var Strframerate : String = ""
    var Strresolution : String = ""
   var Strtrack : String = ""
    
    var image: [PHAsset] = []
    var audio: [String] = []
    var video: [PHAsset] = []
    var FilesArray: [PHAsset] = []
    
    var FilesArraySDCard: [URL] = []
    
    var FilesArrayAudio: [String] = []
    
    
    
    var ResolutionArray: [Any] = []

    
    
    var isStitchPossible: Bool = true

    var index: Int = -1
    var flagmedia = 0
    var flagismultiple: Bool = false
    var sharefiles: [Any] = []
    var ifviewdidloadcalled: Bool = false
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    var background = UIImageView()
    
    @IBOutlet var btnselect: UIButton!
    @IBOutlet var btnBrowseFiles: UIButton!
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var uploadAs: Upload_As?
    var isImoprtToLibrayChecked = false
    var selectedSD_CardFilesURLSArray: [URL]!
    
    @IBOutlet var viewUploadAs: UIView!
    @IBOutlet var btnUploadAs_Individual: UIButton!
    @IBOutlet var btnUploadAs_Group: UIButton!
    @IBOutlet var btnUploadAs_Stitch: UIButton!
    @IBOutlet var btnImportToLibrary: UIButton!

    @IBOutlet var imageUploadAs_Individual: UIImageView!
    @IBOutlet var imageUploadAs_Group: UIImageView!
    @IBOutlet var imageUploadAs_Stitch: UIImageView!
    @IBOutlet var imageImportToLibrary: UIImageView!

    @IBOutlet var btnUploadAs_Done: UIButton!
    @IBOutlet var btnUploadAs_Cancel: UIButton!
    
    
    
    
/*.................................................................*/
    
    @IBOutlet var viewUploadAs1: UIView!
    
    @IBOutlet var viewstitch: UIView!
    @IBOutlet var viewprogress: UIView!
    @IBOutlet var lblcount: UILabel!

    
    @IBOutlet var btnUploadAs_Individual1: UIButton!
    @IBOutlet var btnUploadAs_Group1: UIButton!
    @IBOutlet var btnUploadAs_Stitch1: UIButton!

    @IBOutlet var imageUploadAs_Individual1: UIImageView!
    @IBOutlet var imageUploadAs_Group1: UIImageView!
    @IBOutlet var imageUploadAs_Stitch1: UIImageView!

    @IBOutlet var btnUploadAs_Done1: UIButton!
    @IBOutlet var btnUploadAs_Cancel1: UIButton!
    

/*.................................................................*/

    
    
    @IBAction func btnUploadAs_IndividualAction(_ sender: Any) {
        uploadAs = Upload_As(rawValue: 0)
        imageUploadAs_Individual.image = UIImage(named: "selectradio")
        imageUploadAs_Group.image = UIImage(named: "unselectradio")
        imageUploadAs_Stitch.image = UIImage(named: "unselectradio")
        
        btnImportToLibrary.tag = 0
        imageImportToLibrary.image = UIImage(named: "unchecknew")
    }
    
    @IBAction func btnUploadAs_GroupAction(_ sender: Any) {
        uploadAs = Upload_As(rawValue: 1)
        imageUploadAs_Individual.image = UIImage(named: "unselectradio")
        imageUploadAs_Group.image = UIImage(named: "selectradio")
        imageUploadAs_Stitch.image = UIImage(named: "unselectradio")
        
        btnImportToLibrary.tag = 0
        imageImportToLibrary.image = UIImage(named: "unchecknew")
    }
    
    @IBAction func btnUploadAs_StitchAction(_ sender: Any) {
        if isStitchPossible{
        uploadAs = Upload_As(rawValue: 2)
        imageUploadAs_Individual.image = UIImage(named: "unselectradio")
        imageUploadAs_Group.image = UIImage(named: "unselectradio")
        imageUploadAs_Stitch.image = UIImage(named: "selectradio")
        btnImportToLibrary.tag = 0
        imageImportToLibrary.image = UIImage(named: "unchecknew")
        }
    }
    
    
    
    /*............................. */
    
    @IBAction func btnUploadAs_IndividualAction1(_ sender: Any) {
        app.uploadas = "Individual"
        imageUploadAs_Individual1.image = UIImage(named: "selectradio")
        imageUploadAs_Group1.image = UIImage(named: "unselectradio")
        imageUploadAs_Stitch1.image = UIImage(named: "unselectradio")
    }
    
    @IBAction func btnUploadAs_GroupAction1(_ sender: Any) {
        app.uploadas = "Group"
        imageUploadAs_Individual1.image = UIImage(named: "unselectradio")
        imageUploadAs_Group1.image = UIImage(named: "selectradio")
        imageUploadAs_Stitch1.image = UIImage(named: "unselectradio")
    }
    
    @IBAction func btnUploadAs_StitchAction1(_ sender: Any) {
        app.uploadas = "Stitch"
        imageUploadAs_Individual1.image = UIImage(named: "unselectradio")
        imageUploadAs_Group1.image = UIImage(named: "unselectradio")
        imageUploadAs_Stitch1.image = UIImage(named: "selectradio")
    }
    
    
    
    func ifportrait(videourl:URL) -> Bool {
       
     let asset = AVAsset(url:videourl)
     let width : Float = Float(asset.videoSize().width)
     let height : Float = Float(asset.videoSize().height)
    print(width)
    print(height)
        if height>width || height==width {
         return true
        }else{
       return false
        }
        
    }
    
    /* .................................*/
    
    @IBAction func btnImportToLibraryAction(_ sender: Any) {
        if btnImportToLibrary.tag == 0 {
            btnImportToLibrary.tag = 1
            imageImportToLibrary.image = UIImage(named: "checknew")
            isImoprtToLibrayChecked = true
            
            uploadAs = nil
            imageUploadAs_Individual.image = UIImage(named: "unselectradio")
            imageUploadAs_Group.image = UIImage(named: "unselectradio")
            imageUploadAs_Stitch.image = UIImage(named: "unselectradio")
        } else if btnImportToLibrary.tag == 1 {
            btnImportToLibrary.tag = 0
            imageImportToLibrary.image = UIImage(named: "unchecknew")
            isImoprtToLibrayChecked = false
            
            uploadAs = nil
            imageUploadAs_Individual.image = UIImage(named: "unselectradio")
            imageUploadAs_Group.image = UIImage(named: "unselectradio")
            imageUploadAs_Stitch.image = UIImage(named: "unselectradio")
        }
    }
    
    
    
    
    
    
    @IBAction func btnyesstitch(_ sender: Any) {
        KGModal.sharedInstance()?.hide()
        appdelegate.wrongstitch = true

    app.uploadas = "Stitch"
                var story: UIStoryboard?
                    if IS_IPHONE {
                    story = UIStoryboard(name: "Main", bundle: nil)
                        } else {
                    story = UIStoryboard(name: "MainIPAD", bundle: nil)
         }
        var SelectedVideo: SelectedVC?
            SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedVideo") as? SelectedVC
                SelectedVideo!.isfromview = "video"
          SelectedVideo!.assestarray = self.FilesArray
            self.navigationController?.pushViewController(SelectedVideo!, animated: true)
        
        
    }
    
    @IBAction func btnyesgroup(_ sender: Any) {
        KGModal.sharedInstance()?.hide()

    app.uploadas = "Group"
        var story: UIStoryboard?
            if IS_IPHONE {
            story = UIStoryboard(name: "Main", bundle: nil)
                } else {
            story = UIStoryboard(name: "MainIPAD", bundle: nil)
 }
var SelectedVideo: SelectedVC?
    SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedVideo") as? SelectedVC
        SelectedVideo!.isfromview = "video"
  SelectedVideo!.assestarray = self.FilesArray
    self.navigationController?.pushViewController(SelectedVideo!, animated: true)
                        
    }
    
    
    @IBAction func btncancel(_ sender: Any) {

        KGModal.sharedInstance()?.hide()
        
    }
    
    
        @IBAction func btnUploadAs_DoneAction1(_ sender: Any) {
            

                            var story: UIStoryboard?
                            if IS_IPHONE {
                                story = UIStoryboard(name: "Main", bundle: nil)
                            } else {
                                story = UIStoryboard(name: "MainIPAD", bundle: nil)
                            }
                            let SelectedVideo: SelectedVC?
            
            
            
                            if flagmedia == 0 {
                                
                                if app.uploadas == "Stitch" {
                                
                                    KGModal.sharedInstance()?.hide()
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        self.possible = true
                                        self.ifstitchpossible(count:-1)
                                    }

                                    
                                }else{
                                    
                                    KGModal.sharedInstance()?.hide()

                             
                                    SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedVideo") as? SelectedVC
                                                                  SelectedVideo!.isfromview = "video"
                                                                  SelectedVideo!.assestarray = FilesArray
                                                                  
                                    navigationController?.pushViewController(SelectedVideo!, animated: true)

                                }
                                
                                
                              
                                
                                
            
                            } else {
                                KGModal.sharedInstance()?.hide()


                                SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedAudio") as? SelectedVC
            
                                SelectedVideo!.isfromview = "audio"
                                SelectedVideo!.assestarrayaudio = FilesArrayAudio
                                navigationController?.pushViewController(SelectedVideo!, animated: true)

                            }
                            
        }
    
    
    
    

    @IBAction func btnUploadAs_DoneAction(_ sender: Any) {
        KGModal.sharedInstance()?.hide()
        if uploadAs != nil {
            if uploadAs!.rawValue == 0 { // Individual
                app.isStitch = "Individual"
                
                var story: UIStoryboard?
                if IS_IPHONE {
                    story = UIStoryboard(name: "Main", bundle: nil)
                } else {
                    story = UIStoryboard(name: "MainIPAD", bundle: nil)
                }
                
                let SelectedVideo: SelectedSDcardVC?
                SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedSDImage") as? SelectedSDcardVC
                SelectedVideo!.assestarraySD = FilesArraySDCard
                SelectedVideo?.isfromview = "Group"
                navigationController?.pushViewController(SelectedVideo!, animated: true)
            
            } else if uploadAs!.rawValue == 1 { // Group
                app.isStitch = "Group"

                print("Group")
                var story: UIStoryboard?
                if IS_IPHONE {
                    story = UIStoryboard(name: "Main", bundle: nil)
                } else {
                    story = UIStoryboard(name: "MainIPAD", bundle: nil)
                }
                
                let SelectedVideo: SelectedSDcardVC?
                SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedSDImage") as? SelectedSDcardVC
                SelectedVideo!.assestarraySD = FilesArraySDCard
                SelectedVideo?.isfromview = "Group"
                navigationController?.pushViewController(SelectedVideo!, animated: true)
            } else if uploadAs!.rawValue == 2 { // Stitch
                if isStitchPossible {
                    app.isStitch = "Stitch"
                    
                    var isVideoStitch = false
                    let extensionn = FilesArraySDCard[0].pathExtension
                    print("extensionn: \(extensionn)")
                    let uti = UTTypeCreatePreferredIdentifierForTag(
                        kUTTagClassFilenameExtension,
                        extensionn as CFString,
                        nil)
                    
                    let retainedValue = uti!.takeRetainedValue()
                    print("retainedValue: \(retainedValue)")

                    if UTTypeConformsTo(retainedValue, kUTTypeVideo) || UTTypeConformsTo(retainedValue, kUTTypeMovie) {
                        isVideoStitch = true
                    } else {
                        isVideoStitch = false
                    }
                    
                    var story: UIStoryboard?
                    if IS_IPHONE {
                        story = UIStoryboard(name: "Main", bundle: nil)
                    } else {
                        story = UIStoryboard(name: "MainIPAD", bundle: nil)
                    }
                    
                    let SelectedVideo: SelectedSDcardVC?
                    

                    if isVideoStitch {
                        SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedSDcardVC") as? SelectedSDcardVC
                        SelectedVideo?.isfromview = "video"
                    }
                    else {
                        SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedSDImage") as? SelectedSDcardVC
                        SelectedVideo?.isfromview = "Group"
                    }
            
                    SelectedVideo!.assestarraySD = FilesArraySDCard
//                    if isVideoStitch {
//                        SelectedVideo?.isfromview = "video"
//                    }
//                    else {
                    
//                        SelectedVideo?.isfromview = "Group"
                    
//                    }
                    navigationController?.pushViewController(SelectedVideo!, animated: true)

                }
            }
        } else {
            print("Nothing selected for Upload as")
        }
        
        if isImoprtToLibrayChecked == true {
            print("Yes, Imoprt To Libray")
            DispatchQueue.global(qos: .background).async {
                for i in 0 ..< self.selectedSD_CardFilesURLSArray.count {
                    let extensionn = self.selectedSD_CardFilesURLSArray[i].pathExtension
                    print("URL #\(i): \(self.selectedSD_CardFilesURLSArray[i])")
                    print("extensionn: \(extensionn)")
                    let uti = UTTypeCreatePreferredIdentifierForTag(
                        kUTTagClassFilenameExtension,
                        extensionn as CFString,
                        nil)

                    let retainedValue = uti!.takeRetainedValue()
                    print("retainedValue: \(retainedValue)")

                    if UTTypeConformsTo(retainedValue, kUTTypeImage) {
                        print("This is an image!")
                        let imageee = UIImage(contentsOfFile: self.selectedSD_CardFilesURLSArray[i].path)
                        UIImageWriteToSavedPhotosAlbum(imageee!, self, #selector(LibararyVC.image(imagepath:didFinishSavingWithError:contextInfo:)), nil)
                    } else if UTTypeConformsTo(retainedValue, kUTTypeVideo) {
                        print("This is an video!")
                        UISaveVideoAtPathToSavedPhotosAlbum(self.selectedSD_CardFilesURLSArray[i].path, self, #selector(LibararyVC.video(videoPath:didFinishSavingWithError:contextInfo:)), nil)
                    } else if UTTypeConformsTo(retainedValue, kUTTypeMovie) {
                        print("This is an movie!")
                        UISaveVideoAtPathToSavedPhotosAlbum(self.selectedSD_CardFilesURLSArray[i].path, self, #selector(LibararyVC.video(videoPath:didFinishSavingWithError:contextInfo:)), nil)
                    } else if UTTypeConformsTo(retainedValue, kUTTypeAudio) {
                        print("This is an audio!")
                                                
                        do {
                            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//                            let url = URL(string: self.selectedSD_CardFilesURLSArray[i].path)
                            let url = self.selectedSD_CardFilesURLSArray[i]

                            var tempURL = documentsURL?.appendingPathComponent(url.lastPathComponent)
                            print("tempURL: \(tempURL!)")
                            
                            var newURL: URL!

                            let filename = URL(fileURLWithPath: self.selectedSD_CardFilesURLSArray[i].path).lastPathComponent
                            print("filename: \(filename)")
                            let EXT = URL(fileURLWithPath: self.selectedSD_CardFilesURLSArray[i].path).pathExtension
                            print("EXT: \(EXT)")

                            if EXT == "" {
                                newURL = self.getDocumentsDirector().appendingPathComponent("\(filename.fileName()).m4a")
                                print("newURL: \(newURL)")
                            } else {
                                //                            let list = filename.fileName()
                                //                            let listItems = list!.components(separatedBy: ".")
                                //                            let filename = listItems[0]
                                newURL = self.getDocumentsDirector().appendingPathComponent("\(filename.fileName()).m4a")
                            }

                            print(newURL)
                            tempURL = newURL
                            
                            let isFileCopied = FileManager.default.secureCopyItem(at: url, to: tempURL!)
                            
                            if isFileCopied {
//                                self.perform(#selector(self.movetogalary), with: nil, afterDelay: 1.0)
                                print("audio saved to device successfully")
                            } else {
                                print("There is some error in importing audio file.")
                            }
                        } catch {
                            print(error)
                        }
                        
                    } else {
                        print("(uti?.takeRetainedValue())! = \((uti?.takeRetainedValue())!)")
                    }
                }
            }
        } else {
            print("Do not Imoprt To Libray")
        }

        KGModal.sharedInstance()?.hide()
    }
    
        func getDocumentsDirector() -> URL {
            // Grab CWD and set to paths
    //        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    //        return paths[0]
            
            let documentsURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            
              return documentsURL
        }
    @objc func movetogalary() {
//        alert!.close()
        KGModal.sharedInstance()?.hide()

        tabBarController?.selectedIndex = 2
        let navCtrl = tabBarController?.selectedViewController as? UINavigationController
        navCtrl?.popToRootViewController(animated: false)

//        DispatchQueue.global(qos: .background).async {
//            self.bgTask?.stop()
//        }
    }
    
    @IBAction func btnUploadAs_CancelAction(_ sender: Any) {
        KGModal.sharedInstance()?.hide()
    }
    
    @IBAction func btnUploadAs_CancelAction1(_ sender: Any) {
           KGModal.sharedInstance()?.hide()
       }
    
    @objc func video(videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        if let _ = error {
            print("Error,Video failed to save: \(error)")
//            do {
//                let imageData = try Data(contentsOf: URL(string: videoPath as String)!)
//                print("imageData.count: \(imageData.count)")
//
//                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//                let dataPath = documentsURL?.appendingPathComponent("/videoFileName.mp4")
//                print("dataPath: \(dataPath)")
//                do {
//                    try imageData.write(to:(dataPath)!, options: .atomic)
//                    sleep(1)
//                    PHPhotoLibrary.shared().performChanges({
//                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: dataPath!)
//                    }) { saved, error in
//                        if saved {
//                            print("Successfully,Video was saved")
//                        }
//                        if (error != nil) {
//                            print("ERROR in saving video is: \(error)")
//                        }
//                    }
//                } catch {
//                    print(error)
//                }
//            } catch {
//                print("Unable to load data: \(error)")
//            }
        } else {
            print("Successfully,Video was saved")
            app.getvideo()
        }
    }

    @objc func image(imagepath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        if let _ = error {
            print("Error,image failed to save: \(error)")
        } else {
            print("Successfully,image was saved")
            app.getimage()
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    @IBAction func btnBrowseFilesAction(_ sender: Any) {
        
        isStitchPossible = true

        
        let documentPickerViewController = UIDocumentPickerViewController(documentTypes: ["public.image", "public.movie", "public.video", "public.audio"], in: .import)
        //        let documentPickerViewController = UIDocumentPickerViewController(documentTypes: ["public.item"], in: .import)
        //        let documentPickerViewController = UIDocumentPickerViewController(documentTypes: [kUTTypeFolder as String], in: .open)
        //        let documentPickerViewController = UIDocumentPickerViewController(documentTypes: [kUTTypeFolder as String], in: .import)
        
        documentPickerViewController.delegate = self
        documentPickerViewController.allowsMultipleSelection = true
        present(documentPickerViewController, animated: true) {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
           
        FilesArray = []
        FilesArrayAudio = []
        btnselect.setTitle("Select", for: .normal)
        flagismultiple = false
        flagmedia = 0
        changecolor()
        btnvideo.setBackgroundImage(UIImage(named: "purpule_box"), for: .normal)
        btnvideo.setTitleColor(UIColor.white, for: .normal)
        
        if appdelegate.isfromshare == true {
            let status = PHPhotoLibrary.authorizationStatus()
            
            if status == PHAuthorizationStatus.authorized {
                let window = UIApplication.shared.keyWindow!
                background = UIImageView(frame: window.bounds)
                window.addSubview(background)
                background.image = UIImage(named: "window.png")
                var sharedUserDefaults: UserDefaults?
                sharedUserDefaults = UserDefaults(suiteName: "group.com.latakoo.ext")
                
                
                sharefiles = (sharedUserDefaults?.array(forKey: "SharedExtension"))!
                print(sharefiles as Any)
                let dict = sharefiles[0] as! [String: Any]
                let filetype = dict["Text"] as! String
                
                appdelegate.isfromshare = false
                if filetype == "Video" {
                    perform(#selector(getvideonew), with: nil, afterDelay: 0.1)
                    
                } else {
                    perform(#selector(getimagenew), with: nil, afterDelay: 0.1)
                }
                
                perform(#selector(getfiles), with: nil, afterDelay: 0.1)
            } else if status == PHAuthorizationStatus.denied {
                _ = SCLAlertView().showError("LATAKOO", subTitle: "Please give this app permission to access your photo library in your settings app!", closeButtonTitle: "OK")
            }
        }
        if #available(iOS 10.0, *) {
                  mycollectionview.refreshControl = refreshControl
              } else {
                  mycollectionview.addSubview(refreshControl)
              }
              refreshControl.tintColor = UIColor.init(named: "colordark")
              refreshControl.attributedTitle = NSAttributedString(string: "Refreshing...")
              refreshControl.addTarget(self, action: #selector(refreshlist(_:)), for: .valueChanged)

        // Do any additional setup after loading the view.
    }
    
    @objc private func refreshlist(_ sender: Any) {
           if flagmedia == 0 {
            self.getvideo()
        } else if flagmedia == 1 {
            self.getimages()
            } else {
           self.getaudio()
                }
     
    }
    
    @objc private func hidePullToRefresh(notification: NSNotification) {
          if #available(iOS 10.0, *) {
              mycollectionview.refreshControl = nil
          } else {
              mycollectionview.willRemoveSubview(refreshControl)
          }
      }

      @objc private func showPullToRefresh(notification: NSNotification) {
          if #available(iOS 10.0, *) {
              mycollectionview.refreshControl = refreshControl
          } else {
              mycollectionview.addSubview(refreshControl)
          }
          mycollectionview.tintColor = UIColor(named: "colordark")
          refreshControl.attributedTitle = NSAttributedString(string: "Refreshing...")
          refreshControl.addTarget(self, action: #selector(refreshlist(_:)), for: .valueChanged)
      }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        appdelegate.fileurl = nil
        appdelegate.wrongstitch = false
        appdelegate.AssignmentID = ""
        appdelegate.TopicID = ""
        appdelegate.OrgID = ""
        appdelegate.Assignmentname = ""
        
        
        if appdelegate.checkdownload == "Image" {
                FilesArray = []
                FilesArrayAudio = []
                btnselect.setTitle("Select", for: .normal)
                flagismultiple = false
                flagmedia = 1
                if appdelegate.image.count > 0 {
                    image = appdelegate.image
                    mycollectionview.reloadData()
                    mycollectionview.setContentOffset(CGPoint.zero, animated: true)
                    
                } else {
                    getimages()
                }
                
                changecolor()
                btnimage.setBackgroundImage(UIImage(named: "purpule_box.png"), for: .normal)
                btnimage.setTitleColor(UIColor.white, for: .normal)
            
       
        }else if appdelegate.checkdownload == "Audio" {
         
                FilesArray = []
                   FilesArrayAudio = []
                   btnselect.setTitle("Select", for: .normal)
                   flagismultiple = false
                   flagmedia = 2
                   getaudio()
                   changecolor()
                   btnaudio.setBackgroundImage(UIImage(named: "purpule_box"), for: .normal)
                   btnaudio.setTitleColor(UIColor.white, for: .normal)
       
        }
        appdelegate.checkdownload = ""
        
        getaudio()
        
        appdelegate.ifgotocapture = "NO"
        
        if appdelegate.video.count > 0 {
            video = appdelegate.video
            mycollectionview.reloadData()
            mycollectionview.setContentOffset(CGPoint.zero, animated: true)
            
        } else {
            getvideo()
        }
        
        
        if appdelegate.image.count > 0 {
            image = appdelegate.image
            mycollectionview.reloadData()
            mycollectionview.setContentOffset(CGPoint.zero, animated: true)
            
        }
        
        //  self.perform(#selector(getfiles), with: nil, afterDelay:0.1)
    }
    
    @objc func getfiles() {
        let status = PHPhotoLibrary.authorizationStatus()
        
        if status == PHAuthorizationStatus.authorized {
            flagmedia = 0
            changecolor()
            btnvideo.setBackgroundImage(UIImage(named: "purpule_box.png"), for: .normal)
            btnvideo.setTitleColor(UIColor.white, for: .normal)
            if appdelegate.video.count > 0 {
                video = appdelegate.video
                mycollectionview.reloadData()
                mycollectionview.setContentOffset(CGPoint.zero, animated: true)
                
            } else {
                getvideo()
            }
        } else if status == PHAuthorizationStatus.denied {
            _ = SCLAlertView().showError("LATAKOO", subTitle: "Please give this app permission to access your photo library in your settings app!", closeButtonTitle: "OK")
        } else if status == PHAuthorizationStatus.notDetermined {
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ newStatus in
                
                if newStatus == PHAuthorizationStatus.authorized {
                    self.flagmedia = 0
                    self.changecolor()
                    self.btnvideo.setBackgroundImage(UIImage(named: "purpule_box"), for: .normal)
                    self.btnvideo.setTitleColor(UIColor.white, for: .normal)
                    self.getvideo()
                } else {
                }
            })
        } else if status == PHAuthorizationStatus.restricted {
            // Restricted access - normally won't happen.
        }
    }
    
    @IBAction func btnvideo(_ sender: Any) {
        FilesArray = []
        FilesArrayAudio = []
        btnselect.setTitle("Select", for: .normal)
        flagismultiple = false
        flagmedia = 0
        if appdelegate.video.count > 0 {
            video = appdelegate.video
            mycollectionview.reloadData()
            mycollectionview.setContentOffset(CGPoint.zero, animated: true)
            
        } else {
            getvideo()
        }
        
        changecolor()
        btnvideo.setBackgroundImage(UIImage(named: "purpule_box"), for: .normal)
        btnvideo.setTitleColor(UIColor.white, for: .normal)
    }
    
    @IBAction func btnimage(_ sender: Any) {
        FilesArray = []
        FilesArrayAudio = []
        btnselect.setTitle("Select", for: .normal)
        flagismultiple = false
        flagmedia = 1
        if appdelegate.image.count > 0 {
            image = appdelegate.image
            mycollectionview.reloadData()
            mycollectionview.setContentOffset(CGPoint.zero, animated: true)
            
        } else {
            getimages()
        }
        
        changecolor()
        btnimage.setBackgroundImage(UIImage(named: "purpule_box.png"), for: .normal)
        btnimage.setTitleColor(UIColor.white, for: .normal)
    }
    
    @IBAction func btnaudio(_ sender: Any) {
        FilesArray = []
        FilesArrayAudio = []
        btnselect.setTitle("Select", for: .normal)
        flagismultiple = false
        flagmedia = 2
        getaudio()
        changecolor()
        btnaudio.setBackgroundImage(UIImage(named: "purpule_box"), for: .normal)
        btnaudio.setTitleColor(UIColor.white, for: .normal)
    }
    
    func changecolor() {
        btnaudio.setBackgroundImage(UIImage(named: "white_box_button"), for: .normal)
        btnaudio.setTitleColor(UIColor(named: "pilotcolor"), for: .normal)
        
        btnvideo.setBackgroundImage(UIImage(named: "white_box_button"), for: .normal)
        btnvideo.setTitleColor(UIColor(named: "pilotcolor"), for: .normal)
        
        btnimage.setBackgroundImage(UIImage(named: "white_box.png"), for: .normal)
        btnimage.setTitleColor(UIColor(named: "pilotcolor"), for: .normal)
    }
    
    func getimages() {
        image = []
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
        
        assets.enumerateObjects { (obj, _, _) -> Void in
            // self.image.append(obj)
            self.image.insert(obj, at: 0)
        }
        perform(#selector(hideloader), with: nil, afterDelay: 0.5)
        print(image)
        mycollectionview.reloadData()
        mycollectionview.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func getvideo() {
        video = []
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
        
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.video, options: fetchOptions)
        
        assets.enumerateObjects { (obj, _, _) -> Void in
            // self.video.append(obj)
            self.video.insert(obj, at: 0)
        }
        print(video)
        perform(#selector(hideloader), with: nil, afterDelay: 0.5)

        mycollectionview.reloadData()
        mycollectionview.setContentOffset(CGPoint.zero, animated: true)
    }
    
    @objc func hideloader() {
            refreshControl.endRefreshing()

       }
    
    
    func getaudio() {
        audio = []
        let manager = FileManager.default
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        let recDir = paths[0]
        var contents: [String]?
        do {
            contents = try manager.contentsOfDirectory(atPath: recDir)
            
        } catch {
        }
        
        
        
        
        for item in contents ?? [] {
            if (URL(fileURLWithPath: item).pathExtension == "m4a") || (URL(fileURLWithPath: item).pathExtension == "mp3") || (URL(fileURLWithPath: item).pathExtension == "ogg") {
                if (item == "recording.wav") || (item == "originalRecording.wav") || (item == "trimmerRecording.wav") || (item == "background.wav") || (item == "extract.m4a") || (item == "audio.m4a") || (item == "mixedAudio.m4a") || (item == "newaudio.mp4") {
                } else {
                    let audioFileName = "\(recDir)/\(item)"
                    audio.append(audioFileName)
                }
            }
        }
        perform(#selector(hideloader), with: nil, afterDelay: 0.5)

        mycollectionview.reloadData()
        mycollectionview.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func convertTime(_ time: Int) -> String? {
        let minutes = time / 60
        let seconds = time % 60
        return String(format: "%02ld:%02ld", minutes, seconds)
    }
    
    @IBAction func btnselect(_ sender: Any) {
        if flagismultiple == false {
            btnselect.setTitle("Send", for: .normal)
            flagismultiple = true
        } else {
            
            
            
            if FilesArray.count > 1 || FilesArrayAudio.count > 1 {
             
                
                if flagmedia == 1 {
                    var story: UIStoryboard?
                    
                    if IS_IPHONE {
                        story = UIStoryboard(name: "Main", bundle: nil)
                    } else {
                        story = UIStoryboard(name: "MainIPAD", bundle: nil)
                    }
                    let SelectedVideo: SelectedVC?
                    
                    SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedImage") as? SelectedVC
                    
                    SelectedVideo!.isfromview = "image"
                    SelectedVideo!.assestarray = FilesArray
                    navigationController?.pushViewController(SelectedVideo!, animated: true)

                    
                } else{
                
                    
                
                       imageUploadAs_Individual1.image = UIImage(named: "unselectradio")
                           imageUploadAs_Group1.image = UIImage(named: "unselectradio")
                           imageUploadAs_Stitch1.image = UIImage(named: "unselectradio")
                           btnImportToLibrary.tag = 0
                           KGModal.sharedInstance()?.show(withContentView: viewUploadAs1)
                           KGModal.sharedInstance()?.tapOutsideToDismiss = false
                
                
                }
                
                

                
            } else {
                AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please select  at least two files", hideAfter: 1.0)
            }
        }
    }
    
    func downloadvideofromcloud(count : Int){

      //  if FilesArray.count <= count {
            
        let resourceArray = PHAssetResource.assetResources(for: self.FilesArray[count])
                                      let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false
                                       if  bIsLocallayAvailable == false {
                                        DispatchQueue.main.async {
                                        self.viewCircularProgressBar.labelSize = 30
                                       self.viewCircularProgressBar.safePercent = 100
                                     self.viewCircularProgressBar.setProgress(to: 0, withAnimation: true)
                                   KGModal.sharedInstance()?.show(withContentView: self.viewprogress)
                                        KGModal.sharedInstance()?.tapOutsideToDismiss = false
                                      self.viewCircularProgressBar.backgroundColor = UIColor.clear
                                        }
                                       }
        
              
                  let option1 = PHVideoRequestOptions()
                  option1.isNetworkAccessAllowed = true
                  option1.deliveryMode = .highQualityFormat
                  option1.version = .original
                  option1.progressHandler = {  (progress, error, stop, info) in
                
                    
                    DispatchQueue.main.async {
                        let resourceArray = PHAssetResource.assetResources(for: self.FilesArray[count])

                        let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false
                                                          if  bIsLocallayAvailable == false {
                       print(progress)
                        self.viewCircularProgressBar.setProgress(to: progress, withAnimation: true)
                        }

                    }
          
          }
        
        PHCachingImageManager.default().requestAVAsset(forVideo:self.FilesArray[count], options: option1, resultHandler: { avasset, audioMix, info in
                
            
            DispatchQueue.main.async {

            
                let urlAsset = avasset as? AVURLAsset
                
                let asset =  urlAsset
                let videoTracks = asset!.tracks(withMediaType: .video)
                let AudioTrack = asset!.tracks(withMediaType: .audio)
                var videoTrack: AVAssetTrack? = nil

                   let trackcount = AudioTrack.count
                let width : Float = Float(asset!.videoSize().width)
                let height : Float = Float(asset!.videoSize().height)
                    if videoTracks.count>0 {
                      videoTrack = videoTracks[0];
                  }
                var roundedframerate : Int = Int(videoTrack!.nominalFrameRate)
              
             //   let roundedframerate = frameRate.rounded(.up)


                if roundedframerate > 28 && roundedframerate < 32 {
                roundedframerate = 30
                }
                                   
                 if roundedframerate > 58 && roundedframerate < 62 {
                 roundedframerate = 60
                     }
                 
                
              let  resolution1 =    "\(width) X \(height)"
              let  framerate1 =        "\(roundedframerate)"
              let   track1 = "\(trackcount)"
             
           print("new value")
                
                print(resolution1)
                print(framerate1)
                print(track1)
                
                
                print("old value")
                               
                print(self.Strresolution)
                print(self.Strframerate)
                print(self.Strtrack)
                
                
                
                if resolution1 == self.Strresolution && framerate1 == self.Strframerate && track1 == self.Strtrack {
                                  
                                  let countnew = count + 1
                                  print (countnew)
                
                
                var story: UIStoryboard?
                                          if IS_IPHONE {
                                              story = UIStoryboard(name: "Main", bundle: nil)
                                          } else {
                                              story = UIStoryboard(name: "MainIPAD", bundle: nil)
                                          }
                var SelectedVideo: SelectedVC?
                
                    if countnew == self.FilesArray.count && self.possible == true{
                 
                    KGModal.sharedInstance()?.hide()

                        SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedVideo") as? SelectedVC
                                                                                         SelectedVideo!.isfromview = "video"
                        SelectedVideo!.assestarray = self.FilesArray
                           self.navigationController?.pushViewController(SelectedVideo!, animated: true)

                }else{
                        
                    self.ifstitchpossible(count: countnew)
                }
                                  
                                  
            }else{
            let countnew = count + 1
                    self.possible = false
                    if countnew == self.FilesArray.count  && self.possible == false{
                    KGModal.sharedInstance()?.show(withContentView: self.viewstitch)
                        
                    }else{
                        self.ifstitchpossible(count: countnew)
                    }
                    
            }
            }
        })
    
   // }
        
    }
    
    
    @objc func ifstitchpossible(count : Int){

        
        if count == -1{
            lblcount.text = "1 of \(FilesArray.count)"
    
    let resourceArray = PHAssetResource.assetResources(for: self.FilesArray[0])
                                  let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false
                                   if  bIsLocallayAvailable == false {
                                    DispatchQueue.main.async {
                                    self.viewCircularProgressBar.labelSize = 30
                                   self.viewCircularProgressBar.safePercent = 100
                                 self.viewCircularProgressBar.setProgress(to: 0, withAnimation: true)
                               KGModal.sharedInstance()?.show(withContentView: self.viewprogress)
                                    KGModal.sharedInstance()?.tapOutsideToDismiss = false
                                  self.viewCircularProgressBar.backgroundColor = UIColor.clear
                                    }
                                   }
    

        
            let option1 = PHVideoRequestOptions()
            option1.isNetworkAccessAllowed = true
            option1.deliveryMode = .highQualityFormat
            option1.version = .original
            option1.progressHandler = {  (progress, error, stop, info) in
          
              
                DispatchQueue.main.async {
                    let resourceArray = PHAssetResource.assetResources(for: self.FilesArray[0])

                    let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false
                                                      if  bIsLocallayAvailable == false {
                   print(progress)
                    self.viewCircularProgressBar.setProgress(to: progress, withAnimation: true)
                    }

                }
    
    }
            
            PHCachingImageManager.default().requestAVAsset(forVideo:self.FilesArray[0], options: option1, resultHandler: { avasset, audioMix, info in
                DispatchQueue.main.async {

                let urlAsset = avasset as? AVURLAsset
                
                let asset =  urlAsset
                let videoTracks = asset!.tracks(withMediaType: .video)
                let AudioTrack = asset!.tracks(withMediaType: .audio)
                var videoTrack: AVAssetTrack? = nil

                   let trackcount = AudioTrack.count
                let width : Float = Float(asset!.videoSize().width)
                let height : Float = Float(asset!.videoSize().height)
                    if videoTracks.count>0 {
                      videoTrack = videoTracks[0];
                  }
                    var roundedframerate : Int = Int(videoTrack!.nominalFrameRate)

                    if roundedframerate > 28 && roundedframerate < 32 {
                        roundedframerate = 30
                    }
                    
                    if roundedframerate > 58 && roundedframerate < 62 {
                    roundedframerate = 60
                    }

                    self.Strresolution = "\(width) X \(height)"
                    self.Strframerate =   "\(roundedframerate)"
                    self.Strtrack =       "\(trackcount)"
             

                    self.downloadvideofromcloud(count: 0 )

                
                }
               
            })
       
        }else{
            lblcount.text = "\(count+1) of \(FilesArray.count)"

            self.downloadvideofromcloud(count: count)
        }

    
    }
    
    
    
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if !IS_IPHONE {
            perform(#selector(resetplayer), with: nil, afterDelay: 0.1)
        }
    }
    
    @objc func resetplayer() {
        if mycollectionview != nil {
            mycollectionview.reloadData()
        }
    }
    
    func createimagefile() -> URL {
        let fileName = "tempimage"
        let documentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = documentDirURL.appendingPathComponent(fileName).appendingPathExtension("jpg")
        print("File PAth: \(fileURL.path)")
        
        if FileManager.default.fileExists(atPath: fileURL.path) {
            try! FileManager.default.removeItem(atPath: fileURL.path)
        }
        return fileURL
    }
    
    @objc func getimagenew() {
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
        
        assets.enumerateObjects { (obj, idx, bool) -> Void in
         
         let resources = PHAssetResource.assetResources(for: obj)
         let orgFilename = (resources[0]).originalFilename
                  
         let array = orgFilename.components(separatedBy: ".")
         let name = array[0]
         
         
         for value in self.sharefiles{
          let dict = value as! [String : Any]
          let abc = dict["URL"] as? String
         let array = abc!.components(separatedBy: ".")
          let namenew = array[0]
          if namenew == name {
              self.FilesArray.append(obj)
          }
          
          }
        }
     
     if FilesArray.count>0{
       background.isHidden = true
       background.removeFromSuperview()
         
         
         var story : UIStoryboard?
                    
               if IS_IPHONE {
                    story = UIStoryboard.init(name: "Main", bundle: nil)
                   }else{
                    story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
               }
         let SelectedVideo : SelectedVC?

       SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedImage") as? SelectedVC
       SelectedVideo!.isfromview = "image"
        SelectedVideo!.assestarray = FilesArray
         self.navigationController?.pushViewController(SelectedVideo!, animated: true)

     }else{
      background.isHidden = true
     background.removeFromSuperview()
     }
        print(image)
    }
    
    @objc func getvideonew() {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.video, options: fetchOptions)
        
        assets.enumerateObjects { (obj, idx, bool) -> Void in
         
         let resources = PHAssetResource.assetResources(for: obj)
         let orgFilename = (resources[0]).originalFilename
         
         let array = orgFilename.components(separatedBy: ".")
         let name = array[0]
         for value in self.sharefiles{
         let dict = value as! [String : Any]
         let abc = dict["URL"] as? String
        let array = abc!.components(separatedBy: ".")
         let namenew = array[0]
         if namenew == name {
             self.FilesArray.append(obj)
         }
         
         }

        }
     
     if FilesArray.count>0{
     background.isHidden = true
               background.removeFromSuperview()
      var story : UIStoryboard?
                 
            if IS_IPHONE {
                 story = UIStoryboard.init(name: "Main", bundle: nil)
                }else{
                 story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
            }
         let SelectedVideo : SelectedVC?

         SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedVideo") as? SelectedVC
         SelectedVideo!.isfromview = "video"
         SelectedVideo!.assestarray = FilesArray
         self.navigationController?.pushViewController(SelectedVideo!, animated: true)

     }else{
         background.isHidden = true
         background.removeFromSuperview()
     }
     
        print(video)
    }
}

@available(iOS 13.0, *)
extension LibararyVC {
    @objc func openFileBrowser() {
        let documentPickerViewController = UIDocumentPickerViewController(documentTypes: ["public.image", "public.movie", "public.video", "public.audio"], in: .import)
        //        let documentPickerViewController = UIDocumentPickerViewController(documentTypes: ["public.item"], in: .import)
        //        let documentPickerViewController = UIDocumentPickerViewController(documentTypes: [kUTTypeFolder as String], in: .open)
        //        let documentPickerViewController = UIDocumentPickerViewController(documentTypes: [kUTTypeFolder as String], in: .import)
        
        documentPickerViewController.delegate = self
        documentPickerViewController.allowsMultipleSelection = true
        present(documentPickerViewController, animated: true) {}
    }
}

@available(iOS 13.0, *)
extension LibararyVC: UIDocumentPickerDelegate {
    //    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
    //        print("didPickDocumentAt: \(url)")
    //    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
   
    
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt documentURLs: [URL]) {
        self.FilesArraySDCard = []
        
        let newUrls = documentURLs.compactMap { (url: URL) -> URL? in
            // Create file URL to temporary folder
            var tempURL = URL(fileURLWithPath: NSTemporaryDirectory())
            // Apend filename (name+extension) to URL
            tempURL.appendPathComponent(url.lastPathComponent)
            do {
                // If file with same name exists remove it (replace file with new one)
                if FileManager.default.fileExists(atPath: tempURL.path) {
                    try FileManager.default.removeItem(atPath: tempURL.path)
                }
                // Move file from app_id-Inbox to tmp/filename
                try FileManager.default.moveItem(atPath: url.path, toPath: tempURL.path)
                return tempURL
            } catch {
                print(error.localizedDescription)
                return nil
            }
        }
        
        print(newUrls)
        
      var videostichpossible = true
      var audiostitchpossible = true

        
        for i in 0 ..< newUrls.count {
            self.FilesArraySDCard.append(newUrls[i])
            print("didPickDocumentAt: \(FilesArraySDCard[i])")
            
            let extensionn = newUrls[i].pathExtension
            print("URL #0: \(documentURLs[0])")
            print("extensionn: \(extensionn)")
            let uti = UTTypeCreatePreferredIdentifierForTag(
                kUTTagClassFilenameExtension,
                extensionn as CFString,
                nil)
            
            let retainedValue = uti!.takeRetainedValue()
            print("retainedValue: \(retainedValue)")
            
            if UTTypeConformsTo(retainedValue, kUTTypeVideo) || UTTypeConformsTo(retainedValue, kUTTypeMovie) {
                
                audiostitchpossible = false
            } else {

               videostichpossible = false
            }
            
            if UTTypeConformsTo(retainedValue, kUTTypeImage)  {
                    videostichpossible = false
                    audiostitchpossible = false
            }
            
       
            
                
            }
        if videostichpossible == true && audiostitchpossible == false {
            isStitchPossible = true
       
        
        }
        
        if videostichpossible == false && audiostitchpossible == true {
                isStitchPossible = true
      
        
        }
        
        if videostichpossible == true && audiostitchpossible == true {
                       isStitchPossible = false
      
        
        }
        
        if videostichpossible == false && audiostitchpossible == false {
                isStitchPossible = false
           
        
        }
            
           
            
            
            
        
        
        if newUrls.count > 1 {
//            var story: UIStoryboard?
//            if IS_IPHONE {
//                story = UIStoryboard(name: "Main", bundle: nil)
//            } else {
//                story = UIStoryboard(name: "MainIPAD", bundle: nil)
//            }
//
//            let SelectedVideo: SelectedSDcardVC?
//            SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedSDImage") as? SelectedSDcardVC
//            SelectedVideo!.assestarraySD = FilesArraySDCard
//            SelectedVideo?.isfromview = "Group"
//            navigationController?.pushViewController(SelectedVideo!, animated: true)
            self.selectedSD_CardFilesURLSArray = []
            
            selectedSD_CardFilesURLSArray = FilesArraySDCard
            print("selectedSD_CardFilesURLSArray: \(selectedSD_CardFilesURLSArray)")
            uploadAs = nil
            isImoprtToLibrayChecked = false
            imageUploadAs_Individual.image = UIImage(named: "unselectradio")
            imageUploadAs_Group.image = UIImage(named: "unselectradio")
            imageUploadAs_Stitch.image = UIImage(named: "unselectradio")
            btnImportToLibrary.tag = 0
            imageImportToLibrary.image = UIImage(named: "unchecknew")
            KGModal.sharedInstance()?.show(withContentView: viewUploadAs)
            KGModal.sharedInstance()?.tapOutsideToDismiss = false
            
        } else {
            if documentURLs.count > 0 {
                //            for i in 0 ..< documentURLs.count {
                let extensionn = documentURLs[0].pathExtension
                print("URL #0: \(documentURLs[0])")
                print("extensionn: \(extensionn)")
                let uti = UTTypeCreatePreferredIdentifierForTag(
                    kUTTagClassFilenameExtension,
                    extensionn as CFString,
                    nil)
                
                let retainedValue = uti!.takeRetainedValue()
                print("retainedValue: \(retainedValue)")
                
                if UTTypeConformsTo(retainedValue, kUTTypeImage) {
                    print("This is an image!")
                    //   FilesArraySDCard.append(documentURLs[0])
                    var story: UIStoryboard?
                    if IS_IPHONE {
                        story = UIStoryboard(name: "Main", bundle: nil)
                    } else {
                        story = UIStoryboard(name: "MainIPAD", bundle: nil)
                    }
                    
                    let SelectedVideo: SelectedSDcardVC?
                    SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedSDImage") as? SelectedSDcardVC
                    SelectedVideo!.assestarraySD = FilesArraySDCard
                    SelectedVideo?.isfromview = "image"
                    navigationController?.pushViewController(SelectedVideo!, animated: true)
                } else if UTTypeConformsTo(retainedValue, kUTTypeVideo) {
                    print("This is an video!")
                    DispatchQueue.main.async {
                        //  self.FilesArraySDCard.append(documentURLs[0])
                        
                        var story: UIStoryboard?
                        if IS_IPHONE {
                            story = UIStoryboard(name: "Main", bundle: nil)
                        } else {
                            story = UIStoryboard(name: "MainIPAD", bundle: nil)
                        }
                        let SelectedVideo: SelectedSDcardVC?
                        SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedSDcardVC") as? SelectedSDcardVC
                        SelectedVideo!.assestarraySD = self.FilesArraySDCard
                        SelectedVideo?.isfromview = "video"
                        self.navigationController?.pushViewController(SelectedVideo!, animated: true)
                    }
                } else if UTTypeConformsTo(retainedValue, kUTTypeMovie) {
                    print("This is an movie!")
                    DispatchQueue.main.async {
                        //     self.FilesArraySDCard.append(documentURLs[0])
                        
                        var story: UIStoryboard?
                        if IS_IPHONE {
                            story = UIStoryboard(name: "Main", bundle: nil)
                        } else {
                            story = UIStoryboard(name: "MainIPAD", bundle: nil)
                        }
                        let SelectedVideo: SelectedSDcardVC?
                        SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedSDcardVC") as? SelectedSDcardVC
                        SelectedVideo!.assestarraySD = self.FilesArraySDCard
                        SelectedVideo?.isfromview = "video"
                        self.navigationController?.pushViewController(SelectedVideo!, animated: true)
                    }
                } else if UTTypeConformsTo(retainedValue, kUTTypeAudio) {
                    var story: UIStoryboard?
                    if IS_IPHONE {
                        story = UIStoryboard(name: "Main", bundle: nil)
                    } else {
                        story = UIStoryboard(name: "MainIPAD", bundle: nil)
                    }
                    
                    let SelectedVideo: SelectedSDcardVC?
                    SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedSDAudio") as? SelectedSDcardVC
                    SelectedVideo!.assestarraySD = FilesArraySDCard
                    SelectedVideo?.isfromview = "audio"
                    navigationController?.pushViewController(SelectedVideo!, animated: true)
                    
                } else {}
            }
        }
    }
    
    func PHAssetForFileURL(url: NSURL) -> PHAsset? {
        let imageRequestOptions = PHImageRequestOptions()
        imageRequestOptions.version = .current
        imageRequestOptions.deliveryMode = .fastFormat
        imageRequestOptions.resizeMode = .fast
        imageRequestOptions.isSynchronous = true
        
        let fetchResult = PHAsset.fetchAssets(with: nil)
        //        for var index = 0; index < fetchResult.count; index++ {
        for index in 0 ..< fetchResult.count {
            if let asset = fetchResult[index] as? PHAsset {
                var found = false
                PHImageManager.default().requestImageData(for: asset,
                                                          options: imageRequestOptions) { _, _, _, info in
                    if let urlkey = info!["PHImageFileURLKey"] as? NSURL {
                        if urlkey.absoluteString! == url.absoluteString! {
                            found = true
                        }
                    }
                }
                if found {
                    return asset
                }
            }
        }
        
        return nil
    }
    
    
    
    // MARK: UIDocumentBrowserViewControllerDelegate
    
    func documentPicker(_ controller: UIDocumentBrowserViewController, didRequestDocumentCreationWithHandler importHandler: @escaping (URL?, UIDocumentBrowserViewController.ImportMode) -> Void) {
        let newDocumentURL: URL? = nil
        
        // Set the URL for the new document here. Optionally, you can present a template chooser before calling the importHandler.
        // Make sure the importHandler is always called, even if the user cancels the creation request.
        if newDocumentURL != nil {
            importHandler(newDocumentURL, .move)
        } else {
            importHandler(nil, .none)
        }
    }
    
    // #2.0 - "When the user selects one or more documents in the browser view controller,
    // the system calls your delegate's documentBrowser(_:didPickDocumentURLs:) method."
    private func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentURLs documentURLs: [URL]) {
        guard let sourceURL = documentURLs.first else { return }
        
        // #2.1 - Present the Document View Controller for the first document that was picked.
        // If you support picking multiple items, make sure you handle them all.
        presentDocument(at: sourceURL)
    }
    
    func documentPicker(_ controller: UIDocumentBrowserViewController, didImportDocumentAt sourceURL: URL, toDestinationURL destinationURL: URL) {
        // Present the Document View Controller for the new newly created document
        presentDocument(at: destinationURL)
    }
    
    func documentPicker(_ controller: UIDocumentBrowserViewController, failedToImportDocumentAt documentURL: URL, error: Error?) {
        // Make sure to handle the failed import appropriately, e.g., by presenting an error message to the user.
    }
    
    // MARK: Document Presentation
    
    // #3.0 - Prepare for and present my custom user interface
    // which uses a UIDocument subclass to manipulate and/or
    // display the user-select document.
    func presentDocument(at documentURL: URL) {
        // #3.1 - Get the UIViewController scene on the right side of Main.storyboard
        // which is backed by the DocumentViewController class in the
        // DocumentViewController.swift file and instantiate it.
        // That's my custom UI.
        
        //            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        //            let documentViewController = storyBoard.instantiateViewController(withIdentifier: "DocumentViewController") as! DocumentViewController
        //
        //            // #3.2 - In the template project, the DocumentViewController
        //            // class has a member "document" of type UIDocument, which is an
        //            // "abstract base class." I had to subclass UIDocument.
        //            // I then initialize my UIDocument subclass.
        //            documentViewController.document = Document(fileURL: documentURL)
        //
        //            // #3.3 - Present my custom user interface for
        //            // displaying and manipulating a user-selected
        //            // document.
        //            present(documentViewController, animated: true, completion: nil)
    }
}

extension FileManager {
    open func secureCopyItem(at srcURL: URL, to dstURL: URL) -> Bool {
        do {
            if FileManager.default.fileExists(atPath: dstURL.path) {
                try FileManager.default.removeItem(at: dstURL)
            }
            try FileManager.default.copyItem(at: srcURL, to: dstURL)
        } catch (let error) {
            print("Cannot copy item at \(srcURL) to \(dstURL): \(error)")
            return false
        }
        return true
    }
}

