//
//  UploadVC.swift
//  Latakoo
//
//  Created by Mayur Sardana on 22/01/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//




import AFNetworking
import Alamofire
import AVFoundation
import AWSCore
import AWSS3
import Foundation
import Photos
import SVProgressHUD
import SwiftyJSON
import UIKit
import WebKit
import objective_zip
import AWSMobileClient

class UploadVC: UIViewController, NSURLConnectionDataDelegate, UIWebViewDelegate {
    // MARK: VARIABLE  .........................................

    @IBOutlet var viewmetadata: UIWebView!
    @IBOutlet var viewupload: UIView!
    @IBOutlet var lblcount: UILabel!
    @IBOutlet var compressionprogress: UIProgressView!
    @IBOutlet var preparingprogress: UIProgressView!
    @IBOutlet var uploadingprogress: UIProgressView!
    @IBOutlet var lblcompration: UILabel!
    @IBOutlet var lblprepairing: UILabel!
    @IBOutlet var viewcompress: UIView!
    @IBOutlet var viewprepare: UIView!
    @IBOutlet var viewuploadgreen: UIView!
    @IBOutlet var viewdownloadmore: UIView!
    @IBOutlet var btnpause: UIButton!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var lblmsg: UILabel!
    @IBOutlet var btnmanifest: UIButton!
    @IBOutlet var viewCircularProgressBar: CircularProgressBar!

    // MARK: VARIABLE  .........................................

    var originalsize: Int = 0
    var Compressedsize: Int = 0
    var isimage: String!
    var FilesArray: [PHAsset] = []
    var filearrayaudio: [String] = []
    var filename: String!
    var uploadquality: Int = 2
    var itemcode: String = ""
    var uploadserver: String = ""
    var TID: String = ""
    var ext: String = ""
    var uploadtime: Int = 0
    var incodetime: Int = 0
    var timer = Timer()
    var Encodetimer = Timer()
    var chosenBitrateindex: Int = 0
    var chosenBitrate: Int = 0
    var progress: Float = 0
    var ismultipleupload: Bool = false
    var responseData: Data?
    var videoduration: String = ""
    var totalchunk: Int = 0
    var currentchunk: Int = 0
    var filedata: Data?
    var chunksize: Int = 0
    var lastchunksize: Int = 0
    var dataArr: [Data] = []
    var groupuploadcurrentindex: Int = 0
    var fileurl: URL?
    var assetWriter: AVAssetWriter?
    var assetReader: AVAssetReader?
    var connection: NSURLConnection?

    var thumbpath: String?
    var dictcredential: [String: Any] = [:]
    var videoinfodata: Data?
    var imageinfodata: Data?
    var audioinfodata: Data?
    
    // MARK: HEADER FILES .........................................

    var X_LATAKOO_VIDEO_FORMAT = ""
    var X_PFTP_LOCAL_TS_OFFSET = ""
    var X_LATAKOO_COMPANY = ""
    var X_LATAKOO_ARCH = ""
    var X_LATAKOO_IS64 = ""
    var X_LATAKOO_CORES = ""
    var X_LATAKOO_MEM = ""
    var X_LATAKOO_OS = ""
    var X_LATAKOO_TID = ""
    var X_LATAKOO_APP = ""
    var X_LATAKOO_CODEC = ""
    var X_LATAKOO_ENCODE = ""
    var X_LATAKOO_UPLOAD = ""
    var X_LATAKOO_BITRATE = ""
    var X_LATAKOO_FILESIZE = ""
    var X_LATAKOO_CONTAINER = ""
    var X_LATAKOO_WIDTH = ""
    var X_LATAKOO_HEIGHT = ""
    var X_LATAKOO_HARDWARE = ""
    var X_LATAKOO_MIME_TYPE = ""
    var X_LATAKOO_TITLE = ""
    var X_LATAKOO_KEYWORDS = ""
    var X_LATAKOO_RECIPIENTS = ""
    var X_LATAKOO_ORIG_FOLDER = ""
    var X_LATAKOO_GROUP = ""
    var X_LATAKOO_GROUP_FILENAME = ""
    var LATAKOO_MOBILE_STITCH_UPLOAD_WITH_MEDIA_COMPATIBILITY_WARNING = ""
    var X_LATAKOO_GROUP_ORDER: NSNumber = 0
    var X_LATAKOO_GROUP_PIECES: NSNumber = 0
    var X_LATAKOO_ITEM_CODE = ""
    var webviewloaded: Bool = false
    var bgTask: BackgroundTask?
    var currentrequest = ""
    var uploadlogs  = "IOS File Log => \n"
    var islogapi : Bool = false

    @IBOutlet weak var menifestheight: NSLayoutConstraint!
    
    
    @IBOutlet var viewprogress: UIView!
    @IBOutlet var lblcountnew: UILabel!
    
    
    // MARK: .........................................

    let appdelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
                
        let str = UserDefaults.standard.value(forKey: "isshowmenifest") as? String
              if (str == "NO") {
                menifestheight.constant = 0
                btnmanifest.isHidden = true
              }
     
        
        
        
        viewupload.isHidden = true
        
        if appdelegate.fileurl == nil {
            TID = randomString(length: 32)
                   print("tid => \(TID)")
            appdelegate.appTID = TID
            

        }else{
            
            TID = appdelegate.appTID
        }
        
        tabBarController?.tabBar.isHidden = true
        
        let userdetails = UserDefaults.standard.object(forKey: "userdetails")
        
        uploadlogs.append("userdetails => \(userdetails ?? "") \n")

        
        let modelName = UIDevice.modelName
        uploadlogs.append("Device Name => \(modelName) \n")
          
         let systemVersion = UIDevice.current.systemVersion
        uploadlogs.append("Device OS => \(systemVersion) \n")

        setprogressbarheight()

        bgTask = BackgroundTask()
        DispatchQueue.global(qos: .default).async(execute: {
            self.bgTask?.startBackgroundTasks(2, target: self, selector: #selector(self.backgroundCallback(_:)))

        })


        activity.startAnimating()
        checkifvideo(currentindex: 0)
        setheaderfiles()

    }

    @objc func backgroundCallback(_ info: Any?) {
      //    print("bg task running")
    }

    func setheaderfiles() {
        X_LATAKOO_GROUP_FILENAME = filename
        X_LATAKOO_TITLE = filename
        X_LATAKOO_GROUP = randomString(length: 15)
        if appdelegate.ArrNetworks.count > 0 {
            let string = appdelegate.ArrNetworks.map { String($0) }
                .joined(separator: ", ")
            print("company =>  \(string)")
            X_LATAKOO_COMPANY = string
        } else {
            X_LATAKOO_COMPANY = UserDefaults.standard.value(forKey: "defultnetwork") as! String
        }

        if appdelegate.ArrEmail.count > 0 {
            var jsonData: Data?
            do {
                jsonData = try JSONSerialization.data(withJSONObject: appdelegate.ArrEmail, options: [])
            } catch {
            }
            var jsonString: String?
            if let jsonData = jsonData {
                jsonString = String(data: jsonData, encoding: .utf8)
            }
            X_LATAKOO_RECIPIENTS = jsonString!

            print("emails =>  \(X_LATAKOO_RECIPIENTS)")
        }

        if appdelegate.ArrKeywords.count > 0 {
            var jsonData: Data?
            do {
                jsonData = try JSONSerialization.data(withJSONObject: appdelegate.ArrKeywords, options: [])
            } catch {
            }
            var jsonString: String?
            if let jsonData = jsonData {
                jsonString = String(data: jsonData, encoding: .utf8)
            }
            X_LATAKOO_KEYWORDS = jsonString!
            print("keywords =>  \(jsonString ?? "")")
        }

        let dictuser = UserDefaults.standard.value(forKey: "userdetails") as! [String: Any]
        let bitratearry = dictuser["bitrates_details"] as! [Any]
        let dict = bitratearry[chosenBitrateindex] as! [String: Any]
        print(dict)
//        if (UserDefaults.standard.value(forKey: "server") as! String == "https://dev.latakoo.com/"){
//            chosenBitrate = dict["bitrate"] as! Int
//        }else{
        
        chosenBitrate = Int(dict["bitrate"] as? String ?? "0") ?? 2

      //  }
        print(chosenBitrate)
       
        
        apigetmetadata(tid: TID)
   
    
    }

    // MARK: METADATAUPLOAD
    func apigetmetadata(tid: String) {
             
        if FilesArray.count > 1 || filearrayaudio.count > 1 {
            self.ismultipleupload = true
        }
        
        
        if app.uploadas == "Individual"{
            
            self.ismultipleupload = false

        }
        
                          if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }else{
        AJNotificationView.hideCurrentNotificationViewAndClearQueue()
        AJNotificationView.hideCurrentNotificationView()
        }
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                let organizationid = UserDefaults.standard.value(forKey: "organization_id")
        var forwordurl = "\(UserDefaults.standard.value(forKey: "server") as! String)form/\(organizationid ?? "")/\(TID)/"
      
     if ismultipleupload {
            forwordurl = "\(UserDefaults.standard.value(forKey: "server") as! String)form/\(organizationid ?? "")/group/\(X_LATAKOO_GROUP)/"
        }
     
          var  title = filename.fileName()
          title = String(format:"[\"%@\"]", title)
     
     let assignment_id = appdelegate.AssignmentID
     let manifest_id = appdelegate.OrgID

       var  aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=session.link.get&forward_url=\(forwordurl)&data[network_ids]=\(X_LATAKOO_COMPANY)&data[titles]=\(title )&data[assignment_id]=\(assignment_id )&data[manifest_id]=\(manifest_id )&version=new"
     
     
     
        aStrApi  = AJNotificationView.changetoencodedstring(aStrApi)
       self.uploadlogs.append("\n meta data api  => \n\(aStrApi)")

     
     
        print("meta data get link => \(aStrApi )")
          let heder =  appdelegate.getAPIEncodedHeader(withParameters: aStrApi)
          let headervalue = ["X_PFTP_API_TOKEN":heder]
          APIManager.requestGETURLNEW(aStrApi, params: nil, headers: headervalue as? [String : String], success: {(usrDetail) in
            print(usrDetail)
             
             self.uploadlogs.append("\n meta data api responce  => \n\(usrDetail)")

            if usrDetail["status_code"].rawString() == "0"{
                let usrDetailVal = usrDetail["result"].rawValue as! String
              print(usrDetailVal)
             
             if self.webviewloaded == false{
               self.webviewloaded = true
            
              self.loadwebview(path: usrDetailVal)
             }
                        
             }else
            {
                DispatchQueue.main.async {
                    self.viewupload.isHidden = false
                    SVProgressHUD.dismiss()
//                    self.btnpause.setTitle("Pause", for: .normal)
                }
            
            }
                  },
                                  
                      failure: {(error) in
                       
                        DispatchQueue.main.async {
                            print(error)

                            self.viewupload.isHidden = false
                            SVProgressHUD.dismiss()
//                            self.btnpause.setTitle("Pause", for: .normal)
                        }
                      print(error)
        })
    }

    func loadwebview(path: String) {
        let request = URLRequest(url: URL(string: path)!)
        DispatchQueue.main.async {
            self.viewmetadata?.loadRequest(request)
        }
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        //   appdelegate.HideProgress()
        activity.isHidden = true
        lblmsg.isHidden = true
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        //   SVProgressHUD.dismiss()

        activity.isHidden = true
        lblmsg.isHidden = true
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print(request.url?.absoluteString as Any)
        if (request.url?.absoluteString.hasPrefix("ios:"))! {
            viewupload.isHidden = false
//            self.btnpause.setTitle("Pause", for: .normal)
        }
        return true
    }

    func setprogressbarheight() {
        viewcompress.layer.cornerRadius = viewcompress.frame.size.width / 2
        viewcompress.layer.masksToBounds = true
        viewprepare.layer.cornerRadius = viewprepare.frame.size.width / 2
        viewprepare.layer.masksToBounds = true

        viewuploadgreen.layer.cornerRadius = viewuploadgreen.frame.size.width / 2
        viewuploadgreen.layer.masksToBounds = true

        compressionprogress.transform = viewuploadgreen.transform.scaledBy(x: 1, y: 4)
        preparingprogress.transform = viewuploadgreen.transform.scaledBy(x: 1, y: 4)
        uploadingprogress.transform = viewuploadgreen.transform.scaledBy(x: 1, y: 4)
    }

    @IBAction func btnpause(_ sender: Any) {
       
        if connection != nil {
            connection?.cancel()
        }
                
        if btnpause.titleLabel?.text == "Cancel" {
            self.cancelUploading()
            _ = createaudiofile()
            _ = createimagefile()
            _ = createFile()
        }
        else if btnpause.titleLabel?.text == "Pause" {
            self.cancelUploading()
            appdelegate.fileurl = fileurl
        }
        else {
            _ = createaudiofile()
            _ = createimagefile()
            _ = createFile()
            self.cancelUploading()
        }
        

        navigationController?.popViewController(animated: true)
    }
    
    func cancelUploading() {
        if appdelegate.bucketpTest != "" {
            let awsTask = AWSS3TransferUtility.s3TransferUtility(forKey: appdelegate.bucketpTest)!.getMultiPartUploadTasks()
            if let taskArray = awsTask.result {
                print("taskArray: \(taskArray)")
                for idx in taskArray {
                    print("idx: \(idx)")
                    if let task = idx as? AWSS3TransferUtilityMultiPartUploadTask {
                        task.cancel()
                        AWSS3TransferUtility.remove(forKey: appdelegate.bucketpTest)
                        appdelegate.bucketpTest = ""
                        DispatchQueue.global(qos: .background).async {
                            self.bgTask?.stop()
                        }
                    }
                }
            }
        }
    }
    
    func pauseUploading() {
        if appdelegate.bucketpTest != "" {
            let awsTask = AWSS3TransferUtility.s3TransferUtility(forKey: appdelegate.bucketpTest)!.getMultiPartUploadTasks()
            if let taskArray = awsTask.result {
                print("taskArray: \(taskArray)")
                for idx in taskArray {
                    print("idx: \(idx)")
                    if let task = idx as? AWSS3TransferUtilityMultiPartUploadTask {
//                        task.cancel()
                        task.suspend()
//                        AWSS3TransferUtility.remove(forKey: appdelegate.bucketpTest)
//                        appdelegate.bucketpTest = ""
                        DispatchQueue.global(qos: .background).async {
                            self.bgTask?.stop()
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func btnnometadata(_ sender: Any) {
        DispatchQueue.main.async {
            self.viewupload.isHidden = false
            SVProgressHUD.dismiss()
//            self.btnpause.setTitle("Pause", for: .normal)
        }
    }

    func checkifvideoimageaudio() {
        viewcompress.backgroundColor = UIColor(red: 203 / 255, green: 201 / 255, blue: 0 / 255, alpha: 1.0)
        viewprepare.backgroundColor = UIColor(red: 203 / 255, green: 201 / 255, blue: 0 / 255, alpha: 1.0)
        viewuploadgreen.backgroundColor = UIColor(red: 203 / 255, green: 201 / 255, blue: 0 / 255, alpha: 1.0)
        if isimage == "video" {
            ext = "mp4"
            if FilesArray.count > 1 {
                ismultipleupload = true
                groupvideoupload(currentindex: groupuploadcurrentindex)
            } else {
                ismultipleupload = false
                let asset: PHAsset = FilesArray[0]
                getthumnail(asset: asset)

                print(asset.localIdentifier)
                
                uploadlogs.append("asset localIdentifier => \(asset.localIdentifier ) \n")

                
                let resourceArray = PHAssetResource.assetResources(for: asset)
                                                                 let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false
                                                                  if  bIsLocallayAvailable == false {
                
                DispatchQueue.main.async {

                self.viewCircularProgressBar.labelSize = 30
                    self.viewCircularProgressBar.safePercent = 100
                self.viewCircularProgressBar.setProgress(to: 0, withAnimation: true)
                KGModal.sharedInstance()?.show(withContentView: self.viewprogress)
                KGModal.sharedInstance()?.tapOutsideToDismiss = false
                self.viewCircularProgressBar.backgroundColor = UIColor.clear

                }
                                                                    
                }
                DispatchQueue.global(qos: .background).async {
                    let option1 = PHVideoRequestOptions()
                    option1.isNetworkAccessAllowed = true
                    option1.version = .original
                    option1.deliveryMode = .highQualityFormat
                    option1.progressHandler = {  (progress, error, stop, info) in

                        let resourceArray = PHAssetResource.assetResources(for: asset)
                                                                                    let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false
                                                                                     if  bIsLocallayAvailable == false {
                                   
                                   DispatchQueue.main.async {
                    self.viewCircularProgressBar.setProgress(to: progress, withAnimation: true)
                              
                        }
                        }
                        
                    }
                    
                    
                    
                    var resultAsset: AVAsset?
                    PHCachingImageManager.default().requestAVAsset(forVideo: asset, options: option1, resultHandler: { avasset, _, _ in

                        DispatchQueue.main.async {
                            resultAsset = avasset
                            KGModal.sharedInstance()?.hide()
                            
                            let asset = avasset as? AVURLAsset

                            let duration = resultAsset?.duration
                            let durationTime = CMTimeGetSeconds(duration ?? CMTimeMake(1, 1))
                            self.videoduration = "\(durationTime)"

                            let data = "any string".data(using: .utf8)

                            let messageData = self.MD5New(dattaaa: data! as Data)
                            let strMDhash = messageData.hexString()

                            let filesize = NSNumber(nonretainedObject: asset?.fileSize)
                            self.X_LATAKOO_FILESIZE = "\(filesize)"
                            var filename: String = ""
                            filename = "\(self.filename ?? "").mp4"
                            print(self.filename)
                            self.originalsize = (asset?.fileSize ?? 0)

                            if asset?.url != nil {
                                self.compressvideo(assetUrl: avasset!, videourl: asset!.url, filename: filename, md5hash: strMDhash)
                            } else {
                                let alert = UIAlertController(title: "Alert", message: "File not supported.", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    })
                }
            }

        } else if isimage == "image" {
            ext = "jpg"
            videoduration = "0"
            if FilesArray.count > 1 {
                ismultipleupload = true
                groupimageupload(currentindex: groupuploadcurrentindex)
            } else {
                ismultipleupload = false
                let asset: PHAsset = FilesArray[0]
                var imagedata: Data?
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
                option.isNetworkAccessAllowed = true
                var thumbnail = UIImage()
                option.isSynchronous = true
                manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: { (result, _) -> Void in
                    let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                    imagedata = UIImageJPEGRepresentation(thumbnail, 0.8)
                    let messageData = self.MD5New(dattaaa: imagedata!)
                    let strMDhash = messageData.hexString()
                    let filesize = NSNumber(integerLiteral: imagedata!.count)
                    self.X_LATAKOO_FILESIZE = "\(filesize)"
                    print(imagedata?.count ?? 0)
                    self.filedata = imagedata
                    imagedata = nil
                    var filename: String = ""
                    self.Compressedsize = self.filedata!.count

                    let outputPath = self.createimagefile()
                    self.fileurl = outputPath

                    do {
                        try self.filedata?.write(to: outputPath, options: .atomic)
                        sleep(1)
                    } catch {
                        print(error)
                    }

                    self.originalsize = UIImageJPEGRepresentation(thumbnail, 1.0)!.count
                    self.setcomresslabel(Originalsize: self.originalsize, Compressedsize: self.Compressedsize)

                    filename = "\(self.filename ?? "").jpg"
                    print(self.filename)
                    self.compressionprogress.setProgress(1, animated: true)
                    self.viewcompress.backgroundColor = UIColor(red: 21 / 255, green: 159 / 255, blue: 2 / 255, alpha: 1.0)
                    self.ApiBeginupload(filename: filename, filesize: filesize, md5hash: strMDhash)

                })
            }

        } else {
            ext = "m4a"
            if filearrayaudio.count > 1 {
                print(filearrayaudio)
                ismultipleupload = true
                groupaudioupload(currentindex: groupuploadcurrentindex)
            } else {
                ismultipleupload = false
                print(filearrayaudio)
                var filename: String = ""

                filename = "\(self.filename ?? "")"
                let aData = NSData(contentsOfFile: filearrayaudio[0]) as Data?

                print(aData!.count)
                filedata = aData
                Compressedsize = aData!.count
                originalsize = aData!.count
                setcomresslabel(Originalsize: aData!.count, Compressedsize: aData!.count)
                let messageData = MD5New(dattaaa: aData!)
                let strMDhash = messageData.hexString()
                let filesize = NSNumber(integerLiteral: aData!.count)
                
                let outputPath = createaudiofile()
                fileurl = outputPath
                do {
                    try filedata?.write(to: outputPath, options: .atomic)
                    sleep(1)
                } catch {
                    print(error)
                }
                
                let audioAsset = AVURLAsset(url: fileurl!, options: nil)
                let audioDuration = audioAsset.duration
                let audioDurationSeconds = Float(CMTimeGetSeconds(audioDuration))
                videoduration = String(audioDurationSeconds)
                
                ApiBeginupload(filename: filename, filesize: filesize, md5hash: strMDhash)
            }
        }
    }

    // MARK: ...............SEND DATA TO SERVER...........
    func senddatatoserver() {
        self.btnpause.setTitle("Pause", for: .normal)
        if isimage == "video" {
            var filename: String = ""
            filename = "\(self.filename ?? "").mp4"
            appdelegate.filename = filename
            print(appdelegate.bucketpath)
            print(appdelegate.filename)
            uploadvideotos3()

        } else if isimage == "image" {
            var filename: String = ""
            filename = "\(self.filename ?? "").jpg"
            appdelegate.filename = filename
            print(appdelegate.bucketpath)
            print(appdelegate.filename)
            uploadimagetos3()

        } else {
            var filename: String = ""
            filename = "\(self.filename ?? "").m4a"
            print("filename: \(filename)")
            appdelegate.filename = filename
            print(appdelegate.bucketpath)
            print(appdelegate.filename)
            uploadaudiotos3()
        }
    }

    // MARK: ...............BEGINUPLOAD API...........
    func ApiBeginupload(filename: String, filesize: NSNumber, md5hash: String) {
        preparingprogress.setProgress(1, animated: true)
        viewprepare.backgroundColor = UIColor(red: 21 / 255, green: 159 / 255, blue: 2 / 255, alpha: 1.0)
        lblprepairing.isHidden = false

       
        senddatatoserver()
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
//        self.btnpause.setTitle("Pause", for: .normal)
    }

    @objc func timerAction() {
        uploadtime += 1
        X_LATAKOO_UPLOAD = "\(uploadtime)"
    }

    // MARK: ..............MD5...........
    func MD5New(dattaaa: Data) -> Data {
        let messageData = dattaaa
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))

        _ = digestData.withUnsafeMutableBytes { digestBytes in
            messageData.withUnsafeBytes { messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }

        return digestData
    }

    func initiateConnection(with req: URLRequest?) {
        connection = NSURLConnection(request: req!, delegate: self, startImmediately: false)
        connection?.start()
    }

    func connection(_ connection: NSURLConnection, didSendBodyData bytesWritten: Int, totalBytesWritten: Int, totalBytesExpectedToWrite: Int) {
  
    }

    func connection(_ connection: NSURLConnection, didReceive response: URLResponse) {
        responseData = nil
    }

    func connection(_ connection: NSURLConnection, didReceive data: Data) {
        responseData = data
    }

    func connection(_ connection: NSURLConnection, willCacheResponse cachedResponse: CachedURLResponse) -> CachedURLResponse? {
        return nil
    }

    func connectionDidFinishLoading(_ connection: NSURLConnection) {
        didReceiveData(_responcedata: responseData!)
    }

    func connection(_ connection: NSURLConnection, didFailWithError error: Error) {
        print(error)
        connection.cancel()

                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
        } else {
            AJNotificationView.hideCurrentNotificationViewAndClearQueue()
            AJNotificationView.hideCurrentNotificationView()
        }

        perform(#selector(sendagain), with: nil, afterDelay: 5.0)
    }

    @objc func sendagain() {
        senddatatoserver()
    }

    func didReceiveData(_responcedata: Data) {
        let result = String(data: _responcedata, encoding: .utf8)
        print("content = \(result ?? "")")
        if islogapi == false{

        if currentrequest == "credentialrequest" {
            currentrequest = ""
            do {
                let json = try JSONSerialization.jsonObject(with: responseData!, options: []) as? [String: Any]
                print(json!)
                if (json?["result"]) != nil {
                    let dict = json!["result"] as? [String: Any]
                    print(dict!)
                    credentialdata(dictcredential: dict!)
                } else {
                    refreshToken()
                }
            } catch {
                print(error)
                self.getCredentialThroughMediaUploadAPI()
            }
        }
        }
    }
    
    func showuploadmoreview() {
        _ = createaudiofile()
        _ = createimagefile()
        _ = createFile()
     
        app.uploadas = ""

        self.writeToTextFile()

        appdelegate.fileurl = nil
        appdelegate.AssignmentID = ""
        appdelegate.TopicID = ""
        appdelegate.OrgID = ""
        appdelegate.Assignmentname = ""
        
        uploadingprogress.setProgress(1, animated: false)
        viewuploadgreen.backgroundColor = UIColor(red: 21 / 255, green: 159 / 255, blue: 2 / 255, alpha: 1.0)
        viewdownloadmore.isHidden = false
        btnpause.isHidden = true
        DispatchQueue.global(qos: .background).async {
            self.bgTask?.stop()
        }
    }

    // MARK: HEADER FILES FOR SINGLE MULTIPLE UPLOAD

    func setheaderforfile(api: String) -> [String: String] {
        let apitokem: String = appdelegate.getAPIEncodedHeader(withParameters: api)!

        var dictheader: [String: String] = [:]
        if isimage == "image" {
            if ismultipleupload == true {
                dictheader = ["X_PFTP_API_TOKEN": apitokem,
                              "X_PFTP_LOCAL_TS_OFFSET": "19800",
                              "X_LATAKOO_ARCH": "ARM",
                              "X_LATAKOO_COMPANY": X_LATAKOO_COMPANY,
                              "X_LATAKOO_IS64": "0",
                              "X_LATAKOO_CORES": "6",
                              "X_LATAKOO_MEM": "1037041664",
                              "X_LATAKOO_OS": "iOS",
                              "X_LATAKOO_TID": TID,
                              "X_LATAKOO_APP": "3.1.3",
                              "X_LATAKOO_ENCODE": "0",
                              "X_LATAKOO_CODEC": X_LATAKOO_CODEC,
                              "X_LATAKOO_UPLOAD": X_LATAKOO_UPLOAD,
                              "X_LATAKOO_BITRATE": X_LATAKOO_BITRATE,
                              "X_LATAKOO_FILESIZE": "\(originalsize)",
                              "X_LATAKOO_CONTAINER": X_LATAKOO_CONTAINER,
                              "X_LATAKOO_WIDTH": X_LATAKOO_WIDTH,
                              "X_LATAKOO_HEIGHT": X_LATAKOO_HEIGHT,
                              "X_LATAKOO_MIME_TYPE": "image/jpg",
                              "X_LATAKOO_TITLE": "\(X_LATAKOO_TITLE).jpg",
                              "X_LATAKOO_GROUP_FILENAME": X_LATAKOO_GROUP_FILENAME,
                              "X_LATAKOO_KEYWORDS": X_LATAKOO_KEYWORDS,
                              "X_LATAKOO_RECIPIENTS": X_LATAKOO_RECIPIENTS,
                              "X_LATAKOO_ORIG_FOLDER": X_LATAKOO_GROUP,
                              "X_LATAKOO_GROUP_ORDER": "\(X_LATAKOO_GROUP_ORDER)",
                              "X_LATAKOO_GROUP_PIECES": "\(FilesArray.count)",
                              "X_LATAKOO_GROUP_TIMECODE": "",
                              "X_LATAKOO_GROUP_USE_TIMECODE": "",
                              "X_LATAKOO_ITEM_CODE": X_LATAKOO_ITEM_CODE,
                ]

            } else {
                dictheader = ["X_PFTP_API_TOKEN": apitokem,
                              "X_PFTP_LOCAL_TS_OFFSET": "19800",
                              "X_LATAKOO_ARCH": "ARM",
                              "X_LATAKOO_COMPANY": X_LATAKOO_COMPANY,
                              "X_LATAKOO_IS64": "0",
                              "X_LATAKOO_CORES": "6",
                              "X_LATAKOO_MEM": "1037041664",
                              "X_LATAKOO_OS": "iOS",
                              "X_LATAKOO_TID": TID,
                              "X_LATAKOO_APP": "3.1.3",
                              "X_LATAKOO_ENCODE": "",
                              "X_LATAKOO_CODEC": X_LATAKOO_CODEC,
                              "X_LATAKOO_UPLOAD": X_LATAKOO_UPLOAD,
                              "X_LATAKOO_BITRATE": X_LATAKOO_BITRATE,
                              "X_LATAKOO_FILESIZE": "\(originalsize)",
                              "X_LATAKOO_CONTAINER": X_LATAKOO_CONTAINER,
                              "X_LATAKOO_WIDTH": X_LATAKOO_WIDTH,
                              "X_LATAKOO_HEIGHT": X_LATAKOO_HEIGHT,
                              "X_LATAKOO_MIME_TYPE": "image/jpg",
                              "X_LATAKOO_TITLE": X_LATAKOO_TITLE,
                              "X_LATAKOO_KEYWORDS": X_LATAKOO_KEYWORDS,
                              "X_LATAKOO_RECIPIENTS": X_LATAKOO_RECIPIENTS,
                              "X_LATAKOO_ITEM_CODE": X_LATAKOO_ITEM_CODE,
                ]
            }

        } else if isimage == "video" {
            if ismultipleupload == true {
                dictheader = ["X_PFTP_API_TOKEN": apitokem,
                              "X_PFTP_LOCAL_TS_OFFSET": "19800",
                              "X_LATAKOO_ARCH": "ARM",
                              "X_LATAKOO_COMPANY": X_LATAKOO_COMPANY,
                              "X_LATAKOO_IS64": "0",
                              "X_LATAKOO_CORES": "6",
                              "X_LATAKOO_MEM": "1037041664",
                              "X_LATAKOO_OS": "iOS",
                              "X_LATAKOO_TID": TID,
                              "X_LATAKOO_APP": "3.1.3",
                              "X_LATAKOO_ENCODE": "0",
                              "X_LATAKOO_CODEC": "H.264",
                              "X_LATAKOO_UPLOAD": X_LATAKOO_UPLOAD,
                              "X_LATAKOO_BITRATE": X_LATAKOO_BITRATE,
                              "X_LATAKOO_FILESIZE": "\(originalsize)",
                              "X_LATAKOO_CONTAINER": X_LATAKOO_CONTAINER,
                              "X_LATAKOO_WIDTH": X_LATAKOO_WIDTH,
                              "X_LATAKOO_HEIGHT": X_LATAKOO_HEIGHT,
                              "X_LATAKOO_TITLE": "\(X_LATAKOO_TITLE).mp4",
                              "X_LATAKOO_GROUP_FILENAME": X_LATAKOO_GROUP_FILENAME,
                              "X_LATAKOO_KEYWORDS": X_LATAKOO_KEYWORDS,
                              "X_LATAKOO_RECIPIENTS": X_LATAKOO_RECIPIENTS,
                              "X_LATAKOO_GROUP": X_LATAKOO_GROUP,
                              "X_LATAKOO_GROUP_ORDER": "\(X_LATAKOO_GROUP_ORDER)",
                              "X_LATAKOO_GROUP_PIECES": "\(FilesArray.count)",
                              "X_LATAKOO_GROUP_TIMECODE": "",
                              "X_LATAKOO_GROUP_USE_TIMECODE": "",
                              "X_LATAKOO_ITEM_CODE": X_LATAKOO_ITEM_CODE,
                              "X_LATAKOO_VIDEO_FORMAT": X_LATAKOO_VIDEO_FORMAT,
                ]
                
                if app.wrongstitch == true {
                   dictheader["LATAKOO_MOBILE_STITCH_UPLOAD_WITH_MEDIA_COMPATIBILITY_WARNING"] = "True"
                }
                


            } else {
                dictheader = ["X_PFTP_API_TOKEN": apitokem,
                              "X_PFTP_LOCAL_TS_OFFSET": "19800",
                              "X_LATAKOO_ARCH": "ARM",
                              "X_LATAKOO_COMPANY": X_LATAKOO_COMPANY,
                              "X_LATAKOO_IS64": "0",
                              "X_LATAKOO_CORES": "6",
                              "X_LATAKOO_MEM": "1037041664",
                              "X_LATAKOO_OS": "iOS",
                              "X_LATAKOO_TID": TID,
                              "X_LATAKOO_APP": "3.1.3",
                              "X_LATAKOO_ENCODE": X_LATAKOO_ENCODE,
                              "X_LATAKOO_CODEC": "H.264",
                              "X_LATAKOO_UPLOAD": X_LATAKOO_UPLOAD,
                              "X_LATAKOO_BITRATE": X_LATAKOO_BITRATE,
                              "X_LATAKOO_FILESIZE": "\(originalsize)",
                              "X_LATAKOO_CONTAINER": X_LATAKOO_CONTAINER,
                              "X_LATAKOO_WIDTH": X_LATAKOO_WIDTH,
                              "X_LATAKOO_HEIGHT": X_LATAKOO_HEIGHT,
                              "X_LATAKOO_TITLE": X_LATAKOO_TITLE,
                              "X_LATAKOO_KEYWORDS": X_LATAKOO_KEYWORDS,
                              "X_LATAKOO_RECIPIENTS": X_LATAKOO_RECIPIENTS,
                              "X_LATAKOO_ITEM_CODE": X_LATAKOO_ITEM_CODE,
                              "X_LATAKOO_VIDEO_FORMAT": X_LATAKOO_VIDEO_FORMAT,
                ]
            }

        } else {
            if ismultipleupload == true {
                dictheader = ["X_PFTP_API_TOKEN": apitokem,
                              "X_PFTP_LOCAL_TS_OFFSET": "19800",
                              "X_LATAKOO_ARCH": "ARM",
                              "X_LATAKOO_COMPANY": X_LATAKOO_COMPANY,
                              "X_LATAKOO_IS64": "0",
                              "X_LATAKOO_CORES": "6",
                              "X_LATAKOO_MEM": "1037041664",
                              "X_LATAKOO_OS": "iOS",
                              "X_LATAKOO_TID": TID,
                              "X_LATAKOO_APP": "3.1.3",
                              "X_LATAKOO_ENCODE": "0",
                              "X_LATAKOO_CODEC": X_LATAKOO_CODEC,
                              "X_LATAKOO_UPLOAD": X_LATAKOO_UPLOAD,
                              "X_LATAKOO_BITRATE": X_LATAKOO_BITRATE,
                              "X_LATAKOO_FILESIZE": "\(originalsize)",
                              "X_LATAKOO_CONTAINER": X_LATAKOO_CONTAINER,
                              "X_LATAKOO_WIDTH": X_LATAKOO_WIDTH,
                              "X_LATAKOO_HEIGHT": X_LATAKOO_HEIGHT,
                              "X_LATAKOO_TITLE": "\(X_LATAKOO_TITLE).m4a",
                              "X_LATAKOO_GROUP_FILENAME": X_LATAKOO_GROUP_FILENAME,
                              "X_LATAKOO_KEYWORDS": X_LATAKOO_KEYWORDS,
                              "X_LATAKOO_RECIPIENTS": X_LATAKOO_RECIPIENTS,
                              "X_LATAKOO_GROUP": X_LATAKOO_GROUP,
                              "X_LATAKOO_GROUP_ORDER": "\(X_LATAKOO_GROUP_ORDER)",
                              "X_LATAKOO_GROUP_PIECES": "\(filearrayaudio.count)",
                              "X_LATAKOO_GROUP_TIMECODE": "",
                              "X_LATAKOO_GROUP_USE_TIMECODE": "",
                              "X_LATAKOO_ITEM_CODE": X_LATAKOO_ITEM_CODE,
                ]

            } else {
                dictheader = ["X_PFTP_API_TOKEN": apitokem,
                              "X_PFTP_LOCAL_TS_OFFSET": "19800",
                              "X_LATAKOO_ARCH": "ARM",
                              "X_LATAKOO_COMPANY": X_LATAKOO_COMPANY,
                              "X_LATAKOO_IS64": "0",
                              "X_LATAKOO_CORES": "6",
                              "X_LATAKOO_MEM": "1037041664",
                              "X_LATAKOO_OS": "iOS",
                              "X_LATAKOO_TID": TID,
                              "X_LATAKOO_APP": "3.1.3",
                              "X_LATAKOO_ENCODE": "0",
                              "X_LATAKOO_CODEC": X_LATAKOO_CODEC,
                              "X_LATAKOO_UPLOAD": X_LATAKOO_UPLOAD,
                              "X_LATAKOO_BITRATE": X_LATAKOO_BITRATE,
                              "X_LATAKOO_FILESIZE": "\(originalsize)",
                              "X_LATAKOO_CONTAINER": X_LATAKOO_CONTAINER,
                              "X_LATAKOO_WIDTH": X_LATAKOO_WIDTH,
                              "X_LATAKOO_HEIGHT": X_LATAKOO_HEIGHT,
                              "X_LATAKOO_TITLE": X_LATAKOO_TITLE,
                              "X_LATAKOO_KEYWORDS": X_LATAKOO_KEYWORDS,
                              "X_LATAKOO_RECIPIENTS": X_LATAKOO_RECIPIENTS,
                              "X_LATAKOO_ITEM_CODE": X_LATAKOO_ITEM_CODE,
                ]
            }
        }

        return dictheader
    }
    
    func setheaderforfile1(api: String) -> [String: String] {
        let apitokem: String = appdelegate.getAPIEncodedHeader(withParameters: api)!
       
        var dictheader: [String: String] = [:]

         if isimage == "video" {
           dictheader = ["X_PFTP_API_TOKEN": apitokem,
                         "X_PFTP_LOCAL_TS_OFFSET": "19800",
                         "X_LATAKOO_ARCH": "ARM",
                         "X_LATAKOO_COMPANY": X_LATAKOO_COMPANY,
                         "X_LATAKOO_IS64": "0",
                         "X_LATAKOO_CORES": "6",
                         "X_LATAKOO_MEM": "1037041664",
                         "X_LATAKOO_OS": "iOS",
                         "X_LATAKOO_TID": TID,
                         "X_LATAKOO_APP": "3.1.3",
                         "X_LATAKOO_ENCODE": "0",
                         "X_LATAKOO_CODEC": "H.264",
                         "X_LATAKOO_UPLOAD": X_LATAKOO_UPLOAD,
                         "X_LATAKOO_BITRATE": X_LATAKOO_BITRATE,
                         "X_LATAKOO_FILESIZE": "\(originalsize)",
                         "X_LATAKOO_CONTAINER": X_LATAKOO_CONTAINER,
                         "X_LATAKOO_WIDTH": X_LATAKOO_WIDTH,
                         "X_LATAKOO_HEIGHT": X_LATAKOO_HEIGHT,

                         "X_LATAKOO_TITLE": "\(X_LATAKOO_TITLE)",
                         "X_LATAKOO_GROUP_FILENAME": X_LATAKOO_GROUP_FILENAME,
                         "X_LATAKOO_KEYWORDS": X_LATAKOO_KEYWORDS,
                         "X_LATAKOO_RECIPIENTS": X_LATAKOO_RECIPIENTS,
                         "X_LATAKOO_ORIG_FOLDER": X_LATAKOO_GROUP,
                         "X_LATAKOO_GROUP_ORDER": "\(X_LATAKOO_GROUP_ORDER)",
                         "X_LATAKOO_GROUP_PIECES": "\(FilesArray.count)",
                         "X_LATAKOO_GROUP_TIMECODE": "",
                         "X_LATAKOO_GROUP_USE_TIMECODE": "",
                         "X_LATAKOO_ITEM_CODE": X_LATAKOO_ITEM_CODE,
                         "X_LATAKOO_VIDEO_FORMAT": X_LATAKOO_VIDEO_FORMAT]
            


       } else  {
           dictheader = ["X_PFTP_API_TOKEN": apitokem,
                         "X_PFTP_LOCAL_TS_OFFSET": "19800",
                         "X_LATAKOO_ARCH": "ARM",
                         "X_LATAKOO_COMPANY": X_LATAKOO_COMPANY,
                         "X_LATAKOO_IS64": "0",
                         "X_LATAKOO_CORES": "6",
                         "X_LATAKOO_MEM": "1037041664",
                         "X_LATAKOO_OS": "iOS",
                         "X_LATAKOO_TID": TID,
                         "X_LATAKOO_APP": "3.1.3",
                         "X_LATAKOO_ENCODE": "0",
                         "X_LATAKOO_CODEC": X_LATAKOO_CODEC,
                         "X_LATAKOO_UPLOAD": X_LATAKOO_UPLOAD,
                         "X_LATAKOO_BITRATE": X_LATAKOO_BITRATE,
                         "X_LATAKOO_FILESIZE": "\(originalsize)",
                         "X_LATAKOO_CONTAINER": X_LATAKOO_CONTAINER,
                         "X_LATAKOO_WIDTH": X_LATAKOO_WIDTH,
                         "X_LATAKOO_HEIGHT": X_LATAKOO_HEIGHT,
                         "X_LATAKOO_TITLE": "\(X_LATAKOO_TITLE)",
                         "X_LATAKOO_GROUP_FILENAME": X_LATAKOO_GROUP_FILENAME,
                         "X_LATAKOO_KEYWORDS": X_LATAKOO_KEYWORDS,
                         "X_LATAKOO_RECIPIENTS": X_LATAKOO_RECIPIENTS,
                         "X_LATAKOO_ORIG_FOLDER": X_LATAKOO_GROUP,
                         "X_LATAKOO_GROUP_ORDER": "\(X_LATAKOO_GROUP_ORDER)",
                         "X_LATAKOO_GROUP_PIECES": "\(filearrayaudio.count)",
                         "X_LATAKOO_GROUP_TIMECODE": "",
                         "X_LATAKOO_GROUP_USE_TIMECODE": "",
                         "X_LATAKOO_ITEM_CODE": X_LATAKOO_ITEM_CODE,
                         "X_LATAKOO_VIDEO_FORMAT": X_LATAKOO_VIDEO_FORMAT]
       }
        
        return dictheader

    }

    func setheaderforfile2(api: String) -> [String: String] {
        let apitokem: String = appdelegate.getAPIEncodedHeader(withParameters: api)!

        var dictheader: [String: String] = [:]
        dictheader = ["X_PFTP_API_TOKEN": apitokem,
                      "X_PFTP_LOCAL_TS_OFFSET": "19800",
                      "X_LATAKOO_ARCH": "ARM",
                      "X_LATAKOO_COMPANY": X_LATAKOO_COMPANY,
                      "X_LATAKOO_IS64": "0",
                      "X_LATAKOO_CORES": "6",
                      "X_LATAKOO_MEM": "1037041664",
                      "X_LATAKOO_OS": "iOS",
                      "X_LATAKOO_TID": TID,
                      "X_LATAKOO_APP": "3.1.3",
                      "X_LATAKOO_ENCODE": "0",
                      "X_LATAKOO_CODEC": "",
                      "X_LATAKOO_UPLOAD": X_LATAKOO_UPLOAD,
                      "X_LATAKOO_BITRATE": X_LATAKOO_BITRATE,
                      "X_LATAKOO_FILESIZE": "\(originalsize)",
                      "X_LATAKOO_CONTAINER": X_LATAKOO_CONTAINER,
                      "X_LATAKOO_WIDTH": X_LATAKOO_WIDTH,
                      "X_LATAKOO_HEIGHT": X_LATAKOO_HEIGHT,
                      "X_LATAKOO_TITLE": "\(X_LATAKOO_TITLE)",
                      "X_LATAKOO_KEYWORDS": X_LATAKOO_KEYWORDS,
                      "X_LATAKOO_RECIPIENTS": X_LATAKOO_RECIPIENTS,
                      "X_LATAKOO_GROUP_TIMECODE": "",
                      "X_LATAKOO_GROUP_USE_TIMECODE": "",
                      "X_LATAKOO_ITEM_CODE": X_LATAKOO_ITEM_CODE,
                      "X_LATAKOO_VIDEO_FORMAT": X_LATAKOO_VIDEO_FORMAT]

        return dictheader
    }

    
    
    
    
    
    
    @IBAction func btnmanifest(_ sender: Any) {
        appdelegate.isgotomanifest = true
        
        var story: UIStoryboard?
               if IS_IPHONE {
                   story = UIStoryboard(name: "Main", bundle: nil)
               } else {
                   story = UIStoryboard(name: "MainIPAD", bundle: nil)
               }
               let TabbarController = story?.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
               TabbarController.selectedIndex = 1
               navigationController?.pushViewController(TabbarController, animated: false)
        
    }
    

    @IBAction func btnpilot(_ sender: Any) {
        var story: UIStoryboard?

        if IS_IPHONE {
            story = UIStoryboard(name: "Main", bundle: nil)
        } else {
            story = UIStoryboard(name: "MainIPAD", bundle: nil)
        }
        let TabbarController = story?.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
        TabbarController.selectedIndex = 1
        navigationController?.pushViewController(TabbarController, animated: false)
    }

    @IBAction func btnuploadmore(_ sender: Any) {
        var story: UIStoryboard?

        if IS_IPHONE {
            story = UIStoryboard(name: "Main", bundle: nil)
        } else {
            story = UIStoryboard(name: "MainIPAD", bundle: nil)
        }
        let TabbarController = story?.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController

        if appdelegate.ifgotocapture == "YES" {
            TabbarController.selectedIndex = 0
        } else {
            TabbarController.selectedIndex = 2
        }
        navigationController?.pushViewController(TabbarController, animated: false)
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }

    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        return String((0 ..< length).map { _ in letters.randomElement()! })
    }

    func groupimageupload(currentindex: Int) {
        autoreleasepool {
            lblcount.text = "\(currentindex + 1) of \(FilesArray.count)"
            lblcountnew.text = "\(currentindex + 1) of \(FilesArray.count)"

            let asset: PHAsset = FilesArray[currentindex]
            var imagedata: Data?
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.isNetworkAccessAllowed = true
            var thumbnail = UIImage()
            option.isSynchronous = true
            manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: { (result, _) -> Void in
                let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                imagedata = UIImageJPEGRepresentation(result!, 0.8)
                let messageData = self.MD5New(dattaaa: imagedata!)
                let strMDhash = messageData.hexString()
                let filesize = NSNumber(integerLiteral: imagedata!.count)
                self.X_LATAKOO_FILESIZE = "\(filesize)"
                self.filedata = imagedata
                imagedata = nil
                var filename: String = ""
                self.Compressedsize = self.filedata!.count
                self.originalsize = UIImageJPEGRepresentation(thumbnail, 1.0)!.count
                self.setcomresslabel(Originalsize: self.originalsize, Compressedsize: self.Compressedsize)

                let outputPath = self.createimagefile()
                self.fileurl = outputPath

                do {
                    try self.filedata?.write(to: outputPath, options: .atomic)
                    sleep(1)
                } catch {
                    print(error)
                }

                filename = "\(self.filename ?? "")_\(currentindex + 1).jpg"
                print(self.filename)
                self.compressionprogress.setProgress(1, animated: true)
                self.viewcompress.backgroundColor = UIColor(red: 21 / 255, green: 159 / 255, blue: 2 / 255, alpha: 1.0)
                self.ApiBeginupload(filename: filename, filesize: filesize, md5hash: strMDhash)

            })
        }
    }

    func groupvideoupload(currentindex: Int) {
        lblcount.text = "\(currentindex + 1) of \(FilesArray.count)"
        lblcountnew.text = "\(currentindex + 1) of \(FilesArray.count)"

        let asset: PHAsset = FilesArray[currentindex]

        getthumnail(asset: asset)

        let resourceArray = PHAssetResource.assetResources(for: asset)
                                                         let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false
                                                          if  bIsLocallayAvailable == false {
        
        DispatchQueue.main.async {

        self.viewCircularProgressBar.labelSize = 30
            self.viewCircularProgressBar.safePercent = 100
        self.viewCircularProgressBar.setProgress(to: 0, withAnimation: true)
        KGModal.sharedInstance()?.show(withContentView: self.viewprogress)
        KGModal.sharedInstance()?.tapOutsideToDismiss = false
        self.viewCircularProgressBar.backgroundColor = UIColor.clear

        }
                                                            
        }
        
            
        print(asset.localIdentifier)
            let option1 = PHVideoRequestOptions()
            option1.isNetworkAccessAllowed = true
            option1.version = .original
            option1.deliveryMode = .highQualityFormat
            option1.progressHandler = {  (progress, error, stop, info) in

                let resourceArray = PHAssetResource.assetResources(for: asset)
                                                                            let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false
                                                                             if  bIsLocallayAvailable == false {
                           
                           DispatchQueue.main.async {
                            print(progress)
            self.viewCircularProgressBar.setProgress(to: progress, withAnimation: true)
                      
                }
                }
                
            }
                 
            var resultAsset: AVAsset?
            PHCachingImageManager.default().requestAVAsset(forVideo: asset, options: option1, resultHandler: { avasset, _, _ in

                DispatchQueue.main.async {
                    resultAsset = avasset
                    KGModal.sharedInstance()?.hide()

                    let asset = avasset as? AVURLAsset

                    print("size of video is => \(asset?.fileSize)")

                    let duration = resultAsset?.duration
                    let durationTime = CMTimeGetSeconds(duration ?? CMTimeMake(1, 1))
                    self.videoduration = "\(durationTime)"

                    let data = "any string".data(using: .utf8)

                    let messageData = self.MD5New(dattaaa: data! as Data)
                    let strMDhash = messageData.hexString()

                    let filesize = NSNumber(nonretainedObject: asset?.fileSize)
                    self.X_LATAKOO_FILESIZE = "\(filesize)"
                    var filename: String = ""
                    filename = "\(self.filename ?? "")_\(currentindex + 1).mp4"
                    print(self.filename)

                    self.originalsize = (asset?.fileSize ?? 0)

                    if asset?.url != nil {
                        self.compressvideo(assetUrl: avasset!, videourl: asset!.url, filename: filename, md5hash: strMDhash)
                    } else {
                        let alert = UIAlertController(title: "Alert", message: "File not supported.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        
    }

    func groupaudioupload(currentindex: Int) {
        lblcount.text = "\(currentindex + 1) of \(filearrayaudio.count)"
        lblcountnew.text = "\(currentindex + 1) of \(FilesArray.count)"

        print(filearrayaudio)
        var filename: String = ""
        filename = "\(self.filename ?? "")_\(currentindex + 1).m4a"
        print("filename: \(filename)")
        let aData = NSData(contentsOfFile: filearrayaudio[currentindex]) as Data?
        print(aData!.count)
        filedata = aData
        Compressedsize = aData!.count
        originalsize = aData!.count
        setcomresslabel(Originalsize: aData!.count, Compressedsize: aData!.count)
        let messageData = MD5New(dattaaa: aData!)
        let strMDhash = messageData.hexString()
        let filesize = NSNumber(integerLiteral: aData!.count)
        let outputPath = createaudiofile()
        fileurl = outputPath
        
        do {
            try filedata?.write(to: outputPath, options: .atomic)
            sleep(1)
        } catch {
            print(error)
        }
        
        
        let audioAsset = AVURLAsset(url: fileurl!, options: nil)
                      let audioDuration = audioAsset.duration
                      let audioDurationSeconds = Float(CMTimeGetSeconds(audioDuration))
                      videoduration = String(audioDurationSeconds)
        
        ApiBeginupload(filename: filename, filesize: filesize, md5hash: strMDhash)
    }

    override func viewDidDisappear(_ animated: Bool) {
        timer.invalidate()
    }

    func resetprogress() {
        viewcompress.backgroundColor = UIColor(red: 203 / 255, green: 201 / 255, blue: 0 / 255, alpha: 1.0)
        viewprepare.backgroundColor = UIColor(red: 203 / 255, green: 201 / 255, blue: 0 / 255, alpha: 1.0)
        viewuploadgreen.backgroundColor = UIColor(red: 203 / 255, green: 201 / 255, blue: 0 / 255, alpha: 1.0)
        compressionprogress.setProgress(0, animated: true)
        preparingprogress.setProgress(0, animated: true)
        uploadingprogress.setProgress(0, animated: false)
        lblprepairing.isHidden = true
        lblcompration.text = ""
    }

    // MARK: ...............video compression start...........
    func compressvideo(assetUrl: AVAsset, videourl: URL, filename: String, md5hash: String) {

         self.uploadlogs.append("\n upload file name => \n\(filename)")

         
          let asset =  assetUrl
         var compressedbitrate : NSNumber  = 0
          var videoTrack: AVAssetTrack? = nil
          let videoTracks = asset.tracks(withMediaType: .video)
         let width : Float = Float(asset.videoSize().width)
         let height : Float = Float(asset.videoSize().height)
         
         
         X_LATAKOO_WIDTH = "\(Int(width))"
         X_LATAKOO_HEIGHT = "\(Int(height))"

         
           if videoTracks.count>0 {
             videoTrack = videoTracks[0];
         }
         
       

         
            self.progress = 0
            print("Resolution = \(width) X \(height)")
         
         self.uploadlogs.append("\n Video Resolution => \n\("Resolution = \(width) X \(height)")")

             let frameRate : Float = videoTrack!.nominalFrameRate
             let  bps : Float  = videoTrack!.estimatedDataRate
             print("Frame rate == \(frameRate)")
         
         self.uploadlogs.append("\n Frame rate => \n\("Frame rate == \(frameRate)")")

         
              print(String(format: "bps rate == %ld", Int(bps)))
         
         self.uploadlogs.append("\n video bitrate  => \n\(String(format: "bps rate == %ld", Int(bps)))")

            let bits_per_pixel: Float = (width * height * 24 * frameRate) / 1048576
            let Str1 = "\(bits_per_pixel)"
            let Str2 = "\(chosenBitrate)"
            let  dictuser = UserDefaults.standard.value(forKey: "userdetails") as! [String : Any]
            let   formulaString = dictuser["variable_bitrate_formula"] as! String
            print(formulaString)
         
         self.uploadlogs.append("\n formula => \n\(formulaString)")

         
         var modifiedFormulaString = formulaString.replacingOccurrences(of: "api_bitrate", with:Str2)
          print(modifiedFormulaString)
         
         
         
          modifiedFormulaString = modifiedFormulaString.replacingOccurrences(of: "uncompressed_bitrate", with:Str1)
         print(modifiedFormulaString)
         let e = NSExpression(format: modifiedFormulaString)
              let result = e.expressionValue(with: nil, context: nil) as? NSNumber
              if let result = result {
                 print("\(result)")
              }
         
         var bitsend: Int
         bitsend = chosenBitrate * 1000
         compressedbitrate = NSNumber.init(value: result!.doubleValue * 1000000)
         let originalvideobitrate  = NSNumber.init(value:Double(bps))
         var bitrateformpressvideo : NSNumber = 0
         
         print(result!.floatValue * 1000000)
         print(bitsend)

         
         self.uploadlogs.append("\n video will compress on bitrate => \n\(result!.floatValue * 1000000)")
        
        
        

        X_LATAKOO_BITRATE = "0"

        print(appdelegate.fileurl)
          
        if appdelegate.fileurl != nil {
            fileurl = appdelegate.fileurl
            Compressedsize = fileurl!.fileSize()!
            setcomresslabel(Originalsize: originalsize, Compressedsize: Compressedsize)
            Senddata(videourl: fileurl!, filename: filename, md5hash: md5hash)
            return
        }

        print("this code should not be run")

        let outputPath = createFile()
        fileurl = outputPath

        if bitsend == 0 {
            
            fileurl = videourl
            Compressedsize = originalsize
            Senddata(videourl: fileurl!, filename: filename, md5hash: md5hash)
            setcomresslabel(Originalsize: originalsize, Compressedsize: originalsize)
            return
//            let videodata = NSData(contentsOf: videourl)
//
//            if videodata == nil {
//                bitrateformpressvideo = originalvideobitrate
//                compressedbitrate = bitrateformpressvideo
//
//            } else {
//                do {
//                    try videodata?.write(to: outputPath, options: .atomic)
//                    sleep(2)
//                    Compressedsize = originalsize
//
//                    Senddata(videourl: outputPath, filename: filename, md5hash: md5hash)
//                    setcomresslabel(Originalsize: originalsize, Compressedsize: originalsize)
//                } catch {
//                    print(error)
//                }
//
//                return
//            }

        } else if result!.floatValue * 1000000 > bps {
            
            fileurl = videourl
            Compressedsize = originalsize
            Senddata(videourl: fileurl!, filename: filename, md5hash: md5hash)
            setcomresslabel(Originalsize: originalsize, Compressedsize: originalsize)
            return
            
            
//            let videodata = NSData(contentsOf: videourl)
//
//            if videodata == nil {
//                bitrateformpressvideo = originalvideobitrate
//                compressedbitrate = bitrateformpressvideo
//
//            } else {
//                do {
//                    try videodata?.write(to: outputPath, options: .atomic)
//                    sleep(2)
//                    Compressedsize = originalsize
//
//                    Senddata(videourl: outputPath, filename: filename, md5hash: md5hash)
//                    setcomresslabel(Originalsize: originalsize, Compressedsize: originalsize)
//                } catch {
//                    print(error)
//                }
//
//                return
//            }

        } else {
            bitrateformpressvideo = compressedbitrate
        }
        print("video original bitrate \(originalvideobitrate)")
        print("video bitrate after compress \(originalvideobitrate)")
        print("video bitrate on that video will comressed \(bitrateformpressvideo)")
        X_LATAKOO_BITRATE = "\(bitsend)"
        print("video bitrate that will goes to server \(X_LATAKOO_BITRATE)")

        self.uploadlogs.append("\n video original bitrate => \(originalvideobitrate)")
        self.uploadlogs.append("\n video bitrate after compress \(originalvideobitrate)")
        self.uploadlogs.append("\n video bitrate on that video will comressed \(bitrateformpressvideo)")
        self.uploadlogs.append("\n video bitrate that will goes to server \(X_LATAKOO_BITRATE)")
        
        
        compressFile(urlToCompress: videourl, outputURL: outputPath, bitrate: compressedbitrate) { _ in

            DispatchQueue.main.async {
                
                self.X_LATAKOO_VIDEO_FORMAT = ""
                
                self.Compressedsize = outputPath.fileSize()!
                self.setcomresslabel(Originalsize: self.originalsize, Compressedsize: self.Compressedsize)
                self.Senddata(videourl: outputPath, filename: filename, md5hash: md5hash)
            }
        }

        return
    }

    // MARK: .................................................................

    func Senddata(videourl: URL, filename: String, md5hash: String) {
        compressionprogress.setProgress(1, animated: true)
        viewcompress.backgroundColor = UIColor(red: 21 / 255, green: 159 / 255, blue: 2 / 255, alpha: 1.0)

        let filesize = NSNumber(integerLiteral: videourl.fileSize()!)

        ApiBeginupload(filename: filename, filesize: filesize, md5hash: md5hash)
    }

    func createFile() -> URL {
        let fileName = "tempvideo"
        let documentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = documentDirURL.appendingPathComponent(fileName).appendingPathExtension("mp4")
        print("File PAth: \(fileURL.path)")

        if FileManager.default.fileExists(atPath: fileURL.path) {
            try! FileManager.default.removeItem(atPath: fileURL.path)
        }
        
        self.uploadlogs.append("\n local path for file => \(fileURL.path)")

        return fileURL
    }

    func createimagefile() -> URL {
        let fileName = "tempimage"
        let documentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = documentDirURL.appendingPathComponent(fileName).appendingPathExtension("jpg")
        print("File PAth: \(fileURL.path)")

        if FileManager.default.fileExists(atPath: fileURL.path) {
            try! FileManager.default.removeItem(atPath: fileURL.path)
        }
        self.uploadlogs.append("\n local path for file => \(fileURL.path)")

        return fileURL
    }

    func createaudiofile() -> URL {
        let fileName = "tempvideo"
        let documentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = documentDirURL.appendingPathComponent(fileName).appendingPathExtension("m4a")
        print("File PAth: \(fileURL.path)")

        if FileManager.default.fileExists(atPath: fileURL.path) {
            try! FileManager.default.removeItem(atPath: fileURL.path)
        }
        
        self.uploadlogs.append("\n local path for file => \(fileURL.path)")

        return fileURL
    }

    func setcomresslabel(Originalsize: Int, Compressedsize: Int) {
        
        if Compressedsize<100 || Originalsize<100 {
                          SCLAlertView().showError("Error", subTitle:"This file is less than 100 Bytes and may be corrupt. Please re-record for upload.", closeButtonTitle:"OK")
                          self.navigationController?.popViewController(animated: true)
                          return
                      }
        
        self.uploadlogs.append("\n video original => \(Originalsize)")
        self.uploadlogs.append("\n video size after compression => \(Compressedsize)")
        
        var percentage = Int((CGFloat(Compressedsize) / CGFloat(Originalsize)) * 100)

        if percentage > 100 {
            percentage = 100
        }
        let suffixKB = "KB"
        let suffixMB = "MB"
        let suffixGB = "GB"
        var formattedSize = Compressedsize / 1024
        var formattedExt = suffixKB
        if formattedSize > 1024 {
            formattedSize /= 1024
            formattedExt = suffixMB
        }
        if formattedSize > 1024 {
            formattedSize /= 1024
            formattedExt = suffixGB
        }
        let result = String(format: "%li%@ (%li%%)", formattedSize, formattedExt, percentage)
        compressionprogress.setProgress(1.0, animated: true)
        lblcompration.isHidden = false
        lblcompration.text = "Complete. Compressed size: \(result)"
        viewcompress.backgroundColor = UIColor(red: 21 / 255, green: 159 / 255, blue: 2 / 255, alpha: 1.0)
        
        self.uploadlogs.append("Complete compress sized => \(result)")

    }

    func compressFile(urlToCompress: URL, outputURL: URL, bitrate: NSNumber, completion: @escaping (URL) -> Void) {
        // video file to make the asset

        var audioFinished = false
        var videoFinished = false

        let asset = AVAsset(url: urlToCompress)

        // create asset reader
        do {
            assetReader = try AVAssetReader(asset: asset)
        } catch {
            assetReader = nil
        }

        guard let reader = assetReader else {
            fatalError("Could not initalize asset reader probably failed its try catch")
        }

        var videoTrack: AVAssetTrack?
        var audioTrack: AVAssetTrack?

        if asset.tracks(withMediaType: .audio).first != nil {
            audioTrack = asset.tracks(withMediaType: AVMediaType.audio).first!
        } else {
            audioTrack = nil
        }
        if asset.tracks(withMediaType: .video).first != nil {
            videoTrack = asset.tracks(withMediaType: AVMediaType.video).first!

        } else {
            let alert = UIAlertController(title: "Alert", message: "File not supported.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                self.navigationController?.popViewController(animated: true)
            }))
            present(alert, animated: true, completion: nil)
            return
        }

        let videoReaderSettings: [String: Any] = [kCVPixelBufferPixelFormatTypeKey as String!: kCVPixelFormatType_32ARGB]

        // ADJUST BIT RATE OF VIDEO HERE

        let videoSettings: [String: Any] = [
            AVVideoCompressionPropertiesKey: [AVVideoAverageBitRateKey: bitrate],
            AVVideoCodecKey: AVVideoCodecType.h264,
            AVVideoHeightKey: videoTrack!.naturalSize.height,
            AVVideoWidthKey: videoTrack!.naturalSize.width,
        ]

        let assetReaderVideoOutput = AVAssetReaderTrackOutput(track: videoTrack!, outputSettings: videoReaderSettings)

        var assetReaderAudioOutput: AVAssetReaderTrackOutput!

        if audioTrack != nil {
            assetReaderAudioOutput = AVAssetReaderTrackOutput(track: audioTrack!, outputSettings: nil)
        }

        if reader.canAdd(assetReaderVideoOutput) {
            reader.add(assetReaderVideoOutput)
        } else {
            fatalError("Couldn't add video output reader")
        }

        if audioTrack != nil {
            if reader.canAdd(assetReaderAudioOutput) {
                reader.add(assetReaderAudioOutput)
            } else {
                fatalError("Couldn't add audio output reader")
            }
        }

        let audioInput = AVAssetWriterInput(mediaType: AVMediaType.audio, outputSettings: nil)
        let videoInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: videoSettings)
        videoInput.transform = videoTrack!.preferredTransform
        // we need to add samples to the video input

        let videoInputQueue = DispatchQueue(label: "videoQueue")
        let audioInputQueue = DispatchQueue(label: "audioQueue")

        do {
            assetWriter = try AVAssetWriter(outputURL: outputURL, fileType: AVFileType.mov)
        } catch {
            assetWriter = nil
        }
        guard let writer = assetWriter else {
            fatalError("assetWriter was nil")
        }

        writer.shouldOptimizeForNetworkUse = true
        writer.add(videoInput)
        //  if audioTrack != nil {
        writer.add(audioInput)
        // }

        writer.startWriting()
        reader.startReading()
        writer.startSession(atSourceTime: kCMTimeZero)

        let closeWriter: () -> Void = {
            if audioFinished && videoFinished {
                self.assetWriter!.finishWriting(completionHandler: {
                    completion((self.assetWriter?.outputURL)!)

                })

                self.assetReader!.cancelReading()
            }
        }

        if audioTrack != nil {
            audioInput.requestMediaDataWhenReady(on: audioInputQueue) {
                while audioInput.isReadyForMoreMediaData {
                    let sample = assetReaderAudioOutput.copyNextSampleBuffer()
                    if sample != nil {
                        audioInput.append(sample!)
                    } else {
                        audioInput.markAsFinished()
                        DispatchQueue.main.async {
                            audioFinished = true
                            closeWriter()
                        }
                        break
                    }
                }
            }
        } else {
            audioInput.markAsFinished()
            DispatchQueue.main.async {
                audioFinished = true
                closeWriter()
            }
        }
        videoInput.requestMediaDataWhenReady(on: videoInputQueue) {
            // request data here

            while videoInput.isReadyForMoreMediaData {
                let sample = assetReaderVideoOutput.copyNextSampleBuffer()
                if sample != nil {
                    videoInput.append(sample!)
                } else {
                    videoInput.markAsFinished()
                    DispatchQueue.main.async {
                        videoFinished = true
                        closeWriter()
                    }
                    break
                }

                let asset = asset
                let duration = asset.duration
                let durationTime = CMTimeGetSeconds(duration)
                let timeStamp = CMSampleBufferGetPresentationTimeStamp(sample!)
                let timeSecond = CMTimeGetSeconds(timeStamp)
                let per = timeSecond / durationTime

                if self.progress < Float(per) {
                    self.progress = Float(per)
                    DispatchQueue.main.async {
                        print("compress progress \(Float(per))")
                        self.compressionprogress.setProgress(Float(per), animated: true)
                    }
                }
            }
        }
    }

    func ServiceAttackToAssignment() {
        let urlString = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)media/\(TID)/assign-assignment/"
        let request = NSMutableURLRequest()
        request.url = URL(string: urlString)
        request.httpMethod = "PUT"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer \(UserDefaults.standard.value(forKey: "Authtoken") ?? "")", forHTTPHeaderField: "Authorization")

        var Parameter: [String: Any] = [:]

        Parameter["file_name"] = X_LATAKOO_TITLE
        Parameter["assignment_id"] = appdelegate.TopicID
        Parameter["assignment_child_id"] = appdelegate.AssignmentID

        print(Parameter)
        print(urlString)


        var myData: Data?
        do {
            myData = try JSONSerialization.data(withJSONObject: Parameter, options: .prettyPrinted)
        } catch {
        }
        var putData = Data()
        if let myData = myData {
            putData.append(Data(myData))
        }
        request.httpBody = putData
        var response: URLResponse?

        var responseData: Data?
        do {
            responseData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
        } catch let err {
            print(err)
        }

        do {
            let json = try JSONSerialization.jsonObject(with: responseData!, options: []) as? [String: Any]
            print(json!)
            self.TID = self.randomString(length: 32)

        } catch {
            print(error)
        }
    }

    ////AWS S3/////////////////////////AWS S3///////////////////AWS S3/////////////////////////AWS S3/////////////////////////

    func checkifvideo(currentindex: Int) {

        if isimage == "video" {
            ext = "mp4"
            if FilesArray.count > 1 {
                ismultipleupload = true
            } else {
                ismultipleupload = false
            }

            let asset: PHAsset = FilesArray[currentindex]

            print(asset.localIdentifier)
                
            
            let resourceArray = PHAssetResource.assetResources(for: asset)
                                                                            let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false
                                                                             if  bIsLocallayAvailable == false {
                           
                           DispatchQueue.main.async {

                           self.viewCircularProgressBar.labelSize = 30
                               self.viewCircularProgressBar.safePercent = 100
                           self.viewCircularProgressBar.setProgress(to: 0, withAnimation: true)
                           KGModal.sharedInstance()?.show(withContentView: self.viewprogress)
                           KGModal.sharedInstance()?.tapOutsideToDismiss = false
                            self.viewCircularProgressBar.backgroundColor = UIColor.clear

                           
                           }
                                                                               
                           }

            
                
                let option1 = PHVideoRequestOptions()
                           option1.isNetworkAccessAllowed = true
                           option1.version = .original
                           option1.deliveryMode = .highQualityFormat
                           option1.progressHandler = {  (progress, error, stop, info) in

                               let resourceArray = PHAssetResource.assetResources(for: asset)
                                                                                           let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false
                                                                                            if  bIsLocallayAvailable == false {
                                          
                                          DispatchQueue.main.async {
                           self.viewCircularProgressBar.setProgress(to: progress, withAnimation: true)
                                print(progress)
                                     
                               }
                               }
                               
                           }
                
                
               
                PHCachingImageManager.default().requestAVAsset(forVideo: asset, options: option1, resultHandler: { avasset, _, _ in

                    DispatchQueue.main.async {
                        KGModal.sharedInstance()?.hide()

                        var resultAsset: AVAsset?
                        resultAsset = avasset
                        let duration = resultAsset?.duration
                        let durationTime = CMTimeGetSeconds(duration ?? CMTimeMake(1, 1))
                        self.videoduration = "\(durationTime)"
                        if avasset != nil{
                        self.creteinfoXML(asset: avasset!)
                        }
                    }
                })
            
        } else if isimage == "image" {
            ext = "jpg"
            videoduration = ""

            if FilesArray.count > 1 {
                ismultipleupload = true
            } else {
                ismultipleupload = false
            }
            let asset: PHAsset = FilesArray[0]
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.isNetworkAccessAllowed = true
            var thumbnail = UIImage()
            option.isSynchronous = true
            manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: { (result, _) -> Void in
                let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                self.creteinfoXMLImage(image: thumbnail)

            })

        } else {
            ext = "m4a"
            if filearrayaudio.count > 1 {
                ismultipleupload = true
            } else {
                ismultipleupload = false
            }

            print(filearrayaudio)
//            filename = "\(filename ?? "").m4a"
            print("filename: \(filename)")
            creteinfoXMLAudio()
        }
    }

    func sendThumbNailToS3(path: String) {
        

        DispatchQueue.main.async {
            let imagePathStr = path

            AWSS3Manager.shared.uploadMediaThumbnail(imagePath: imagePathStr, progress: { [weak self] progress in
                //  guard let strongSelf = self else { return }
                print(progress)

            }) { [weak self] uploadedFileUrl, error in

                //  guard let strongSelf = self else { return }
                if let finalPath = uploadedFileUrl as? String {
                    // strongSelf.s3UrlLabel.text = "Uploaded file url: " + finalPath
                    print("Uploaded thumbnail at url: " + finalPath)
                    //                strongSelf.apicomplete()
                    
                    self?.uploadlogs.append("Uploaded thumbnail at url: " + finalPath)


                } else {
                    print("\(String(describing: error?.localizedDescription))")
                    let appearance = SCLAlertView.SCLAppearance(
                        kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                        kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                        kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                        showCloseButton: false,
                        dynamicAnimatorActive: true,
                        buttonsLayout: .horizontal
                    )

                    let alert = SCLAlertView(appearance: appearance)
                    _ = alert.addButton("OK") {
                        self?.navigationController?.popViewController(animated: true)
                    }
                    _ = alert.showSuccess("ERROR Thumb Uploading", subTitle: error?.localizedDescription)
                    self!.uploadlogs.append("thumb Uploading failed")
                    self!.writeToTextFile()

                }
            }
        }
    }

    func uploadvideotos3() {
       
         
        
        DispatchQueue.main.async {
            let videoUrl = self.fileurl!
            AWSS3Manager.shared.uploadVideo(videoUrl: videoUrl, progress: { [weak self] progress in
                guard let strongSelf = self else { return }
              
                print(progress)
                
                self?.uploadlogs.append("Uploaded video file Progress: \(progress) ")

                
                
                strongSelf.uploadingprogress.progress = Float(progress)
            }) { [weak self] uploadedFileUrl, error in
                guard let strongSelf = self else { return }
                if let finalPath = uploadedFileUrl as? String {
                    // strongSelf.s3UrlLabel.text = "Uploaded file url: " + finalPath
                    print("Uploaded file url: " + finalPath)
                    
                    self?.uploadlogs.append("Uploaded file at url: " + finalPath)

                    //                strongSelf.apivideoping()
                    strongSelf.apiMediaUploadComplete()
                } else {
                    print("\(String(describing: error?.localizedDescription))")
                    let appearance = SCLAlertView.SCLAppearance(
                        kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                        kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                        kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                        showCloseButton: false,
                        dynamicAnimatorActive: true,
                        buttonsLayout: .horizontal
                    )

                    let alert = SCLAlertView(appearance: appearance)
                    _ = alert.addButton("OK") {
                        self?.navigationController?.popViewController(animated: true)
                    }
                    _ = alert.showSuccess("ERROR Video Uploading", subTitle: error?.localizedDescription)
                    self!.uploadlogs.append("Video Uploading failed")
                    self!.writeToTextFile()

                }
            }
        }
    }

    func uploadimagetos3() {
        DispatchQueue.main.async {
            let videoUrl = self.fileurl!
            AWSS3Manager.shared.uploadImage(imagePath: videoUrl.path, progress: { [weak self] progress in
                guard let strongSelf = self else { return }
                print(progress)
                strongSelf.uploadingprogress.progress = Float(progress)
            }) { [weak self] uploadedFileUrl, error in
                guard let strongSelf = self else { return }
                if let finalPath = uploadedFileUrl as? String {
                    print("Uploaded file url: " + finalPath)
                    strongSelf.apiMediaUploadComplete()
                } else {
                    print("\(String(describing: error?.localizedDescription))")
                    let appearance = SCLAlertView.SCLAppearance(
                        kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                        kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                        kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                        showCloseButton: false,
                        dynamicAnimatorActive: true,
                        buttonsLayout: .horizontal
                    )

                    let alert = SCLAlertView(appearance: appearance)
                    _ = alert.addButton("OK") {
                        self?.navigationController?.popViewController(animated: true)
                    }
                    _ = alert.showSuccess("ERROR", subTitle: error?.localizedDescription)
                    self!.uploadlogs.append("image Uploading failed")
                    self!.writeToTextFile()

                }
            }
        }
    }

    func uploadaudiotos3() {
        DispatchQueue.main.async {
            let videoUrl = self.fileurl!
            AWSS3Manager.shared.uploadAudio(audioUrl: videoUrl, progress: { [weak self] progress in
                guard let strongSelf = self else { return }
                print(progress)
                strongSelf.uploadingprogress.progress = Float(progress)
            }) { [weak self] uploadedFileUrl, error in
                guard let strongSelf = self else { return }
                if let finalPath = uploadedFileUrl as? String {
                    // strongSelf.s3UrlLabel.text = "Uploaded file url: " + finalPath
                    print("Uploaded file url: " + finalPath)
                    //                strongSelf.apivideoping()
                    strongSelf.apiMediaUploadComplete()
                } else {
                    print("\(String(describing: error?.localizedDescription))")
                    let appearance = SCLAlertView.SCLAppearance(
                        kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                        kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                        kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                        showCloseButton: false,
                        dynamicAnimatorActive: true,
                        buttonsLayout: .horizontal
                    )

                    let alert = SCLAlertView(appearance: appearance)
                    _ = alert.addButton("OK") {
                        self?.navigationController?.popViewController(animated: true)
                    }
                    _ = alert.showSuccess("ERROR", subTitle: error?.localizedDescription)
                    self!.uploadlogs.append("audio Uploading failed")
                    self!.writeToTextFile()

                }
            }
        }
    }
    
    public struct Units {
      
      public let bytes: Int64
      
      public var kilobytes: Double {
        return Double(bytes) / 1_024
      }
      
      public var megabytes: Double {
        return kilobytes / 1_024
      }
      
      public var gigabytes: Double {
        return megabytes / 1_024
      }
      
      public init(bytes: Int64) {
        self.bytes = bytes
      }

    public func getReadableUnit() -> String {
        
        switch bytes {
        case 0..<1_024:
          return "\(bytes) bytes"
        case 1_024..<(1_024 * 1_024):
          return "\(String(format: "%.2f", kilobytes)) kbps"
        case 1_024..<(1_024 * 1_024 * 1_024):
          return "\(String(format: "%.2f", megabytes)) mbps"
        case (1_024 * 1_024 * 1_024)...Int64.max:
          return "\(String(format: "%.2f", gigabytes)) gbps"
        default:
          return "\(bytes) bytes"
        }
      }
    }
    
    
    func apiMediaUploadComplete() {
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        //     appdelegate.ShowProgress()

        var filename = ""

        timer.invalidate()
        
        print(uploadtime)
        print(Compressedsize)

        
        
        let filetransferinsec = Compressedsize/uploadtime
        
        print("upload speed = > \(Units(bytes: Int64(filetransferinsec)).getReadableUnit())")

        self.uploadlogs.append("\n upload speed = > \(Units(bytes: Int64(filetransferinsec)).getReadableUnit())")

        
        if ismultipleupload {
            filename = "\(self.filename ?? "")_\(groupuploadcurrentindex + 1).\(ext)"
            X_LATAKOO_GROUP_ORDER = NSNumber(integerLiteral: groupuploadcurrentindex + 1)
        } else {
            filename = "\(self.filename ?? "").\(ext)"
        }
        
        
    
        
        
        var aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)media/upload/complete"
        print("Media Upload Complete API => \(aStrApi)")
        
        var headervalue: [String: String] = [:]

        if app.uploadas == "Group" {
            headervalue = setheaderforfile1(api: aStrApi)

        }else if app.uploadas == "Individual"{
            headervalue = setheaderforfile2(api: aStrApi)

        }else{
            headervalue = setheaderforfile(api: aStrApi)

        }
        
        
        
        aStrApi = AJNotificationView.changetoencodedstring(aStrApi)
        print("headervalue: \(headervalue)")

        let parametersData: [String: Any] = [
            "filename": filename,
            "length": videoduration,
            "s3_filename": self.appdelegate.filenameStr,
            "headers": headervalue,
        ]
        
        
        self.uploadlogs.append("\n video ping  api  => \n\(aStrApi)")

        self.uploadlogs.append("\n video ping  param  => \n\(parametersData)")

        print("parametersData: \(parametersData)")

        //        let encoding = URLEncoding.httpBody
        let encoding = Alamofire.JSONEncoding.default
        Alamofire.request(aStrApi, method: .post, parameters: parametersData as Parameters, encoding: encoding, headers: headervalue).responseJSON { (responseObject) -> Void in
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                let usrDetail = resJson
                print("media/upload/complete response: \(usrDetail)")
                   self.uploadlogs.append("\n video ping  api responce => \n\(usrDetail)")
                print(self.uploadlogs)
                self.ServiceAttackToAssignment()

                print("\(self.groupuploadcurrentindex) TID IS => \(self.TID)")
                
                if self.ismultipleupload {
                    let myupload = UserDefaults.standard.value(forKey: "myuploads") as! Bool
                    if myupload == true {
                        if self.isimage == "image" {
                            appdelegate.startLocalNotificationMessage(message: "\(self.groupuploadcurrentindex + 1) of \(self.FilesArray.count) Upload Complete", title: "Latakoo")
                        }
                        else if self.isimage == "video" {
                            appdelegate.startLocalNotificationMessage(message: "\(self.groupuploadcurrentindex + 1) of \(self.FilesArray.count) Upload Complete", title: "Latakoo")
                        }
                        else if self.isimage == "audio" {
                            appdelegate.startLocalNotificationMessage(message: "\(self.groupuploadcurrentindex + 1) of \(self.filearrayaudio.count) Upload Complete", title: "Latakoo")
                        }
                        else {
                            appdelegate.startLocalNotificationMessage(message: "\(self.groupuploadcurrentindex + 1) of \(self.FilesArray.count) Upload Complete", title: "Latakoo")
                        }
//                        appdelegate.startLocalNotificationMessage(message: "\(self.groupuploadcurrentindex + 1) of \(self.FilesArray.count) Upload Complete", title: "Latakoo")
                    }

                    if self.isimage == "image" {
                        if self.groupuploadcurrentindex == self.FilesArray.count - 1 {
                            self.uploadingprogress.setProgress(1, animated: true)
                            DispatchQueue.main.async {
                                self.showuploadmoreview()
                            }

                        } else {
                            self.uploadingprogress.setProgress(1, animated: true)
                            self.resetprogress()
                            self.groupuploadcurrentindex = self.groupuploadcurrentindex + 1
                            //  self.groupimageupload(currentindex: self.groupuploadcurrentindex)
                            self.checkifvideo(currentindex: self.groupuploadcurrentindex)
                        }

                    } else if self.isimage == "video" {
                        if self.groupuploadcurrentindex == self.FilesArray.count - 1 {
                            self.uploadingprogress.setProgress(1, animated: true)
                            DispatchQueue.main.async {
                                self.showuploadmoreview()
                            }

                        } else {
                            self.uploadingprogress.setProgress(1, animated: true)
                            self.resetprogress()
                            self.groupuploadcurrentindex = self.groupuploadcurrentindex + 1
                            // self.groupvideoupload(currentindex: self.groupuploadcurrentindex)
                            self.checkifvideo(currentindex: self.groupuploadcurrentindex)
                        }

                    } else {
                        if self.groupuploadcurrentindex == self.filearrayaudio.count - 1 {
                            self.uploadingprogress.setProgress(1, animated: true)
                            DispatchQueue.main.async {
                                self.showuploadmoreview()
                            }
                        } else {
                            self.uploadingprogress.setProgress(1, animated: true)
                            self.resetprogress()
                            self.groupuploadcurrentindex = self.groupuploadcurrentindex + 1
                            // self.groupaudioupload(currentindex: self.groupuploadcurrentindex)
                            self.checkifvideo(currentindex: self.groupuploadcurrentindex)
                        }
                    }

                } else {
                    self.uploadingprogress.setProgress(1, animated: true)
                    DispatchQueue.main.async {
                        self.showuploadmoreview()
                    }
                    let myupload = UserDefaults.standard.value(forKey: "myuploads") as! Bool
                    if myupload == true {
                        appdelegate.startLocalNotificationMessage(message: "Upload Complete", title: "Latakoo")
                    }
                }
            }

            if responseObject.result.isFailure {
                print("error in media/upload/complete api")
                appdelegate.HideProgress()
            }
        }
    }

    func createthumb() -> URL {
        let fileName = "tempthumbnail"
        let documentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = documentDirURL.appendingPathComponent(fileName).appendingPathExtension("jpg")
        print("File PAth: \(fileURL.path)")

        if FileManager.default.fileExists(atPath: fileURL.path) {
            try! FileManager.default.removeItem(atPath: fileURL.path)
        }
        return fileURL
    }

    func getCredentialThroughMediaUploadAPI() {
                            if !Reachability.isConnectedToNetwork() {
            return
        }
        
        let headers = ["Authorization":
            "Bearer \(UserDefaults.standard.value(forKey: "Authtoken") ?? "")"]
        let aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)media/upload"
        print(aStrApi)
        print(headers)
        
        uploadlogs.append("AWS Cred Request  => \(aStrApi) \n")


                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")

            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "NO INTERNET", hideAfter: 1.0)
            return
        }

        currentrequest = "credentialrequest"
        var request = URLRequest(url: URL(string: aStrApi)!)
        request.httpMethod = "POST"
        request.httpBody = videoinfodata
        request.allHTTPHeaderFields = headers
        initiateConnection(with: request)
    }

    func credentialdata(dictcredential: [String: Any]) {
        let dictnew = dictcredential["credentials"] as! [String: Any]
        let dictbucket = dictcredential["bucket"] as! [String: Any]
        print(dictnew)
        let accesskey = dictnew["accessKeyId"] as! String
        let secretekey = dictnew["secretAccessKey"] as! String
        let bucketname = dictbucket["name"] as! String
        let sessiontoken = dictnew["sessionToken"] as! String
        let expiration = dictnew["expiration"] as! String
        let fileNam = dictbucket["fileName"] as! String
        appdelegate.filenameStr = fileNam
        let bucketp = dictbucket["key"] as! String
        print("Key to upload: \(bucketp)")
        let bucketpa = bucketp.dropLast()
        print("bucketpa remove last: \(bucketpa)")
        let bucketpath = "\(bucketpa)dat"
        print("bucketpath final: \(bucketpath)")
        print("Access key = > \(accesskey)")
        print("secret key = > \(secretekey)")
        print("bucket name = > \(bucketname)")
        print("bucket path = > \(bucketpath)")
        print("sessiontoken path = > \(sessiontoken)")
        print("expiration = > \(expiration)")
        
        uploadlogs.append("AWS Cred Responce  => \(dictcredential) \n")

        
        appdelegate.bucketpTest = bucketp
        appdelegate.secretKey = secretekey
        appdelegate.accessKey = accesskey
        appdelegate.bucketname = bucketname
        appdelegate.bucketpath = bucketpath
        appdelegate.sessionToken = sessiontoken
        appdelegate.expiration = expiration
        appdelegate.fileNameFromServer = "\(fileNam).dat"
        appdelegate.thumbnailBucketpath = "\(appdelegate.bucketpath).jpg"
        print("expirself.appdelegate.thumbnailBucketpathation = > \(appdelegate.thumbnailBucketpath)")

        let credentialsProvider = AWSBasicSessionCredentialsProvider(accessKey: accesskey, secretKey: secretekey, sessionToken: sessiontoken)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.USEast1, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let transferUtilityConfigurationWithRetry = AWSS3TransferUtilityConfiguration()
        transferUtilityConfigurationWithRetry.retryLimit = 10
//        transferUtilityConfigurationWithRetry.timeoutIntervalForResource = 15 * 60 // 15 minutes
        transferUtilityConfigurationWithRetry.bucket = bucketname
        AWSS3TransferUtility.register(with: configuration!, transferUtilityConfiguration: transferUtilityConfigurationWithRetry, forKey: appdelegate.bucketpTest)

        //   getuploadpoint()
        checkifvideoimageaudio()
    }

    func refreshToken() {
        let urlString = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)auth/refresh-token"
        let request = NSMutableURLRequest()
        request.url = URL(string: urlString)
        request.httpMethod = "PUT"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        var Parameter: [AnyHashable: Any] = [:]
        Parameter["token"] = UserDefaults.standard.object(forKey: "Authtoken")
        Parameter["refreshToken"] = UserDefaults.standard.object(forKey: "RefreshToken")
        var myData: Data?
        do {
            myData = try JSONSerialization.data(withJSONObject: Parameter, options: .prettyPrinted)
        } catch {
        }
        var putData = Data()
        if let myData = myData {
            putData.append(Data(myData))
        }
        request.httpBody = putData
        var response: URLResponse?

        var responseData: Data?
        do {
            responseData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
        } catch let err {
            print(err)
            self.Login()

        }

        do {
            let json = try JSONSerialization.jsonObject(with: responseData!, options: []) as? [String: Any]
            print(json!)
            if (json?["result"]) != nil {
                let dict = json!["result"] as? [String: Any]
                let AuthToken = dict!["token"] as? String
                UserDefaults.standard.setValue(AuthToken, forKey: "Authtoken")
                UserDefaults.standard.synchronize()
                getCredentialThroughMediaUploadAPI()
            }else{
                
                self.Login()

            }

        } catch {
            print(error)
            self.Login()

        }
    }

    /// This function is used to authorize user with AWS.
    private func connectWithAWS() -> AWSServiceConfiguration? {
        /// Simple session credentials with keys and session token.
        let credentialsProvider = AWSBasicSessionCredentialsProvider(accessKey: appdelegate.accessKey, secretKey: appdelegate.secretKey, sessionToken: appdelegate.sessionToken)

        /// A service configuration object.
        guard let configuration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialsProvider) else {
            return nil
        }
        return configuration
    }

    func replace(myString: String, _ index: Int, _ newChar: Character) -> String {
        var chars = Array(myString.characters) // gets an array of characters
        chars[index] = newChar
        let modifiedString = String(chars)
        return modifiedString
    }

    func getthumnail(asset: PHAsset) {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        option.isNetworkAccessAllowed = true
        DispatchQueue.global(qos: .background).async {
            manager.requestImage(for: asset, targetSize: CGSize(width: 600, height: 600), contentMode: .aspectFill, options: option, resultHandler: { (result, _) -> Void in
                let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                DispatchQueue.main.async {
                    let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                    let imagedata = UIImageJPEGRepresentation(thumbnail, 1.0)
                    let outputPath = self.createthumb()
                    self.thumbpath = outputPath.path
                    print(self.thumbpath!)
                    do {
                        try imagedata?.write(to: outputPath, options: .atomic)
                        sleep(1)
                    } catch {
                        print(error)
                    }

                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        self.sendThumbNailToS3(path: self.thumbpath!)
                    }
                }
            })
        }
    }

    func codecForVideoAsset(asset: AVURLAsset, mediaType: CMMediaType) -> String? {
        let formatDescriptions = asset.tracks.flatMap { $0.formatDescriptions }
        let mediaSubtypes = formatDescriptions
            .filter { CMFormatDescriptionGetMediaType($0 as! CMFormatDescription) == mediaType }
            .map { CMFormatDescriptionGetMediaSubType($0 as! CMFormatDescription).toString() }
        return mediaSubtypes.first
    }

    func creteinfoXML(asset: AVAsset) {
        let asset = asset
        var videoTrack: AVAssetTrack?
        let videoTracks = asset.tracks(withMediaType: .video)
        let width: Float = Float(asset.videoSize().width)
        let height: Float = Float(asset.videoSize().height)
        if videoTracks.count > 0 {
            videoTrack = videoTracks[0]
        }

        let frameRate: Float = videoTrack!.nominalFrameRate
        let bps: Float = videoTrack!.estimatedDataRate

        let str: String = videoTrack?.mediaFormat ?? "avc1"
        
        
        X_LATAKOO_VIDEO_FORMAT = str
         
//        if chosenBitrate != 0 {
//            X_LATAKOO_VIDEO_FORMAT = ""
//            let str : String = videoTrack?.mediaFormat ?? "vide/avc1"
//            if str == "vide/hvc1"{
//                X_LATAKOO_VIDEO_FORMAT = "HEVC"
//            }
//
//        }
        
        print("X_LATAKOO_VIDEO_FORMAT: \(X_LATAKOO_VIDEO_FORMAT)")

//        let str : String = videoTrack?.mediaFormat ?? "vide/avc1"
//            print(str)
//        if str == "vide/hvc1"{
//         X_LATAKOO_VIDEO_FORMAT = "HEVC"
//        }else{
//        X_LATAKOO_VIDEO_FORMAT = "AVC"
//        }

        //  X_LATAKOO_VIDEO_FORMAT =

        let dictheader = [
            "Complete name": filename,
            "Format": X_LATAKOO_VIDEO_FORMAT,
            "File size": "\(originalsize)",
            "Format profile": "QuickTime",
            "Duration": videoduration,
            "Bit rate": "\(bps)",
            "Width": "\(width)",
            "Height": "\(height)",
            "Frame rate": "\(frameRate)",
        ]

        
        
        uploadlogs.append("video property Cred Responce  => \(dictheader) \n")

        var jsonData: Data?
        do {
            jsonData = try JSONSerialization.data(withJSONObject: dictheader, options: .prettyPrinted)
        } catch {
        }
        var jsonString: String?
        if let jsonData = jsonData {
            jsonString = String(data: jsonData, encoding: .utf8)
        }

        print(jsonString!)

        writeDataToFile(file: jsonString!)
    }

    func writeDataToFile(file: String) -> Bool {
        let documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        do {
            try FileManager.default.createDirectory(atPath: documentsPath.path!, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            NSLog("Unable to create directory \(error.debugDescription)")
        }

        let str = file

        let fileName = documentsPath.appendingPathComponent("videoinfo.txt")

        do {
            try str.write(to: fileName!, atomically: false, encoding: String.Encoding.utf8)
            sleep(1)
            videoinfodata = NSData(contentsOf: fileName!) as Data?
            print(videoinfodata?.count)
            getCredentialThroughMediaUploadAPI()
            return true
        } catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
            return false
        }
    }

    func creteinfoXMLImage(image: UIImage) {
        originalsize = UIImageJPEGRepresentation(image, 1.0)!.count

        let dictheader = [
            "Complete name": filename,
            "Format": ".jpg",
            "File size": "\(originalsize)",
            "Format profile": "QuickTime",
        ]

        var jsonData: Data?
        do {
            jsonData = try JSONSerialization.data(withJSONObject: dictheader, options: .prettyPrinted)
        } catch {
        }
        var jsonString: String?
        if let jsonData = jsonData {
            jsonString = String(data: jsonData, encoding: .utf8)
        }

        print(jsonString!)

        writeDataToFile(file: jsonString!)
    }

    func writeDataToFileImage(file: String) -> Bool {
        let documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        do {
            try FileManager.default.createDirectory(atPath: documentsPath.path!, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            NSLog("Unable to create directory \(error.debugDescription)")
        }

        let str = file

        let fileName = documentsPath.appendingPathComponent("imageinfo.txt")

        do {
            try str.write(to: fileName!, atomically: false, encoding: String.Encoding.utf8)
            sleep(1)
            imageinfodata = NSData(contentsOf: fileName!) as Data?
            print(imageinfodata?.count)
            getCredentialThroughMediaUploadAPI()
            return true
        } catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
            return false
        }
    }

    func creteinfoXMLAudio() {
        let dictheader = [
            "Complete name": filename,
            "Format": ".m4a",
        ]

        var jsonData: Data?
        do {
            jsonData = try JSONSerialization.data(withJSONObject: dictheader, options: .prettyPrinted)
        } catch {
        }
        var jsonString: String?
        if let jsonData = jsonData {
            jsonString = String(data: jsonData, encoding: .utf8)
        }

        print(jsonString!)

        writeDataToFile(file: jsonString!)
    }

    func writeDataToFileAudio(file: String) -> Bool {
        let documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        do {
            try FileManager.default.createDirectory(atPath: documentsPath.path!, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            NSLog("Unable to create directory \(error.debugDescription)")
        }

        let str = file

        let fileName = documentsPath.appendingPathComponent("audioinfo.txt")

        do {
            try str.write(to: fileName!, atomically: false, encoding: String.Encoding.utf8)
            sleep(1)
            audioinfodata = NSData(contentsOf: fileName!) as Data?
            print(audioinfodata?.count)
            getCredentialThroughMediaUploadAPI()
            return true
        } catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
            return false
        }
    }
    
    
    
    func writeToTextFile() {


         let tempDir = NSTemporaryDirectory()
          let timeStamp = Date().timeIntervalSince1970
          let timeStampObj = NSNumber(value: Int32(timeStamp))
          let fileName = "\(tempDir)/log_ios_\(timeStampObj).log.zip"

          if FileManager.default.fileExists(atPath: fileName) {
              do {
                  try FileManager.default.removeItem(atPath: fileName)
              } catch {
              }
          }
        islogapi = true

          let content = uploadlogs
        
        print(content)
     
        let zipFile = OZZipFile(fileName: fileName, mode: .create, legacy32BitMode:true)
        let stream2 = zipFile.writeInZip(withName: "IOS_log.txt", compressionLevel:.default)

        let data = content.data(using: .utf8)
        
        stream2.write(data!)
        stream2.finishedWriting()
          zipFile.close()

          let data1 = NSData(contentsOfFile: fileName) as Data?
          if FileManager.default.fileExists(atPath: fileName) {
              do {
                  try FileManager.default.removeItem(atPath: fileName)
              } catch {
              }
          }
    
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                          //     appdelegate.ShowProgress()
        var aStrApi = "https://latakoo.com/-/api2/?method=system.log.client&type=latakoo_client_zip&email=\(UserDefaults.standard.value(forKey: "Email") as? String ?? "")"
                          print("sendblob API => \(aStrApi)")
               aStrApi  = AJNotificationView.changetoencodedstring(aStrApi)
                
               
        
        
        
             islogapi = true
                let heder =  appdelegate.getAPIEncodedHeader(withParameters: aStrApi)
                let headervalue = ["X_PFTP_API_TOKEN":heder]
               var request = URLRequest(url: URL.init(string: aStrApi)!)
      
               print(request)

        
               request.httpMethod = "POST"
               request.httpBody = data1
               request.allHTTPHeaderFields = headervalue as? [String : String]
                self.initiateConnection(with: request)
       

      }
    
    
    
       func Login(){
                          
                      
                        
                        let qualityOfServiceClass = DispatchQoS.QoSClass.background
                        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
                        backgroundQueue.async(execute: {
                       DispatchQueue.main.async(execute: { () -> Void in
                      let headers = ["User-Agent":"HTTP_USER_AGENT:Android",
                                     "os_type": "IOS",
                                     "os_version": "IOS14",
                                     "app_version": "3.1.3"]
                        
                        
                        
                        let stremail = UserDefaults.standard.value(forKey: "Email") as! String
                        let strpassword = UserDefaults.standard.value(forKey: "Password") as! String
                        print(stremail)
                        print(strpassword)

                                                                                                
                                                                                                

                        let aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)auth/sign-in"
                        let aDictParam : [String:Any]!
                        aDictParam = ["email":stremail,
                                      "password":strpassword
                        ]
                        print(aStrApi)
                        print(aDictParam)
                        print(headers)
                    let encoding = Alamofire.JSONEncoding.default
                    Alamofire.request(aStrApi, method: .post, parameters: aDictParam, encoding:encoding, headers: headers).responseJSON { (responseObject) -> Void in
                        print(responseObject)
                    if responseObject.result.isSuccess {
                    let resJson = JSON(responseObject.result.value!)
                    let usrDetail = resJson
                                                                   print(resJson)
                                                                                        
                                                                                              
                                                                                                
                                                                                                
                                                                if usrDetail["result"].exists() {
                                                                 let usrDetailVal = usrDetail["result"].rawValue as! [String:Any]
                                                                    let RefreshToken = usrDetailVal["refreshToken"] as? String
                                                                    let AuthToken = usrDetailVal["token"] as? String
                                                                    print(RefreshToken!)
                                                                    print(AuthToken!)

                                                                    
                                                                    
                                                                    UserDefaults.standard.setValue(AuthToken, forKey: "Authtoken")
                                                                     UserDefaults.standard.setValue(RefreshToken, forKey: "RefreshToken")
                                                                    UserDefaults.standard.synchronize()
                                                             
                                                                    self.getCredentialThroughMediaUploadAPI()

                        
                        }
                                                                                          
                            }

                                                               
                                                               if responseObject.result.isFailure {
                                                                
                                        }
                                                           }
                                

                        
                            })
                        })
                    }
            
        
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

extension String {
    func fileName() -> String {
        return URL(fileURLWithPath: self).deletingPathExtension().lastPathComponent
    }

    func fileExtension() -> String {
        return URL(fileURLWithPath: self).pathExtension
    }
}

extension AVAsset {
    func videoSize() -> CGSize {
        let visual = AVMediaCharacteristic.visual
        let vTrack = tracks(withMediaCharacteristic: visual)[0]
        var error: NSError?
        let keyPath = #keyPath(AVAssetTrack.naturalSize)
        if vTrack.statusOfValue(forKey: keyPath, error: &error) == .loaded {
            return vTrack.orientationBasedSize
        } else {
            var size = CGSize()
            let dg = DispatchGroup()
            dg.enter()
            vTrack.loadValuesAsynchronously(forKeys: [keyPath]) {
                size = vTrack.orientationBasedSize
                dg.leave()
            }
            dg.wait()
            return size
        }
    }
}

extension AVAssetTrack {
    var orientation: (UIInterfaceOrientation, AVCaptureDevice.Position) {
        var orientation: UIInterfaceOrientation = .unknown
        var device: AVCaptureDevice.Position = .unspecified
        let t = preferredTransform

        if t.a == 0 && t.b == 1.0 && t.d == 0 {
            orientation = .portrait

            if t.c == 1.0 {
                device = .front
            } else if t.c == -1.0 {
                device = .back
            }
        } else if t.a == 0 && t.b == -1.0 && t.d == 0 {
            orientation = .portraitUpsideDown

            if t.c == -1.0 {
                device = .front
            } else if t.c == 1.0 {
                device = .back
            }
        } else if t.a == 1.0 && t.b == 0 && t.c == 0 {
            orientation = .landscapeRight

            if t.d == -1.0 {
                device = .front
            } else if t.d == 1.0 {
                device = .back
            }
        } else if t.a == -1.0 && t.b == 0 && t.c == 0 {
            orientation = .landscapeLeft

            if t.d == 1.0 {
                device = .front
            } else if t.d == -1.0 {
                device = .back
            }
        }

        return (orientation, device)
    }

    var isPortrait: Bool {
        return orientation.0.isPortrait
    }

    var orientationBasedSize: CGSize {
        guard isPortrait else {
            return naturalSize
        }

        return CGSize(width: naturalSize.height, height: naturalSize.width)
    }
}

extension AVURLAsset {
    var fileSize: Int? {
        let keys: Set<URLResourceKey> = [.totalFileSizeKey, .fileSizeKey]
        let resourceValues = try? url.resourceValues(forKeys: keys)

        return resourceValues?.fileSize ?? resourceValues?.totalFileSize
    }
}

extension URL {
    func fileSize() -> Int? {
        var fileSize: Int = 0
        var fileSizeValue = 0.0
        try? fileSizeValue = (resourceValues(forKeys: [URLResourceKey.fileSizeKey]).allValues.first?.value as! Double?)!
        if fileSizeValue > 0.0 {
            fileSize = Int(fileSizeValue)
        }
        return fileSize
    }
}

extension Date {
    static func getCurrentDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss a"
        return dateFormatter.string(from: Date())
    }
}

extension AVAssetTrack {
    var mediaFormat: String {
        var format = ""
        let descriptions = formatDescriptions as! [CMFormatDescription]
        for (index, formatDesc) in descriptions.enumerated() {
            // Get String representation of media type (vide, soun, sbtl, etc.)
            let type =
                CMFormatDescriptionGetMediaType(formatDesc).toString()
            // Get String representation media subtype (avc1, aac, tx3g, etc.)
            let subType =
                CMFormatDescriptionGetMediaSubType(formatDesc).toString()
            // Format string as type/subType
//            format += "\(type)/\(subType)"
            format += "\(subType)"
            // Comma separate if more than one format description
            if index < descriptions.count - 1 {
                format += ","
            }
        }
        return format
    }
}

extension FourCharCode {
    // Create a String representation of a FourCC
    func toString() -> String {
        let bytes: [CChar] = [
            CChar((self >> 24) & 0xFF),
            CChar((self >> 16) & 0xFF),
            CChar((self >> 8) & 0xFF),
            CChar(self & 0xFF),
            0,
        ]
        let result = String(cString: bytes)
        let characterSet = CharacterSet.whitespaces
        return result.trimmingCharacters(in: characterSet)
    }
}

