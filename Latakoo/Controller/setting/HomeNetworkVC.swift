//
//  HomeNetworkVC.swift
//  Latakoo
//
//  Created by Shruti Aggarwal on 21/08/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import UIKit
import Foundation

class HomeNetworkVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return networks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! filtercell
        let dict = networks[indexPath.row]
        cell.lblnetwokname.text = dict["company_name"] as? String
        let companyid = dict["company_id"] as? String
        if homenetwork .contains(companyid!){
        cell.imgtick.isHidden = false
        }else{
        cell.imgtick.isHidden = true
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = networks[indexPath.row]
      
            let companyid = "\(dict["company_id"] as? String ?? "")"
            if homenetwork .contains(companyid){
                if let index = homenetwork.index(of:companyid) {
                        homenetwork.remove(at: index)
                }
            }else{
                homenetwork.insert(companyid, at:0)
            }
     
        tblfilter.reloadData()
    }
    
    @IBOutlet weak var tblfilter: UITableView!
    
    var networks : [[String:Any]] = []
    var selectednetwork : [String] = []
    var homenetwork : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        networks  = UserDefaults.standard.value(forKey:"AvailableNetworks") as! [[String : Any]]
        
        networks = networks.sorted { ($0["company_name"] as! String).localizedCaseInsensitiveCompare($1["company_name"] as! String) == ComparisonResult.orderedAscending }

      
        print(networks)

        
        homenetwork = UserDefaults.standard.value(forKey:"homenetwork") as? [String] ?? []
        tblfilter.reloadData()
        
      
    }
    

    
    @IBAction func btnback(_ sender: Any) {
        print(homenetwork)
        
        UserDefaults.standard.set(homenetwork, forKey:"homenetwork")
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
}
