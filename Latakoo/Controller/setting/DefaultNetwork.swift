//
//  DefaultNetwork.swift
//  Latakoo
//
//  Created by Shruti Aggarwal on 21/08/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import UIKit

class DefaultNetwork: UIViewController,UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return networks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! filtercell
        let dict = networks[indexPath.row] 
        cell.lblnetwokname.text = dict["company_name"] as? String
        let companyid = "\(dict["company_id"] as? String ?? "")"
        if companyid == defaultnetwork{
            cell.imgtick.isHidden = false
        }else{
            cell.imgtick.isHidden = true
        }
        return cell
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = networks[indexPath.row] 
        let companyid = "\(dict["company_id"] as? String ?? "")"
        self.serviceChangedefualtNetwork(networkid: companyid)
     
    }
    
    @IBOutlet weak var tblfilter: UITableView!
    
    var networks : [[String : Any]] = []
    var defaultnetwork : String = ""
    let appdelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        
      defaultnetwork =  UserDefaults.standard.value(forKey:"defultnetwork") as! String
      
        



    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
         networks  = UserDefaults.standard.value(forKey:"AvailableNetworks") as! [[String : Any]]
         networks = networks.sorted { ($0["company_name"] as! String).localizedCaseInsensitiveCompare($1["company_name"] as! String) == ComparisonResult.orderedAscending }

        print(networks)
        tblfilter.reloadData()
        
        
    }
    
    
    
    @IBAction func btnback(_ sender: Any) {
      
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func serviceChangedefualtNetwork(networkid:String){
        
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 2.0)
            return
        }
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.ShowProgress()
        var aStrApi = ""
        
        aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=user.set.defaultnetwork&id=\(networkid)"
        
      
        APIManager.requestGETURL(aStrApi, success: {(usrDetail) in
            print(usrDetail)
            if usrDetail["status_code"].rawString() == "0"{
                
                UserDefaults.standard.set(networkid, forKey: "defultnetwork")
                UserDefaults.standard.synchronize()
                
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.HideProgress()
                
                print("Internet is Not available.")
//                AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeGreen, title:"Changed Successfully!", hideAfter: 2.0)
                self.navigationController?.popViewController(animated: true)
                
                
            }else{
                AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title:"Unable to set this network as your default network", hideAfter: 2.0)
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.HideProgress()
                
                
            }
            
        }, failure: {(error) in
            print(error)
            appdelegate.HideProgress()
            
            
        })
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
                if !IS_IPHONE{
                    self.perform(#selector(changeloader), with: nil, afterDelay: 0.2)

           }
       }
       
    @objc func changeloader(){
          appdelegate.initializeLoader()
      }
    
    
}
