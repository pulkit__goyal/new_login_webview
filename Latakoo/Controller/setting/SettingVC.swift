//
//  SettingVC.swift
//  Latakoo
//
//  Created by Shruti Aggarwal on 19/08/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import AWSSNS
import UIKit

class SettingVC: UIViewController {
    @IBOutlet var lbldefaultnetworkname: UILabel!
    @IBOutlet var lblusername: UILabel!
    @IBOutlet var lblVersion: UILabel!

    @IBOutlet var lblquality: UILabel!
    var dictuser: [String: Any] = [:]
    var bitratearry: [Any] = []
    @IBOutlet var sliderview: StepSlider!
    @IBOutlet var myscrollview: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        if IS_IPHONE {
            myscrollview.contentSize = CGSize(width: view.frame.size.width, height: 800)
        }

        lblusername.text = UserDefaults.standard.value(forKey: "Email") as! String
        if UserDefaults.standard.value(forKey: "server") as! String == "https://dev.latakoo.com/" {
            // lblVersion.text = "Version 3.0 (DEV)"
            lblVersion.text = "Version 3.1.3"

        } else {
            // lblVersion.text = "Version 2.9 (LIVE)"
            lblVersion.text = "Version 3.1.3"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        dictuser = UserDefaults.standard.value(forKey: "userdetails") as! [String: Any]
        print(dictuser)
        bitratearry = dictuser["bitrates_details"] as! [Any]
        print(bitratearry)
        
        sliderview.maxCount = UInt(bitratearry.count)

        if UserDefaults.standard.value(forKey: "selectedbitrate") != nil {
            var count = 0
            for value in bitratearry {
                print(value)
                let dict = value as! [String: Any]
                print(dict)
                
                if dict["label"] as! String == UserDefaults.standard.value(forKey: "selectedbitrate") as! String {
                    sliderview.index = UInt(count)
                }
                
                count = count + 1
            }
            
        } else {
            sliderview.index = 0
            let dict = bitratearry[0] as! [String: Any]
            lblquality.text = dict["label"] as? String
        }
        
        let networks = UserDefaults.standard.value(forKey: "AvailableNetworks") as! [Any]
        let defaultnetwork = UserDefaults.standard.value(forKey: "defultnetwork") as! String
        
        for dict in networks {
            let dictnetwork = dict as! [String: Any]
            print(dictnetwork)
            let networkid = dictnetwork["company_id"] as! String
            if networkid == defaultnetwork {
                lbldefaultnetworkname.text = dictnetwork["company_name"] as? String
            }
        }
    }
    
    @IBAction func btnhomenetwork(_ sender: Any) {
        var story: UIStoryboard?
                   
        if IS_IPHONE {
            story = UIStoryboard(name: "Main", bundle: nil)
        } else {
            story = UIStoryboard(name: "MainIPAD", bundle: nil)
        }
        let HomeNetworkVC = story?.instantiateViewController(withIdentifier: "HomeNetworkVC") as! HomeNetworkVC
        self.navigationController?.pushViewController(HomeNetworkVC, animated: true)
    }
    
    @IBAction func btncaptureprefrence(_ sender: Any) {
        var story: UIStoryboard?
                   
        if IS_IPHONE {
            story = UIStoryboard(name: "Main", bundle: nil)
        } else {
            story = UIStoryboard(name: "MainIPAD", bundle: nil)
        }
        let CapturePrefrence = story?.instantiateViewController(withIdentifier: "CapturePrefrence") as! CapturePrefrence
        self.navigationController?.pushViewController(CapturePrefrence, animated: true)
    }
    
    @IBAction func defaultnetwork(_ sender: Any) {
        var story: UIStoryboard?
                           
        if IS_IPHONE {
            story = UIStoryboard(name: "Main", bundle: nil)
        } else {
            story = UIStoryboard(name: "MainIPAD", bundle: nil)
        }
        let DefaultNetwork = story?.instantiateViewController(withIdentifier: "DefaultNetwork") as! DefaultNetwork
        self.navigationController?.pushViewController(DefaultNetwork, animated: true)
    }
    
    @IBAction func btnnetworkprefrence(_ sender: Any) {
        var story: UIStoryboard?
                   
        if IS_IPHONE {
            story = UIStoryboard(name: "Main", bundle: nil)
        } else {
            story = UIStoryboard(name: "MainIPAD", bundle: nil)
        }
        let NotificationVC = story?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(NotificationVC, animated: true)
    }

    @IBAction func btnlogout(_ sender: Any) {
        self.showSimpleAlert()
    }
    
    func showSimpleAlert() {
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
            kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
            kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
            showCloseButton: false,
            dynamicAnimatorActive: true,
            buttonsLayout: .horizontal
        )
        let alert = SCLAlertView(appearance: appearance)
        _ = alert.addButton("NO") {}
        _ = alert.addButton("Sign out") {
            let str = UserDefaults.standard.value(forKey: "isshowmenifest") as? String
            if str == "YES" {
                self.disableendpoint()
            }
                            
            let deviceTokenString = UserDefaults.standard.object(forKey: "DeviceToken") as? String

//                                   let domain = Bundle.main.bundleIdentifier!
//                                                                       UserDefaults.standard.removePersistentDomain(forName: domain)
                            
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
                            
            UserDefaults.standard.synchronize()
                            
            UserDefaults.standard.set(deviceTokenString, forKey: "DeviceToken")
            UserDefaults.standard.synchronize()
                            
            print(deviceTokenString ?? "")

            var story: UIStoryboard?
                                                                                  
            if IS_IPHONE {
                story = UIStoryboard(name: "Main", bundle: nil)
            } else {
                story = UIStoryboard(name: "MainIPAD", bundle: nil)
            }
                            
          //  let LoginVC = story?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
           // LoginVC.hidesBottomBarWhenPushed = true
          //  self.navigationController?.pushViewController(LoginVC, animated: false)
            
//            let ViewController = story?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//            ViewController.hidesBottomBarWhenPushed = true
//            self.navigationController?.pushViewController(ViewController, animated: false)
            
            
            let LoginWebview = story?.instantiateViewController(withIdentifier: "LoginWebview") as! LoginWebview
            LoginWebview.hidesBottomBarWhenPushed = true
            LoginWebview.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(LoginWebview, animated: false)

        }
                           
        let icon = UIImage(named: "logoutwhite.png")
        let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
                           
        _ = alert.showCustom("Sign out?", subTitle: "Are you sure you want to sign out?", color: color, circleIconImage: icon!)
    }
    
    @IBAction func sliderquality(_ sender: Any) {
        let newindex = Int(sliderview.index)
        let dict = bitratearry[newindex] as! [String: Any]
        lblquality.text = dict["label"] as? String
        UserDefaults.standard.set(dict["label"] as! String, forKey: "selectedbitrate")
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    func disableendpoint() {
        let deviceTokenString = UserDefaults.standard.object(forKey: "DeviceToken") as? String
        let manager = AWSSNS.default()
        let EndPointArn = UserDefaults.standard.value(forKey: "EndpointArn") as? String
        let attributes = [
            "Token": deviceTokenString ?? "",
            "Enabled": "false"
        ]
        let seai = AWSSNSSetEndpointAttributesInput()
        seai?.attributes = attributes
        seai?.endpointArn = EndPointArn
        let setEndpointAttributesTask: AWSTask = manager.setEndpointAttributes(seai!)
        setEndpointAttributesTask.continueWith(block: { task in
            if let result = task.result, let error1 = task.error {
                print("response : \(result), error: \(error1)")
            }

            if task.error != nil {
                if let error1 = task.error {
                    print("error from user subscribe -> \(error1)")
                }
            } else {
                if let result = task.result {
                    print("Response for Default Network Subscribe -> \(result)")
                }
            }
            return nil
        })
    }
}
