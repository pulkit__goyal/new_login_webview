//
//  NotificationVC.swift
//  Latakoo
//
//  Created by Shruti Aggarwal on 21/08/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import UIKit
import AWSSNS


class notificationCell: UITableViewCell {
    @IBOutlet weak var togglebutton: UISwitch!
    @IBOutlet weak var lblnetworkname: UILabel!
    
}

class NotificationVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return networks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! notificationCell
       
        let dict = networks[indexPath.row] 
        cell.lblnetworkname.text = dict["name"] as? String
        cell.togglebutton.tag = indexPath.row
        if indexPath.row == 0 {
            
            if UserDefaults.standard.value(forKey:"allnotification") as! Bool == false{
                cell.togglebutton.setOn(false, animated: true)
            }else{
                cell.togglebutton.setOn(true, animated: true)
            }
            
            
        }else{
            if dict["notify"] as! Bool == false {
                cell.togglebutton.setOn(false, animated: true)
            }else{
                cell.togglebutton.setOn(true, animated: true)
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
    }
    
    @IBOutlet weak var tblnotify: UITableView!
    
    var networks : [[String:Any]] = []

    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var myuploadswitch: UISwitch!


    override func viewDidLoad() {
        super.viewDidLoad()
        let myupload = UserDefaults.standard.value(forKey:"myuploads") as! Bool
        if myupload == false {
            myuploadswitch.setOn(myupload, animated: true)
        }else{
            myuploadswitch.setOn(myupload, animated: true)
        }
        
        networks  = UserDefaults.standard.value(forKey:"notification_preferences") as! [[String : Any]]
        print(networks)

        
          networks = networks.sorted { ($0["name"] as! String).localizedCaseInsensitiveCompare($1["name"] as! String) == ComparisonResult.orderedAscending }
        
        print(networks)

      
        tblnotify.reloadData()
        

    }
    

    @IBAction func youruploadswitch(_ sender: UISwitch) {
        UserDefaults.standard.set(sender.isOn, forKey: "myuploads")
        UserDefaults.standard.synchronize()
    }
    
    
    @IBAction func btnback(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func switchnotification(_ sender: UISwitch) {
        print(sender.tag)
        let AvailableNetworks : [Any] = UserDefaults.standard.value(forKey: "AvailableNetworks") as! [Any]

        
        if sender.tag == 0 {
            
            
            self.setallnetwork(onoff: sender.isOn)
            
            for dict in AvailableNetworks {
                let dictnew = dict as! [String:Any]
                print(dictnew)
                    
                    let topicarn = dictnew["topic_arn"] as? String
                    print(topicarn ?? "")
                        
                        if sender.isOn == true {
                            
                            DispatchQueue.global(qos: .background).async {

                                       self.subscribefortop(topic: topicarn ?? "")
                            }
                                 
                        }else{
                            
                            DispatchQueue.global(qos: .background).async {

                                       self.subscribefortopnew(topic: topicarn ?? "")
                            }
                }
               
            }
            
            
        }else{
            
            
            let dict = networks[sender.tag]
            let networkid = dict["id"] as? String

            
            
            for dict in AvailableNetworks {
                let dictnew = dict as! [String:Any]
                print(dictnew)
                let companyidd = "\(dictnew["company_id"] as! String)"
                if companyidd == networkid {
                    
                    let topicarn = dictnew["topic_arn"] as? String
                    print(topicarn ?? "")
                        
                        if sender.isOn == true {
                            DispatchQueue.global(qos: .background).async {
                                       self.subscribefortop(topic: topicarn ?? "")
                                     }

                                   }else{
                               DispatchQueue.global(qos: .background).async {
                            self.subscribefortopnew(topic: topicarn ?? "")
                                   }
                                }
                    
                    break;
                }

                
            }

            
        self.setnotification(onoff: sender.isOn, networkid: networkid!, dictnew: dict, index: sender.tag)
            
           
            
            
        }
    }
    
    
    
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    
    
    
    
    func setnotification(onoff : Bool , networkid : String , dictnew : [String:Any] , index : Int)  {
             
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }
        
        appdelegate.ShowProgress()
        
        var str:String?
        
        if onoff == true {
            str = "1"
        }else{
            str = "0"
        }
        
        
        let  aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=user.set.notification&net_id=\(networkid)&value=\(str ?? "0")"
                  let heder =  appdelegate.getAPIEncodedHeader(withParameters: aStrApi)
          let headervalue = ["X_PFTP_API_TOKEN":heder]
          APIManager.requestPOSTURL(aStrApi, params: nil, headers: headervalue as? [String : String], success: {(usrDetail) in
            print(usrDetail)
            if usrDetail["status_code"].rawString() == "0"{
                self.appdelegate.HideProgress()
                let dict : [String:Any] = ["name":dictnew["name"] as! String , "id":networkid , "notify":onoff]
                self.networks[index] = dict
                self.tblnotify.reloadData()
                print(dict)
            }else{
                self.view.makeToast(usrDetail["message"].rawString())
                self.appdelegate.HideProgress()
            }
        },
                                  
             failure: {(error) in
             print(error)
                self.appdelegate.HideProgress()
        })
    }

    
    
    func setallnetwork(onoff : Bool )  {
             
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }
          appdelegate.ShowProgress()
        
        var str:String?
        
        if onoff == true {
            str = "1"
        }else{
            str = "0"
        }
        
        let  aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=user.set.allnotifications&value=\(str ?? "0")"
                  let heder =  appdelegate.getAPIEncodedHeader(withParameters: aStrApi)
          let headervalue = ["X_PFTP_API_TOKEN":heder]
        
        print(aStrApi);
           
          APIManager.requestPOSTURL(aStrApi, params: nil, headers: headervalue as? [String : String], success: {(usrDetail) in
            print(usrDetail)
            if usrDetail["status_code"].rawString() == "0"{
                self.appdelegate.HideProgress()
                self.setallnetworkonoff(onoff: onoff)
               
            }else{
                self.view.makeToast(usrDetail["message"].rawString())
                self.appdelegate.HideProgress()
            }
        },
                                  
             failure: {(error) in
             print(error)
                self.appdelegate.HideProgress()
        })
    }
    
    
    func setallnetworkonoff(onoff:Bool){
        
        
        
        UserDefaults.standard.set(onoff, forKey: "allnotification")
        var arr : [Any] = []
        for dict in networks {
            print(dict)
        let dictnew  = dict 
            let dict : [String:Any] = ["name": dictnew["name"] as! String , "id":dictnew["id"] as! String , "notify":onoff]
            arr.append(dict)
        }
        networks = arr as! [[String : Any]]
        self.tblnotify.reloadData()
    }
    
    
    
    func UNsubscribefortop(SubscribeArn:String){
        let sns = AWSSNS.default()
        let input = AWSSNSUnsubscribeInput()
        input?.subscriptionArn = SubscribeArn
        print(input?.subscriptionArn! as Any)
        sns.unsubscribe(input!, completionHandler: { error in
                  if error != nil {

                      print("Error occurred: [\((error as NSError?)?.code ?? 0)]")
                  } else {

                      print("Unsubscribe successfully")
                  }
              })
        
    }

    
    
    
    func subscribefortop(topic:String){
         
           let plateformarn = appdelegate.SNSPlatformApplicationArn
           print(plateformarn)
           let sns = AWSSNS.default()
         let endpointRequest = AWSSNSCreatePlatformEndpointInput()
         endpointRequest?.platformApplicationArn = plateformarn
         let deviceTokenString = UserDefaults.standard.value(forKey:"DeviceToken")
         endpointRequest?.token = deviceTokenString as? String
        sns.createPlatformEndpoint(endpointRequest!).continueWith { (task) -> AnyObject? in
                if task.error != nil {
                 print("Error creating platform endpoint: \(String(describing: task.error)  )")
                    return nil
                }
                 let result = task.result!
                let subscribeInput = AWSSNSSubscribeInput()
                subscribeInput?.topicArn = topic
                subscribeInput?.endpoint = result.endpointArn
                print("Enfdpoint created")
                print("Endpoint arn: \(result.endpointArn!)")
                subscribeInput?.protocols = "application"
             sns.subscribe(subscribeInput!).continueWith { (task) -> AnyObject? in
                    
                    if task.error != nil {
                     print("Error subscribing: \(String(describing: task.error))")
                        return nil
                    }
                    print("Subscribed succesfully")
                 print("Subscription arn: \(String(describing: task.result?.subscriptionArn!))")
                    let subscriptionConfirmInput = AWSSNSConfirmSubscriptionInput()


                    subscriptionConfirmInput?.topicArn = topic
                   sns.confirmSubscription(subscriptionConfirmInput!).continueWith { (task) -> AnyObject? in
                        
                        if task.error != nil {
                         
                        }
                     
                        return nil
                    }
                    return nil
                }
                return nil
            }
         
         
     }
    

    override func viewWillDisappear(_ animated: Bool) {
        UserDefaults.standard.set(networks, forKey: "notification_preferences")
        UserDefaults.standard.synchronize()
    }


 func subscribefortopnew(topic:String){
         
           let plateformarn = appdelegate.SNSPlatformApplicationArn
           print(plateformarn)
           let sns = AWSSNS.default()
         let endpointRequest = AWSSNSCreatePlatformEndpointInput()
         endpointRequest?.platformApplicationArn = plateformarn
         let deviceTokenString = UserDefaults.standard.value(forKey:"DeviceToken")
         endpointRequest?.token = deviceTokenString as? String
        sns.createPlatformEndpoint(endpointRequest!).continueWith { (task) -> AnyObject? in
                if task.error != nil {
                 print("Error creating platform endpoint: \(String(describing: task.error)  )")
                    return nil
                }
                 let result = task.result!
                let subscribeInput = AWSSNSSubscribeInput()
                subscribeInput?.topicArn = topic
                subscribeInput?.endpoint = result.endpointArn
                print("Enfdpoint created")
                print("Endpoint arn: \(result.endpointArn!)")
                subscribeInput?.protocols = "application"
             sns.subscribe(subscribeInput!).continueWith { (task) -> AnyObject? in
                    
                    if task.error != nil {
                     print("Error subscribing: \(String(describing: task.error))")
                        return nil
                    }
                    print("Subscribed succesfully")
                 print("Subscription arn: \(String(describing: task.result?.subscriptionArn!))")
                    let subscriptionConfirmInput = AWSSNSConfirmSubscriptionInput()
                self.UNsubscribefortop(SubscribeArn: task.result!.subscriptionArn!)
                    subscriptionConfirmInput?.topicArn = topic
                   sns.confirmSubscription(subscriptionConfirmInput!).continueWith { (task) -> AnyObject? in
                        
                        if task.error != nil {
                         
                        }
                     
                        return nil
                    }
                    return nil
                }
                return nil
            }
         
         
     }
    
}
 
