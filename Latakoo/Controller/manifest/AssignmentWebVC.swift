//
//  AssignmentWebVC.swift
//  Latakoo
//
//  Created by Mayur Sardana on 02/07/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//

import UIKit

class AssignmentWebVC: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var webview: UIWebView!
    var weburl: String?
    var isfeed: String?
    @IBOutlet weak var viewheight: NSLayoutConstraint!
     @IBOutlet weak var header: UILabel!
    @IBOutlet weak var btnsend: UIButton!

    let appdelegate = UIApplication.shared.delegate as! AppDelegate

    
    override func viewDidLoad() {
        super.viewDidLoad()
        appdelegate.ShowProgress()
        if isfeed == "EDIT" {
               viewheight.constant = 0
            header.text = "Edit Topic"
            btnsend.isHidden = true

           }else{
            header.text = "Assignments"
           }
        let targetURL = URL(string:weburl!)
        
        var request: URLRequest? = nil
        if let targetURL = targetURL {
            request = URLRequest(url: targetURL)
        }
        if let request = request {
            webview.loadRequest(request)
        }

    }
    
    
    
    @IBAction func btnsendtoassignment(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 2
        let navCtrl = tabBarController?.selectedViewController as? UINavigationController
        navCtrl?.popToRootViewController(animated: false)
    }
    
    
    @IBAction func btback(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
           appdelegate.HideProgress()
       }
       
       func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
         return true
       }
       
       func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
           appdelegate.HideProgress()
       }
    override var prefersStatusBarHidden: Bool {
           return true
       }
       

    
}
