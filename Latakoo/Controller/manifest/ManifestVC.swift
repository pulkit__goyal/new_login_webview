//
//  ManifestVC.swift
//  Latakoo
//
//  Created by Mayur Sardana on 30/06/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//

import UIKit
import AFNetworking
import SwiftyJSON
import Alamofire
import TableViewReloadAnimation

class MenifestCell: UITableViewCell {
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lbllocation: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var btnrequest: UIButton!
    @IBOutlet weak var btnfollow: UIButton!
    @IBOutlet weak var imgnext: UIImageView!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var btnswitch: UISwitch!

    override func awakeFromNib() {
        super.awakeFromNib()
        btnfollow.layer.borderColor = UIColor.lightGray.cgColor
        btnfollow.layer.borderWidth = 1
        btnfollow.layer.cornerRadius = 10
        btnrequest.layer.borderColor = UIColor.lightGray.cgColor
        btnrequest.layer.borderWidth = 1
        btnrequest.layer.cornerRadius = 10
    }

}

class ManifestVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, filterdelegate, UIWebViewDelegate {
    func fromfilter(_ controller: FilterMenifestVC) {
        if flagtable == 200 {
            self.Servicegettopic()
        } else {
            self.Servicegetmytopic()

        }
    }
    
    @IBOutlet weak var viewalert: UIView!
    @IBOutlet weak var lblalert: UILabel!
    @IBOutlet weak var tblmenifest: UITableView!
    @IBOutlet weak var btnfeed: UIButton!
    @IBOutlet weak var btncreate: UIButton!
    @IBOutlet weak var btnasignment: UIButton!
    @IBOutlet weak var lblfeed: UILabel!
    @IBOutlet weak var lblcreate: UILabel!
    @IBOutlet weak var lblasignment: UILabel!
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet var searchbar: UISearchBar!
    private let refreshControl = UIRefreshControl()

    @IBOutlet weak var btnfilter: UIButton!
    @IBOutlet weak var btnsearch: UIButton!
    var Arrtopic: [Any] = []
    var ArrMytopic: [Any] = []

    let appdelegate = UIApplication.shared.delegate as! AppDelegate

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Arrtopic.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MenifestCell
        let dict = Arrtopic[indexPath.row] as? [String: Any]
        cell.lbltitle.text = dict?["title"] as? String
        if String(format: "%@", dict?["location"] as? String ?? "") == "<null>" {
            cell.lbllocation.text = ""
        } else {
            cell.lbllocation.text = dict?["location"] as? String
        }
        cell.btnfollow.tag = indexPath.row
        cell.btnrequest.tag = indexPath.row


        if dict?["isRequested"] as! Bool == true {
            cell.btnrequest.setTitle("Requested", for: .normal)
            cell.btnrequest.backgroundColor = UIColor.init(named: "purple")
            cell.btnrequest.setTitleColor(UIColor.init(named: "textfieldbackground"), for: .normal)
        } else {
            cell.btnrequest.setTitle("Request", for: .normal)
            cell.btnrequest.backgroundColor = UIColor.init(named: "textfieldbackground")
            if #available(iOS 13.0, *) {
                cell.btnrequest.setTitleColor(UIColor.label, for: .normal)
            } else {
                cell.btnrequest.setTitleColor(UIColor.black, for: .normal)
            }
        }

        if dict?["isFollowed"] as! Bool == true {
            cell.btnfollow.setTitle("Followed", for: .normal)
            cell.btnfollow.backgroundColor = UIColor.init(named: "purple")
            cell.btnfollow.setTitleColor(UIColor.init(named: "textfieldbackground"), for: .normal)
        } else {
            cell.btnfollow.setTitle("Follow", for: .normal)
            cell.btnfollow.backgroundColor = UIColor.init(named: "textfieldbackground")
            if #available(iOS 13.0, *) {
                cell.btnfollow.setTitleColor(UIColor.label, for: .normal)
            } else {
                cell.btnfollow.setTitleColor(UIColor.black, for: .normal)
            }
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if IS_IPHONE {
            return 130
        } else {
            return 160
        }

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = Arrtopic[indexPath.row] as? [String: Any]

        appdelegate.OrgID = String(format: "%@", dict?["org_id"] as! NSNumber)

        var story: UIStoryboard?

        if IS_IPHONE {
            story = UIStoryboard.init(name: "Manifest", bundle: nil)
        } else {
            story = UIStoryboard.init(name: "ManifestIPAD", bundle: nil)
        }

        let TopicDetail = story?.instantiateViewController(withIdentifier: "TopicDetail") as! TopicDetail
        TopicDetail.dictback = dict
        TopicDetail.topicid = String(format: "%@", dict?["id"] as! NSNumber)

        self.navigationController?.pushViewController(TopicDetail, animated: true)
    }

    var count = 0
    var flagcheck = 0
    var request = 0
    var follow = 0
    var Arrnetwok: [Any]?
    var indexcheck = 0
    var asignmentarray: [Any]?
    var feedarray: [Any]?
    var flagtable = 0
    var arrorgname: [Any]?
    var arrorgid: [Any]?
    var arrtags: [Any]?
    var ArrNetwork_name: [String] = []
    var ArrNetwork_id: [String] = []
    var refrescontroller: UIRefreshControl?

    override func viewDidLoad() {
        super.viewDidLoad()


        searchbar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        searchbar.showsCancelButton = true
        searchbar.delegate = self
        if #available(iOS 13.0, *) {
            searchbar.tintColor = UIColor.label
        } else {
            searchbar.tintColor = UIColor.black
        }
        // Do any additional setup after loading the view.
        btnfeed.setImage(UIImage(named: "list"), for: .normal)
        btnasignment.setImage(UIImage(named: "menifest123"), for: .normal)
        lblfeed.textColor = UIColor.init(named: "purple")
        flagtable = 200
        self.Servicegettopic()
        webview.isHidden = true
        self.servicegettags()


        let network = UserDefaults.standard.value(forKey: "AvailableNetworks") as? [Any] ?? []
        print(network)
        let defaultnetwork = UserDefaults.standard.value(forKey: "defultnetwork") as! String
        var dicttop: [String: Any] = [:]
        for dict in network {
            print(dict)
            let dict1 = dict as? [String: Any]
            let ArrPermission = dict1!["permissions"] as? [String]
            if ArrPermission!.contains("write") || ArrPermission!.contains("move_copy") {

                let networkid = dict1!["company_id"] as! String
                if networkid != defaultnetwork {
                    ArrNetwork_name.append((dict1?["company_name"] as? String)!)
                    ArrNetwork_id.append((dict1?["company_id"] as? String)!)
                } else {
                    dicttop = dict1!
                }

            }

        }

        ArrNetwork_name.insert("\((dicttop["company_name"] as! String)) (Default)", at: 0)
        ArrNetwork_id.insert("\((dicttop["company_id"] as! String))", at: 0)

        appdelegate.arrrequest = ArrNetwork_id

        
        if #available(iOS 10.0, *) {
                  tblmenifest.refreshControl = refreshControl
              } else {
                  tblmenifest.addSubview(refreshControl)
              }
              refreshControl.tintColor = UIColor.init(named: "colordark")
              refreshControl.attributedTitle = NSAttributedString(string: "loading...")
              refreshControl.addTarget(self, action: #selector(refreshlist(_:)), for: .valueChanged)

        
        
        
    }
    @objc private func refreshlist(_ sender: Any) {
        if flagtable == 200 {
                  self.Servicegettopic()
              } else {
                  self.Servicegetmytopic()

              }
    
    }
    
    
    
    

    @IBAction func btnlist(_ sender: UIButton) {
        self.colorchange()

        btnfeed.setImage(UIImage(named: "list"), for: .normal)
        lblfeed.textColor = UIColor.init(named: "purple")
        webview.isHidden = true
        flagtable = 200

        self.Servicegettopic()

    }

    @IBAction func btncreate(_ sender: UIButton) {
        self.colorchange()
        webview.isHidden = false
        btncreate.setImage(UIImage(named: "fillcreate"), for: .normal)
        lblcreate.textColor = UIColor.init(named: "purple")
        btnfilter.isHidden = true
        btnsearch.isHidden = true

        appdelegate.ShowProgress()

        let heder = appdelegate.getAPIEncodedHeader(withParameters: "")


        let timeZone = NSTimeZone.local as NSTimeZone
        let timezoname = timeZone.name
        print("\(timezoname)")

        var targetURL: URL? = nil
        let object = UserDefaults.standard.value(forKey: "userId") as! String

        targetURL = URL(string: "https://manifest.latakoo.com/api/remote/index.php?ownerid=\(object)&assignmentid=&mode=add&authtoken=\(heder ?? "")&TZ=\(timezoname)")
        
        print("https://manifest.latakoo.com/api/remote/index.php?ownerid=\(object)&assignmentid=&mode=add&authtoken=\(heder ?? "")&TZ=\(timezoname)")

        var request: URLRequest? = nil
        if let targetURL = targetURL {
            request = URLRequest(url: targetURL)
        }
        webview.scalesPageToFit = true
        if let request = request {
            webview.loadRequest(request)
        }

    }

    @IBAction func btnasignment(_ sender: UIButton) {

        self.colorchange()
        webview.isHidden = true
        btnasignment.setImage(UIImage(named: "fill_assignment"), for: .normal)
        lblasignment.textColor = UIColor.init(named: "purple")
        flagtable = 300
        self.Servicegetmytopic()
    }

    @IBAction func btnrequest(_ sender: UIButton) {

        appdelegate.arrrequestids = []
        let dict = Arrtopic[sender.tag] as? [String: Any]
        let assID = String(format: "%@", dict?["id"] as! NSNumber)

        if dict?["isRequested"] as! Bool == true {
            let appearance = SCLAlertView.SCLAppearance(
                kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                showCloseButton: false,
                dynamicAnimatorActive: true,
                buttonsLayout: .horizontal
            )

            let alert = SCLAlertView(appearance: appearance)
            _ = alert.addButton("OK") {

            }
            _ = alert.showSuccess("Alert", subTitle: "you have already requested for this asignment")

            return
        } else {


            let options = YActionSheet(title: "Request Copy to..", dismissButtonTitle: "Cancel", otherButtonTitles: ArrNetwork_name, dismissOnSelect: true, ismenifest: true)

            options!.show(in: self, withYActionSheetBlock: { buttonIndex, isCancel in
                if isCancel {
                } else {



                    let appearance = SCLAlertView.SCLAppearance(
                        kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                        kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                        kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                        showCloseButton: false,
                        dynamicAnimatorActive: true,
                        buttonsLayout: .horizontal
                    )
                    let alert = SCLAlertView(appearance: appearance)
                    _ = alert.addButton("Cancel") {
                    }
                    _ = alert.addButton("Yes") {

                        self.servicerequest(strapi: "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)assignments/\(assID)/request")
                    }

                    let icon = UIImage(named: "copywhite.png")
                    let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
                    
                    let stringRepresentation = self.appdelegate.arrrnames.joined(separator:",") // "1-2-3"
                          print(stringRepresentation)

                    _ = alert.showCustom("Alert", subTitle: "Are you sure you want to copy this file to \(stringRepresentation) ?", color: color, circleIconImage: icon!)

                }

            })
        }

    }

    @IBAction func btnsearch(_ sender: UIButton) {
        tblmenifest.tableHeaderView = searchbar
        tblmenifest.contentOffset = CGPoint.zero
    }

    @IBAction func btnfilter(_ sender: UIButton) {

        var story: UIStoryboard?

        if IS_IPHONE {
            story = UIStoryboard.init(name: "Manifest", bundle: nil)
        } else {
            story = UIStoryboard.init(name: "ManifestIPAD", bundle: nil)
        }

        let FilterMenifestVC = story?.instantiateViewController(withIdentifier: "FilterMenifestVC") as! FilterMenifestVC
        FilterMenifestVC.delegate = self
        self.navigationController?.pushViewController(FilterMenifestVC, animated: true)

    }

    @IBAction func pilot(_ sender: Any) {

        let str = UserDefaults.standard.value(forKey: "isshowmenifest") as? String
        if (str == "YES") {

            let options = YActionSheet(title1: "", dismissButtonTitle: "", otherButtonTitles: ["Pilot", "Manifest"], dismissOnSelect: true, ispilot: true)
            options!.show(in: self, withYActionSheetBlock: { buttonIndex, isCancel in
                if buttonIndex == 0 {
                    self.navigationController?.popViewController(animated: false)
                }
            })
        }
    }

    @IBAction func btnfollow(_ sender: UIButton) {
        let dict = Arrtopic[sender.tag] as? [String: Any]

        let assID = String(format: "%@", dict?["id"] as! NSNumber)
        if dict?["isFollowed"] as! Bool {
            self.ServiceFollowUnfollowAssignment(strapi: "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)assignments/\(assID)/unfollow")
        } else {
            self.ServiceFollowUnfollowAssignment(strapi: "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)assignments/\(assID)/follow")
        }
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if (flagtable == 300) {
            var urlString = String(format: "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)assignments/feed?my=1&search=%%%@%%", searchBar.text ?? "")
            urlString = (urlString as NSString).addingPercentEscapes(using: String.Encoding.utf8.rawValue) ?? ""


            self.search(strapi: urlString)
        } else {
            var urlString = String(format: "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)assignments/feed?search=%%%@%%", searchBar.text ?? "")
            urlString = (urlString as NSString).addingPercentEscapes(using: String.Encoding.utf8.rawValue) ?? ""
            self.search(strapi: urlString)
        }

    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.tblmenifest.tableHeaderView = nil
    }

    func colorchange() {
        btnfeed.setImage(UIImage(named: "blanklist"), for: .normal)
        btnasignment.setImage(UIImage(named: "menifest123"), for: .normal)
        btncreate.setImage(UIImage(named: "unfillcreate"), for: .normal)
        lblfeed.textColor = UIColor.darkGray
        lblasignment.textColor = UIColor.darkGray
        lblcreate.textColor = UIColor.darkGray
        btnfilter.isHidden = false
        btnsearch.isHidden = false
    }

    func Servicegettopic() {
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }

        appdelegate.ShowProgress()

        let headers = ["Authorization":
                "Bearer \(UserDefaults.standard.value(forKey: "Authtoken") ?? "")"]
        var aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)assignments/feed"

        if UserDefaults.standard.value(forKey: "ArrTags") != nil {
            let arr = UserDefaults.standard.object(forKey: "ArrTags") as? [AnyHashable]

            var tags = ""


            for i in 0..<(arr?.count ?? 0) {

                if (((arr?[i] as? [String: String])?["flag"]) == "On") {

                    if let object = (arr?[i] as? [AnyHashable: Any])?["Name"] {
                        tags += "\(object),"
                    }
                }
            }

            if tags.count != 0 {
                let str = tags.dropLast()
                aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)assignments/feed?tags=\(str)"
            }
        }

        aStrApi = (aStrApi as NSString).addingPercentEscapes(using: String.Encoding.utf8.rawValue) ?? ""



        print(aStrApi)
        print(headers)

        let encoding = URLEncoding.httpBody
        Alamofire.request(aStrApi, method: .get, parameters: nil, encoding: encoding, headers: headers).responseJSON { (responseObject) -> Void in

            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                let usrDetail = resJson
                print(usrDetail)
                if usrDetail["result"].exists() {
                    self.Arrtopic = []
                    self.tblmenifest.reloadData()
                    self.appdelegate.HideProgress()
                    self.refreshControl.endRefreshing()

                    self.Arrtopic = usrDetail["result"].rawValue as! [Any]
                    if self.Arrtopic.count > 0 {

                        self.viewalert.isHidden = true
                        self.tblmenifest.reloadData(
                            with: .simple(duration: 1.5, direction: .rotation3D(type: .deadpool),
                                constantDelay: 0))
                    } else {
                        self.viewalert.isHidden = false
                        self.Arrtopic = []
                        self.tblmenifest.reloadData()
                        self.lblalert.text = "There are no recent Assignments"

                    }
                } else {

                    self.refreshToken()

                }

            }
            if responseObject.result.isFailure {
                self.appdelegate.HideProgress()
                self.refreshControl.endRefreshing()


            }


        }





    }

    func Servicegetmytopic() {
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }

        appdelegate.ShowProgress()

        let headers = ["Authorization":
                "Bearer \(UserDefaults.standard.value(forKey: "Authtoken") ?? "")"]
        var aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)assignments/feed?my=1"



        if UserDefaults.standard.value(forKey: "ArrTags") != nil {
            let arr = UserDefaults.standard.object(forKey: "ArrTags") as? [AnyHashable]

            var tags = ""


            for i in 0..<(arr?.count ?? 0) {

                if (((arr?[i] as? [String: String])?["flag"]) == "On") {

                    if let object = (arr?[i] as? [AnyHashable: Any])?["Name"] {
                        tags += "\(object),"
                    }
                }
            }

            if tags.count != 0 {
                let str = tags.dropLast()
                aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)assignments/feed?my=1&tags=\(str)"
            }
        }

        aStrApi = (aStrApi as NSString).addingPercentEscapes(using: String.Encoding.utf8.rawValue) ?? ""
        print(aStrApi)
        print(headers)

        let encoding = URLEncoding.httpBody
        Alamofire.request(aStrApi, method: .get, parameters: nil, encoding: encoding, headers: headers).responseJSON { (responseObject) -> Void in

            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                let usrDetail = resJson
                print(usrDetail)
                if usrDetail["result"].exists() {
                    self.appdelegate.HideProgress()
                    self.refreshControl.endRefreshing()
                    
                    self.Arrtopic = usrDetail["result"].rawValue as! [Any]

                    if self.Arrtopic.count > 0 {
                        self.Arrtopic = []
                        self.tblmenifest.reloadData()
                        self.viewalert.isHidden = true
                        self.tblmenifest.reloadData(
                            with: .simple(duration: 1.5, direction: .rotation3D(type: .deadpool),
                                constantDelay: 0))
                    } else {
                        self.Arrtopic = []
                        self.tblmenifest.reloadData()
                        self.viewalert.isHidden = false
                        self.lblalert.text = "You have not been assigned and are not following any recent Assignments"
                    }


                }else{
                    
                }

            }
            if responseObject.result.isFailure {
                self.appdelegate.HideProgress()
                self.refreshControl.endRefreshing()


            }


        }





    }

    func ServiceFollowUnfollowAssignment(strapi: String) {
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }

        appdelegate.ShowProgress()
        let headers = ["Authorization":
                "Bearer \(UserDefaults.standard.value(forKey: "Authtoken") ?? "")"]
        let aStrApi = strapi

        print(aStrApi)
        print(headers)

        let encoding = URLEncoding.httpBody
        Alamofire.request(aStrApi, method: .post, parameters: nil, encoding: encoding, headers: headers).responseJSON { (responseObject) -> Void in

            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                let usrDetail = resJson
                print(usrDetail)
                self.appdelegate.HideProgress()
                if self.flagtable == 200 {
                    self.Servicegettopic()
                } else {
                    self.Servicegetmytopic()
                }
            }
            if responseObject.result.isFailure {
                self.appdelegate.HideProgress()
            }
        }
    }

    func servicerequest(strapi: String) {
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }


        appdelegate.ShowProgress()
        let headers = ["Authorization":
                "Bearer \(UserDefaults.standard.value(forKey: "Authtoken") ?? "")"]
        let aStrApi = strapi

//        let paramsJSON = JSON(appdelegate.arrrequestids)
//        let paramsString = paramsJSON.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)

        // print(paramsString!)
        
     


        let param = ["network_ids": appdelegate.arrrequestids]
        print(aStrApi)
        print(headers)
        print(param)

        let encoding = Alamofire.JSONEncoding.default
        Alamofire.request(aStrApi, method: .post, parameters: param as Parameters, encoding: encoding, headers: headers).responseJSON { (responseObject) -> Void in

            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                let usrDetail = resJson
                print(usrDetail)
                self.appdelegate.HideProgress()
                if self.flagtable == 200 {
                    self.Servicegettopic()
                } else {
                    self.Servicegetmytopic()
                }
            }
            if responseObject.result.isFailure {
                self.appdelegate.HideProgress()


            }

        }


    }

    func search(strapi: String) {
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }

        appdelegate.ShowProgress()
        let headers = ["Authorization":
                "Bearer \(UserDefaults.standard.value(forKey: "Authtoken") ?? "")"]
        let aStrApi = strapi

        print(aStrApi)
        print(headers)

        let encoding = URLEncoding.httpBody
        Alamofire.request(aStrApi, method: .get, parameters: nil, encoding: encoding, headers: headers).responseJSON { (responseObject) -> Void in

            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                let usrDetail = resJson
                print(usrDetail)
                if usrDetail["result"].exists() {
                    self.appdelegate.HideProgress()

                    self.Arrtopic = usrDetail["result"].rawValue as! [Any]
                    if self.Arrtopic.count > 0 {
                        self.viewalert.isHidden = true
                        self.tblmenifest.reloadData(
                            with: .simple(duration: 1.5, direction: .rotation3D(type: .deadpool),
                                constantDelay: 0))
                    } else {

                        self.Arrtopic = []
                        self.tblmenifest.reloadData()
                        self.viewalert.isHidden = false
                        self.lblalert.text = "There are no Assignments"

                    }
                }

            }
            if responseObject.result.isFailure {
                self.appdelegate.HideProgress()


            }

        }


    }

    func servicegettags() {
                            if !Reachability.isConnectedToNetwork() {

            return
        }


        let headers = ["Authorization":
                "Bearer \(UserDefaults.standard.value(forKey: "Authtoken") ?? "")"]
        let aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)assignments/tags"
        print(aStrApi)
        print(headers)

        let encoding = URLEncoding.httpBody
        Alamofire.request(aStrApi, method: .get, parameters: nil, encoding: encoding, headers: headers).responseJSON { (responseObject) -> Void in

            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                let usrDetail = resJson
                print(usrDetail)
                if usrDetail["result"].exists() {
                    let dict = usrDetail["result"].rawValue as! [String: Any]
                    let Arrtags = dict["tags"] as? [String]
                    print(Arrtags!)
                    var arr: [Any] = []
                    for i in 0..<Arrtags!.count {
                        var Dict: [String: String] = [:]
                        Dict["Name"] = Arrtags?[i]
                        Dict["flag"] = "Off"
                        arr.append(Dict)
                    }

                    self.arrtags = arr
                    print(arr)
                    UserDefaults.standard.set(self.arrtags, forKey: "ArrTags")



                }

            }


            if responseObject.result.isFailure {


            }

        }
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        appdelegate.HideProgress()

    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {

    }

    func refreshToken() {

        let urlString = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)auth/refresh-token"
        let request = NSMutableURLRequest()
        request.url = URL(string: urlString)
        request.httpMethod = "PUT"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        var Parameter: [AnyHashable: Any] = [:]
        Parameter["token"] = UserDefaults.standard.object(forKey: "Authtoken")
        Parameter["refreshToken"] = UserDefaults.standard.object(forKey: "RefreshToken")
        var myData: Data? = nil
        do {
            myData = try JSONSerialization.data(withJSONObject: Parameter, options: .prettyPrinted)
        } catch {
        }
        var putData = Data()
        if let myData = myData {
            putData.append(Data(myData))
        }
        request.httpBody = putData
        var response: URLResponse?

        var responseData: Data? = nil
        do {
            responseData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
        } catch let err {
            print(err)
        }

//        var resSrt: String? = nil
//        if let responseData = responseData {
//            resSrt = String(data: responseData, encoding: .ascii)
//        }

        do {
            let json = try JSONSerialization.jsonObject(with: responseData!, options: []) as? [String: Any]
            print(json!)
            if (json?["result"]) != nil {

                let dict = json!["result"] as? [String: Any]
                let AuthToken = dict!["token"] as? String
                UserDefaults.standard.setValue(AuthToken, forKey: "Authtoken")
                UserDefaults.standard.synchronize()
                self.Servicegettopic()
                self.servicegettags()
            } else {
                appdelegate.HideProgress()
            }



        } catch {
            print(error)

        }


//        let token = (JSONresponse?["result"] as? NSObject)?.value(forKey: "token") as? String
//        if (JSONresponse?["result"] as? NSObject)?.value(forKey: "token") != nil {
//
//            UserDefaults.standard.set("\(token ?? "")", forKey: "Authtoken")
//            UserDefaults.standard.synchronize()
//        }
    }
}


