//
//  SendAssignment.swift
//  Latakoo
//
//  Created by Mayur Sardana on 02/07/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

protocol SendAssignmentDelegate : class {
    func fromsend(_ controller: SendAssignment)
}

class SendAssignment: UIViewController,UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return assignmentarray.count
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = (tableView.dequeueReusableCell(withIdentifier: "Cell") as? EmailCell)!
        let dict = assignmentarray[indexPath.row] as? [String:Any]
        cell.lblname.text = dict!["title"] as? String
        if indexPath.row == indexcheck {
            cell.lblcheck.isHidden = false
        }else{
            cell.lblcheck.isHidden = true

        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == indexcheck {
            indexcheck = -1
               }else{
         indexcheck = indexPath.row
        let dict = assignmentarray[indexPath.row] as? [String:Any]
        let asignmentid = String(format:"%@",dict?["id"] as! NSNumber )
        appdelegate.AssignmentID = asignmentid
        appdelegate.TopicID = String(format:"%@",dict?["topicId"] as! NSNumber )
       appdelegate.Assignmentname = dict?["title"] as? String ?? ""
        appdelegate.OrgID = String(format:"%@",dict?["orgId"] as! NSNumber )
        }
        tableView.reloadData()
    
    }
    

    @IBOutlet weak var txttitle: UITextField!
    @IBOutlet weak var tableView: UITableView!
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    var assignmentarray : [Any] = []
    var indexcheck : Int = -1
    weak var delegate: SendAssignmentDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.servicegetassignment()

    }
    
    @IBAction func btnback(_ sender: UIButton) {

        self.navigationController?.popViewController(animated: true)
               if indexcheck == -1{
                  appdelegate.Assignmentname = txttitle.text!
                  appdelegate.AssignmentID = ""
                  appdelegate.TopicID = ""
                  appdelegate.OrgID = ""
               }
        delegate?.fromsend(self)
        
    }
    
    
    func servicegetassignment(){
                              if !Reachability.isConnectedToNetwork() {
          print("Internet is Not available.")
          AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
              return
          }
              
              appdelegate.ShowProgress()
                          
              let headers = ["Authorization" :
                          "Bearer \(UserDefaults.standard.value(forKey:"Authtoken") ?? "")"]
                          var aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)assignments"
              
              
                          aStrApi = (aStrApi as NSString).addingPercentEscapes(using: String.Encoding.utf8.rawValue) ?? ""

              
              
                          print(aStrApi)
                          print(headers)

                                  let encoding = URLEncoding.httpBody
                                         Alamofire.request(aStrApi, method: .get, parameters:nil, encoding:encoding, headers: headers).responseJSON { (responseObject) -> Void in
                                                                 
                                                   if responseObject.result.isSuccess {
                                              let resJson = JSON(responseObject.result.value!)
                                              let usrDetail = resJson
                                              print(usrDetail)
                                              if usrDetail["result"].exists() {
                                                  self.appdelegate.HideProgress()
                                                self.assignmentarray = usrDetail["result"].rawValue as! [Any]
                                                 self.tableView.reloadData()
                                                 
                                              }else{
                                            self.appdelegate.HideProgress()
                                                
                                                    }

                                          }
                                          if responseObject.result.isFailure {
                                              self.appdelegate.HideProgress()

                                              
                                          }
                                                           
              
                         }
                                  

                          
                         
              
          }
    
    
    
     func refreshToken() {

               let urlString = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)auth/refresh-token"
               let request = NSMutableURLRequest()
               request.url = URL(string: urlString)
               request.httpMethod = "PUT"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue("application/json", forHTTPHeaderField: "Accept")
               var Parameter: [AnyHashable : Any] = [:]
               Parameter["token"] = UserDefaults.standard.object(forKey: "Authtoken")
               Parameter["refreshToken"] = UserDefaults.standard.object(forKey: "RefreshToken")
               var myData: Data? = nil
               do {
                   myData = try JSONSerialization.data(withJSONObject: Parameter, options: .prettyPrinted)
               } catch {
               }
               var putData = Data()
               if let myData = myData {
                   putData.append(Data(myData))
               }
               request.httpBody = putData
               var response: URLResponse?

               var responseData: Data? = nil
               do {
                   responseData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
               } catch let err {
                   print(err)
               }

    
               
               do{
                   let json = try JSONSerialization.jsonObject(with: responseData!, options: []) as? [String : Any]
                   print(json!)
                   if (json?["result"]) != nil {
                       
                       let dict = json!["result"] as? [String:Any]
                       let AuthToken = dict!["token"] as? String
                       UserDefaults.standard.setValue(AuthToken, forKey: "Authtoken")
                       UserDefaults.standard.synchronize()
                       self.servicegetassignment()
                   }else{
                       appdelegate.HideProgress()
                   }

                   
                 
               }catch{
                   print(error)
                   
               }
               
                
     
           }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
