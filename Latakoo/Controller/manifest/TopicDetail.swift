//
//  TopicDetail.swift
//  Latakoo
//
//  Created by Mayur Sardana on 02/07/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//

import UIKit
import AFNetworking
import SwiftyJSON
import Alamofire
import TableViewReloadAnimation

class topicdetailcell: UITableViewCell {
     @IBOutlet weak var lbltitle: UILabel!
     @IBOutlet weak var lbllocation: UILabel!
     @IBOutlet weak var subtitle: UILabel!
     @IBOutlet weak var btnrequest: UIButton!
     @IBOutlet weak var imgnext: UIImageView!
    
    override func awakeFromNib() {
           super.awakeFromNib()
          
       }
    
}


class TopicDetail: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return Arrfields.count
        } else {
            return asignmentarray.count
        }
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

           if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as? topicdetailcell
            let dict = Arrfields[indexPath.row] as? [String:Any]
            cell?.lbltitle.text = dict!["label"] as? String
            if String(format:"%@",dict?["value"] as? String ?? "") == "<null>" {
                cell?.lbllocation.text = ""
            }else{
                cell?.lbllocation.text = dict!["value"] as? String

            }
            return cell!
           } else {

               let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? topicdetailcell
               let dict = asignmentarray[indexPath.row] as? [String:Any]
            cell!.lbltitle.text = dict?["title"] as? String
                      if String(format:"%@",dict?["location"] as? String ?? "") == "<null>" {
                        cell!.lbllocation.text = ""
                      }else{
                        cell!.lbllocation.text = dict?["location"] as? String
                      }

                    cell?.btnrequest.tag = indexPath.row
                    cell!.btnrequest.layer.borderColor = UIColor.lightGray.cgColor
                    cell!.btnrequest.layer.borderWidth = 1
                    cell!.btnrequest.layer.cornerRadius = 10
                   if dict?["isRequested"] as! Bool  == true {
                     cell!.btnrequest.setTitle("Requested", for: .normal)

                      cell!.btnrequest.backgroundColor = UIColor.init(named:"purple")
                      cell!.btnrequest.setTitleColor(UIColor.init(named: "textfieldbackground"), for: .normal)
                          }else{
                       cell!.btnrequest.setTitle("Request", for: .normal)
                       cell!.btnrequest.backgroundColor = UIColor.init(named:"textfieldbackground")
                       if #available(iOS 13.0, *) {
                         cell!.btnrequest.setTitleColor(UIColor.label, for: .normal)
                          } else {
                          cell!.btnrequest.setTitleColor(UIColor.black, for: .normal)
                            }
                          }

             
            return cell!
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {


           let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 70))
            view.backgroundColor = UIColor(named: "tabbackground")
        
          
           if section == 0 {
            
            let lbl = UILabel(frame: CGRect(x: 18, y: 9, width: self.view.bounds.size.width - 90, height: 48))
            lbl.font = UIFont(name: "Arial-BoldMT", size: 20)

            if #available(iOS 13.0, *) {
                lbl.textColor = UIColor.label
            } else {
                lbl.textColor = UIColor.black
            }
            view.addSubview(lbl)
            lbl.numberOfLines = 2

            if dictback != nil {
            
            lbl.text = dictback?["title"] as? String
            lbl.lineBreakMode = .byWordWrapping
                let lbl1 = UILabel(frame: CGRect(x: 18, y: 58, width: 80, height: 2))
                lbl1.backgroundColor = UIColor(named: "purple")
            view.addSubview(lbl1)
            if dictback?["isHomeOrg"] as? Bool == true {
                   let but = UIButton(type: .roundedRect)
                   but.addTarget(self, action: #selector(buttonClicked(_:)), for: .touchUpInside)
                   but.frame = CGRect(x: self.view.frame.size.width - 60, y: 8, width: 30, height: 30)
                   but.setImage(UIImage(named: "pencil"), for: .normal)
                   if #available(iOS 13.0, *) {
                       but.tintColor = UIColor.label
                   } else {
                       but.tintColor = UIColor.black
                   }

                   view.addSubview(but)
                view.bringSubview(toFront: but)
               }
            }
            
           } else {
            
            

            let lbl = UILabel(frame: CGRect(x: 18, y: 9, width: self.view.bounds.size.width - 90, height: 25))
            lbl.font = UIFont(name: "Arial-BoldMT", size: 20)

            if #available(iOS 13.0, *) {
                lbl.textColor = UIColor.label
            } else {
                lbl.textColor = UIColor.black
            }
            view.addSubview(lbl)
            lbl.numberOfLines = 1
            lbl.text = "Assignments"
            let lbl1 = UILabel(frame: CGRect(x: 18, y: 35, width: 80, height: 2))

            lbl1.backgroundColor = UIColor(named: "purple")
            view.addSubview(lbl1)
           }

         

           return view
           }
    
    
    @objc func buttonClicked(_ sender: UIButton?) {

    let heder =  appdelegate.getAPIEncodedHeader(withParameters: "")
         let object  = UserDefaults.standard.value(forKey:"userId") as! String
           let timeZone = NSTimeZone.local as NSTimeZone
           let timezoname = timeZone.name
           print("\(timezoname)")
        
        var story : UIStoryboard?
                                             
                                        if IS_IPHONE {
                                             story = UIStoryboard.init(name: "Manifest", bundle: nil)
                                            }else{
                                             story = UIStoryboard.init(name: "ManifestIPAD", bundle: nil)
                                        }
        
        
                  let AssignmentWebVC = story?.instantiateViewController(withIdentifier: "AssignmentWebVC") as! AssignmentWebVC
        AssignmentWebVC.weburl = "https://manifest.latakoo.com/api/remote/index.php?ownerid=\(object)&assignmentid=&topic_id=\(topicid ?? "")&mode=edittopic&authtoken=\(heder ?? "")&TZ=\(timezoname)"
        
            print("https://manifest.latakoo.com/api/remote/index.php?ownerid=\(object)&assignmentid=&topic_id=\(topicid ?? "")&mode=edittopic&authtoken=\(heder ?? "")&TZ=\(timezoname)")
        
                  AssignmentWebVC.isfeed = "EDIT"
                  self.navigationController?.pushViewController(AssignmentWebVC, animated: true)
        
       }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

           if indexPath.section == 0 {
               var str: String? = nil
            let dict = Arrfields[indexPath.item] as? [String:Any]
            str = dict?["value"] as? String
           
            let font = UIFont(name: "Helvetica", size: 17)
            let width = view.frame.size.width
            let height = heightForView(text: str ?? "", font: font!, width: width)

             return height + 70

            
           } else {

            if IS_IPHONE{
                       return 130
                   }else{
                       return 160
                   }
           }



       }

       func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        if section == 0 {
            return 70

        }else{
            return 45

        }

       }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
           return 2
       }

      
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        if indexPath.section == 1 {
           
            let dict = asignmentarray[indexPath.row] as? [String:Any]
            let asignmentid = String(format:"%@",dict?["id"] as! NSNumber )
            appdelegate.AssignmentID = asignmentid
            appdelegate.TopicID = topicid!
            appdelegate.Assignmentname = dict?["title"] as? String ?? ""
            
            
            var story : UIStoryboard?
                                                 
                                            if IS_IPHONE {
                                                 story = UIStoryboard.init(name: "Manifest", bundle: nil)
                                                }else{
                                                 story = UIStoryboard.init(name: "ManifestIPAD", bundle: nil)
                                            }
            
            let AssignmentWebVC = story?.instantiateViewController(withIdentifier: "AssignmentWebVC") as! AssignmentWebVC
            let object  = UserDefaults.standard.value(forKey:"userId") as! String
            AssignmentWebVC.weburl = "https://manifest.latakoo.com/api/remote/?ownerid=\(object)&assignmentid=\(asignmentid )"
            
            print("https://manifest.latakoo.com/api/remote/?ownerid=\(object)&assignmentid=\(asignmentid )")
            AssignmentWebVC.isfeed = "Detail"
            self.navigationController?.pushViewController(AssignmentWebVC, animated: true)
                }
           }
        
    

    
       @IBOutlet weak var tblmenifest: UITableView!
        var topicid: String?
        var isfeed: String?
        var dictback: [String : Any]?
        var ArrNetwork_name: [String] = []
        var ArrNetwork_id: [String] = []
        var Arrfields: [Any] = []

        var refrescontroller: UIRefreshControl?
        @IBOutlet var searchbar: UISearchBar!
        var asignmentarray : [Any] = []
    let appdelegate = UIApplication.shared.delegate as! AppDelegate


    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchbar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
             searchbar.showsCancelButton = true
                     searchbar.delegate = self
                    if #available(iOS 13.0, *) {
                        searchbar.tintColor = UIColor.label
                    } else {
                        searchbar.tintColor = UIColor.black
                    }
             // Do any additional setup after loading the view.
            
//        Arrfields = dictback?["fields"] as! [Any]
//
//         print(dictback!)
//         print(Arrfields)
        
         // tblmenifest.reloadData()
        self.serviceGetAssignment()
       // self.Servicegettopic()
             
               let network = UserDefaults.standard.value(forKey: "AvailableNetworks") as? [Any] ?? []
                   print(network)
                   let   defaultnetwork =  UserDefaults.standard.value(forKey:"defultnetwork") as! String
             var dicttop : [String:Any] = [:]
                   for dict in network {
                       print(dict)
                       let dict1 = dict as? [String:Any]
                       let ArrPermission = dict1!["permissions"] as? [String]
                       if ArrPermission!.contains("write") || ArrPermission!.contains("move_copy") {
                         
                         let networkid = dict1!["company_id"] as! String
                                    if networkid !=  defaultnetwork{
                                   ArrNetwork_name.append((dict1?["company_name"] as? String)!)
                                  ArrNetwork_id.append((dict1?["company_id"] as? String)!)
                                    }else{
                                     dicttop = dict1!
                                   }
                         
                                    }
                       
                                }
             
             ArrNetwork_name.insert("\((dicttop["company_name"] as! String)) (Default)", at: 0)
             ArrNetwork_id.insert("\((dicttop["company_id"] as! String))", at: 0)

             appdelegate.arrrequest = ArrNetwork_id

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
         self.Servicegettopic()

    }
    
    @IBAction func btnlist(_ sender: UIButton) {

        self.navigationController?.popViewController(animated: true)
     
    }
    
    @IBAction func btnrequest(_ sender: UIButton) {

           appdelegate.arrrequestids = []
           let dict = asignmentarray[sender.tag] as? [String:Any]
           let assID = String(format:"%@",dict?["id"] as! NSNumber )

              if dict?["isRequested"] as! Bool == true{
                   let appearance = SCLAlertView.SCLAppearance(
                                                                 kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                                                                 kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                                                                 kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                                                                 showCloseButton: false,
                                                                 dynamicAnimatorActive: true,
                                                                 buttonsLayout: .horizontal
                                       )

                                       let alert = SCLAlertView(appearance: appearance)
                                       _ = alert.addButton("OK"){
                                           
                                       }
                                       _ = alert.showSuccess("Alert", subTitle:"you have already requested for this asignment")
                   
                     return
                 } else {

                   
                     let options = YActionSheet(title: "Request Copy to..", dismissButtonTitle: "Cancel", otherButtonTitles: ArrNetwork_name, dismissOnSelect: true, ismenifest: true)

                     options!.show(in: self, withYActionSheetBlock: { buttonIndex, isCancel in
                         if isCancel {
                         } else {



                           let appearance = SCLAlertView.SCLAppearance(
                                    kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                                    kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                                    kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                                    showCloseButton: false,
                                    dynamicAnimatorActive: true,
                                    buttonsLayout: .horizontal
                                )
                                let alert = SCLAlertView(appearance: appearance)
                                _ = alert.addButton("Cancel"){
                                }
                                _ = alert.addButton("Yes") {

                                   self.servicerequest(strapi: "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)assignments/\(assID )/request")
                           }
                                
                                let icon = UIImage(named:"copywhite.png")
                                let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
                                
                               let stringRepresentation = self.appdelegate.arrrnames.joined(separator:",") // "1-2-3"
                                                       print(stringRepresentation)

                                                 _ = alert.showCustom("Alert", subTitle: "Are you sure you want to copy this file to \(stringRepresentation) ?", color: color, circleIconImage: icon!)
                           
                         }

                     })
                 }

             }

   
    func Servicegettopic(){
                           if !Reachability.isConnectedToNetwork() {
       print("Internet is Not available.")
       AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
           return
       }
           
           appdelegate.ShowProgress()
                       
           let headers = ["Authorization" :
                       "Bearer \(UserDefaults.standard.value(forKey:"Authtoken") ?? "")"]
                       var aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)assignments/feed"
           
           
                       aStrApi = (aStrApi as NSString).addingPercentEscapes(using: String.Encoding.utf8.rawValue) ?? ""

           
           
                       print(aStrApi)
                       print(headers)

                               let encoding = URLEncoding.httpBody
                                      Alamofire.request(aStrApi, method: .get, parameters:nil, encoding:encoding, headers: headers).responseJSON { (responseObject) -> Void in
                                                              
                                                if responseObject.result.isSuccess {
                                           let resJson = JSON(responseObject.result.value!)
                                           let usrDetail = resJson
                                           print(usrDetail)
                                           if usrDetail["result"].exists() {
                                               self.appdelegate.HideProgress()

                                               let topicarray = usrDetail["result"].rawValue as! [Any]
                                                 
                                            for dict in topicarray {
                                            
                                                let dictnew = dict as? [String:Any]
                                                let strtopicid = String(format:"%@",dictnew!["id"] as! NSNumber )
                                                
                                                print(strtopicid)
                                                print(self.topicid!)

                                                
                                                if strtopicid == self.topicid {
                                                    self.dictback = dictnew
                                                    self.Arrfields = self.dictback?["fields"] as! [Any]
                                                    self.tblmenifest.reloadData()
                                                }
                                                
                                            }
                                                  
                                              
                                           }else{
                                               
                                               
                                               }

                                       }
                                       if responseObject.result.isFailure {
                                           self.appdelegate.HideProgress()

                                           
                                       }
                                                        
           
                      }
                               

                       
                      
           
       }
    
      func servicerequest(strapi : String){
                                  if !Reachability.isConnectedToNetwork() {
              print("Internet is Not available.")
              AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
                  return
              }
                  
            
                  appdelegate.ShowProgress()
                  let headers = ["Authorization" :
                              "Bearer \(UserDefaults.standard.value(forKey:"Authtoken") ?? "")"]
                              let aStrApi = strapi
            
    //        let paramsJSON = JSON(appdelegate.arrrequestids)
    //        let paramsString = paramsJSON.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)
           
           // print(paramsString!)

            let param = ["network_ids":appdelegate.arrrequestids]
                    print(aStrApi)
                    print(headers)
                    print(param)

                    let encoding = Alamofire.JSONEncoding.default
                Alamofire.request(aStrApi, method: .post, parameters:param as Parameters, encoding:encoding, headers: headers).responseJSON { (responseObject) -> Void in
                                                                     
                                                       if responseObject.result.isSuccess {
                                                  let resJson = JSON(responseObject.result.value!)
                                                  let usrDetail = resJson
                                                  print(usrDetail)
                                                       self.appdelegate.HideProgress()
                                                                   
                                                                self.serviceGetAssignment()
                                                                
                                              }
                                              if responseObject.result.isFailure {
                                                  self.appdelegate.HideProgress()

                                                  
                                              }
                  
                             }
                              
                  
              }
    
    
    func serviceGetAssignment(){
                           if !Reachability.isConnectedToNetwork() {
       print("Internet is Not available.")
       AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
           return
       }
           
           appdelegate.ShowProgress()
                       
           let headers = ["Authorization" :
                       "Bearer \(UserDefaults.standard.value(forKey:"Authtoken") ?? "")"]
        let aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)assignments/\(topicid ?? "0")/children"
           

           
                       print(aStrApi)
                       print(headers)

                               let encoding = URLEncoding.httpBody
                                      Alamofire.request(aStrApi, method: .get, parameters:nil, encoding:encoding, headers: headers).responseJSON { (responseObject) -> Void in
                                                              
                                                if responseObject.result.isSuccess {
                                           let resJson = JSON(responseObject.result.value!)
                                           let usrDetail = resJson
                                           print(usrDetail)
                                           if usrDetail["result"].exists() {
                                               self.appdelegate.HideProgress()

                                               self.asignmentarray = usrDetail["result"].rawValue as! [Any]
                                            self.tblmenifest.reloadData()
                                           }

                                       }
                                       if responseObject.result.isFailure {
                                           self.appdelegate.HideProgress()

                                           
                                       }
                                                        
           
                      }
                               
                       
                      
       }
        
    
     func refreshToken() {

            let urlString = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)auth/refresh-token"
            let request = NSMutableURLRequest()
            request.url = URL(string: urlString)
            request.httpMethod = "PUT"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            var Parameter: [AnyHashable : Any] = [:]
            Parameter["token"] = UserDefaults.standard.object(forKey: "Authtoken")
            Parameter["refreshToken"] = UserDefaults.standard.object(forKey: "RefreshToken")
            var myData: Data? = nil
            do {
                myData = try JSONSerialization.data(withJSONObject: Parameter, options: .prettyPrinted)
            } catch {
            }
            var putData = Data()
            if let myData = myData {
                putData.append(Data(myData))
            }
            request.httpBody = putData
            var response: URLResponse?

            var responseData: Data? = nil
            do {
                responseData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
            } catch let err {
                print(err)
            }

 
            
            do{
                let json = try JSONSerialization.jsonObject(with: responseData!, options: []) as? [String : Any]
                print(json!)
                if (json?["result"]) != nil {
                    
                    let dict = json!["result"] as? [String:Any]
                    let AuthToken = dict!["token"] as? String
                    UserDefaults.standard.setValue(AuthToken, forKey: "Authtoken")
                    UserDefaults.standard.synchronize()
                    self.Servicegettopic()
                }else{
                    appdelegate.HideProgress()
                }

                
              
            }catch{
                print(error)
                
            }
            
             
  
        }
    override var prefersStatusBarHidden: Bool {
        return false
    }

}
