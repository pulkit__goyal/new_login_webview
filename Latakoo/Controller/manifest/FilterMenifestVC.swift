//
//  FilterMenifestVC.swift
//  Latakoo
//
//  Created by Mayur Sardana on 01/07/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//

import UIKit

class menifestfiltercell: UITableViewCell {
     @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var btnswitch: UISwitch!
}
protocol filterdelegate : class {
    func fromfilter(_ controller: FilterMenifestVC)

}

class FilterMenifestVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Arrtags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! menifestfiltercell
        let dict = Arrtags[indexPath.row] as! [String:Any]
        cell.lbltitle.text = dict["Name"] as? String
        let flag = dict["flag"] as? String
        cell.btnswitch.tag = indexPath.row
        if flag == "On"{
            cell.btnswitch.setOn(true, animated: true)
        }else{
            cell.btnswitch.setOn(false, animated: true)

            
        }
       
        return cell
     }
    

    @IBOutlet weak var tbltag: UITableView!
    weak var delegate: filterdelegate?
    var Arrtags : [Any] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Arrtags = UserDefaults.standard.value(forKey: "ArrTags") as! [Any]
        tbltag.reloadData()
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func `switch`(_ sender: UISwitch) {
         var Arr = Arrtags
        let switchView = sender
        let dict = Arrtags[sender.tag] as? [String:Any]
        if switchView.isOn {
        var Dict: [String : String] = [:]
            Dict["Name"] = dict?["Name"] as? String
        Dict["flag"] = "On"
            Arr[sender.tag] = Dict
           } else {
        var Dict: [String : String] = [:]
        Dict["Name"] = dict?["Name"] as? String
        Dict["flag"] = "Off"
            Arr[sender.tag] = Dict
           }

               Arrtags = Arr
           
           UserDefaults.standard.set(Arrtags, forKey: "ArrTags")
           tbltag.reloadData()
    
    }
    
    @IBAction func back(_ sender: UIButton) {
      
        self.navigationController?.popViewController(animated: true)
        delegate?.fromfilter(self)
      
      }
    
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
