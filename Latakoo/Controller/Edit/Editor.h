//
//  Editor.h
//  Latakoo
//
//  Created by Mayur Sardana on 24/02/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Editor : NSObject
 
 + (void)imagetovideo:(UIImage *)image filename:(NSString *)name
 completion:(void(^)(NSURL *converimageurl, NSError *error))completion;


+(void)changevideo1:(NSURL*)videourl  withOutPath:(NSString*)outpath
completion:(void(^)(NSURL *convervideourl, NSError *error ,BOOL))completion;


+(void)changevideo:(NSURL*)videourl  withOutPath:(NSString*)outpath
completion:(void(^)(NSURL *convervideourl, NSError *error ,BOOL))completion;

+(BOOL)ifvideoportrait:(NSURL*)videourl;

+(void)mergeAndExportVideos:(NSArray*)videosPathArray  withOutPath:(NSString*)outpath
completion:(void(^)(NSURL *finalurl, NSError *error))completion;

+(void)Resetvideosize:(CGSize)newsize videourl:(NSURL*)Finalurl
completion:(void(^)(NSURL *finalurl, NSError *error))completion;
+(void)ExtractAudio:(NSURL*)Finalurl
completion:(void(^)(NSURL *finalurl, NSError *error))completion;

+(void)extractAudiofromVideo:(NSURL*)Finalurl
                  completion:(void(^)(NSURL *finalurl, NSError *error))completion;



+ (void) combineVoices:(NSURL*)AudioOFvideo lib:(NSArray*)ArrLibSave  rec:(NSArray*)ArrRecordingSave
            completion:(void(^)(NSURL *finalurl, NSError *error))completion;


+(void)mergeAndSave:(NSURL*)videourl audio:(NSURL*)mergedaudio
         completion:(void(^)(NSURL *finalurl, NSError *error))completion;

+(void)saveaudio;


@end

NS_ASSUME_NONNULL_END

