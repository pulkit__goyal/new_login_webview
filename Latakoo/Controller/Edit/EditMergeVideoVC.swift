//  EditMergeVideoVC.swift
//  Latakoo
//  Created by Mayur Sardana on 24/02/20.
//  Copyright © 2020 parangat technology. All rights reserved.

import AVFoundation
import AVKit
import MobileCoreServices
import Photos
import SVProgressHUD
import UIKit

class AllCell: UICollectionViewCell {
    @IBOutlet var imgvideo: UIImageView!
    @IBOutlet var btnedit: UIButton!
    @IBOutlet var imgicon: UIImageView!
    @IBOutlet var lblduration: UILabel!
    @IBOutlet var lbl101: UILabel!
    @IBOutlet var lbl301: UILabel!
    @IBOutlet var btnPlush: UIButton!
}

class AudioCell: UITableViewCell {
    @IBOutlet var lblmusic: UILabel!
}

class EditMergeVideoVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, SAVideoRangeSliderDelegate, EditAdddelegate, EditImageDelegate, videoEditDelegate, EditVideoDelegate,EditViewControllerDelegate,EditAudioDelegate,AVAudioPlayerDelegate{
    func NewAudio(_ controller: EditAudioVC, file: URL, delete: Bool, IsFromLib: Bool) {
        
        if IsFromLib {
         if delete == true {
             self.btn101new.sendActions(for: .touchUpInside)
         } else {
                 self.ArrLibSave[0] =  app.TrimThisAudio
                 self.libCollection.reloadData()
               // self.addlibplayer()
                 self.combinevoice()
               }
            
        }else{
            if delete == true {
                self.btn201new.sendActions(for: .touchUpInside)
            } else {
                    self.ArrRecordingSave[0] =  app.TrimThisAudio
                    self.recordCollection.reloadData()
                    // self.addrecplayer()

                    self.combinevoice()
            }
        }
    }
    
    
  
    func newvideo(_ controller: EditViewController, file: PHAsset, delete: Bool) {
        if delete == false {
            print(file)
            Arrmergerdfiles = []
            currentassetindex = 0
            arrfiles[indexafter!] = file
            var Dict: [AnyHashable: Any] = [:]
            Dict["leftvalue"] = ""
            Dict["rightvalue"] = ""
            Dict["volumekey"] = "1.0"
            Dict["orginalfile"] = file
            Dict["trimfile"] = ""
            Dict["phasset"] = "YES"
            arrimagevideos[indexafter!] = Dict
            print(arrfiles)
            mycollectionview.reloadData()
            self.makeurl()
        } else {
            Arrmergerdfiles = []
            currentassetindex = 0
            arrfiles.remove(at: indexafter!)
            arrimagevideos.remove(at: indexafter!)
            print(arrfiles)
            
            if arrfiles.count>0 {
                mycollectionview.reloadData()
                self.makeurl()
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func newvideo(_ controller: EditVideo, file: PHAsset, delete: Bool) {
        if delete == false {
            print(file)
            Arrmergerdfiles = []
            currentassetindex = 0
            arrfiles[indexafter!] = file
            var Dict: [AnyHashable: Any] = [:]
            Dict["leftvalue"] = ""
            Dict["rightvalue"] = ""
            Dict["volumekey"] = "1.0"
            Dict["orginalfile"] = file
            Dict["trimfile"] = ""
            Dict["phasset"] = "YES"
            arrimagevideos[indexafter!] = Dict
            print(arrfiles)
            mycollectionview.reloadData()
            self.makeurl()
        } else {
            Arrmergerdfiles = []
            currentassetindex = 0
            arrfiles.remove(at: indexafter!)
            arrimagevideos.remove(at: indexafter!)
            print(arrfiles)
            
            if arrfiles.count>0 {
                mycollectionview.reloadData()
                self.makeurl()
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(audio.count)
        return audio.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! AudioCell
        
        let theFileName = (audio[indexPath.item] as! NSString).lastPathComponent
        cell.lblmusic.text = theFileName
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var str = String(format: "%.0f", CMTimeGetSeconds(self.player!.currentTime()))
        if str == "nan" {
            str = "0"
        }
        let duration = self.player?.currentItem?.asset.duration
        let seconds = Int(CMTimeGetSeconds(duration!))
        var Dict: [String: Any] = [:]
        Dict["leftvalue"] = ""
        Dict["rightvalue"] = ""
        Dict["volumekey"] = "1.0"
        Dict["orginalfile"] = (audio[indexPath.item] as! NSString).lastPathComponent
        Dict["trimfile"] = ""
        Dict["playtime"] = "0"
        Dict["videoduration"] = "\(seconds)"
        self.ArrLibSave.append(Dict)
        print(self.ArrLibSave)
        KGModal.sharedInstance()?.hide()
        self.libCollection.reloadData()
        self.recordCollection.reloadData()
        self.HideButton()
        self.combinevoice()
        
  

    }
    
    @IBAction func deletebutton(_ sender: UIButton) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    
    func AddFiles(_ controller: EditAdd, file: [PHAsset]) {
        Arrmergerdfiles = []
        currentassetindex = 0
        for asset in file {
            indexafter = indexafter! + 1
            arrfiles.insert(asset, at: indexafter!)
            var Dict: [AnyHashable: Any] = [:]
            Dict["leftvalue"] = ""
            Dict["rightvalue"] = ""
            Dict["volumekey"] = "1.0"
            Dict["orginalfile"] = asset
            Dict["trimfile"] = ""
            Dict["phasset"] = "YES"
            arrimagevideos.insert(Dict, at: indexafter!)
        }
        print(arrfiles)
        mycollectionview.reloadData()
        self.makeurl()
    }
    
    func newfile(_ controller: EditImageVC, file: PHAsset, delete: Bool) {
        if delete == false {
            print(file)
            Arrmergerdfiles = []
            currentassetindex = 0
            arrfiles[indexafter!] = file
            var Dict: [AnyHashable: Any] = [:]
            Dict["leftvalue"] = ""
            Dict["rightvalue"] = ""
            Dict["volumekey"] = "1.0"
            Dict["orginalfile"] = file
            Dict["trimfile"] = ""
            Dict["phasset"] = "YES"
            arrimagevideos[indexafter!] = Dict
            print(arrfiles)
            mycollectionview.reloadData()
            self.makeurl()
        } else {
            Arrmergerdfiles = []
            currentassetindex = 0
            arrfiles.remove(at: indexafter!)
            arrimagevideos.remove(at: indexafter!)
            print(arrfiles)
            
            if arrfiles.count>0 {
                mycollectionview.reloadData()
                self.makeurl()
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func newvideo(_ controller: OptiViewController, file: PHAsset, delete: Bool) {
        if delete == false {
            print(file)
            Arrmergerdfiles = []
            currentassetindex = 0
            arrfiles[indexafter!] = file
            var Dict: [AnyHashable: Any] = [:]
            Dict["leftvalue"] = ""
            Dict["rightvalue"] = ""
            Dict["volumekey"] = "1.0"
            Dict["orginalfile"] = file
            Dict["trimfile"] = ""
            Dict["phasset"] = "YES"
            arrimagevideos[indexafter!] = Dict
            print(arrfiles)
            mycollectionview.reloadData()
            self.makeurl()
        } else {
            Arrmergerdfiles = []
            currentassetindex = 0
            arrfiles.remove(at: indexafter!)
            arrimagevideos.remove(at: indexafter!)
            print(arrfiles)
            
            if arrfiles.count>0 {
                mycollectionview.reloadData()
                self.makeurl()
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBOutlet var lblheader: UILabel!
    var header: String = ""
    var arrfiles: [PHAsset] = []
    var arrimagevideos: [Any] = []
    var ArrLibSave: [Any] = []
    var ArrRecordingSave: [Any] = []
    var Arrmergerdfiles: [Any] = []
    var mytimer: Timer?
    var player: AVPlayer?
    var controller: AVPlayerViewController?
    var FilesUrls: [String] = []
    let app = UIApplication.shared.delegate as! AppDelegate
    var currentassetindex: Int = 0
    var startX: Float = 0.0
    var startY: Float = 0.0
    var trimmmerframe: CGFloat = 0
    var indexafter: Int?
    var MergerAudioUrl: URL?
    var Audioofvideo: URL?
    var FinalVideoUrl: URL?
    var EditvideoUrl: URL?
    var AudioRecord: URL?
    var AudioLibrary: URL?
    var audio: [Any] = []
    let playnew = UIButton(type: .custom)
        var alert: SCLAlertViewResponder?
    
    
    var RecaudioPlayer: AVAudioPlayer?
    var LibaudioPlayer: AVAudioPlayer?

    var libaudiostarttime : Float = 0.0
    var Recaudiostarttime : Float = 0.0
    
    var libaudioseektime : Float = 0.0
    var Recaudioseektime : Float = 0.0


    // var libplayer: AVPlayer?
    // var libplayerLayer: AVPlayerLayer?
    
    // var recPlayer: AVPlayer?
    //  var recplayerLayer: AVPlayerLayer?
    
    @IBOutlet var mycollectionview: UICollectionView!
    @IBOutlet var btnplay: UIButton!
    @IBOutlet var imgVideo: UIImageView!
    @IBOutlet var viewvideo: UIView!
    @IBOutlet var viewaudio: UIView!
    @IBOutlet var movieView: UIView!
    @IBOutlet var btnlibrary: UIButton!
    @IBOutlet var btnrecor: UIButton!
    var flagMedia: String?
    @IBOutlet var viewlibrary: UIView!
    var pathString: String?
    @IBOutlet var libCollection: UICollectionView!
    @IBOutlet var recordCollection: UICollectionView!
    @IBOutlet var trackerView: UIView!
    var prevTrackerTime: CGFloat = 0.0
    @IBOutlet var trimmerview: SAVideoRangeSlider!
    @IBOutlet var btn101new: UIButton!
    @IBOutlet var btn201new: UIButton!
    @IBOutlet var thumview: UIImageView!
    @IBOutlet var btnvideo: UIButton!
    @IBOutlet var btnaudio: UIButton!
    @IBOutlet var touchview: UIView!
    @IBOutlet var viewright: UIView!
    @IBOutlet var viewleft: UIView!
    @IBOutlet var runningtime: UILabel!
    @IBOutlet var librarylabel: UILabel!
    @IBOutlet var recordlabel: UILabel!
    @IBOutlet var progresstime: UILabel!
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        option.isNetworkAccessAllowed = true
        
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: arrfiles[0], targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: { (result, _) -> Void in
            let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
            self.imgVideo.image = thumbnail
            
        })
        
        let documentsDirectory = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/bharat").absoluteString
        if !FileManager.default.fileExists(atPath: documentsDirectory) {
            do {
                try FileManager.default.createDirectory(atPath: documentsDirectory, withIntermediateDirectories: false, attributes: nil)
            } catch {}
        }
        
        progresstime.layer.cornerRadius = progresstime.frame.size.width / 2
        progresstime.clipsToBounds = true
        lblheader.text = header
        
        for i in 0 ..< arrfiles.count {
            let url = arrfiles[i]
            var Dict: [AnyHashable: Any] = [:]
            Dict["leftvalue"] = ""
            Dict["rightvalue"] = ""
            Dict["volumekey"] = "1.0"
            Dict["orginalfile"] = url
            Dict["trimfile"] = ""
            Dict["phasset"] = "YES"
            
            arrimagevideos.append(Dict)
        }
        
        self.perform(#selector(makeurl), with: nil, afterDelay: 0.2)
        viewaudio.isHidden = false
        viewvideo.isHidden = true
        trimmmerframe = trackerView.frame.origin.x
        
        self.buttonframe()
        self.getaudio()
        btnaudio.backgroundColor = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
          btnaudio.setTitleColor(UIColor.white, for: .normal)
    }
    
    
    
    func addlibplayer () {
        if ArrLibSave.count>0 {
            let dict = ArrLibSave[0] as! [String: Any]
            let filename = dict["orginalfile"]! as! String
             let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
             let recDir = paths[0]
             let audioFileName = "\(recDir)/\(filename)"
               
                    try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                     try! AVAudioSession.sharedInstance().setActive(true)
            
                     try! LibaudioPlayer = AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: audioFileName))
                     LibaudioPlayer!.prepareToPlay()
                     LibaudioPlayer!.pause()
                     LibaudioPlayer!.delegate = self
        }
    }
    
    func addrecplayer () {
        if ArrRecordingSave.count>0 {
            let dict = ArrRecordingSave[0] as! [String: Any]
            let filename = dict["orginalfile"]! as! String
             let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
             let recDir = paths[0]
             let audioFileName = "\(recDir)/\(filename)"
               
                    try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                     try! AVAudioSession.sharedInstance().setActive(true)
            
                     try! RecaudioPlayer = AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: audioFileName))
                     RecaudioPlayer!.prepareToPlay()
                     RecaudioPlayer!.pause()
                     RecaudioPlayer!.delegate = self
        }
    }
    
    
    
    @objc func makeurl() {
        app.ShowProgress()
        
        if Arrmergerdfiles.count == arrimagevideos.count {
            arrimagevideos = Arrmergerdfiles
            
            let videoName = "MixVideo.mov"
            let documentsDirectory = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/bharat").absoluteString
            let exportPath = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(videoName).absoluteString
            
            Editor.mergeAndExportVideos(Arrmergerdfiles, withOutPath: exportPath) { URL, _ in
                DispatchQueue.main.async {
                    let videoasset = AVAsset(url: URL)
                    // self.playmovieatpath(videoasset)
                    print(URL)
                    self.EditvideoUrl = URL
                    self.FinalVideoUrl = URL
                    self.extractaudiofromvideo(video: URL)
                    
                    if self.ArrLibSave.count>0 || self.ArrRecordingSave.count>0{
                        
                    }else{
                        self.playmovieatpath(videoasset, Finalurl: URL)

                    }
                    
                }
            }
        } else {
            let dict = arrimagevideos[currentassetindex] as? [String: Any]
            if dict!["phasset"] as? String == "YES" {
                let asset = arrfiles[currentassetindex]
                if asset.mediaType == .image {
                    self.changeimagetovideo(asset: asset)
                } else {
                    self.changevideoorintation(asset: asset)
                }
            } else {
                self.Arrmergerdfiles.append(dict as Any)
                self.currentassetindex = self.currentassetindex + 1
                self.makeurl()
            }
        }
    }
    
    func extractaudiofromvideo(video: URL) {
        // do stuff
        
        Editor.extractAudio(video) { url, _ in
            
            let bundleDirectory = Bundle.main.bundlePath
            let audio_inputFilePath = URL(fileURLWithPath: bundleDirectory).appendingPathComponent("20m.mp3").absoluteString
            let audio_inputFileUrl = URL(string: audio_inputFilePath)
            
            if url == audio_inputFileUrl {
                Editor.extractAudiofromVideo(video) { URL, _ in
                
                    
                    self.Audioofvideo = URL
                    
                    if self.ArrLibSave.count>0 || self.ArrRecordingSave.count>0{
                        self.combinevoice()
                    }
              
                
                
                }
                
            } else {
              
                
                self.Audioofvideo = url
           
                if self.ArrLibSave.count>0 || self.ArrRecordingSave.count>0{
                    self.combinevoice()
                }
            
            }
        }
    }
    
    func changeimagetovideo(asset: PHAsset) {
        let asset = asset
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        option.isNetworkAccessAllowed = true
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: { (result, _) -> Void in
            DispatchQueue.main.async {
                print("will get image  here")
                
                var size: CGSize
                var resizedImage: UIImage?
                if asset.pixelHeight>asset.pixelWidth, asset.pixelWidth < 1126 {
                    size = CGSize(width: 750, height: 1080)
                    resizedImage = self.resizeImage(image: result!, size: size)
                } else if asset.pixelHeight>asset.pixelWidth {
                    size = CGSize(width: 1000, height: 1080)
                    resizedImage = self.resizeImage(image: result!, size: size)
                    
                } else if asset.pixelHeight == asset.pixelWidth {
                    size = CGSize(width: 1334, height: 1080)
                    resizedImage = self.resizeImage(image: result!, size: size)
                    
                } else {
                    size = CGSize(width: 1920, height: 1080)
                    resizedImage = self.resizeImage(image: result!, size: size)
                }
                
                let resources = PHAssetResource.assetResources(for: asset)
                let orgFilename = resources[0].originalFilename
                
                self.imagetovideo(image: resizedImage!, filename: orgFilename)
            }
        })
    }
    
    func imagetovideo(image: UIImage, filename: String) {
        Editor.imagetovideo(image, filename: filename) { URL, _ in
            DispatchQueue.main.async {
                //   let Imageasset = AVAsset(url: URL)
                
                var Dict: [AnyHashable: Any] = [:]
                Dict["leftvalue"] = ""
                Dict["rightvalue"] = ""
                Dict["volumekey"] = "1.0"
                Dict["orginalfile"] = URL
                Dict["trimfile"] = ""
                Dict["phasset"] = "NO"
                
                self.Arrmergerdfiles.append(Dict)
                
                self.currentassetindex = self.currentassetindex + 1
                self.makeurl()
            }
        }
    }
    
    func resizeImage(image: UIImage, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let newimage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newimage!
    }
    
    
    func ifportrait(videourl:URL) -> Bool {
       
     let asset = AVAsset(url:videourl)
     let width : Float = Float(asset.videoSize().width)
     let height : Float = Float(asset.videoSize().height)
    print(width)
    print(height)
        if height>width || height==width {
         return true
        }else{
       return false
        }
        
    }
    
    func changevideoorintation(asset: PHAsset) {
        let option1 = PHVideoRequestOptions()
        app.HideProgress()
        option1.isNetworkAccessAllowed = true
        option1.version = .original
        option1.deliveryMode = .highQualityFormat
        option1.isNetworkAccessAllowed = true
        option1.progressHandler = { progress, _, _, _ in
            SVProgressHUD.setDefaultMaskType(.black)
            SVProgressHUD.showProgress(Float(progress), status: "Please wait. Downloading file from iCloud.")
        }
        
        PHCachingImageManager.default().requestAVAsset(forVideo: asset, options: option1, resultHandler: { avasset, _, _ in
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.app.ShowProgress()
                if let urlAsset = avasset as? AVURLAsset {
                    let localVideoUrl: URL = urlAsset.url as URL
                    
                   // if self.ifportrait(videourl: localVideoUrl){
                    if Editor.ifvideoportrait(localVideoUrl) {
                        Editor.changevideo(localVideoUrl, withOutPath: localVideoUrl.lastPathComponent) { URL, _, ifsuccess in
                            if ifsuccess == false {
                                self.app.HideProgress()
                                let alert = UIAlertController(
                                    title: "Alert",
                                    message: "Some file not supported..",
                                    preferredStyle: .alert)
                                
                                let yesButton = UIAlertAction(
                                    title: "OK",
                                    style: .default,
                                    handler: { _ in
                                        self.navigationController?.popViewController(animated: true)
                                        
                                    })
                                
                                alert.addAction(yesButton)
                                self.present(alert, animated: true)
                                
                            } else {
                                DispatchQueue.main.async {
                                    //  self.Arrmergerdfiles .append(URL)
                                    var Dict: [AnyHashable: Any] = [:]
                                    Dict["leftvalue"] = ""
                                    Dict["rightvalue"] = ""
                                    Dict["volumekey"] = "1.0"
                                    Dict["orginalfile"] = URL
                                    Dict["trimfile"] = ""
                                    Dict["phasset"] = "NO"
                                    
                                    self.Arrmergerdfiles.append(Dict)
                                    self.currentassetindex = self.currentassetindex + 1
                                    self.makeurl()
                                }
                            }
                        }
                    } else {
                        Editor.changevideo1(localVideoUrl, withOutPath: localVideoUrl.lastPathComponent) { URL, _, ifsuccess in
                            DispatchQueue.main.async {
                                // self.Arrmergerdfiles .append(URL)
                                
                                if ifsuccess == false {
                                    self.app.HideProgress()
                                    let alert = UIAlertController(
                                        title: "Alert",
                                        message: "Some file not supported..",
                                        preferredStyle: .alert)
                                    
                                    let yesButton = UIAlertAction(
                                        title: "OK",
                                        style: .default,
                                        handler: { _ in
                                            self.navigationController?.popViewController(animated: true)
                                            
                                        })
                                    
                                    alert.addAction(yesButton)
                                    self.present(alert, animated: true)
                                    
                                } else {
                                    var Dict: [AnyHashable: Any] = [:]
                                    Dict["leftvalue"] = ""
                                    Dict["rightvalue"] = ""
                                    Dict["volumekey"] = "1.0"
                                    Dict["orginalfile"] = URL
                                    Dict["trimfile"] = ""
                                    Dict["phasset"] = "NO"
                                    self.Arrmergerdfiles.append(Dict)
                                    self.currentassetindex = self.currentassetindex + 1
                                    self.makeurl()
                                }
                            }
                        }
                    }
                } else {
                    self.app.HideProgress()
                    let alert = UIAlertController(
                        title: "Alert",
                        message: "File not supported..",
                        preferredStyle: .alert)
                    
                    let yesButton = UIAlertAction(
                        title: "OK",
                        style: .default,
                        handler: { _ in
                            self.navigationController?.popViewController(animated: true)
                            
                        })
                    
                    alert.addAction(yesButton)
                    self.present(alert, animated: true)
                }
            }
        })
    }
    
    @IBAction func btnback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        trimmerview.removeFromSuperview()
    }
    
    @IBAction func save(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: nil, message: "Choose an export size", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Medium - 360p", style: .default, handler: { _ in
            let newsize = CGSize(width: 568, height: 320)
            self.resetvideosize(newsize: newsize)
            
            self.dismiss(animated: true) {}
        }))
        
        actionSheet.addAction(UIAlertAction(title: "HD - 720p", style: .default, handler: { _ in
            let newsize = CGSize(width: 1280, height: 720)
            self.resetvideosize(newsize: newsize)
            
            self.dismiss(animated: true) {}
        }))
        
        actionSheet.addAction(UIAlertAction(title: "HD - 1080p", style: .default, handler: { _ in
            
            let newsize = CGSize(width: 1920, height: 1080)
            
            self.resetvideosize(newsize: newsize)
            
            self.dismiss(animated: true) {}
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            
            self.dismiss(animated: true) {}
        }))
        
        if IS_IPHONE {
            actionSheet.popoverPresentationController?.sourceView = self.view
            
        } else {
            actionSheet.popoverPresentationController?.sourceView = sender
        }
        
        present(actionSheet, animated: true) {
            print("option menu presented")
        }
    }
    
    func resetvideosize(newsize: CGSize) {
        DispatchQueue.main.async {
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            self.alert = SCLAlertView(appearance: appearance).showWait("LATAKOO", subTitle: "Exporting...", closeButtonTitle: nil, timeout: nil, colorStyle: nil, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: SCLAnimationStyle.topToBottom)
        }
        
        sleep(UInt32(0.5))
        Editor.resetvideosize(newsize, videourl: FinalVideoUrl!) { _, _ in
            DispatchQueue.main.async {
                UISaveVideoAtPathToSavedPhotosAlbum((self.FinalVideoUrl?.path)!, self, nil, nil)
                self.perform(#selector(self.getvideo), with: nil, afterDelay: 05)
            }
        }
    }
    
    @objc func getvideo() {
        alert!.close()
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.video, options: fetchOptions)
        let lastPHAsset = assets.lastObject
        
        app.all.insert(lastPHAsset!, at: 0)
        app.video.insert(lastPHAsset!, at: 0)
        
        var dict: [String: Any] = [:]
        
        if UserDefaults.standard.value(forKey: "renamedict") != nil {
            dict = UserDefaults.standard.value(forKey: "renamedict") as! [String: Any]
            print(dict)
            
            dict[lastPHAsset!.localIdentifier] = self.lblheader.text
            
        } else {
            dict[lastPHAsset!.localIdentifier] = self.lblheader.text
        }
        
        UserDefaults.standard.set(dict, forKey: "renamedict")
        UserDefaults.standard.synchronize()
        print(dict)
        //        self.tabBarController?.selectedIndex = 2
        //        let navCtrl = tabBarController?.selectedViewController as? UINavigationController
        //        navCtrl?.popToRootViewController(animated: false)
        
        trimmerview.removeFromSuperview()
        
        var story: UIStoryboard?
        if IS_IPHONE {
            story = UIStoryboard(name: "Main", bundle: nil)
        } else {
            story = UIStoryboard(name: "MainIPAD", bundle: nil)
        }
        let TabbarController = story?.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
        TabbarController.selectedIndex = 2
        self.navigationController?.pushViewController(TabbarController, animated: false)
    }
    
    func playmovieatpath(_ MoviePath: AVAsset?, Finalurl: URL) {
        app.HideProgress()
        
        if player == nil {
        } else {
            player!.pause()
            player = nil
            controller!.removeFromParentViewController()
            controller!.view.removeFromSuperview()
            trimmerview.removeFromSuperview()
        }
        
        if MoviePath == nil {
            return
        }
        let playerItem = AVPlayerItem(asset: MoviePath!)
        if MoviePath != nil {
            player = AVPlayer(playerItem: playerItem)
        }
        
        movieView.backgroundColor = UIColor.clear
        controller = AVPlayerViewController()
        addChildViewController(controller!)
        view.addSubview(controller!.view)
        
        let width = UIScreen.main.bounds.size.width
        if IS_IPHONE {
            controller?.view.frame = CGRect(x: movieView.frame.origin.x, y: movieView.frame.origin.y, width: width, height: width * 9.0 / 16.0)
            playnew.frame = CGRect(x: movieView.frame.origin.x, y: movieView.frame.origin.y, width: width, height: width * 9.0 / 16.0)
            
        } else {
            controller?.view.frame = CGRect(x: movieView.frame.origin.x, y: movieView.frame.origin.y, width: width, height: 311)
            playnew.frame = CGRect(x: movieView.frame.origin.x, y: movieView.frame.origin.y, width: width, height: 311)
        }
        
        controller!.player = player
        
        playnew.isHidden = false
        playnew.setImage(UIImage(named: "play"), for: .normal)
        playnew.backgroundColor = .clear
        playnew.addTarget(self, action: #selector(playnewmethod), for: .touchUpInside)
        playnew.tintColor = UIColor.clear
        self.view.addSubview(playnew)
        self.view.bringSubview(toFront: playnew)
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            // report for an error
        }
        
        player!.pause()
        
        NotificationCenter.default.addObserver(self, selector: #selector(EditMergeVideoVC.itemDidFinishPlaying(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        
        if IS_IPHONE {
            trimmerview = SAVideoRangeSlider(frame1: CGRect(x: trimmerview.frame.origin.x, y: trimmerview.frame.origin.y, width: trimmerview.frame.size.width, height: trimmerview.frame.size.height), videoUrl: Finalurl)
        } else {
            trimmerview = SAVideoRangeSlider(frame1: CGRect(x: trimmerview.frame.origin.x, y: trimmerview.frame.origin.y, width: libCollection.frame.size.width, height: trimmerview.frame.size.height), videoUrl: Finalurl)
        }
        trimmerview.delegate = self
        viewaudio.addSubview(trimmerview)
        trimmerview.isHidden = true
        viewaudio.bringSubview(toFront: viewright)
        //
        
        trimmerview.isHidden = false
        
        if mytimer != nil {
            mytimer!.invalidate()
            if mytimer != nil {
                mytimer = nil
            }
        }
        mytimer = Timer.scheduledTimer(
            timeInterval: 0.1,
            target: self,
            selector: #selector(seekToTime),
            userInfo: nil,
            repeats: true)
        
        Timer.scheduledTimer(
            timeInterval: 0.1,
            target: self,
            selector: #selector(checkifplaying), userInfo: nil,
            repeats: true)
    }
    
    @objc func playnewmethod(sender: UIButton) {
        playnew.isHidden = true
        player?.play()
    }
    
    @objc func itemDidFinishPlaying(_ notification: Notification) {}
    
    @objc func checkifplaying() {
        if player!.rate != 0, player?.error == nil {
            if mytimer == nil {
                viewaudio.bringSubview(toFront: touchview)
                mytimer = Timer.scheduledTimer(
                    timeInterval: 0.1,
                    target: self,
                    selector: #selector(seekToTime),
                    userInfo: nil,
                    repeats: true)
            }
        }
    }
    
    @IBAction func btnaudio(_ sender: Any) {
        viewaudio.isHidden = false
        viewvideo.isHidden = true
        btnaudio.backgroundColor = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
               btnaudio.setTitleColor(UIColor.white, for: .normal)
        btnaudio.setTitle("TIMELINE", for: .normal)

    }
    
    @IBAction func btnvideo(_ sender: Any) {
        viewaudio.isHidden = true
        viewvideo.isHidden = false
        
        btnaudio.setTitle("← BACK TO TIMELINE", for: .normal)
        
        btnaudio.backgroundColor = UIColor.white
        let color =  UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
        btnaudio.setTitleColor(color, for: .normal)
        
        

    }
    
    @IBAction func btnplay(_ sender: Any) {
        btnplay.isHidden = true
        imgVideo.isHidden = true
    }
    
    @IBAction func library(_ sender: Any) {
        //          loadAudioFiles()
        player!.pause()
        
        if audio.count>0 {
            KGModal.sharedInstance().show(withContentView: viewlibrary)
            tableView.reloadData()
        } else {
            _ = SCLAlertView().showError("", subTitle: "No audio found!", closeButtonTitle: "OK")
        }
    }
    
    @IBAction func recrod(_ sender: Any) {
        player!.pause()
        AudioTrimmerViewController.presentAudioTrimmerController(self) { _, trimedFilePath in
            if let trimedFilePath = trimedFilePath {
                print("\(trimedFilePath)")
            }
            if trimedFilePath == nil {
            } else {
                var str = String(format: "%.0f", CMTimeGetSeconds(self.player!.currentTime()))
                if str == "nan" {
                    str = "0"
                }
                
                let duration = self.player?.currentItem?.asset.duration
                let seconds = Int(CMTimeGetSeconds(duration!))
                
                var Dict: [String: Any] = [:]
                Dict["leftvalue"] = ""
                Dict["rightvalue"] = ""
                Dict["volumekey"] = "1.0"
                Dict["orginalfile"] = trimedFilePath?.lastPathComponent
                Dict["trimfile"] = ""
                Dict["playtime"] = "0"
                Dict["videoduration"] = "\(seconds)"
                self.ArrRecordingSave.append(Dict)
                self.recordCollection.reloadData()
                self.libCollection.reloadData()
                self.HideButton()
              //  self.addrecplayer()

               self.combinevoice()
            }
        }
    }
    
    func combinevoice() {
        app.ShowProgress()
        Editor.combineVoices(Audioofvideo!, lib: ArrLibSave, rec: ArrRecordingSave) { URL, _ in
            self.MergerAudioUrl = URL
            self.mergeaudioandvideo()
        }
    }
    
    func mergeaudioandvideo() {
        Editor.mergeAndSave(FinalVideoUrl!, audio: MergerAudioUrl!) { URL, _ in
            DispatchQueue.main.async {
                let videoasset = AVAsset(url: URL)
                self.FinalVideoUrl = URL
                self.app.HideProgress()
                self.playmovieatpath(videoasset, Finalurl: URL)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        player?.pause()
    }
    
    @IBAction func new101(_ sender: Any) {
        player!.pause()
        ArrLibSave = []
        self.HideButton()
        self.combinevoice()
        libCollection.reloadData()
    }
    
    @IBAction func new201(_ sender: Any) {
        player!.pause()
        ArrRecordingSave = []
        self.HideButton()
        self.combinevoice()
        recordCollection.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1100 {
            return ArrLibSave.count
        } else if collectionView.tag == 1300 {
            return ArrRecordingSave.count
        } else {
            return arrimagevideos.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! AllCell
        
        if collectionView.tag == 1100 {
            if ArrLibSave.count>0 {
                let dict = ArrLibSave[indexPath.item] as! [String: Any]
                let newtime = dict["orginalfile"]! as! String
                cell.lbl101.text = newtime
                let duration = player?.currentItem?.asset.duration
                var seconds: Float?
                if let duration = duration {
                    seconds = Float(CMTimeGetSeconds(duration))
                }
                let widhthh = libCollection.frame.size.width
                let point = widhthh / CGFloat(seconds ?? 0.0)
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
                var audioFileName: String?
                if dict["trimfile"]! as! String == "" {
                    let object = dict["orginalfile"] as! String
                    audioFileName = "\(paths ?? "")/\(object)"
                    
                } else {
                    audioFileName = dict["trimfile"]! as? String
                }
                let url = URL(fileURLWithPath: audioFileName ?? "")
                let audioAsset = AVURLAsset(url: url, options: nil)
                let audioDuration = audioAsset.duration
                let audioDurationSeconds = CGFloat(CMTimeGetSeconds(audioDuration))
                let playtime = CGFloat((dict["playtime"] as! NSString).doubleValue)
                cell.lbl101.frame = CGRect(x: CGFloat(point * playtime), y: 0, width: CGFloat(audioDurationSeconds * point), height: cell.lbl101.frame.size.height)
            }
            
            return cell
            
        } else if collectionView.tag == 1300 {
            if ArrRecordingSave.count>0 {
                let dict = ArrRecordingSave[indexPath.item] as! [String: Any]
                let newtime = dict["orginalfile"]! as! String
                cell.lbl301.text = newtime
                let duration = player?.currentItem?.asset.duration
                var seconds: Float?
                if let duration = duration {
                    seconds = Float(CMTimeGetSeconds(duration))
                }
                let widhthh = libCollection.frame.size.width
                let point = widhthh / CGFloat(seconds ?? 0.0)
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
                var audioFileName: String?
                if dict["trimfile"]! as! String == "" {
                    let object = dict["orginalfile"] as! String
                    audioFileName = "\(paths ?? "")/\(object)"
                    
                } else {
                    audioFileName = dict["trimfile"]! as? String
                }
                let url = URL(fileURLWithPath: audioFileName ?? "")
                let audioAsset = AVURLAsset(url: url, options: nil)
                let audioDuration = audioAsset.duration
                let audioDurationSeconds = CGFloat(CMTimeGetSeconds(audioDuration))
                let playtime = CGFloat((dict["playtime"] as! NSString).doubleValue)
                cell.lbl301.frame = CGRect(x: CGFloat(point * playtime), y: 0, width: CGFloat(audioDurationSeconds * point), height: cell.lbl301.frame.size.height)
            }
            return cell
        } else {
            cell.lblduration.text = ""
            cell.btnedit.tag = indexPath.item
            cell.btnPlush.tag = indexPath.item
            
            let dict = arrimagevideos[indexPath.item] as! [String: Any]
            
            let url = dict["trimfile"] as! String
            
            if url != "" {
                let url = (arrimagevideos[indexPath.item] as? [AnyHashable: Any])?["trimfile"] as? URL
                var asset: AVURLAsset?
                if let url = url {
                    asset = AVURLAsset(url: url, options: nil)
                }
                var generator: AVAssetImageGenerator?
                if let asset = asset {
                    generator = AVAssetImageGenerator(asset: asset)
                }
                generator?.appliesPreferredTrackTransform = true
                var error: Error?
                let thumbTime = CMTimeMakeWithSeconds(Float64(0), 30)
                var refImg: CGImage?
                do {
                    refImg = try generator?.copyCGImage(at: thumbTime, actualTime: nil)
                } catch {}
                if error != nil {
                    print("\(error?.localizedDescription ?? "")")
                }
                var FrameImage: UIImage?
                if let refImg = refImg {
                    FrameImage = UIImage(cgImage: refImg)
                }
                cell.imgvideo.image = FrameImage
                (cell.viewWithTag(10) as? UIImageView)?.isHidden = false
                
                let assetnew = dict["orginalfile"] as! PHAsset
                if assetnew.mediaType == .image {
                    cell.lblduration.text = ""
                } else {
                    let seconds = asset?.duration.seconds
                    let minutes = seconds! / 60
                    let seconds1 = Int(seconds!) % 60
                    let videoDuration = String(format: "%2ld:%2ld", minutes, seconds1)
                    cell.lblduration.text = "\(videoDuration)"
                }
                
            } else {
                let asset = arrfiles[indexPath.item]
                if asset.mediaType == .image {
                    let manager = PHImageManager.default()
                    let option = PHImageRequestOptions()
                    option.isNetworkAccessAllowed = true
                    var thumbnail = UIImage()
                    option.isSynchronous = true
                    manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: { (result, _) -> Void in
                        let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                        cell.imgvideo.image = thumbnail
                    })
                    
                } else {
                    let manager = PHImageManager.default()
                    let option = PHImageRequestOptions()
                    option.isNetworkAccessAllowed = true
                    var thumbnail = UIImage()
                    option.isSynchronous = true
                    manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: { (result, _) -> Void in
                        let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                        cell.imgvideo.image = thumbnail
                        
                    })
                    
                    cell.lblduration.text = ""
                    
                    DispatchQueue.global(qos: .background).async {
                        let option1 = PHVideoRequestOptions()
                        option1.isNetworkAccessAllowed = true
                        option1.deliveryMode = .highQualityFormat
                        option1.version = .original
                        option1.progressHandler = { _, _, _, _ in
                        }
                        var resultAsset: AVAsset?
                        PHImageManager.default().requestAVAsset(forVideo: asset, options: option1, resultHandler: { avasset, _, _ in
                            resultAsset = avasset
                            let duration = resultAsset?.duration
                            let durationTime = CMTimeGetSeconds(duration ?? kCMTimeZero)
                            print(durationTime)
                            DispatchQueue.main.async {
                                cell.lblduration.text = self.convertTime(Int(durationTime))
                            }
                        })
                    }
                }
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let duration = self.player?.currentItem?.asset.duration
        let seconds = Int(CMTimeGetSeconds(duration!))
        app.videoduration = "\(seconds)"
        print(app.videoduration!)
        
        if collectionView.tag == 1100 {
            
                let dict = ArrLibSave[indexPath.item]
                app.TrimThisAudio = dict as! [String: Any]
                print(app.TrimThisAudio)
                app.isitunes = "lib"
                player?.pause()
            print(FinalVideoUrl!)
            
            var story : UIStoryboard?
//            if IS_IPHONE {
//                             story = UIStoryboard(name: "Main", bundle: nil)
//                         } else {
//                             story = UIStoryboard(name: "MainIPAD", bundle: nil)
//                         }
            
                story = UIStoryboard(name: "Main", bundle: nil)

            
                         let EditAudioVC = story?.instantiateViewController(withIdentifier: "EditAudioVC") as! EditAudioVC
                         EditAudioVC.videoUrl = self.EditvideoUrl
                   EditAudioVC.IsFromLib = true
                EditAudioVC.delegate = self
                self.navigationController?.pushViewController(EditAudioVC, animated: true)
            


        } else if collectionView.tag == 1300 {
            let dict = ArrRecordingSave[indexPath.item]
            app.TrimThisAudio = dict as! [String: Any]
            print(app.TrimThisAudio)
            app.isitunes = "mic"
            player?.pause()
            
            var story : UIStoryboard?
                  //   if IS_IPHONE {
//                    story = UIStoryboard(name: "Main", bundle: nil)
//                                  } else {
//                    story = UIStoryboard(name: "MainIPAD", bundle: nil)
//                                  }
            
            story = UIStoryboard(name: "Main", bundle: nil)

                let EditAudioVC = story?.instantiateViewController(withIdentifier: "EditAudioVC") as! EditAudioVC
                    EditAudioVC.videoUrl = self.EditvideoUrl
                 EditAudioVC.IsFromLib = false
                 EditAudioVC.delegate = self
                self.navigationController?.pushViewController(EditAudioVC, animated: true)
            
        }
    }
    
    @IBAction func btnplush(_ sender: UIButton) {
        indexafter = sender.tag
        var story: UIStoryboard?
        
        if IS_IPHONE {
            story = UIStoryboard(name: "Main", bundle: nil)
        } else {
            story = UIStoryboard(name: "MainIPAD", bundle: nil)
        }
        let EditAdd = story?.instantiateViewController(withIdentifier: "EditAdd") as! EditAdd
        EditAdd.delegate = self
        self.navigationController?.pushViewController(EditAdd, animated: true)
    }
    
    @IBAction func btnedit(_ sender: UIButton) {
        indexafter = sender.tag
        
        let asset = arrfiles[sender.tag]
        if asset.mediaType == .image {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.isNetworkAccessAllowed = true
            
            var thumbnail = UIImage()
            option.isSynchronous = true
            manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: { (result, _) -> Void in
                let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                var story: UIStoryboard?
                
                if IS_IPHONE {
                    story = UIStoryboard(name: "Main", bundle: nil)
                } else {
                    story = UIStoryboard(name: "MainIPAD", bundle: nil)
                }
                let EditImageVC = story?.instantiateViewController(withIdentifier: "EditImageVC") as! EditImageVC
                EditImageVC.bigimage = thumbnail
                EditImageVC.delegate = self
                self.navigationController?.pushViewController(EditImageVC, animated: true)
                
            })
            
        } else {
            var story: UIStoryboard?
            
            if IS_IPHONE {
                story = UIStoryboard(name: "Main", bundle: nil)
            } else {
            story = UIStoryboard(name: "MainIPAD", bundle: nil)
            }
            //        let OptiViewController = story?.instantiateViewController(withIdentifier: "OptiViewController") as! OptiViewController
            //                                                  OptiViewController.videoasset = asset
            //                                                  OptiViewController.delegate = self
            //                                               self.navigationController?.pushViewController(OptiViewController, animated: true)
            
            let EditVideo = story?.instantiateViewController(withIdentifier: "EditVideo") as! EditVideo
            EditVideo.videoasset = asset
            EditVideo.delegate = self
            self.navigationController?.pushViewController(EditVideo, animated: true)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = event?.allTouches?.first
        if touch?.view == trackerView || touch?.view == touchview {
            player?.pause()
            if mytimer != nil {
                mytimer!.invalidate()
                if mytimer != nil {
                    mytimer = nil
                }
            }
            viewaudio.bringSubview(toFront: trackerView)
            let location = touch?.location(in: view)
            startX = Float((location?.x ?? 0.0) - trackerView.center.x)
            startY = Float(trackerView.center.y)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = event?.allTouches?.first
        if touch?.view == trackerView {
            var location = touch?.location(in: view)
            location!.x = (location?.x ?? 0.0) - CGFloat(startX)
            location!.y = CGFloat(startY)
            print("tracker x \(trackerView.frame.origin.x) , collectionview x \(libCollection.frame.origin.x)")
            if trackerView.frame.origin.x>viewleft.frame.origin.x {
                print("First Step")
                print("tracker x \(trackerView.frame.origin.x) , check lesser x \(viewright.frame.origin.x - trackerView.frame.size.width)")
                if trackerView.frame.origin.x <= viewright.frame.origin.x - trackerView.frame.size.width - 2 {
                    trackerView.center = location!
                    let duration = player?.currentItem?.asset.duration
                    var seconds: Float?
                    if let duration = duration {
                        seconds = Float(CMTimeGetSeconds(duration))
                    }
                    let widhthh = libCollection.frame.size.width
                    let point = widhthh / CGFloat(seconds ?? 0.0)
                    let newvalue = Float(Double(trackerView.frame.origin.x) - Double(viewleft.frame.origin.x))
                   

                    print(newvalue)

                    
                    let seeektoplayer = newvalue / Float(point)
                    
                    if seeektoplayer < 0 {
                        progresstime.text = "\(0)"
                    } else {
                        progresstime.text = String(format: "%.0f", seeektoplayer)
                    }
                    
                    //  runningtime.text = convertTime(Int(seeektoplayer))
                    player!.seek(to: CMTimeMakeWithSeconds(Float64(seeektoplayer), 60000))
                } else {
                    trackerView.center = location!
                }
            } else {
                trackerView.center = location!
            }
        }
    }
    
    func convertTime(_ time: Int) -> String? {
        let minutes = time / 60
        let seconds = time % 60
        return String(format: "%02ld:%02ld", minutes, seconds)
    }
    
    func cleardefaultdata() {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        let dataPath = URL(fileURLWithPath: paths!).appendingPathComponent("/bharat").absoluteString
        
        let fileManager = FileManager.default
        
        do {
            try fileManager.removeItem(atPath: dataPath)
        } catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
    
    @objc func seekToTime() {
        viewaudio.bringSubview(toFront: touchview)
        let duration = player?.currentItem?.asset.duration
        let currentitem = Float(CMTimeGetSeconds(player!.currentTime()))
        //  runningtime.text = convertTime(Int(currentitem))
        var seconds: Float?
        if let duration = duration {
            seconds = Float(CMTimeGetSeconds(duration))
        }
        let widhthh = libCollection.frame.size.width
        let point = widhthh / CGFloat(seconds ?? 0.0)
        let durationnew = CGFloat(abs(prevTrackerTime - CGFloat(currentitem)))
        let animate = (durationnew>1) ? false : true
        prevTrackerTime = CGFloat(currentitem)
        var trackerFrame = trackerView.frame
        trackerFrame.origin.x = CGFloat(trimmmerframe + point * CGFloat(currentitem))
        
        if trackerFrame.origin.x.isNaN {
            trackerFrame.origin.x = trimmmerframe
        }
        
        if currentitem < 0 {
            progresstime.text = "\(0)"
        } else {
            progresstime.text = String(format: "%.0f", currentitem)
        }
        
        trackerView.removeFromSuperview()
        if animate {
            UIView.animate(withDuration: TimeInterval(durationnew), delay: 0, options: [.curveLinear, .beginFromCurrentState], animations: {
                self.trackerView.frame = trackerFrame
                self.trackerView.layer.masksToBounds = true
                self.trackerView.layer.cornerRadius = 2
                self.viewaudio.addSubview(self.trackerView)
                self.viewaudio.bringSubview(toFront: self.touchview)
            })
        } else {
            trackerView.frame = trackerFrame
            trackerView.layer.masksToBounds = true
            trackerView.layer.cornerRadius = 2
            viewaudio.addSubview(trackerView)
        }
    }
    
    func buttonframe() {
        if IS_IPHONE {
            let width = UIScreen.main.bounds.size.width
           
            var buttonframe1 = btnaudio.frame
            buttonframe1.origin.y = movieView.frame.origin.y + width * 9.0 / 16.0
            btnaudio.frame = buttonframe1
            view.addSubview(btnaudio)
            let movieframe = CGRect(x: movieView.frame.origin.x, y: movieView.frame.origin.y, width: width, height: width * 9.0 / 16.0)
            let movieframe1 = CGRect(x: movieView.frame.origin.x, y: 0, width: width, height: width * 9.0 / 16.0)
            movieView.frame = movieframe
            imgVideo.frame = movieframe1
            view.addSubview(movieView)
            movieView.addSubview(imgVideo)
        }
    }
    
    func HideButton() {
        if ArrLibSave.count>0 {
            btnlibrary.isEnabled = false
        } else {
            btnlibrary.isEnabled = true
        }
        
        if ArrRecordingSave.count>0 {
            btnrecor.isEnabled = false
        } else {
            btnrecor.isEnabled = true
        }
        
        self.libCollection.reloadData()
        self.recordCollection.reloadData()
    }
    
    
    
    func getaudio() {
        audio = []
        let manager = FileManager.default
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let recDir = paths[0]
        var contents: [String]?
        do {
            contents = try manager.contentsOfDirectory(atPath: recDir)
            
        } catch {}
        for item in contents ?? [] {
            if (URL(fileURLWithPath: item).pathExtension == "m4a") || (URL(fileURLWithPath: item).pathExtension == "mp3") || (URL(fileURLWithPath: item).pathExtension == "ogg") {
                if (item == "recording.wav") || (item == "originalRecording.wav") || (item == "trimmerRecording.wav") || (item == "background.wav") || (item == "extract.m4a") || (item == "audio.m4a") || (item == "mixedAudio.m4a") || (item == "newaudio.mp4") {
                } else {
                    let audioFileName = "\(recDir)/\(item)"
                    audio.append(audioFileName)
                }
            }
        }
        
        self.tableView.reloadData()
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if !IS_IPHONE {
            self.perform(#selector(resetplayer), with: nil, afterDelay: 0.1)
            self.perform(#selector(changeloader), with: nil, afterDelay: 0.2)
        }
    }
    
    @objc func changeloader() {
        app.initializeLoader()
    }
    
    @objc func resetplayer() {
        if FinalVideoUrl != nil {
            let videoasset = AVAsset(url: FinalVideoUrl!)
            self.app.HideProgress()
            self.playmovieatpath(videoasset, Finalurl: FinalVideoUrl!)
        }
        
        if recordCollection != nil {
            recordCollection.reloadData()
            libCollection.reloadData()
        }
        // self.playmovieatpath(self.Myasset)
    }
}


