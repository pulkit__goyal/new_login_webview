//
//  EditVideo.swift
//  Latakoo
//
//  Created by ABC on 28/09/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//

import UIKit
import Photos
import AVKit
import AVFoundation
import SVProgressHUD
import DSWaveformImage

protocol EditVideoDelegate:class {
    func newvideo(_ controller: EditVideo, file: PHAsset , delete: Bool)
}

class EditVideo: UIViewController,ICGVideoTrimmerDelegate {
    
    let app = UIApplication.shared.delegate as! AppDelegate
    weak var delegate: EditVideoDelegate?
    @IBOutlet weak var Movieview: UIView!
    @IBOutlet weak var volumeslider: UISlider!
    var restartOnPlay = false
    @IBOutlet weak var starttime: UILabel!
    @IBOutlet weak var endtime: UILabel!
    @IBOutlet weak var middle1: UILabel!
    @IBOutlet weak var middle2: UILabel!
    @IBOutlet weak var trim: UIButton!
    @IBOutlet weak var cut: UIButton!
    @IBOutlet weak var btndone: UIButton!
    
    @IBOutlet var viewright: UIView!
    @IBOutlet var viewleft: UIView!
    @IBOutlet weak var paybackview: UIView!

    @IBOutlet weak var trimmerView: ICGVideoTrimmerView!
    @IBOutlet weak var trackerView: UIView!
    @IBOutlet weak var tracker1: UIView!
        @IBOutlet var touchview: UIView!
    var startX: Float = 0.0
    var startY: Float = 0.0
    var isportrait : Bool?
    var prevTrackerTime: CGFloat = 0.0
    var startTime: CGFloat = 0.0
    var stopTime: CGFloat = 0.0
    var Middle1 : CGFloat = 0
    var Middle2  : CGFloat = 0
    var player = AVPlayer()
    let controller = AVPlayerViewController()
    var playbackTimeCheckerTimer: Timer?
    var trimmerPositionChangedTimer: Timer?
    var videoPlaybackPosition: CGFloat = 0.0
    let playnew = UIButton(type: .custom)
    var url1 :URL?
    var url2 :URL?
    var asset : AVAsset?
    var firstpartasset : AVAsset?
    var secondpartasset : AVAsset?
    var videoasset : PHAsset?
    @IBOutlet weak var volumes: UISlider!
    var flagcut : Bool = false
    var finalurl : URL?
    var trimmmerframe : CGFloat?
    var mytimer: Timer?
    @IBOutlet var progresstime: UILabel!
    var dragtrimmer = true

   @IBOutlet var imageView : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        progresstime.text = "\(0)"
        progresstime.layer.cornerRadius = progresstime.frame.size.width / 2
        progresstime.clipsToBounds = true

        middle1.isHidden = true
        middle2.isHidden = true
        trimmmerframe = trackerView.frame.origin.x
        btndone.isHidden = true
        self.buttonframe()
         
        let option1 = PHVideoRequestOptions()
        option1.isNetworkAccessAllowed = true
        option1.deliveryMode = .highQualityFormat
        option1.version = .original
        option1.progressHandler = {  (progress, error, stop, info) in
            
            SVProgressHUD.setDefaultMaskType(.black)
            SVProgressHUD.showProgress(Float(progress), status: "Please wait. Downloading file from iCloud.")
            
            
        }
        var resultAsset: AVAsset?
        PHCachingImageManager.default().requestAVAsset(forVideo: videoasset!, options: option1, resultHandler: { avasset, audioMix, info in
            DispatchQueue.main.async{
                SVProgressHUD.dismiss()
                resultAsset = avasset
                let duration = resultAsset?.duration
                let durationTime = CMTimeGetSeconds(duration ??  CMTimeMake(1, 1))
                print(durationTime)
                let urlAsset = avasset as? AVURLAsset
                let localVideoUrl = urlAsset?.url
                self.isportrait  =   Editor.ifvideoportrait(localVideoUrl!)

                
                self.play(url:localVideoUrl!, iscut: false)
                self.asset = resultAsset
            }
        })
        
    }
    
    
    func ifportrait(videourl:URL) -> Bool {
       
     let asset = AVAsset(url:videourl)
     let width : Float = Float(asset.videoSize().width)
     let height : Float = Float(asset.videoSize().height)
    print(width)
    print(height)
        if height>width || height==width {
         return true
        }else{
       return false
        }
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        if mytimer != nil {
            mytimer!.invalidate()
            if mytimer != nil {
                mytimer = nil
            }
        }
    }
    
    func seekVideo(toPos pos: CGFloat) {
        videoPlaybackPosition = pos
        let time = CMTimeMakeWithSeconds(Float64(videoPlaybackPosition), (player.currentTime().timescale))
        player.seek(to: time, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if dragtrimmer == false {
            return
        }
        
           let touch = event?.allTouches?.first
        if touch?.view == trackerView || touch?.view == touchview {
            player.pause()
            playnew.setImage(UIImage.init(named: "play"), for: .normal)
               if mytimer != nil {
                   mytimer!.invalidate()
                   if mytimer != nil {
                       mytimer = nil
                   }
               }
           // self.view.bringSubview(toFront: trackerView)
               let location = touch?.location(in: view)
               startX = Float((location?.x ?? 0.0) - trackerView.center.x)
               startY = Float(trackerView.center.y)
           }
       }
       
       override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
           if dragtrimmer == false {
                   return
               }
        
           let touch = event?.allTouches?.first
           if touch?.view == trackerView {
               var location = touch?.location(in: view)
               location!.x = (location?.x ?? 0.0) - CGFloat(startX)
               location!.y = CGFloat(startY)
            
               if trackerView.frame.origin.x>viewleft.frame.origin.x {
                 
                   if trackerView.frame.origin.x <= viewright.frame.origin.x - trackerView.frame.size.width - 2 {
                       trackerView.center = location!
                    let duration = player.currentItem?.asset.duration
                       var seconds: Float = 0
                       if let duration = duration {
                           seconds = Float(CMTimeGetSeconds(duration))
                       }
                       let widhthh = imageView.frame.size.width
                    let point = widhthh / CGFloat(seconds)
                       let newvalue = Float(Double(trackerView.frame.origin.x + 11) - Double(imageView.frame.origin.x))
                    
                    
                       print(Double(trackerView.frame.origin.x ))
                       print(Double(imageView.frame.origin.x ))
                       print(newvalue)
                    
                    
                    let seeektoplayer = newvalue / Float(point)
                       
                    
                    var roundedValue4 = seeektoplayer.rounded(.up)
                        print("current item rounded \(roundedValue4)")

                    if roundedValue4 > seconds {
                    roundedValue4 = seconds
                    roundedValue4 = seeektoplayer.rounded(.up)
                    }
                    

                    
                    
                        
                       if seeektoplayer < 0 {
                           progresstime.text = "\(0)"
                       } else {
                           progresstime.text = "\(Int(roundedValue4))"
                       }
                       
                    player.seek(to: CMTimeMakeWithSeconds(Float64(seeektoplayer), 60000))
                   } else {
                       trackerView.center = location!
                   }
               } else {
                   trackerView.center = location!
               }
           }
       }
    
    
    func trimmerViewDidEndEditing(_ trimmerView: ICGVideoTrimmerView) {
        
      dragtrimmer = true

    
    }
    
    
    
    
    func trimmerView(_ trimmerView: ICGVideoTrimmerView, didChangeLeftPosition startTime: CGFloat, rightPosition endTime: CGFloat) {
        dragtrimmer = false

        stopPlaybackTimeChecker()
        restartOnPlay = true
        player.pause()
        playnew.setImage(UIImage.init(named: "play"), for: .normal)
        self.trimmerView.hideTracker(true)

        self.startTime = startTime
        self.stopTime = endTime
        
        let roundedValue1 = startTime.rounded(.up)
                   print("current item rounded \(roundedValue1)")
                   
        let roundedValue2 = endTime.rounded(.up)
                   print("current item rounded \(roundedValue2)")
        starttime.text = timeFormatted(roundedValue1, isWithMinutes: false)
        endtime.text = timeFormatted(roundedValue2, isWithMinutes: false)
        
        
    }
        
    func trimmerView(_ trimmerView: ICGVideoTrimmerView, didChangeLeftPosition startTime: CGFloat, middleStart MiddleTime: CGFloat, mIddleEnd MiddleEnd: CGFloat, rightPosition endTime: CGFloat) {
        
        dragtrimmer = false

        restartOnPlay = true
        stopPlaybackTimeChecker()
        player.pause()
        playnew.setImage(UIImage.init(named: "play"), for: .normal)
        self.trimmerView.hideTracker(true)

        self.startTime = startTime
        self.stopTime = endTime
        self.Middle1 = MiddleTime
        self.Middle2 = MiddleEnd
        
        
        let roundedValue1 = startTime.rounded(.up)
             print("current item rounded \(roundedValue1)")
             
        let roundedValue2 = endTime.rounded(.up)
             print("current item rounded \(roundedValue2)")
        
        let roundedValue4 = MiddleTime.rounded(.up)
        print("current item rounded \(roundedValue4)")
        
        let roundedValue5 = MiddleEnd.rounded(.up)
        print("current item rounded \(roundedValue5)")

        
        starttime.text = timeFormatted(roundedValue1, isWithMinutes: false)
        endtime.text = timeFormatted(roundedValue2, isWithMinutes: false)
        middle1.text = timeFormatted(roundedValue4, isWithMinutes: false)
        middle2.text = timeFormatted(roundedValue5, isWithMinutes: false)
    }
    
    func timeFormatted(_ interval: CGFloat, isWithMinutes: Bool) -> String? {
        
        var milliseconds = UInt(interval * 1000)
        var seconds = milliseconds / 1000
        milliseconds %= 1000
        let minutes = seconds / 60
        seconds %= 60
        //    unsigned long hours = minutes / 60;
        //    minutes %= 60;
        
        var strMillisec = NSNumber(value: milliseconds).stringValue
        if strMillisec.count > 2 {
            strMillisec = (strMillisec as NSString).substring(to: 2)
        }
        
        if isWithMinutes {
            return String(format: "%02ld:%02ld:%02ld", Int(minutes), Int(seconds), Int(strMillisec) ?? 0)
        } else {
            return String(format: "%02ld:%02ld", Int(minutes), Int(seconds))
        }
    }
    
    func startPlaybackTimeChecker() {
        
        stopPlaybackTimeChecker()
        playbackTimeCheckerTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                                        selector:
                                                            #selector(EditVideo.onPlaybackTimeChecker), userInfo: nil, repeats: true)
    }
    
    func stopPlaybackTimeChecker() {
        
        playbackTimeCheckerTimer?.invalidate()
        playbackTimeCheckerTimer = nil
    }
    
    @objc func onPlaybackTimeChecker() {
        let curTime = player.currentTime()
        var seconds = CMTimeGetSeconds(curTime)
        if seconds < 0 {
            seconds = Float64(0) // this happens! dont know why.
        }
        
       

        
        let widhthh = imageView.frame.size.width
        
        var sec = CGFloat(seconds)
         
        
        
       
        
        let duration = player.currentItem?.asset.duration
                             var secondss: CGFloat?
                             if let duration = duration {
                                 secondss = CGFloat(CMTimeGetSeconds(duration))
                             }
        
        let point = widhthh / (secondss ?? 0)

        print(point)
        
        

        self.paybackview?.frame.origin.x = point*startTime + imageView.frame.origin.x

             print(point*startTime)
        
         sec = sec - startTime
        let viewwidth = point*sec

           



        UIView.animate(withDuration: 0.1, animations: {
        self.paybackview!.frame.size.width = viewwidth
            })
        
        
        videoPlaybackPosition = CGFloat(seconds)
        trimmerView.seek(toTime: CGFloat(seconds))
        if videoPlaybackPosition >= stopTime - 0.1{
            videoPlaybackPosition = startTime
            seekVideo(toPos: startTime)
            trimmerView.seek(toTime: startTime)
            playnew.setImage(UIImage.init(named: "play"), for: .normal)
            player.pause()
        }
        
        
        
        if videoPlaybackPosition < startTime{
            videoPlaybackPosition = startTime + 0.1
                   seekVideo(toPos: videoPlaybackPosition)
                   trimmerView.seek(toTime: videoPlaybackPosition )
                   playnew.setImage(UIImage.init(named: "pause"), for: .normal)
                   player.play()
               }
    }
    
    @objc func itemDidFinishPlaying(_ notification: Notification) {
        let startTimer = CMTimeMake(Int64(startTime), 1)
        player.seek(to: startTimer)
        playnew.setImage(UIImage.init(named: "play"), for: .normal)
        player.pause()
    }
    
    func play(url:URL , iscut:Bool){
        

        
        let waveformImageDrawer = WaveformImageDrawer()
            waveformImageDrawer.waveformImage(fromAudioAt:url,
                                                 size: imageView!.bounds.size,
                                          style: .striped,
                                          position: .middle) { image in
            // need to jump back to main queue
            DispatchQueue.main.async {
                self.imageView!.image = image
            }
        }
        
        
        if player == nil {
            
        }else{
            player.pause()
            controller.removeFromParentViewController()
            controller.view.removeFromSuperview()
            //  trimmerView.removeFromSuperview()
        }
        app.HideProgress()
        
        finalurl = url
        trimmerView.iscut = iscut
        trimmerView.asset = AVAsset.init(url: url)
        trimmerView.themeColor = UIColor.lightGray
        trimmerView.showsRulerView = false
        trimmerView.maxLength = 2000
        trimmerView.rulerLabelInterval = 10
        trimmerView.trackerColor = UIColor.clear
        trimmerView.delegate = self
        trimmerView.resetSubviews()
        trimmerView.showsRulerView = false
        trimmerView.hideTracker(true)
        trimmerView.thumbWidth = 15
        
        
        
        let playerItem = AVPlayerItem(asset:AVAsset.init(url: url))
        player = AVPlayer(playerItem: playerItem)
        Movieview.backgroundColor = UIColor.clear
        let width = UIScreen.main.bounds.size.width
        controller.showsPlaybackControls = false
        if IS_IPHONE {
            
            controller.view.frame = CGRect(x: Movieview.frame.origin.x, y: 72, width: width, height: width * 9.0 / 16.0)
            playnew.frame = CGRect(x: Movieview.frame.origin.x, y: 72, width: width, height: width * 9.0 / 16.0)
        }else{
            controller.view.frame = CGRect(x: Movieview.frame.origin.x, y: Movieview.frame.origin.y, width: width, height: 300)
            playnew.frame = CGRect(x: Movieview.frame.origin.x, y: Movieview.frame.origin.y, width: width, height: 300)
        }
        
        addChildViewController(controller)
        view.addSubview(controller.view)
        
        
        playnew.isHidden = false
        playnew.setImage(UIImage.init(named: "play"), for: .normal)
        playnew.backgroundColor = .clear
        playnew.addTarget(self, action: #selector(playnewmethod), for: .touchUpInside)
        playnew.tintColor = UIColor.clear
        self.view.addSubview(playnew)
        self.view.bringSubview(toFront: playnew)
        
        controller.player = player
        player.pause()
        NotificationCenter.default.addObserver(self, selector: #selector(EditVideo.itemDidFinishPlaying(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        
        
        
        
        mytimer?.invalidate()
        mytimer = nil
        
        
        mytimer = Timer.scheduledTimer(
            timeInterval: 0.1,
            target: self,
            selector: #selector(seekToTime),
            userInfo: nil,
            repeats: true)
        
        
        
        Timer.scheduledTimer(
            timeInterval: 0.1,
            target: self,
            selector: #selector(checkifplaying),
            userInfo: nil,
            repeats: true)
        
        
        tracker1.layer.masksToBounds = true
        tracker1.layer.cornerRadius = 2
         dragtrimmer = true
        
    }
    
    @objc  func checkifplaying() {
        if player.rate != 0, player.error == nil {
            if self.mytimer == nil {
                dragtrimmer = true
                self.mytimer = Timer.scheduledTimer(
                        timeInterval: 0.1,
                        target: self,
                        selector: #selector(self.seekToTime),
                        userInfo: nil,
                        repeats: true)
                }
            }
        }
      
    
    
    
    
    
    @objc func seekToTime() {
        
        if dragtrimmer == false {
          
            
            return
     
        
        }
        
        
        let duration = player.currentItem?.asset.duration
        let currentitem = CGFloat(CMTimeGetSeconds(player.currentTime()))

        var seconds: Float = 0
        if let duration = duration {
            seconds = Float(CMTimeGetSeconds(duration))
        }
        let widhthh = imageView.frame.size.width
        let point = widhthh / CGFloat(seconds)
        let durationnew = CGFloat(abs(prevTrackerTime - CGFloat(currentitem)))
        
        
        let animate = (durationnew>1) ? false : true
        prevTrackerTime = CGFloat(currentitem)
         let posToMove =   point * CGFloat(currentitem)
       
       print("current item \(currentitem)")
        
        let roundedValue4 = currentitem.rounded(.up)
        print("current item rounded \(roundedValue4)")

        progresstime.text = "\(Int(roundedValue4))"

        var trackerFrame = trackerView.frame
      
        if currentitem <= 0 {
             trackerFrame.origin.x = trimmmerframe!

        }else{
            
            if flagcut == false {
                trackerFrame.origin.x = posToMove + imageView.frame.origin.x-11

                
            }else{
                
              if currentitem > self.startTime && currentitem < self.stopTime {

             

                
                  if currentitem > self.Middle1 && currentitem < self.Middle2 {
                    self.seekVideo(toPos: CGFloat(Float(self.Middle2)+0.5))

                  }else{
                    trackerFrame.origin.x = posToMove + imageView.frame.origin.x-11

                }
                 
              
              }else{
                trackerFrame.origin.x = posToMove + imageView.frame.origin.x-11

                }
                
            }

        }
        if animate {
            UIView.animate(withDuration: TimeInterval(durationnew), delay: 0, options: [.curveLinear, .beginFromCurrentState], animations: { [self] in
                self.trackerView.frame = trackerFrame
                self.trackerView.layer.masksToBounds = true
                self.trackerView.layer.cornerRadius = 2
                self.view.addSubview(self.trackerView)
             //   self.view.bringSubview(toFront: self.trimmerView)

            })
        } else {
            trackerView.frame = trackerFrame
            trackerView.layer.masksToBounds = true
            trackerView.layer.cornerRadius = 2
            view.addSubview(trackerView)
           // self.view.bringSubview(toFront: self.trimmerView)

        }
    }
    
    func buttonframe() {
        
        if IS_IPHONE {
            let width = UIScreen.main.bounds.size.width
            var buttonframe = cut.frame
            buttonframe.origin.y = 72 + width * 9.0 / 16.0
            cut.frame = buttonframe
            view.addSubview(cut)
            var buttonframe1 = trim.frame
            buttonframe1.origin.y = 72 + width * 9.0 / 16.0
            trim.frame = buttonframe1
            view.addSubview(trim)
            let movieframe = CGRect(x: Movieview.frame.origin.x, y: 72, width: width, height: width * 9.0 / 16.0)
            Movieview.frame = movieframe
            view.addSubview(Movieview)
        }
        
    }
    
    @objc func playnewmethod(sender : UIButton){
        
        
        if player.isPlaying {
            dragtrimmer = true
            playnew.setImage(UIImage.init(named: "play"), for: .normal)
            player.pause()
            trimmerView.hideTracker(true)
            stopPlaybackTimeChecker()
            self.view.bringSubview(toFront: playnew)
            
            
              
        } else {
            dragtrimmer = true
            trimmerView.hideTracker(true)
            playnew.setImage(UIImage.init(named: "pause"), for: .normal)
            self.view.bringSubview(toFront: playnew)
            player.play()
            startPlaybackTimeChecker()
        }
        
        self.trimmerView.hideTracker(true)
    }
    
    @IBAction func btnsave(_ sender: Any) {
        
        progresstime.text = "0"

        if player.isPlaying {
            dragtrimmer = true
            playnew.setImage(UIImage.init(named: "play"), for: .normal)
            player.pause()
            trimmerView.hideTracker(true)
            stopPlaybackTimeChecker()
            self.view.bringSubview(toFront: playnew)
        }
        
        app.ShowProgress()
        
        
        if flagcut == true {
           self.cutfirsthalf()
        }else{
            self.trimvideo()
        }
    }
    
    @IBAction func btndelete(_ sender: Any) {
        
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
            kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
            kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
            showCloseButton: false,
            dynamicAnimatorActive: true,
            buttonsLayout: .horizontal
        )
        let alert = SCLAlertView(appearance: appearance)
        _ = alert.addButton("Cancel"){
        }
        _ = alert.addButton("Yes") {
            
            var flag = 0
            let fetchOptions = PHFetchOptions()
            fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
            let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
            
            assets.enumerateObjects { (obj, idx, bool) -> Void in
                if flag == 0{
                    flag = 1
                    self.navigationController?.popViewController(animated: false)
                    self.delegate?.newvideo(self, file: obj, delete: true)
                }
            }
            
        }
        
        let icon = UIImage(named:"deleteWhite.png")
        let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
        
        _ = alert.showCustom("Alert", subTitle: "Are you sure you want to delete this?", color: color, circleIconImage: icon!)
        
        
        
        
        
        
    }
    
    func mix(videoUrl: URL) {
        
        let videoAsset = AVAsset(url: videoUrl)
        
        let audioVideoComposition = AVMutableComposition()
        
        let audioMix = AVMutableAudioMix()
        var mixParameters = [AVMutableAudioMixInputParameters]()
        
        let videoCompositionTrack = audioVideoComposition
            .addMutableTrack(withMediaType: .video, preferredTrackID: .init())!
        
        let audioCompositionTrack = audioVideoComposition
            .addMutableTrack(withMediaType: .audio, preferredTrackID: .init())!
        
        
        
        let videoAssetTrack = videoAsset.tracks(withMediaType: .video)[0]
        let audioAssetTrack = videoAsset.tracks(withMediaType: .audio).first
        
        let audioParameters = AVMutableAudioMixInputParameters(track: audioAssetTrack)
        audioParameters.trackID = audioCompositionTrack.trackID
        
        
        
        audioParameters.setVolume(volumes.value, at: kCMTimeZero)
        
        mixParameters.append(audioParameters)
        
        audioMix.inputParameters = mixParameters
        
        /// prevents video from unnecessary rotations
        videoCompositionTrack.preferredTransform = videoAssetTrack.preferredTransform
        
        do {
            let timeRange = CMTimeRange(start: kCMTimeZero, duration: videoAsset.duration)
            
            try videoCompositionTrack.insertTimeRange(timeRange, of: videoAssetTrack, at: kCMTimeZero)
            
            if let audioAssetTrack = audioAssetTrack {
                try audioCompositionTrack.insertTimeRange(timeRange, of: audioAssetTrack, at: kCMTimeZero)
            }
            
        } catch {
            
        }
        
        
        
        var tmpDirectory: [String]? = nil
        do {
            tmpDirectory = try FileManager.default.contentsOfDirectory(atPath: NSTemporaryDirectory())
        } catch {
        }
        for file in tmpDirectory ?? [] {
            do {
                try FileManager.default.removeItem(atPath: "\(NSTemporaryDirectory())\(file)")
            } catch {
            }
        }
        let tempDir = NSTemporaryDirectory()
        let exportUrl = URL(fileURLWithPath: tempDir).appendingPathComponent("mixvolume.mp4")
        
        print(exportUrl)
        
        
        let exportSession = AVAssetExportSession(
            asset: audioVideoComposition,
            presetName: AVAssetExportPresetHighestQuality
        )
        
        exportSession?.audioMix = audioMix
        exportSession?.outputFileType = .mov
        exportSession?.outputURL = exportUrl
        
        exportSession?.exportAsynchronously(completionHandler: {
            guard let status = exportSession?.status else { return }
            
            switch status {
            case .completed:
                print(exportUrl)
                DispatchQueue.main.async {
                    UISaveVideoAtPathToSavedPhotosAlbum((exportUrl.path), self, #selector(EditVideo.video(videoPath:didFinishSavingWithError:contextInfo:)), nil)
                }
                
            case .failed:
                DispatchQueue.main.async {
                    UISaveVideoAtPathToSavedPhotosAlbum((self.finalurl!.path), self, #selector(EditVideo.video(videoPath:didFinishSavingWithError:contextInfo:)), nil)
                }
                break
                
            default:
                print(status)
            }
        })
    }
    
    @objc func video(videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject)
    {
        if let _ = error {
            print("Error,Video failed to save")
        }else{
            print("Successfully,Video was saved")
            self.getvideo()
        }
    }
    
    func getvideo(){
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.video, options: fetchOptions)
        let lastPHAsset = assets.lastObject
        self.app.HideProgress()
        self.navigationController?.popViewController(animated: false)
        self.delegate?.newvideo(self, file: lastPHAsset!, delete: false)
        
    }
    
    func trimvideo(){
        let length = Float(asset!.duration.value) / Float(asset!.duration.timescale)
        print("video length: \(length) seconds")
        
        let tempDir = NSTemporaryDirectory()
        let outputURL = URL(fileURLWithPath: tempDir).appendingPathComponent("firstpart.mp4")
        
        if FileManager.default.fileExists(atPath: outputURL.path) {
            do {
                try FileManager.default.removeItem(atPath: outputURL.path)
            }catch{
            }
        }
        
        print(outputURL)
        
        guard let exportSession = AVAssetExportSession(asset: asset!, presetName: AVAssetExportPresetPassthrough) else { return }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mp4
        
        let timeRange = CMTimeRange(start: CMTime(seconds: (Double(startTime)), preferredTimescale: 1000),
                                    end: CMTime(seconds: (Double(stopTime)), preferredTimescale: 1000))
        
        exportSession.timeRange = timeRange
        exportSession.exportAsynchronously {
            switch exportSession.status {
            case .completed:
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    self.asset = AVAsset(url:outputURL)
                    let duration = self.asset!.duration
                    let durationTime = CMTimeGetSeconds(duration)
                    print("the duration is",durationTime)
                    
                    if self.asset != nil {
                        self.mix(videoUrl: outputURL)
                    }else{
                        self.navigationController?.popViewController(animated: true)
                    }
                })
                
            case .failed:
                print("failed \(exportSession.error.debugDescription)")
            case .cancelled:
                print("cancelled \(exportSession.error.debugDescription)")
            default: break
            }
        }
    }
    
    func cutfirsthalf() {
        let length = Float(asset!.duration.value) / Float(asset!.duration.timescale)
        print("video length: \(length) seconds")
        
        let tempDir = NSTemporaryDirectory()
        let outputURL = URL(fileURLWithPath: tempDir).appendingPathComponent("firstpart.mp4")
        
        if FileManager.default.fileExists(atPath: outputURL.path) {
            do {
                try FileManager.default.removeItem(atPath: outputURL.path)
            }catch{
                
            }
        }
        
        print(outputURL)
        
        guard let exportSession = AVAssetExportSession(asset: asset!, presetName: AVAssetExportPresetPassthrough) else { return }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mp4
        
        let timeRange = CMTimeRange(start: CMTime(seconds: (Double(startTime)), preferredTimescale: 1000),
                                    end: CMTime(seconds: (Double(Middle1)), preferredTimescale: 1000))
        
        exportSession.timeRange = timeRange
        exportSession.exportAsynchronously {
            switch exportSession.status {
            case .completed:
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    self.firstpartasset = AVAsset(url:outputURL)
                    self.cutsecondhalf()
                })
                
            case .failed:
                print("failed \(exportSession.error.debugDescription)")
            case .cancelled:
                print("cancelled \(exportSession.error.debugDescription)")
            default: break
            }
        }
    }
    
    func cutsecondhalf(){
        let length = Float(asset!.duration.value) / Float(asset!.duration.timescale)
        print("video length: \(length) seconds")
        
        let tempDir = NSTemporaryDirectory()
        let outputURL = URL(fileURLWithPath: tempDir).appendingPathComponent("Secondpart.mp4")
        
        if FileManager.default.fileExists(atPath: outputURL.path) {
            do {
                try FileManager.default.removeItem(atPath: outputURL.path)
            }catch{
            }
        }
        
        print(outputURL)
        
        guard let exportSession = AVAssetExportSession(asset: asset!, presetName: AVAssetExportPresetPassthrough) else { return }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mp4
        
        let timeRange = CMTimeRange(start: CMTime(seconds: (Double(Middle2)), preferredTimescale: 1000),
                                    end: CMTime(seconds: (Double(stopTime)), preferredTimescale: 1000))
        
        exportSession.timeRange = timeRange
        exportSession.exportAsynchronously {
            switch exportSession.status {
            case .completed:
                DispatchQueue.main.async {
                    self.secondpartasset  = AVAsset(url:outputURL)
                    self.mergeTwoVideosArry(arrayVideos: [self.firstpartasset!,self.secondpartasset!])
                }
            case .failed:
                print("failed \(exportSession.error.debugDescription)")
            case .cancelled:
                print("cancelled \(exportSession.error.debugDescription)")
            default: break
            }
        }
    }
    
    func orientationFromTransform(transform: CGAffineTransform) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
      
        
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
            assetOrientation = .right
            isPortrait = true
        } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
            assetOrientation = .left
            isPortrait = true
      
        } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
           
            assetOrientation = .up
      
        
        } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
            assetOrientation = .down
       
        }
        return (assetOrientation, isPortrait)
    }
    
    func mergeTwoVideosArry(arrayVideos:[AVAsset]){
        
      let mainComposition = AVMutableComposition()
      let compositionVideoTrack = mainComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
      let soundtrackTrack = mainComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        var insertTime = kCMTimeZero
          

          if isportrait! {
                      compositionVideoTrack?.preferredTransform = CGAffineTransform(rotationAngle: .pi/2 )
                      }
          
          
      for videoAsset in arrayVideos {
        try! compositionVideoTrack?.insertTimeRange(CMTimeRangeMake(kCMTimeZero, videoAsset.duration), of: videoAsset.tracks(withMediaType: .video)[0], at: insertTime)
          
          let clipVideoTrack = videoAsset.tracks(withMediaType: AVMediaType.video).last
          
          let transform = clipVideoTrack!.preferredTransform
          let assetInfo = orientationFromTransform(transform: transform)
        
        if !isportrait! {

          if assetInfo.orientation == .down {

              compositionVideoTrack?.preferredTransform = CGAffineTransform(rotationAngle: .pi )
          }else{

              compositionVideoTrack?.preferredTransform = videoAsset.preferredTransform

          }
        }
        
      
        
        
          

        try! soundtrackTrack?.insertTimeRange(CMTimeRangeMake(kCMTimeZero, videoAsset.duration), of: videoAsset.tracks(withMediaType: .audio)[0], at: insertTime)
          insertTime = CMTimeAdd(insertTime, videoAsset.duration)
      }
         
        
        //Create Directory path for Save
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        var outputURL = documentDirectory.appendingPathComponent("MergeTwoVideos")
        do {
            try FileManager.default.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
            outputURL = outputURL.appendingPathComponent("\(outputURL.lastPathComponent).mp4")
        }catch let error {
            print(error.localizedDescription)
        }
        
        //Remove existing file
        self.deleteFile(outputURL)
        
        //export the video to as per your requirement conversion
        if let exportSession = AVAssetExportSession(asset: mainComposition, presetName: AVAssetExportPresetHighestQuality) {
            exportSession.outputURL = outputURL
            exportSession.outputFileType = AVFileType.mov
            exportSession.shouldOptimizeForNetworkUse = true
            
            /// try to export the file and handle the status cases
            exportSession.exportAsynchronously(completionHandler: {
                switch exportSession.status {
                case .completed :
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {

                        self.mix(videoUrl: outputURL)

                    })
                    
                case .failed:
                    if let _error = exportSession.error?.localizedDescription {
                        print(_error)
                    }
                case .cancelled:
                    if let _error = exportSession.error?.localizedDescription {
                        print(_error)
                    }
                default:
                    if let _error = exportSession.error?.localizedDescription {
                        print(_error)
                    }
                }
            })
        }
    }
    
    func deleteFile(_ filePath:URL) {
        guard FileManager.default.fileExists(atPath: filePath.path) else {
            return
        }
        do {
            try FileManager.default.removeItem(atPath: filePath.path)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
    
    @IBAction func btntrim(_ sender: Any) {
        middle1.isHidden = true
        middle2.isHidden = true
        btndone.isHidden = true
        flagcut = false
        tracker1.isHidden = true

        trim.backgroundColor = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
        trim.setTitleColor(UIColor.white, for: .normal)
        cut.backgroundColor = UIColor.white
        cut.setTitleColor(UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0), for: .normal)
        
        var trackerFrame = trackerView.frame
        trackerFrame.origin.x = trimmmerframe!
        trackerView.layer.masksToBounds = true
        trackerView.layer.cornerRadius = 2
        view.addSubview(trackerView)
        self.play(url: finalurl!, iscut: flagcut)
    }
    
    @IBAction func btndone(_ sender: Any) {
        app.ShowProgress()
        
        if flagcut == true {
            self.cutfirsthalf()
        }else{
            self.trimvideo()
        }
    }
    
    @IBAction func btnback(_ sender: Any) {
     
        
        if player.isPlaying {
            dragtrimmer = true
            playnew.setImage(UIImage.init(named: "play"), for: .normal)
            player.pause()
            trimmerView.hideTracker(true)
            stopPlaybackTimeChecker()
            self.view.bringSubview(toFront: playnew)
        }
        
        self.navigationController?.popViewController(animated: true)
   
    }
    
    @IBAction func volumeslider(_ sender: Any) {
        self.player.volume = volumes.value
        print(volumes.value)
    }
    
    @IBAction func btncut(_ sender: Any) {
        middle1.isHidden = false
        middle2.isHidden = false
        //btndone.isHidden = false
        flagcut = true
        tracker1.isHidden = true
        
       
        
       var trackerFrame = trackerView.frame
        trackerFrame.origin.x = trimmmerframe!
        trackerView.layer.masksToBounds = true
        trackerView.layer.cornerRadius = 2
       view.addSubview(trackerView)
        
        cut.backgroundColor = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
        cut.setTitleColor(UIColor.white, for: .normal)
        trim.backgroundColor = UIColor.white
        trim.setTitleColor(UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0), for: .normal)
        self.play(url: finalurl!, iscut: flagcut)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if !IS_IPHONE{
            self.perform(#selector(resetplayer), with: nil, afterDelay: 0.1)
            self.perform(#selector(changeloader), with: nil, afterDelay: 0.2)
        }
    }
    
    @objc func changeloader(){
        app.initializeLoader()
    }
    
    @objc func resetplayer(){
        if finalurl != nil {
            self.play(url: finalurl!, iscut: flagcut)
        }
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}
