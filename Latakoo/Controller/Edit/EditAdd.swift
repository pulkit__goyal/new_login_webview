//
//  EditAdd.swift
//  Latakoo
//
//  Created by Mayur Sardana on 18/06/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//

import UIKit
import Photos

protocol EditAdddelegate:class {
    func AddFiles(_ controller: EditAdd, file: [PHAsset])
}

class EditAdd: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    weak var delegate: EditAdddelegate?

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return all.count;
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"cell", for: indexPath) as! librarycell
        
        cell.imgthumb.clipsToBounds = true
        cell.imgthumb.contentMode = .scaleAspectFill
        
        cell.lblcount.layer.cornerRadius = cell.lblcount.frame.size.width/2
            cell.lblcount.layer.borderColor = UIColor.white.cgColor
            cell.lblcount.layer.borderWidth = 1
            cell.lblcount.layer.masksToBounds = true
             
        
            
            
            
            
            if let asset = all[indexPath.item] as? PHAsset {
                switch asset.mediaType {
                case .image:
                    
                    cell.overlayview.isHidden = true
                    cell.lblduration.isHidden = true
                    cell.lblvideoname.isHidden = true
                    cell.imgvideo.isHidden = true
                    let manager = PHImageManager.default()
                    let option = PHImageRequestOptions()
                    option.isNetworkAccessAllowed = true

                    var thumbnail = UIImage()
                    option.isSynchronous = true
                    
                    
                    manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
                        let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                        cell.imgthumb.image = thumbnail
                        
                    })
                    
                    
                    break
                    
                    
                    
                case .video:
                    
                    cell.overlayview.isHidden = false
                    cell.lblduration.isHidden = false
                    cell.lblvideoname.isHidden = false
                    cell.imgvideo.isHidden = false
                    
                    let manager = PHImageManager.default()
                    let option = PHImageRequestOptions()
                    option.isNetworkAccessAllowed = true

                    var thumbnail = UIImage()
                    option.isSynchronous = true
                    manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
                        let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                        cell.imgthumb.image = thumbnail
                        
                        
                    })
                    
                    DispatchQueue.global(qos: .background).async {

                    let resources = PHAssetResource.assetResources(for: asset)
                    let orgFilename = (resources[0]).originalFilename
                        DispatchQueue.main.async{

                    cell.lblvideoname.text = orgFilename
                    
                        }
                    }
                    
                    
                    
                    
                    DispatchQueue.global(qos: .background).async {
                        let option1 = PHVideoRequestOptions()
                        option1.isNetworkAccessAllowed = true
                        option1.deliveryMode = .highQualityFormat
                        option1.version = .original
                        option1.progressHandler = {  (progress, error, stop, info) in
                               
                        
                        }

                        var resultAsset: AVAsset?
                        PHCachingImageManager.default().requestAVAsset(forVideo: asset, options: option1, resultHandler: { avasset, audioMix, info in
                            resultAsset = avasset
                            let duration = resultAsset?.duration
                            let durationTime = CMTimeGetSeconds(duration ??  CMTimeMake(1, 1))
                            print(durationTime)
                            DispatchQueue.main.async{
                                cell.lblduration.text = self.convertTime(Int(durationTime))
                            }
                        })
                    }
                    
                    break
                    
                
                    
                default:
                    
                    break
                }
            }

            
             let asset = all[indexPath.item]

            let resourceArray = PHAssetResource.assetResources(for: asset)
                      let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false // If this returns NO, then the asset is in iCloud and not saved locally yet
                       if bIsLocallayAvailable {
                           cell.imgcloud.isHidden = true
                       }else{
                           cell.imgcloud.isHidden = false
                       }
            
            if FilesArray.contains(all[indexPath.item]) {
                           cell.viewselection.isHidden = false
                           cell.lblcount.isHidden = false
                           cell.imgbigcheck.isHidden = false
                           cell.lblcount.text = "\(FilesArray.index(of: all[indexPath.item])!+1)"
                       }else{
                            cell.viewselection.isHidden = true
                            cell.lblcount.isHidden = true
                            cell.imgbigcheck.isHidden = true
                       }
    
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if IS_IPHONE{
                  return CGSize(width:collectionView.frame.size.width/3, height: 110)
              }else{
                  return CGSize(width:collectionView.frame.size.width/3, height: 200)
              }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
            if let index = FilesArray.index(of: all[indexPath.item]) {
            FilesArray.remove(at: index)
        }else{
            FilesArray.append(all[indexPath.item])
        }
            
        
        let index = IndexPath(row: indexPath.item, section: 0)
          mycollectionview.reloadItems(at:[index])
               
       
    }
    
    @IBOutlet weak var mycollectionview: UICollectionView!
    let app = UIApplication.shared.delegate as! AppDelegate

    
    
    var all : [PHAsset] = []
    var FilesArray : [PHAsset] = []
    @IBOutlet weak var btnvideo: UIButton!
    @IBOutlet weak var btnimage: UIButton!
       var image : [PHAsset] = []
       var video : [PHAsset] = []
       var flagmedia = 0
    let appdelegate = UIApplication.shared.delegate as! AppDelegate

    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        
        
              FilesArray  = []
               flagmedia = 0
              if appdelegate.video.count>0 {
                  video = appdelegate.video
                   all = video
                  mycollectionview.reloadData()
                  mycollectionview.setContentOffset(CGPoint.zero, animated: true)

              }else{
                self.perform(#selector(getfiles), with: nil, afterDelay:0.1)

              }
              
              self.changecolor()
              btnvideo.setBackgroundImage(UIImage.init(named:"purpule_box"), for:.normal)
              btnvideo.setTitleColor(UIColor.white, for: .normal)
        

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnvideo(_ sender: Any) {
         FilesArray  = []
         flagmedia = 0
        if appdelegate.video.count>0 {
            video = appdelegate.video
            all = video
            mycollectionview.reloadData()
            mycollectionview.setContentOffset(CGPoint.zero, animated: true)

        }else{
            self.getvideo()
        }
        
        self.changecolor()
        btnvideo.setBackgroundImage(UIImage.init(named:"purpule_box"), for:.normal)
        btnvideo.setTitleColor(UIColor.white, for: .normal)
    }
    
    @IBAction func btnimage(_ sender: Any) {
        FilesArray  = []
        flagmedia = 1
      if appdelegate.image.count>0 {
           image = appdelegate.image
           all = image
           mycollectionview.reloadData()
        mycollectionview.setContentOffset(CGPoint.zero, animated: true)

           }else{
           self.getimages()
           }

        self.changecolor()
        btnimage.setBackgroundImage(UIImage.init(named:"purpule_box.png"), for:.normal)
        btnimage.setTitleColor(UIColor.white, for: .normal)
    }
    
    
    
    func getimages(){
           image = []
           
           let fetchOptions = PHFetchOptions()
           fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
           let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
           
           assets.enumerateObjects { (obj, idx, bool) -> Void in
              // self.image.append(obj)
               self.image.insert(obj, at: 0)

           }
           print(image)
          all = image
        
           mycollectionview.reloadData()
           mycollectionview.setContentOffset(CGPoint.zero, animated: true)

       }
       func getvideo(){
           video = []
           let fetchOptions = PHFetchOptions()
           fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
           
           let assets = PHAsset.fetchAssets(with: PHAssetMediaType.video, options: fetchOptions)
           
           assets.enumerateObjects { (obj, idx, bool) -> Void in
               //self.video.append(obj)
               self.video.insert(obj, at: 0)

           }
           print(video)
           all = video
           mycollectionview.reloadData()
           mycollectionview.setContentOffset(CGPoint.zero, animated: true)

       }
    
    
     func changecolor(){
         
         btnvideo.setBackgroundImage(UIImage.init(named:"white_box_button"), for:.normal)
         btnvideo.setTitleColor(UIColor.init(named:"pilotcolor"), for: .normal)
         
         
         btnimage.setBackgroundImage(UIImage.init(named:"white_box.png"), for:.normal)
         btnimage.setTitleColor(UIColor.init(named:"pilotcolor"), for: .normal)
         
         }
    
    @objc func getfiles(){
        
        
        
        let status = PHPhotoLibrary.authorizationStatus()
                         
                         if (status == PHAuthorizationStatus.authorized) {
                          flagmedia = 0
                            self.getvideo()

                         }
                             
                         else if (status == PHAuthorizationStatus.denied) {

                    _ = SCLAlertView().showError("LATAKOO", subTitle:"Please give this app permission to access your photo library in your settings app!", closeButtonTitle:"OK")
                             
                         }
               }
        
             
   
    
    
    @IBAction func btnnext(_ sender: Any) {
         self.navigationController?.popViewController(animated: false)
        if FilesArray.count>0{
            delegate?.AddFiles(self, file: FilesArray)
        }

    }
    @IBAction func btnback(_ sender: Any) {
          
        self.navigationController?.popViewController(animated: true)
      }
    
    
   
    
   
    
    
    
    
    func convertTime(_ time: Int) -> String? {
        let minutes = time / 60
        let seconds = time % 60
        return String(format: "%02ld:%02ld", minutes, seconds)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
           if !IS_IPHONE{
               self.perform(#selector(resetplayer), with: nil, afterDelay: 0.1)
           }
       }
       
       
       @objc func resetplayer(){
           if mycollectionview != nil{
                      mycollectionview.reloadData()
                  }       }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
}
