import UIKit
import Photos
import MobileCoreServices


class EditfilenameVC: UIViewController, UICollectionViewDelegateFlowLayout
, UICollectionViewDataSource, UICollectionViewDelegate, ReorderCollectionViewDelegate {

    @IBOutlet weak var txtname: UITextField!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var lblduration: UILabel!

    func finishReorderingItem(_ fromItem: Any!, toItemAt toIndexPath: IndexPath!, needReorder: Bool) {

        if (needReorder) {
            mycollectionview.reloadData()
            for i in 0..<all.count {
                if all[i] == fromItem as! PHAsset {
                    all.swapAt(i, toIndexPath.item)
                }
            }
        } else {
            mycollectionview.reloadData()
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return all.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReorderCollectionViewCell", for: indexPath) as! ReorderCollectionViewCell

        let asset = all[indexPath.item]
        cell.representedObject = asset
        
        cell.imgthumb.clipsToBounds = true
        cell.imgthumb.contentMode = .scaleAspectFill
        cell.imgvideo.clipsToBounds = true
        cell.imgvideo.contentMode = .scaleAspectFill

        switch asset.mediaType {
        case .image:
            cell.lblduration.isHidden = true
            cell.imgvideo.isHidden = true
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.isNetworkAccessAllowed = true

            var thumbnail = UIImage()
            option.isSynchronous = true
            manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: option, resultHandler: { (result, info) -> Void in
                    let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                    cell.imgthumb.image = thumbnail
                })
            break

        case .video:
            cell.lblduration.isHidden = false
            cell.imgvideo.isHidden = false

            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.isNetworkAccessAllowed = true

            var thumbnail = UIImage()
            option.isSynchronous = true
            manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: option, resultHandler: { (result, info) -> Void in
                    let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                    cell.imgthumb.image = nil
                    cell.imgthumb.image = thumbnail
                })

            DispatchQueue.global(qos: .background).async {
                let option1 = PHVideoRequestOptions()
                option1.isNetworkAccessAllowed = true
                option1.deliveryMode = .highQualityFormat
                option1.version = .original
        
                option1.progressHandler = { (progress, error, stop, info) in
                }
                var resultAsset: AVAsset?
                PHCachingImageManager.default().requestAVAsset(forVideo: asset, options: option1, resultHandler: { avasset, audioMix, info in
                    resultAsset = avasset
                    let duration = resultAsset?.duration
                    let durationTime = CMTimeGetSeconds(duration ?? CMTimeMake(1, 1))
                    print(durationTime)
                    DispatchQueue.main.async {
                        cell.lblduration.text = self.convertTime(Int(durationTime))
                    }
                })
            }
            break
        default:
            break
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }

    var all: [PHAsset] = []

    @IBOutlet weak var mycollectionview: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UserDefaults.standard.set("5", forKey: "imageduration")
        slider.value = 5
    }

    func convertTime(_ time: Int) -> String? {
        let minutes = time / 60
        let seconds = time % 60
        return String(format: "%02ld:%02ld", minutes, seconds)
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }

    @IBAction func btnnext(_ sender: Any) {
        if txtname.text == "" {
            let appearance = SCLAlertView.SCLAppearance(dynamicAnimatorActive: true)
            _ = SCLAlertView(appearance: appearance).showNotice("Alert", subTitle: "Enter project name", closeButtonTitle: "OK")
        } else {
            var story: UIStoryboard?
            if IS_IPHONE {
                story = UIStoryboard.init(name: "Main", bundle: nil)
            } else {
                story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
            }

            let EditMergeVideoVC = story?.instantiateViewController(withIdentifier: "EditMergeVideoVC") as! EditMergeVideoVC
            EditMergeVideoVC.header = txtname.text ?? ""
            EditMergeVideoVC.arrfiles = all
            EditMergeVideoVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(EditMergeVideoVC, animated: true)
        }
    }

    @IBAction func btnback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func slider(_ sender: Any) {
        lblduration.text = "\(Int(slider.value))"
        UserDefaults.standard.set(lblduration.text, forKey: "imageduration")
        UserDefaults.standard.synchronize()
    }

    func reorderingItemAlpha(_ collectionview: UICollectionView?) -> CGFloat {
        return 0.3
    }
}
