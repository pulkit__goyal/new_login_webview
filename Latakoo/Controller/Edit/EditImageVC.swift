//
//  EditImageVC.swift
//  Latakoo
//
//  Created by Mayur Sardana on 18/06/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//

import UIKit
import CLImageEditor
import Photos

protocol EditImageDelegate:class {
    func newfile(_ controller: EditImageVC, file: PHAsset ,delete: Bool)
}

class EditImageVC: UIViewController,EFImageViewZoomDelegate,CLImageEditorDelegate,CLImageEditorTransitionDelegate, CLImageEditorThemeDelegate{
    @IBOutlet weak var bigimageview: EFImageViewZoom!
    var bigimage : UIImage!
    weak var delegate: EditImageDelegate?
    let App = UIApplication.shared.delegate as? AppDelegate


    override func viewDidLoad() {
        super.viewDidLoad()
        bigimageview.image = bigimage
        bigimageview.contentMode = .left
        bigimageview._delegate = self
        
         let editor = CLImageEditor(image: bigimage, delegate: self)
            
        
        if #available(iOS 13.0, *) {
            if UITraitCollection.current.userInterfaceStyle == .dark {
                  CLImageEditorTheme().backgroundColor = UIColor.black
                  CLImageEditorTheme().toolbarColor = UIColor.black.withAlphaComponent(0.8)
                  CLImageEditorTheme().toolbarTextColor = UIColor.white
                  CLImageEditorTheme().toolIconColor = "white"
                  CLImageEditorTheme().statusBarStyle = UIStatusBarStyle.lightContent

            }
            else {
                      CLImageEditorTheme().backgroundColor = UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
                       CLImageEditorTheme().toolbarColor = UIColor.white.withAlphaComponent(1.0)
                       CLImageEditorTheme().toolbarTextColor = UIColor.black
                       CLImageEditorTheme().toolIconColor = "black"
                       CLImageEditorTheme().statusBarStyle = UIStatusBarStyle.default

            }
        }
    }
    
    
    
    @IBAction func backbutton(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
       
       @IBAction func btnsave(_ sender: Any) {
        
        
        UIImageWriteToSavedPhotosAlbum(bigimageview.image!, self, #selector(EditImageVC.image(imagepath:didFinishSavingWithError:contextInfo:)), nil)

      
        
        
    
    }
    
    
    
    @objc func image(imagepath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        if let _ = error {
            print("Error,image failed to save")
        } else {
            print("Successfully,image was saved")
            getimages()
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    @objc func getimages(){
        
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
        let lastPHAsset = assets.lastObject
         App?.all.insert(lastPHAsset!, at: 0)
          App?.image.insert(lastPHAsset!, at: 0)
        self.navigationController?.popViewController(animated: false)
        self.delegate?.newfile(self, file: lastPHAsset!, delete: false)

       }
    
    
       @IBAction func btnedit(_ sender: Any) {
//        let editor = CLImageEditor(image: bigimage, delegate: self)
//        present(editor!, animated: true)
        
        guard let editor = CLImageEditor(image: bigimage, delegate: self) else {
                   return;
               }
        editor.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        self.present(editor, animated: true, completion: {});
        
        
       }
       
       @IBAction func btndelete(_ sender: Any) {
        let appearance = SCLAlertView.SCLAppearance(
                            kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                            kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                            kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                            showCloseButton: false,
                            dynamicAnimatorActive: true,
                            buttonsLayout: .horizontal
                        )
                        let alert = SCLAlertView(appearance: appearance)
                        _ = alert.addButton("Cancel"){
                        }
                        _ = alert.addButton("Yes") {

                          var flag = 0
                                    let fetchOptions = PHFetchOptions()
                                    fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
                                    let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
                                    
                                    assets.enumerateObjects { (obj, idx, bool) -> Void in
                                     if flag == 0{
                                         flag = 1
                                         self.navigationController?.popViewController(animated: false)
                                         self.delegate?.newfile(self, file: obj, delete: true)
                                     }
                                 }
                        }
                        let icon = UIImage(named:"deleteWhite.png")
                        let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
                        
                        _ = alert.showCustom("Alert", subTitle: "Are you sure you want to delete this?", color: color, circleIconImage: icon!)
    }
    
    
    
    
    
    func imageEditor(_ editor: CLImageEditor!, didFinishEditingWith image: UIImage!) {
        
        editor.dismiss(animated: true, completion: nil)
        self.bigimageview.image = image
    }
    
    func imageEditorDidCancel(_ editor: CLImageEditor!) {
       
        editor.dismiss(animated: true, completion: nil)

    }
    
    func imageEditor(_ editor: CLImageEditor!, didDismissWith imageView: UIImageView!, canceled: Bool) {
        
    }
    
    func imageEditor(_ editor: CLImageEditor!, willDismissWith imageView: UIImageView!, canceled: Bool) {
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
