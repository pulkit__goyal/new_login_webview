

#import "Editor.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreMotion/CoreMotion.h>
#import "SDAVAssetExportSession.h"
@implementation Editor


+ (void)imagetovideo:(UIImage *)image filename:(NSString *)name
                     completion:(void(^)(NSURL *converimageurl, NSError *error))completion
{
    
        
        
        NSError *error = nil;
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSString *documentsDirectory = [NSHomeDirectory()
                                        stringByAppendingPathComponent:@"Documents/bharat"];
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory])
      [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder

    
            NSString *videoOutputPath = [documentsDirectory stringByAppendingPathComponent:@"test_output.mov"];
    

    
            if ([fileMgr removeItemAtPath:videoOutputPath error:&error] != YES)

                
                NSLog(@"Unable to delete file:");
            CGSize imageSize = CGSizeMake(1920, 1080);
            NSUInteger fps = 60;
            AVAssetWriter *videoWriter = [[AVAssetWriter alloc] initWithURL:
                                          [NSURL fileURLWithPath:videoOutputPath] fileType:AVFileTypeQuickTimeMovie
                                                                      error:&error];
            NSParameterAssert(videoWriter);
            NSDictionary *videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                           AVVideoCodecTypeH264, AVVideoCodecKey,
                                           [NSNumber numberWithInt:imageSize.width], AVVideoWidthKey,
                                           [NSNumber numberWithInt:imageSize.height], AVVideoHeightKey,
                                           nil];
            
            
            
            
            AVAssetWriterInput* videoWriterInput = [AVAssetWriterInput
                                                    assetWriterInputWithMediaType:AVMediaTypeVideo
                                                    outputSettings:videoSettings];
            
            AVAssetWriterInputPixelBufferAdaptor *adaptor = [AVAssetWriterInputPixelBufferAdaptor
                                                             assetWriterInputPixelBufferAdaptorWithAssetWriterInput:videoWriterInput
                                                             sourcePixelBufferAttributes:nil];
    
    
    

            NSParameterAssert(videoWriterInput);
            NSParameterAssert([videoWriter canAddInput:videoWriterInput]);
            videoWriterInput.expectsMediaDataInRealTime = NO;
            [videoWriter addInput:videoWriterInput];
            [videoWriter startWriting];
            [videoWriter startSessionAtSourceTime:kCMTimeZero];
            CVPixelBufferRef buffer = NULL;
            int frameCount = 0;
             NSString *duration = [[NSUserDefaults standardUserDefaults]valueForKey:@"imageduration"];
           
       float time = [duration floatValue];
    
    float numberOfSecondsPerFrame = time/2;
       
          if (numberOfSecondsPerFrame == 0) {
             numberOfSecondsPerFrame = 0.5 ;
           }
    
    
            double frameDuration = fps * numberOfSecondsPerFrame;
            NSLog(@"**************************************************");
            for(int i=0; i<2; i++) {
                @autoreleasepool{
                    
                    buffer = [self pixelBufferFromCGImage:[image CGImage] :image.size];
                    
                    BOOL append_ok = NO;
                    int j = 0;
                    while (!append_ok && j < 30) {
                        
                         @autoreleasepool{
                        
                        if (adaptor.assetWriterInput.readyForMoreMediaData)  {
                        //    NSLog(@"Processing video frame (%d,%lu)",frameCount,(unsigned long)[imageArray count]);
                            CMTime frameTime = CMTimeMake(frameCount*frameDuration,(int32_t) fps);
                            append_ok = [adaptor appendPixelBuffer:buffer withPresentationTime:frameTime];
                            
                            CVPixelBufferPoolRef bufferPool = adaptor.pixelBufferPool;
                            
                            
                            NSParameterAssert(bufferPool != NULL);
                            
                            
                            CVPixelBufferRelease(buffer);
                            
                            if(!append_ok){
                                NSError *error = videoWriter.error;
                                if(error!=nil) {
                                    NSLog(@"Unresolved error %@,%@.", error, [error userInfo]);
                                }
                            }
                        }
                        else {
                            printf("adaptor not ready %d, %d\n", frameCount, j);
                            [NSThread sleepForTimeInterval:0.1];
                        }
                        j++;
                        
                         }
                        
                    }
                    if (!append_ok) {
                        printf("error appending image %d times %d\n, with error.", frameCount, j);
                    }
               
                frameCount++;
                
            }
                
            }
        
        
        
        
        
            //  CVBufferRelease(buffer);
            [videoWriterInput markAsFinished];
           [videoWriter finishWritingWithCompletionHandler:^(){
     
           
           NSLog (@"finished writing");
         
          }];
    
            videoWriterInput = nil;
           videoWriter = nil;
            adaptor = nil;
         //   buffer = NULL;
        
            [NSThread sleepForTimeInterval:1.0];
             NSLog(@"Bharat");
            AVMutableComposition* mixComposition = [AVMutableComposition composition];
            NSString *bundleDirectory = [[NSBundle mainBundle] bundlePath];
            NSString *audio_inputFilePath = [bundleDirectory stringByAppendingPathComponent:@"newaudio.mp3"];
            NSURL    *audio_inputFileUrl = [NSURL fileURLWithPath:audio_inputFilePath];
            NSURL    *video_inputFileUrl = [NSURL fileURLWithPath:videoOutputPath];
    
    
            NSString *outputFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mov",name]];
    
            NSURL    *outputFileUrl = [NSURL fileURLWithPath:outputFilePath];
            if ([[NSFileManager defaultManager] fileExistsAtPath:outputFilePath])
                [[NSFileManager defaultManager] removeItemAtPath:outputFilePath error:nil];
            CMTime nextClipStartTime = kCMTimeZero;
            AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:video_inputFileUrl options:nil];
            CMTimeRange video_timeRange = CMTimeRangeMake(kCMTimeZero,videoAsset.duration);
            AVMutableCompositionTrack *a_compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    
    
            if (![videoAsset tracksWithMediaType:AVMediaTypeVideo] || ![videoAsset tracksWithMediaType:AVMediaTypeVideo].count){
              
                return;
            }
    
            [a_compositionVideoTrack insertTimeRange:video_timeRange ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:nextClipStartTime error:nil];
    
    
            AVURLAsset* audioAsset = [[AVURLAsset alloc]initWithURL:audio_inputFileUrl options:nil];
            CMTimeRange audio_timeRange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration);
            AVMutableCompositionTrack *b_compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
            [b_compositionAudioTrack insertTimeRange:audio_timeRange ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:nextClipStartTime error:nil];
        AVMutableComposition *composition = [AVMutableComposition composition];
        AVMutableCompositionTrack *compositionVideoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
        AVMutableCompositionTrack *compositionAudioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        AVAssetTrack *videoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject];
        AVAssetTrack *audioTrack = [[videoAsset tracksWithMediaType:AVMediaTypeAudio] firstObject];
        CGFloat FirstAssetScaleToFitRatio = 1080/videoTrack.naturalSize.height;
        CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
        AVMutableVideoCompositionLayerInstruction *layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:compositionVideoTrack];
        [layerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(videoTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(0, 0)) atTime:kCMTimeZero];
        [layerInstruction setOpacity:0.0 atTime:videoAsset.duration];
        AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
        instruction.timeRange = CMTimeRangeMake( kCMTimeZero, videoAsset.duration);
        instruction.layerInstructions = @[layerInstruction];
        AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
        videoComposition.instructions = @[instruction];
        videoComposition.frameDuration = CMTimeMake(1, 30); //select the frames per second
        videoComposition.renderScale = 1.0;
        videoComposition.renderSize = CGSizeMake(1920, 1080); //select you video size
            AVAssetExportSession* _assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
        
        void(^exportCompletion)(void) = ^{
                  dispatch_async(dispatch_get_main_queue(), ^{
                      if (completion) completion(_assetExport.outputURL, _assetExport.error);
                  });
              };
          
        
            _assetExport.outputFileType = AVFileTypeQuickTimeMovie;
            _assetExport.outputURL = outputFileUrl;
             _assetExport.videoComposition = videoComposition;
        
            [_assetExport exportAsynchronouslyWithCompletionHandler:
             ^(void ) {
                 
                exportCompletion();

                
             }
             ];
        
    
    
}



+ (CVPixelBufferRef) pixelBufferFromCGImage: (CGImageRef) image : (CGSize) sizee {
    
    CGSize size = CGSizeMake(1920, 1080);
    int buffersystem = 0;
    
    if (sizee.width==1000) {
         buffersystem = 440;
    }else  if (sizee.width==750) {
        buffersystem = 570;

    } else if (sizee.width==1334){
        buffersystem = 300;

    }else{
        buffersystem = 0;

        
    }
    
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                             nil];
    CVPixelBufferRef pxbuffer = NULL;
    
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault,
                                          size.width,
                                          size.height,
                                          kCVPixelFormatType_32ARGB,
                                          (__bridge CFDictionaryRef) options,
                                          &pxbuffer);
    if (status != kCVReturnSuccess){
        NSLog(@"Failed to create pixel buffer");
    }
    
    CVPixelBufferLockBaseAddress(pxbuffer, 0);
    void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
    
    
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(pxdata, size.width,
                                                 size.height, 8, 4*size.width, rgbColorSpace,
                                                 kCGImageAlphaPremultipliedFirst);
    //kCGImageAlphaNoneSkipFirst);
    CGContextConcatCTM(context, CGAffineTransformMakeRotation(0));
    CGContextDrawImage(context, CGRectMake(buffersystem, 0, CGImageGetWidth(image),
                                           CGImageGetHeight(image)), image);
    CGColorSpaceRelease(rgbColorSpace);
    CGContextRelease(context);
    CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
    
    return pxbuffer;
    
}


 /////change video rotation///////
+(void)changevideo:(NSURL*)videourl  withOutPath:(NSString*)outpath
completion:(void(^)(NSURL *convervideourl, NSError *error , BOOL))completion


{
    
    NSArray *arrayOfComponents = [outpath componentsSeparatedByString:@"."];
    outpath = [arrayOfComponents objectAtIndex:0];
    NSLog(@"%@",outpath);
    
    NSError *error = nil;
    AVURLAsset *videoAssetURL = [[AVURLAsset alloc] initWithURL:videourl options:nil];
    AVMutableComposition *composition = [AVMutableComposition composition];
    AVMutableCompositionTrack *compositionVideoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *compositionAudioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    
    AVAssetTrack *videoTrack = [[videoAssetURL tracksWithMediaType:AVMediaTypeVideo] firstObject];
    AVAssetTrack *audioTrack = [[videoAssetURL tracksWithMediaType:AVMediaTypeAudio] firstObject];
    [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAssetURL.duration) ofTrack:videoTrack atTime:kCMTimeZero error:&error];
    
    if (audioTrack != nil) {
        [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAssetURL.duration) ofTrack:audioTrack atTime:kCMTimeZero error:&error];
    }
    
 CGSize size = [videoTrack naturalSize];
    int newvalue = 720;
    
      if (size.height>1080) {
          
          newvalue = 1080;
      
      }
    CGFloat FirstAssetScaleToFitRatio = newvalue/videoTrack.naturalSize.height;

    CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
  
    
    
    
    
    
    AVMutableVideoCompositionLayerInstruction *layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:compositionVideoTrack];
    
   
    float value = 575;
    

    if (size.height<=480) {
        value = -325;
    }
    
    if (size.height>=1080) {
        value = 650;
    }
    
//    if (size.height==1080) {
//           value = 1300;
//       }
   
    
   if (size.height==2160) {
         value = 450;
    }
    
    
    
      [layerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(videoTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(value, 0)) atTime:kCMTimeZero];
    [layerInstruction setOpacity:0.0 atTime:videoAssetURL.duration];
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake( kCMTimeZero, videoAssetURL.duration);
    instruction.layerInstructions = @[layerInstruction];
    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.instructions = @[instruction];
    videoComposition.frameDuration = CMTimeMake(1, 30); //select the frames per second
    videoComposition.renderScale = 1.0;
    videoComposition.renderSize = CGSizeMake(1920, 1080); //select you video size
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:composition presetName:AVAssetExportPresetHighestQuality];
   
             NSString *documentsDirectory = [NSHomeDirectory()
                                             stringByAppendingPathComponent:@"Documents/bharat"];
         
         
         if (![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory])
           [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:NO attributes:nil error:&error];
    
        NSString *outputFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mov",outpath]];
        NSURL    *outputFileUrl = [NSURL fileURLWithPath:outputFilePath];
        if ([[NSFileManager defaultManager] fileExistsAtPath:outputFilePath])
            [[NSFileManager defaultManager] removeItemAtPath:outputFilePath error:nil];
    exportSession.outputURL = outputFileUrl;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    exportSession.videoComposition = videoComposition;
    exportSession.shouldOptimizeForNetworkUse = NO;
    exportSession.timeRange = CMTimeRangeMake(kCMTimeZero, videoAssetURL.duration);
    
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        
        switch ([exportSession status]) {
                
            case AVAssetExportSessionStatusCompleted: {
                dispatch_async(dispatch_get_main_queue(), ^{

                    NSLog(@"output file url =>%@",outputFileUrl);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(outputFileUrl, exportSession.error,true);
                    });
                
                    
                });
           
           
                break;
            }
            case AVAssetExportSessionStatusFailed: {
                
               dispatch_async(dispatch_get_main_queue(), ^{
                                      completion(videourl, exportSession.error,false);
                             NSLog(@"output file error =>%@",exportSession.error);

                                  });
                
                           
                break;
            }
                
            default: {
                break;
            }
        }
        
        
    }];
    
    
    
  
    
}

+(void)changevideo1:(NSURL*)videourl  withOutPath:(NSString*)outpath
completion:(void(^)(NSURL *convervideourl, NSError *error , BOOL))completion

{
    
    NSArray *arrayOfComponents = [outpath componentsSeparatedByString:@"."];
    outpath = [arrayOfComponents objectAtIndex:0];
    NSLog(@"%@",outpath);
    
    
    NSError *error = nil;
    AVURLAsset *videoAssetURL = [[AVURLAsset alloc] initWithURL:videourl options:nil];
    
    AVMutableComposition *composition = [AVMutableComposition composition];
    AVMutableCompositionTrack *compositionVideoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *compositionAudioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    
    AVAssetTrack *videoTrack = [[videoAssetURL tracksWithMediaType:AVMediaTypeVideo] firstObject];
    AVAssetTrack *audioTrack = [[videoAssetURL tracksWithMediaType:AVMediaTypeAudio] firstObject];
    
    [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAssetURL.duration) ofTrack:videoTrack atTime:kCMTimeZero error:&error];
    
    if (audioTrack != nil) {
    [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAssetURL.duration) ofTrack:audioTrack atTime:kCMTimeZero error:&error];
    }
    
    CGFloat FirstAssetScaleToFitRatio = 1080/videoTrack.naturalSize.height;
    
    CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
    
    AVMutableVideoCompositionLayerInstruction *layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:compositionVideoTrack];
    
    [layerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(videoTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(0, 0)) atTime:kCMTimeZero];
    
    [layerInstruction setOpacity:0.0 atTime:videoAssetURL.duration];
    
    
    
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake( kCMTimeZero, videoAssetURL.duration);
    instruction.layerInstructions = @[layerInstruction];
    
    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.instructions = @[instruction];
    videoComposition.frameDuration = CMTimeMake(1, 30); //select the frames per second
    videoComposition.renderScale = 1.0;
    videoComposition.renderSize = CGSizeMake(1920, 1080); //select you video size
    
    
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:composition presetName:AVAssetExportPresetHighestQuality];
    
           NSString *documentsDirectory = [NSHomeDirectory()
                                           stringByAppendingPathComponent:@"Documents/bharat"];
       
       
       if (![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory])
         [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:NO attributes:nil error:&error];
    
    
    
    NSString *outputFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mov",outpath]];
    NSURL    *outputFileUrl = [NSURL fileURLWithPath:outputFilePath];
    
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:outputFilePath]){
        [[NSFileManager defaultManager] removeItemAtPath:outputFilePath error:nil];
    }
    
    
    exportSession.outputURL = outputFileUrl;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie; //very important select you video format (AVFileTypeQuickTimeMovie, AVFileTypeMPEG4, etc...)
    exportSession.videoComposition = videoComposition;
    exportSession.shouldOptimizeForNetworkUse = NO;
    exportSession.timeRange = CMTimeRangeMake(kCMTimeZero, videoAssetURL.duration);
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        switch ([exportSession status]) {
            case AVAssetExportSessionStatusCompleted: {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"output file url =>%@",outputFileUrl);
                        completion(outputFileUrl, exportSession.error,true);
                                       
                  
                });
                break;
            }
            case AVAssetExportSessionStatusFailed: {
                dispatch_async(dispatch_get_main_queue(), ^{
                        completion(videourl, exportSession.error,false);
                    NSLog(@"output file error =>%@",exportSession.error);

                    });
                
                break;
            }
                
            default: {
                break;
            }
        }
    }];
}





+(BOOL)ifvideoportrait:(NSURL*)videourl{
    AVAsset *currentAsset = [AVAsset assetWithURL:videourl];
    
    
    if (![currentAsset tracksWithMediaType:AVMediaTypeVideo] || ![currentAsset tracksWithMediaType:AVMediaTypeVideo].count){
        return false;
    }
    
    
    AVAssetTrack *currentAssetTrack = [[currentAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    
    
    
    UIImageOrientation currentAssetOrientation  = UIImageOrientationUp;
    BOOL  isCurrentAssetPortrait  = NO;
    CGAffineTransform currentTransform = currentAssetTrack.preferredTransform;
    if(currentTransform.a == 0 && currentTransform.b == 1.0 && currentTransform.c == -1.0 && currentTransform.d == 0)  {
        currentAssetOrientation= UIImageOrientationRight; isCurrentAssetPortrait = YES;
        
    }
    
    if(currentTransform.a == 0 && currentTransform.b == -1.0 && currentTransform.c == 1.0 && currentTransform.d == 0)  {
        currentAssetOrientation =  UIImageOrientationLeft; isCurrentAssetPortrait = YES;
    }
    
    if(currentTransform.a == 1.0 && currentTransform.b == 0 && currentTransform.c == 0 && currentTransform.d == 1.0)   {currentAssetOrientation =  UIImageOrientationUp;
    }
    
    if(currentTransform.a == -1.0 && currentTransform.b == 0 && currentTransform.c == 0 && currentTransform.d == -1.0) {currentAssetOrientation = UIImageOrientationDown;}
    
    CGSize size = [currentAsset naturalSize];
    
    if (size.height>size.width || size.height==size.width) {
        isCurrentAssetPortrait = YES;
    }
    return isCurrentAssetPortrait;
    
}



+(void)mergeAndExportVideos:(NSArray*)videosPathArray  withOutPath:(NSString*)outpath
completion:(void(^)(NSURL *finalurl, NSError *error))completion

{


    CGFloat totalDuration;
    totalDuration = 0;

    AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];

    AVMutableCompositionTrack *videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                                        preferredTrackID:kCMPersistentTrackID_Invalid];

    AVMutableCompositionTrack *audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio
                                                                        preferredTrackID:kCMPersistentTrackID_Invalid];

    CMTime insertTime = kCMTimeZero;

    for (NSDictionary *dict in videosPathArray)
    {
        AVAsset *asset;
        
        NSString *str = [dict objectForKey:@"trimfile"];
        
        if ([str isEqualToString:@""]) {
          
            asset  =  [AVAsset assetWithURL:[dict objectForKey:@"orginalfile"]];

        }else{
            asset  =  [AVAsset assetWithURL:[dict objectForKey:@"trimfile"]];

        }

        
        
        if (![asset tracksWithMediaType:AVMediaTypeVideo] || ![asset tracksWithMediaType:AVMediaTypeVideo].count){
            
            

            
            dispatch_async(dispatch_get_main_queue(), ^{
//                                               [self.navigationController popViewControllerAnimated:YES];
//                [Utils showOKAlertWithTitle:@"" message:@"Some file not supported.."];

                                              });
            return;
        }
        
        AVAssetTrack *currentAssetTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];



        CMTimeRange timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration);
        
     
        

        [videoTrack insertTimeRange:timeRange
                            ofTrack:[[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0]
                             atTime:insertTime
                              error:nil];
        
        if (![asset tracksWithMediaType:AVMediaTypeAudio] || ![asset tracksWithMediaType:AVMediaTypeAudio].count){
            
        //    [Utils showOKAlertWithTitle:@"" message:[NSString stringWithFormat:@"File name %@ not supported...",file.lastPathComponent]];

          

            dispatch_async(dispatch_get_main_queue(), ^{
//                [Utils showOKAlertWithTitle:@"" message:@"Some file not supported.."];
//                [self.navigationController popViewControllerAnimated:YES];
                

                                             
            });
            return;
        }

        [audioTrack insertTimeRange:timeRange
                            ofTrack:[[asset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]
                             atTime:insertTime
                              error:nil];
        
        videoTrack.preferredTransform = currentAssetTrack.preferredTransform;
        insertTime = CMTimeAdd(insertTime,asset.duration);
    }

    
    NSString* videoName = @"MixVideo.mov";
       NSString *documentsDirectory = [NSHomeDirectory()
                                       stringByAppendingPathComponent:@"Documents/bharat"];
       NSString *exportPath = [documentsDirectory stringByAppendingPathComponent:videoName];
    
    
    NSURL *mergeFileURL = [NSURL fileURLWithPath:exportPath];
    if ([[NSFileManager defaultManager] fileExistsAtPath:exportPath])
    {
        [[NSFileManager defaultManager] removeItemAtPath:exportPath error:nil];
    }

    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetPassthrough];
    exporter.outputURL = mergeFileURL;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.shouldOptimizeForNetworkUse = NO;

    [exporter exportAsynchronouslyWithCompletionHandler:^{

        switch ([exporter status])
        {
            case AVAssetExportSessionStatusFailed:
                
            {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"output file url =>%@",mergeFileURL);
                    NSLog(@"output file error =>%@",exporter.error);

                        completion(mergeFileURL, exporter.error);
                });
                
            }
                break;

            case AVAssetExportSessionStatusCancelled:
                break;

            case AVAssetExportSessionStatusCompleted:
                dispatch_async(dispatch_get_main_queue(), ^{
                [NSThread sleepForTimeInterval:1.0];
                NSLog(@"output file url =>%@",mergeFileURL);
                completion(mergeFileURL, exporter.error);
                    
                                     
                });
                break;


        }
    }];
}


+(void)Resetvideosize:(CGSize)newsize videourl:(NSURL*)Finalurl
completion:(void(^)(NSURL *finalurl, NSError *error))completion
{

    AVURLAsset* videoasset = [[AVURLAsset alloc]initWithURL:Finalurl options:nil];



    NSString* videoName = @"resolutionvideo.mov";
    NSString *exportPath = [NSTemporaryDirectory() stringByAppendingPathComponent:videoName];
    NSNumber * bitrate ;
    if (newsize.height==1080) {

        bitrate = @15300000;
        
    }else if (newsize.height==720){
        bitrate = @10600000;
    }
    else{
        bitrate = @3500000;
    }
    [[NSFileManager defaultManager] removeItemAtPath:exportPath error:NULL];
    SDAVAssetExportSession *encoder = [SDAVAssetExportSession.alloc initWithAsset:videoasset];
    encoder.outputFileType = AVFileTypeQuickTimeMovie;
    encoder.outputURL = [NSURL fileURLWithPath:exportPath];
    encoder.videoSettings = @
    {
    AVVideoCodecKey: AVVideoCodecTypeH264,
    AVVideoWidthKey: [NSNumber numberWithInt:newsize.width],
    AVVideoHeightKey: [NSNumber numberWithInt:newsize.height],
    AVVideoCompressionPropertiesKey: @
        {
        AVVideoAverageBitRateKey: bitrate,
        AVVideoProfileLevelKey: AVVideoProfileLevelH264High40,
        AVVideoExpectedSourceFrameRateKey: [NSNumber numberWithInteger:30]

        },
    };
    encoder.audioSettings = @
    {
    AVFormatIDKey: @(kAudioFormatMPEG4AAC),
    AVNumberOfChannelsKey: @2,
    AVSampleRateKey: @44100,
    AVEncoderBitRateKey: @128000,
    };


    [encoder exportAsynchronouslyWithCompletionHandler:^
    {
        if (encoder.status == AVAssetExportSessionStatusCompleted)
        {
            NSLog(@"%@", encoder.outputURL);
          //  FinalUrl = encoder.outputURL;
            completion(encoder.outputURL, encoder.error);

           
        }
        else if (encoder.status == AVAssetExportSessionStatusCancelled)
        {
            NSLog(@"Video export cancelled");
        }
        else
        {
            NSLog(@"Video export failed with error: %@ (%ld)", encoder.error.localizedDescription, (long)encoder.error.code);
            completion(encoder.outputURL, encoder.error);

        }
    }];
}


+(void)ExtractAudio:(NSURL*)Finalurl
completion:(void(^)(NSURL *finalurl, NSError *error ))completion
{
NSURL *videoFileUrl = Finalurl;
AVURLAsset *anAsset = [[AVURLAsset alloc] initWithURL:videoFileUrl options:nil];
    AVMutableComposition*   newAudioAsset = [AVMutableComposition composition];
    AVMutableCompositionTrack*  dstCompositionTrack;
    
    
    dstCompositionTrack = [newAudioAsset addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    
 
    
    if (![anAsset tracksWithMediaType:AVMediaTypeAudio] || ![anAsset tracksWithMediaType:AVMediaTypeAudio].count){
        NSString *bundleDirectory = [[NSBundle mainBundle] bundlePath];
        NSString *audio_inputFilePath = [bundleDirectory stringByAppendingPathComponent:@"20m.mp3"];
        NSURL    *audio_inputFileUrl = [NSURL fileURLWithPath:audio_inputFilePath];
        completion(audio_inputFileUrl,nil);


        return;
    }
    
    
    [dstCompositionTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, anAsset.duration)
                                 ofTrack:[[anAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:kCMTimeZero error:nil];
    AVAssetExportSession*   exportSesh = [[AVAssetExportSession alloc] initWithAsset:newAudioAsset presetName:AVAssetExportPresetPassthrough];
    exportSesh.outputFileType = AVFileTypeCoreAudioFormat;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *audioPath = [documentsDirectory stringByAppendingPathComponent:@"extract.caf"];
    exportSesh.outputURL=[NSURL fileURLWithPath:audioPath];
        if ([[NSFileManager defaultManager] fileExistsAtPath:audioPath]){
            [[NSFileManager defaultManager] removeItemAtPath:audioPath error:nil];
        }
    
    [exportSesh exportAsynchronouslyWithCompletionHandler:^{
        AVAssetExportSessionStatus  status = exportSesh.status;
        NSLog(@"exportAsynchronouslyWithCompletionHandler: %li\n", (long)status);
        if(AVAssetExportSessionStatusFailed == status) {
            NSString *bundleDirectory = [[NSBundle mainBundle] bundlePath];
                               NSString *audio_inputFilePath = [bundleDirectory stringByAppendingPathComponent:@"20m.mp3"];
                               NSURL    *audio_inputFileUrl = [NSURL fileURLWithPath:audio_inputFilePath];
            
            completion(audio_inputFileUrl,exportSesh.error);
        } else if(AVAssetExportSessionStatusCompleted == status) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{

                //completion(nil,exportSesh.error);

                
                
               completion([NSURL fileURLWithPath:audioPath],exportSesh.error);
                
            });
        }
                           
    }];


}

+(void)extractAudiofromVideo:(NSURL*)Finalurl
completion:(void(^)(NSURL *finalurl, NSError *error))completion

{
    NSURL *videoFileUrl = Finalurl;
    AVURLAsset *anAsset = [[AVURLAsset alloc] initWithURL:videoFileUrl options:nil];
    AVAssetExportSession *exportSession=[AVAssetExportSession exportSessionWithAsset:anAsset presetName:AVAssetExportPresetAppleM4A];
    [exportSession determineCompatibleFileTypesWithCompletionHandler:^(NSArray *compatibleFileTypes) {
        NSLog(@"compatiblefiletypes: %@",compatibleFileTypes);
    }];
        NSString* videoName = @"extract.m4a";
       NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
       NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *exportPath = [documentsDirectory stringByAppendingPathComponent:videoName];
        if ([[NSFileManager defaultManager] fileExistsAtPath:exportPath]){
            [[NSFileManager defaultManager] removeItemAtPath:exportPath error:nil];
        }
    NSURL *furl = [NSURL fileURLWithPath:exportPath];
    exportSession.outputURL = furl;
    if (@available(iOS 11_0, *)) {
        exportSession.outputFileType=AVFileTypeAppleM4A;
    } else {
        exportSession.outputFileType=AVFileTypeAppleM4A;
    }
    

    
    CMTime duration = anAsset.duration;
    CMTimeRange range = CMTimeRangeMake(kCMTimeZero, duration);
    exportSession.timeRange = range;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
      
        switch (exportSession.status)
        {
            case AVAssetExportSessionStatusCompleted:
            {
                dispatch_async(dispatch_get_main_queue(), ^{
               completion(furl,exportSession.error);
                });
                break;
            }
            case AVAssetExportSessionStatusFailed:
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"%@",exportSession.error);
                    NSString *bundleDirectory = [[NSBundle mainBundle] bundlePath];
                    NSString *audio_inputFilePath = [bundleDirectory stringByAppendingPathComponent:@"20m.mp3"];
                    NSURL    *audio_inputFileUrl = [NSURL fileURLWithPath:audio_inputFilePath];
                   completion(audio_inputFileUrl,exportSession.error);
                    
                });
                break;
            }
            case AVAssetExportSessionStatusCancelled:
                NSLog(@"Export canceled");
                break;
            default:
               break;
        }
    }];

}

    
+ (void) combineVoices:(NSURL*)AudioOFvideo lib:(NSArray*)ArrLibSave  rec:(NSArray*)ArrRecordingSave
completion:(void(^)(NSURL *finalurl, NSError *error))completion
{
    
    
    NSMutableArray *arrtime = [[NSMutableArray alloc] init];
    NSMutableArray *arrvolume = [[NSMutableArray alloc] init];
    NSMutableArray *arrvalue = [[NSMutableArray alloc] init];
    if (ArrLibSave.count>0){
        
        if ([[[ArrLibSave objectAtIndex:0] objectForKey:@"trimfile"]isEqualToString:@""]) {
            [arrvolume addObject:[NSString stringWithFormat:@"%@",[[ArrLibSave objectAtIndex:0] objectForKey:@"volumekey"]]];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *recDir = [paths objectAtIndex:0];
            NSString *audioFileName  = [NSString stringWithFormat:@"%@/%@" , recDir, [[ArrLibSave objectAtIndex:0] objectForKey:@"orginalfile"]];
            NSURL *url = [NSURL fileURLWithPath:audioFileName];
            [arrvalue addObject:url];
            [arrtime addObject:[NSString stringWithFormat:@"%@",[[ArrLibSave objectAtIndex:0] objectForKey:@"playtime"]]];
        }else{
            
            [arrvolume addObject:[NSString stringWithFormat:@"%@",[[ArrLibSave objectAtIndex:0] objectForKey:@"volumekey"]]];
            [arrvalue addObject:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@",[[ArrLibSave objectAtIndex:0] objectForKey:@"trimfile"]]]];
            [arrtime addObject:[NSString stringWithFormat:@"%@",[[ArrLibSave objectAtIndex:0] objectForKey:@"playtime"]]];
       
        }
    }
    
    if (ArrRecordingSave.count>0){
        if ([[[ArrRecordingSave objectAtIndex:0] objectForKey:@"trimfile"]isEqualToString:@""]) {
            [arrvolume addObject:[NSString stringWithFormat:@"%@",[[ArrRecordingSave objectAtIndex:0] objectForKey:@"volumekey"]]];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *recDir = [paths objectAtIndex:0];
            NSString *audioFileName  = [NSString stringWithFormat:@"%@/%@" , recDir, [[ArrRecordingSave objectAtIndex:0] objectForKey:@"orginalfile"]];
               NSURL *url = [NSURL fileURLWithPath:audioFileName];
            [arrvalue addObject:url];
            [arrtime addObject:[NSString stringWithFormat:@"%@",[[ArrRecordingSave objectAtIndex:0] objectForKey:@"playtime"]]];
            
        }else{
            
            [arrvolume addObject:[NSString stringWithFormat:@"%@",[[ArrRecordingSave objectAtIndex:0] objectForKey:@"volumekey"]]];
            [arrvalue addObject:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@",[[ArrRecordingSave objectAtIndex:0] objectForKey:@"trimfile"]]]];
            [arrtime addObject:[NSString stringWithFormat:@"%@",[[ArrRecordingSave objectAtIndex:0] objectForKey:@"playtime"]]];
        }
        
    }
   
    
    if (AudioOFvideo == nil){
        NSString *bundleDirectory = [[NSBundle mainBundle] bundlePath];
        NSString *audio_inputFilePath = [bundleDirectory stringByAppendingPathComponent:@"20m.mp3"];
        NSURL    *audio_inputFileUrl = [NSURL fileURLWithPath:audio_inputFilePath];
        AudioOFvideo = audio_inputFileUrl;
    }
    
    
      [arrvolume addObject:[NSString stringWithFormat:@"%d",1]];
      [arrvalue addObject:AudioOFvideo];
      [arrtime addObject:[NSString stringWithFormat:@"%f",0.0]];
   
    
    NSMutableArray *trackMixArray = [NSMutableArray array];
    AVMutableComposition *composition = [AVMutableComposition composition];
        for (int i = 0 ; i<arrvalue.count; i++) {
            float timestam = [[arrtime objectAtIndex:i] floatValue];
            AVURLAsset* audioAsset = [[AVURLAsset alloc]initWithURL:[arrvalue objectAtIndex:i] options:nil];
           
            
            if (![audioAsset tracksWithMediaType:AVMediaTypeAudio] || ![audioAsset tracksWithMediaType:AVMediaTypeAudio].count){
                NSString *bundleDirectory = [[NSBundle mainBundle] bundlePath];
                NSString *audio_inputFilePath = [bundleDirectory stringByAppendingPathComponent:@"20m.mp3"];
                NSURL    *audio_inputFileUrl = [NSURL fileURLWithPath:audio_inputFilePath];
                AudioOFvideo = audio_inputFileUrl;
                AVURLAsset* audioAsset1 = [[AVURLAsset alloc]initWithURL:AudioOFvideo options:nil];
                audioAsset = audioAsset1;
            }
            
            AVMutableCompositionTrack* audioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio
                                                                             preferredTrackID:kCMPersistentTrackID_Invalid];
            
            NSError* error;
            CMTime time = CMTimeMakeWithSeconds(timestam, 60000);
            
            
                [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, audioAsset.duration) ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio]objectAtIndex:0] atTime:time error:&error];
            
            
            
            AVMutableAudioMixInputParameters *trackMix = [AVMutableAudioMixInputParameters audioMixInputParametersWithTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio]objectAtIndex:0]];
            float volume = [[arrvolume objectAtIndex:i] floatValue];
                [trackMix setVolume:volume atTime:kCMTimeZero];
            [trackMix setTrackID:i+1];
            [trackMixArray addObject:trackMix];
            if (error)
            {
                NSLog(@"%@", [error localizedDescription]);
            }
        }
  AVMutableAudioMix *audioMix = [AVMutableAudioMix audioMix];
    audioMix.inputParameters = trackMixArray;
    AVAssetExportSession* _assetExport = [[AVAssetExportSession alloc] initWithAsset:composition presetName:AVAssetExportPresetAppleM4A];
    NSString* mixedAudio = @"mixedAudio.m4a";
          NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
          NSString *documentsDirectory = [paths objectAtIndex:0];
           NSString *exportPath = [documentsDirectory stringByAppendingPathComponent:mixedAudio];
           if ([[NSFileManager defaultManager] fileExistsAtPath:exportPath]){
               [[NSFileManager defaultManager] removeItemAtPath:exportPath error:nil];
           }
    NSURL *exportURL = [NSURL fileURLWithPath:exportPath];
    if ([[NSFileManager defaultManager]fileExistsAtPath:exportPath]) {
        [[NSFileManager defaultManager]removeItemAtPath:exportPath error:nil];
    }
    _assetExport.outputFileType = AVFileTypeAppleM4A;
    _assetExport.outputURL = exportURL;
    _assetExport.shouldOptimizeForNetworkUse = NO;
    _assetExport.audioMix = audioMix;
    [_assetExport exportAsynchronouslyWithCompletionHandler:^{
        
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
      
           NSLog(@"Completed Sucessfully");
            NSLog(@"%@",exportURL);
            NSLog(@"success : %@",exportURL);
             completion(exportURL,_assetExport.error);
         });
       
}];
 
}





+(void)mergeAndSave:(NSURL*)videourl audio:(NSURL*)mergedaudio
completion:(void(^)(NSURL *finalurl, NSError *error))completion

{
   
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    NSURL *video_url = videourl;
    NSLog(@"FinalUrl => %@",videourl);
    AVURLAsset *videoAsset = [[AVURLAsset alloc]initWithURL:video_url options:nil];
    AVURLAsset *audioAsset = [[AVURLAsset alloc]initWithURL:mergedaudio options:nil];
    CMTimeRange audio_timeRange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration);
    NSLog(@" video duration %f",CMTimeGetSeconds(videoAsset.duration));
    AVMutableCompositionTrack *b_compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    int  duration1 = CMTimeGetSeconds(audioAsset.duration);
    NSError *error;
    if(duration1==0){
        completion(videourl,error);

        return;
    }else{
    [b_compositionAudioTrack insertTimeRange:audio_timeRange ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:kCMTimeZero error:nil];
    int  duration = CMTimeGetSeconds(videoAsset.duration);
    NSLog(@"video %d, audio %d",duration,duration1);
        
        if (duration==0) {
               completion(videourl,error);
                return;
            }
        
        
    CMTimeRange video_timeRange;
    video_timeRange = CMTimeRangeMake(kCMTimeZero,videoAsset.duration);
    NSLog(@" video duration %f",CMTimeGetSeconds(videoAsset.duration));
    AVMutableCompositionTrack *a_compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    [a_compositionVideoTrack insertTimeRange:video_timeRange ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:nil];
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *outputFilePath = [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"FinalVideo.mov"]];
    NSURL *outputFileUrl = [NSURL fileURLWithPath:outputFilePath];
    if ([[NSFileManager defaultManager] fileExistsAtPath:outputFilePath])
        [[NSFileManager defaultManager] removeItemAtPath:outputFilePath error:nil];
    AVAssetExportSession* _assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetPassthrough];
    _assetExport.outputFileType = AVFileTypeQuickTimeMovie;
    _assetExport.outputURL = outputFileUrl;
    [_assetExport exportAsynchronouslyWithCompletionHandler:
     ^(void ) {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if(_assetExport.status == AVAssetExportSessionStatusCompleted){
                    NSURL *outputURL = _assetExport.outputURL;
                 completion(outputURL,_assetExport.error);

                   
                }else{
                   completion(videourl,_assetExport.error);
                }
             
         });
     }
     ];
    }
}
 
+(void)saveaudio{
    NSString *bundleDirectory = [[NSBundle mainBundle] bundlePath];
    NSString *audio_inputFilePath = [bundleDirectory stringByAppendingPathComponent:@"newaudio.mp3"];
    NSURL  *audio_inputFileUrl = [NSURL fileURLWithPath:audio_inputFilePath];
    NSData *audio = [NSData dataWithContentsOfURL:audio_inputFileUrl];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *recDir = [paths objectAtIndex:0];
    recDir = [recDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%s.mp3","newaudio"]];
    [audio writeToFile:recDir atomically:YES];
}




@end

