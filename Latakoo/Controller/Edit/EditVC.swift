//
//  EditVC.swift
//  Latakoo
//
//  Created by Shruti Aggarwal on 19/08/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import UIKit
import Photos

class EditVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if flagmedia == 0 {
            return all.count;
        }else if flagmedia == 1 {
            return image.count
        }else{
            return video.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"cell", for: indexPath) as! librarycell
        
        cell.imgthumb.clipsToBounds = true
        cell.imgthumb.contentMode = .scaleAspectFill
        
        cell.lblcount.layer.cornerRadius = cell.lblcount.frame.size.width/2
        cell.lblcount.layer.borderColor = UIColor.white.cgColor
        cell.lblcount.layer.borderWidth = 1
        cell.lblcount.layer.masksToBounds = true
        
        if flagmedia == 0 {
            if let asset = all[indexPath.item] as? PHAsset {
                switch asset.mediaType {
                case .image:
                    cell.overlayview.isHidden = true
                    cell.lblduration.isHidden = true
                    cell.lblvideoname.isHidden = true
                    cell.imgvideo.isHidden = true
                    let manager = PHImageManager.default()
                    let option = PHImageRequestOptions()
                    option.isNetworkAccessAllowed = true
                    var thumbnail = UIImage()
                    option.isSynchronous = true
                    manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
                        let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                        cell.imgthumb.image = thumbnail
                    })
                    break
                                        
                case .video:
                    cell.overlayview.isHidden = false
                    cell.lblduration.isHidden = false
                    cell.lblvideoname.isHidden = false
                    cell.imgvideo.isHidden = false
                    
                    let manager = PHImageManager.default()
                    let option = PHImageRequestOptions()
                    option.isNetworkAccessAllowed = true
                    
                    var thumbnail = UIImage()
                    option.isSynchronous = true
                    manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
                        if result != nil{
                            let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                            cell.imgthumb.image = thumbnail
                        }
                    })
                    
                    DispatchQueue.global(qos: .background).async {
                        let resources = PHAssetResource.assetResources(for: asset)
                        let orgFilename = (resources[0]).originalFilename
                        DispatchQueue.main.async{
                            cell.lblvideoname.text = orgFilename
                        }
                    }
                    
                    DispatchQueue.global(qos: .background).async {
                        let option1 = PHVideoRequestOptions()
                        option1.isNetworkAccessAllowed = true
                        option1.deliveryMode = .highQualityFormat
                        option1.version = .original
                        option1.progressHandler = {  (progress, error, stop, info) in
                        }
                        
                        var resultAsset: AVAsset?
                        PHImageManager.default().requestAVAsset(forVideo: asset, options: option1, resultHandler: { avasset, audioMix, info in
                            resultAsset = avasset
                            let duration = resultAsset?.duration
                            let durationTime = CMTimeGetSeconds(duration ??  CMTimeMake(1, 1))
                            print(durationTime)
                            DispatchQueue.main.async{
                                cell.lblduration.text = self.convertTime(Int(durationTime))
                            }
                        })
                    }
                    break
                                        
                default:
                    break
                }
            }
            
            let asset = all[indexPath.item]
            
            let resourceArray = PHAssetResource.assetResources(for: asset)
            let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false // If this returns NO, then the asset is in iCloud and not saved locally yet
            if bIsLocallayAvailable {
                cell.imgcloud.isHidden = true
            }else{
                cell.imgcloud.isHidden = false
            }
            
            if FilesArray.contains(all[indexPath.item]) {
                cell.viewselection.isHidden = false
                cell.lblcount.isHidden = false
                cell.imgbigcheck.isHidden = false
                cell.lblcount.text = "\(FilesArray.index(of: all[indexPath.item])!+1)"
            }else{
                cell.viewselection.isHidden = true
                cell.lblcount.isHidden = true
                cell.imgbigcheck.isHidden = true
            }
            
        } else if flagmedia == 1 {
            
            cell.overlayview.isHidden = true
            cell.lblduration.isHidden = true
            cell.lblvideoname.isHidden = true
            cell.imgvideo.isHidden = true
            
            let asset = image[indexPath.item]
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.isNetworkAccessAllowed = true
            
            var thumbnail = UIImage()
            option.isSynchronous = true
            manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
                let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                cell.imgthumb.image = thumbnail
                
            })
            
            let resourceArray = PHAssetResource.assetResources(for: asset)
            let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false // If this returns NO, then the asset is in iCloud and not saved locally yet
            if bIsLocallayAvailable {
                cell.imgcloud.isHidden = true
            }else{
                cell.imgcloud.isHidden = false
            }
            
            if FilesArray.contains(image[indexPath.item]) {
                cell.viewselection.isHidden = false
                cell.lblcount.isHidden = false
                cell.imgbigcheck.isHidden = false
                cell.lblcount.text = "\(FilesArray.index(of: image[indexPath.item])!+1)"
            }else{
                cell.viewselection.isHidden = true
                cell.lblcount.isHidden = true
                cell.imgbigcheck.isHidden = true
            }
        }else{
            
            let asset = video[indexPath.item]
            
            cell.overlayview.isHidden = false
            cell.lblduration.isHidden = false
            cell.lblvideoname.isHidden = false
            cell.imgvideo.isHidden = false
            
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.isNetworkAccessAllowed = true
            
            var thumbnail = UIImage()
            option.isSynchronous = true
            manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
                let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
                cell.imgthumb.image = thumbnail
            })
            
            let resources = PHAssetResource.assetResources(for: asset)
            let orgFilename = (resources[0]).originalFilename
            cell.lblvideoname.text = orgFilename
            
            DispatchQueue.global(qos: .background).async {
                let option1 = PHVideoRequestOptions()
                option1.isNetworkAccessAllowed = true
                option1.deliveryMode = .highQualityFormat
                option1.version = .original
                option1.progressHandler = {  (progress, error, stop, info) in
                }
                
                var resultAsset: AVAsset?
                PHImageManager.default().requestAVAsset(forVideo: asset, options: option1, resultHandler: { avasset, audioMix, info in
                    resultAsset = avasset
                    let duration = resultAsset?.duration
                    let durationTime = CMTimeGetSeconds(duration ??  CMTimeMake(1, 1))
                    print(durationTime)
                    DispatchQueue.main.async{
                        cell.lblduration.text = self.convertTime(Int(durationTime))
                    }
                })
            }
                        
            let bIsLocallayAvailable = (resources.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false // If this returns NO, then the asset is in iCloud and not saved locally yet
            if bIsLocallayAvailable {
                cell.imgcloud.isHidden = true
            }else{
                cell.imgcloud.isHidden = false
            }
            
            if FilesArray.contains(video[indexPath.item]) {
                cell.viewselection.isHidden = false
                cell.lblcount.isHidden = false
                cell.imgbigcheck.isHidden = false
                cell.lblcount.text = "\(FilesArray.index(of: video[indexPath.item])!+1)"
            }else{
                cell.viewselection.isHidden = true
                cell.lblcount.isHidden = true
                cell.imgbigcheck.isHidden = true
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if IS_IPHONE{
            return CGSize(width:collectionView.frame.size.width/3, height: 110)
        }else{
            return CGSize(width:collectionView.frame.size.width/3, height: 200)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if flagmedia == 0 {
            if let index = FilesArray.index(of: all[indexPath.item]) {
                FilesArray.remove(at: index)
            }else{
                FilesArray.append(all[indexPath.item])
            }
            
            
        }else if flagmedia == 1 {
            if let index = FilesArray.index(of: image[indexPath.item]) {
                FilesArray.remove(at: index)
            }else{
                FilesArray.append(image[indexPath.item])
            }
            
            
        }else{
            if let index = FilesArray.index(of: video[indexPath.item]) {
                FilesArray.remove(at: index)
            }else{
                FilesArray.append(video[indexPath.item])
            }
            
            
        }
        
        let index = IndexPath(row: indexPath.item, section: 0)
        mycollectionview.reloadItems(at:[index])
        
        
    }
    
    @IBOutlet weak var mycollectionview: UICollectionView!
    
    @IBOutlet weak var btnvideo: UIButton!
    @IBOutlet weak var btnimage: UIButton!
    @IBOutlet weak var btnall: UIButton!
    let app = UIApplication.shared.delegate as! AppDelegate
    
    var image : [PHAsset] = []
    var all : [PHAsset] = []
    var video : [PHAsset] = []
    var FilesArray : [PHAsset] = []
    
    var flagmedia = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @objc func getfiles(){
        
        
        
        let status = PHPhotoLibrary.authorizationStatus()
        
        if (status == PHAuthorizationStatus.authorized) {
            flagmedia = 0
            
            if app.all.count>0 {
                FilesArray  = []
                all = app.all
                mycollectionview.reloadData()
                mycollectionview.setContentOffset(CGPoint.zero, animated: true)
                
            }else{
                self.getall()
            }
            
            
        }
        
        else if (status == PHAuthorizationStatus.denied) {
            
            _ = SCLAlertView().showError("LATAKOO", subTitle:"Please give this app permission to access your photo library in your settings app!", closeButtonTitle:"OK")
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.changecolor()
        btnall.setBackgroundImage(UIImage.init(named:"purpule_box.png"), for:.normal)
        btnall.setTitleColor(UIColor.white, for: .normal)
        super.viewWillAppear(true)
        self.perform(#selector(getfiles), with: nil, afterDelay:0.1)
    }
    
    @IBAction func btnAll(_ sender: Any) {
        FilesArray  = []
        flagmedia = 0
        if app.all.count>0 {
            all = app.all
            mycollectionview.reloadData()
            mycollectionview.setContentOffset(CGPoint.zero, animated: true)
            
        }else{
            self.getall()
        }
        
        self.changecolor()
        btnall.setBackgroundImage(UIImage.init(named:"purpule_box.png"), for:.normal)
        btnall.setTitleColor(UIColor.white, for: .normal)
    }
    
    @IBAction func btnimage(_ sender: Any) {
        FilesArray  = []
        flagmedia = 1
        if app.all.count>0 {
            image = app.image
            mycollectionview.reloadData()
            mycollectionview.setContentOffset(CGPoint.zero, animated: true)
            
        }else{
            self.getimages()
        }
        
        self.changecolor()
        btnimage.setBackgroundImage(UIImage.init(named:"purpule_box.png"), for:.normal)
        btnimage.setTitleColor(UIColor.white, for: .normal)
    }
    
    @IBAction func btnvideo(_ sender: Any) {
        FilesArray  = []
        flagmedia = 2
        
        if app.video.count>0 {
            video = app.video
            mycollectionview.reloadData()
            mycollectionview.setContentOffset(CGPoint.zero, animated: true)
            
        }else{
            self.getvideo()
        }
        
        self.changecolor()
        btnvideo.setBackgroundImage(UIImage.init(named:"purpule_box.png"), for:.normal)
        btnvideo.setTitleColor(UIColor.white, for: .normal)
    }
    
    @IBAction func btnnext(_ sender: Any) {
        
        
        if FilesArray.count > 0 {
            
            
            var story : UIStoryboard?
            
            if IS_IPHONE {
                story = UIStoryboard.init(name: "Main", bundle: nil)
            }else{
                story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
            }
            let EditfilenameVC = story?.instantiateViewController(withIdentifier: "EditfilenameVC") as! EditfilenameVC
            EditfilenameVC.all = FilesArray
            self.navigationController?.pushViewController(EditfilenameVC, animated: true)
        }else{
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please select  at least one file", hideAfter: 1.0)
            
        }
    }
    
    func changecolor(){
        btnall.setBackgroundImage(UIImage.init(named:"white_box_button"), for:.normal)
        btnall.setTitleColor(UIColor.init(named:"pilotcolor"), for: .normal)
        btnvideo.setBackgroundImage(UIImage.init(named:"white_box_button"), for:.normal)
        btnvideo.setTitleColor(UIColor.init(named:"pilotcolor"), for: .normal)
        btnimage.setBackgroundImage(UIImage.init(named:"white_box.png"), for:.normal)
        btnimage.setTitleColor(UIColor.init(named:"pilotcolor"), for: .normal)
    }
    
    func getimages(){
        image = []
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
        
        assets.enumerateObjects { (obj, idx, bool) -> Void in
            //  self.image.append(obj)
            self.image.insert(obj, at: 0)
        }
        print(image)
        mycollectionview.reloadData()
    }
    func getvideo(){
        video = []
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.video, options: fetchOptions)
        
        assets.enumerateObjects { (obj, idx, bool) -> Void in
            // self.video.append(obj)
            self.video.insert(obj, at: 0)
            
        }
        print(video)
        mycollectionview.reloadData()
    }
    
    func getall(){
        all = []
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate",ascending: false)]
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d || mediaType = %d", PHAssetMediaType.image.rawValue, PHAssetMediaType.video.rawValue)
        let assets = PHAsset.fetchAssets(with: fetchOptions)
        
        assets.enumerateObjects { (obj, idx, bool) -> Void in
            self.all.append(obj)
            // self.all.insert(obj, at: 0)
            
        }
        FilesArray  = []
        print(all)
        mycollectionview.reloadData()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if !IS_IPHONE{
            self.perform(#selector(resetplayer), with: nil, afterDelay: 0.1)
        }
    }
    
    @objc func resetplayer(){
        
        if mycollectionview != nil{
            mycollectionview.reloadData()
        }
    }
    
    func convertTime(_ time: Int) -> String? {
        let minutes = time / 60
        let seconds = time % 60
        return String(format: "%02ld:%02ld", minutes, seconds)
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
}
