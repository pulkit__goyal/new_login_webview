//
//  HomeVC.swift
//  Latakoo
//
//  Created by Shruti Aggarwal on 14/11/18.
//  Copyright © 2018 parangat technology. All rights reserved.
//

import UIKit
import SDWebImage
import CoreData
import MobileCoreServices
import TableViewReloadAnimation
import Photos
import AWSSNS
import Alamofire
import SwiftyJSON
import Foundation

@IBDesignable

class UISwitchCustom: UISwitch {
    @IBInspectable var OffTint: UIColor? {
        didSet {
            self.tintColor = OffTint
            self.layer.cornerRadius = 16
            self.backgroundColor = OffTint
        }
    }
}


class homecell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var imgpost: UIImageView!
    @IBOutlet weak var imguser: UIImageView!
    @IBOutlet weak var lblusername: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lblnetwork: UILabel!
    @IBOutlet weak var lblcount: UILabel!
    @IBOutlet weak var group: UIImageView!
    @IBOutlet weak var smallthumb: UIImageView!
    @IBOutlet weak var imgwhiteboc: UIImageView!
}

class HomeVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UISearchBarDelegate {
    var Arrhomedata: [Any] = []
    @IBOutlet weak var tblhome: UITableView!
    @IBOutlet weak var txtsearch: UITextField!
    @IBOutlet weak var searchheight: NSLayoutConstraint!

    @IBOutlet weak var btncross: UIButton!

    @IBOutlet weak var lblpilot: UILabel!
    @IBOutlet weak var pilotstackview : UIStackView!

    @IBOutlet weak var btnsearch: UIButton!

    @IBOutlet var searchbar: UISearchBar!

    var flagnoresult = 0
    var currentPage = 0
    var isLoadingList: Bool = false
    var count = 0
    var flag: Int = 0
    var ofset: Int = 0
    var searchDataOfset: Int = 0
    var networksArray: [Any] = []
    var filterdnetworid: String = "0"
    let appdelegate = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet weak var whitebox: UIImageView!

    var issearching: Bool = false
    let bgView = UIView()
    let bgView1 = UIView()
    let backgroundbg = UIView()
    let playnew = UIButton(type: .custom)

    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
        
       // self.Servicegettopic()
        //   self.initializeLoader()
        // appdelegate.initializeLoader()
        
    

        if app.isgotomanifest {
            var story: UIStoryboard?
            if IS_IPHONE {
                story = UIStoryboard.init(name: "Manifest", bundle: nil)
            } else {
                story = UIStoryboard.init(name: "ManifestIPAD", bundle: nil)
            }
            let ManifestVC = story?.instantiateViewController(withIdentifier: "ManifestVC") as! ManifestVC
            ManifestVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(ManifestVC, animated: false)
            app.isgotomanifest = false
        }

        searchbar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        searchbar.showsCancelButton = true
        searchbar.delegate = self
        if #available(iOS 13.0, *) {
            searchbar.tintColor = UIColor.label
        } else {
            searchbar.tintColor = UIColor.black
        }

        let status = PHPhotoLibrary.authorizationStatus()

        if (status == PHAuthorizationStatus.authorized) {

            DispatchQueue.global(qos: .background).async {
                self.getall()
                self.getvideo()
                self.getimages()
            }

        }

        else if (status == PHAuthorizationStatus.denied) {
            // Access has been denied.

        }

        else if (status == PHAuthorizationStatus.notDetermined) {

            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ (newStatus) in

                if newStatus == .authorized {
                    DispatchQueue.global(qos: .background).async {
                        self.getall()
                        self.getvideo()
                        self.getimages()
                    }
                }

            })
        }

        else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.

        }



        self.getHomedata()
        whitebox.isHidden = true
        btnsearch.isHidden = true
        btncross.isHidden = true
        txtsearch.isHidden = true
        searchheight.constant = 0






        if #available(iOS 10.0, *) {
            tblhome.refreshControl = refreshControl
        } else {
            tblhome.addSubview(refreshControl)
        }
        refreshControl.tintColor = UIColor.init(named: "colordark")
        refreshControl.attributedTitle = NSAttributedString(string: "loading...")
        refreshControl.addTarget(self, action: #selector(refreshlist(_:)), for: .valueChanged)



        self.subscribefornetwork()





        if #available(iOS 13.0, *) {
            if UITraitCollection.current.userInterfaceStyle == .dark {
                appdelegate.isdarmode = true
            }
            else {
                appdelegate.isdarmode = false
            }
        }

        let str = UserDefaults.standard.value(forKey: "isshowmenifest") as? String
        print(str!)
        
        if (str == "NO") {
            lblpilot.isHidden = false
            pilotstackview.isHidden = true
            
        } else {
            lblpilot.isHidden = true
            pilotstackview.isHidden = false
            DispatchQueue.global(qos: .background).async {
                self.subscibefortopic()
            }
            if (UserDefaults.standard.value(forKey: "redirecttonext") != nil) {

                self.perform(#selector(gotonext), with: nil, afterDelay: 0.5)
            }



        }

//        UserDefaults.standard.set("https://api2.latakoo.com/", forKey: "serverManifest")

        self.tabBarController?.tabBar.isHidden = false
       // self.tabBarController?.tabBar.backgroundColor = UIColor.init(named: "tabbackground")

    }

    func ShowProgress() {


        bgView.frame = (view.frame)
        bgView1.frame = (view.frame)
        bgView1.backgroundColor = UIColor.black
        bgView1.alpha = 0.3
        appdelegate.imageView!.contentMode = .scaleAspectFit
        appdelegate.imageView!.frame = CGRect(x: 0, y: 0, width: 120, height: 120)
        appdelegate.imageView!.backgroundColor = UIColor.init(named: "loaderbackground")
        appdelegate.imageView!.layer.cornerRadius = 60
        appdelegate.imageView!.layer.masksToBounds = true
        appdelegate.imageView!.center = (view.center)
        bgView.addSubview(appdelegate.imageView!)
        playnew.frame = CGRect(x: 0, y: 0, width: 120, height: 120)
        playnew.setTitle("Cancel..", for: .normal)
        if #available(iOS 13.0, *) {
            playnew.setTitleColor(UIColor.init(named: "pilotcolor"), for: .normal)
        } else {
            // Fallback on earlier versions
            playnew.setTitleColor(UIColor.white, for: .normal)
        }
        playnew.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        playnew.backgroundColor = .clear
        playnew.addTarget(self, action: #selector(playnewmethod), for: .touchUpInside)
        playnew.tintColor = UIColor.clear
        playnew.center = (view.center)
        playnew.frame.origin.y = self.view.frame.size.height / 2 + 20;
        self.view.addSubview(playnew)


        bgView.isHidden = true
        bgView1.isHidden = true
        playnew.isHidden = true

        DispatchQueue.main.async {
            self.bgView.addSubview(self.appdelegate.imageView!)
            self.view.addSubview(self.bgView1)
            self.view.addSubview(self.bgView)
            self.view.bringSubview(toFront: self.bgView1)
            self.view.bringSubview(toFront: self.bgView)
            self.view.bringSubview(toFront: self.playnew)

            self.bgView.isHidden = false
            self.bgView1.isHidden = false

            if self.issearching {
                self.playnew.isHidden = false
            }
        }
    }

    @objc func playnewmethod(sender: UIButton) {
        self.stopTheDamnRequests()
        self.HideProgress()

    }

    func stopTheDamnRequests() {
        if #available(iOS 9.0, *) {
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                tasks.forEach { $0.cancel() }
            }
        } else {
            Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
                sessionDataTask.forEach { $0.cancel() }
                uploadData.forEach { $0.cancel() }
                downloadData.forEach { $0.cancel() }
            }
        }
    }

    func HideProgress() {
        DispatchQueue.main.async {
            self.bgView.removeFromSuperview()
            self.bgView1.removeFromSuperview()
            self.playnew.removeFromSuperview()

            self.self.appdelegate.imageView!.removeFromSuperview()

            self.bgView.isHidden = true
            self.bgView1.isHidden = true
            self.playnew.isHidden = true

        }
    }

    @objc func gotonext() {

        var strnotification: String? = nil


        let DIct = UserDefaults.standard.dictionary(forKey: "userpush")
        strnotification = "\(DIct?["notificationType"] as? String ?? "request")"

        UserDefaults.standard.removeObject(forKey: "redirecttonext")
        if strnotification == "createassignment" {

            UserDefaults.standard.synchronize()
            var asignmentid = UserDefaults.standard.value(forKey: "assignmentID") as? String
            let DIct = UserDefaults.standard.dictionary(forKey: "userpush")
            asignmentid = DIct?["assignmentID"] as? String

            var story: UIStoryboard?

            if IS_IPHONE {
                story = UIStoryboard.init(name: "Manifest", bundle: nil)
            } else {
                story = UIStoryboard.init(name: "ManifestIPAD", bundle: nil)
            }
            let VC = story?.instantiateViewController(withIdentifier: "AssignmentWebVC") as? AssignmentWebVC
            let object = UserDefaults.standard.value(forKey: "userId") as! String
            VC?.weburl = "https://manifest.latakoo.com/api/remote/?ownerid=\(object)&assignmentid=\(asignmentid ?? "")"
            VC?.isfeed = "NO"
            appdelegate.Assignmentname = DIct?["assignmentTitle"] as? String ?? "0"
            appdelegate.AssignmentID = "\(DIct?["assignmentID"] as? String ?? "0")"
            appdelegate.OrgID = "\(DIct?["orgID"] as? String ?? "0")"
            appdelegate.TopicID = "\(DIct?["topicID"] as? String ?? "0")"
            VC?.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(VC!, animated: true)
            
        } else if (strnotification == "createtopicassignment") || (strnotification == "updatetopic") {
            let DIct = UserDefaults.standard.dictionary(forKey: "userpush")

            var story: UIStoryboard?

            if IS_IPHONE {
                story = UIStoryboard.init(name: "Manifest", bundle: nil)
            } else {
                story = UIStoryboard.init(name: "ManifestIPAD", bundle: nil)
            }
            let VC = story?.instantiateViewController(withIdentifier: "TopicDetail") as? TopicDetail
            VC?.topicid = "\(DIct?["topicID"] as! String)"
            VC?.isfeed = "NO"
            VC?.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(VC!, animated: true)

        } else {

            var story: UIStoryboard?

            if IS_IPHONE {
                story = UIStoryboard.init(name: "Manifest", bundle: nil)
            } else {
                story = UIStoryboard.init(name: "ManifestIPAD", bundle: nil)
            }

            let VC = story?.instantiateViewController(withIdentifier: "ManifestVC") as? ManifestVC
            VC?.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(VC!, animated: false)
        }

    }

    @IBAction func manifest(_ sender: Any) {

        let str = UserDefaults.standard.value(forKey: "isshowmenifest") as? String
        if (str == "YES") {

            let options = YActionSheet(title1: "", dismissButtonTitle: "", otherButtonTitles: ["Pilot", "Manifest"], dismissOnSelect: true, ispilot: true)
            options!.show(in: self, withYActionSheetBlock: { buttonIndex, isCancel in
                if buttonIndex == 1 {


                    var story: UIStoryboard?

                    if IS_IPHONE {
                        story = UIStoryboard.init(name: "Manifest", bundle: nil)
                    } else {
                        story = UIStoryboard.init(name: "ManifestIPAD", bundle: nil)
                    }

                    let ManifestVC = story?.instantiateViewController(withIdentifier: "ManifestVC") as! ManifestVC
                    ManifestVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(ManifestVC, animated: false)



                }
            })
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        NotificationCenter.default.addObserver(self, selector: #selector(self.hidePullToRefresh), name: NSNotification.Name(rawValue: "hidePullToRefresh"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.showPullToRefresh), name: NSNotification.Name(rawValue: "showPullToRefresh"), object: nil)

        //  self.perform(#selector(shownavbar), with: nil, afterDelay: 0.1)

        if appdelegate.isfilter == true {
            flag = 1
            ofset = 0
            searchDataOfset = 0
            currentPage = 0
            self.getHomedata()
            appdelegate.isfilter = false
            self.ShowProgress()
        }

        if IS_IPHONE {
            appdelegate.restrictRotation = .portrait
        } else {
            appdelegate.restrictRotation = .all
        }

   //     self.tabBarController?.tabBar.backgroundColor = UIColor.init(named: "tabbackground")


    }

    @objc private func hidePullToRefresh(notification: NSNotification) {
        if #available(iOS 10.0, *) {
            tblhome.refreshControl = nil
        } else {
            tblhome.willRemoveSubview(refreshControl)
        }
    }

    @objc private func showPullToRefresh(notification: NSNotification) {
        if #available(iOS 10.0, *) {
            tblhome.refreshControl = refreshControl
        } else {
            tblhome.addSubview(refreshControl)
        }
        refreshControl.tintColor = UIColor(named: "colordark")
        refreshControl.attributedTitle = NSAttributedString(string: "loading...")
        refreshControl.addTarget(self, action: #selector(refreshlist(_:)), for: .valueChanged)
    }

    @objc private func refreshlist(_ sender: Any) {
        flag = 1
        ofset = 0
        searchDataOfset = 0
        currentPage = 0
        self.getHomedata()
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }

    //MARK: - get pilot data
    func getHomedata() {

                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

        if flag == 0 {
            self.ShowProgress()
        }

        

        let ishomeselected = UserDefaults.standard.value(forKey: "filterednetwork") as! String

        print(ishomeselected)

        if ishomeselected == "All" {
            filterdnetworid = "0"

        }else if ishomeselected == "youruploads"{
            
            filterdnetworid = "1"
            
            self.getyourupload()
            
            return;

        }
        
        else if ishomeselected == "HomeNetworks" {
            let homenetwork = UserDefaults.standard.value(forKey: "homenetwork") as? [String] ?? []
            filterdnetworid = homenetwork.joined(separator: ",")


        } else {
            let filerednetwork = UserDefaults.standard.value(forKey: "selectedfilterNetwork") as? [String] ?? []

            filterdnetworid = filerednetwork.joined(separator: ",")

        }



        print(filterdnetworid)



        var tokenVal: String!
        if UserDefaults.standard.value(forKey: "deviceTokenString") == nil {
            tokenVal = "C649C67E66101C370344466BE9B2388C86DEF9641F2F655B5D526653EEF1BBFA"
        } else {
            tokenVal = UserDefaults.standard.value(forKey: "deviceTokenString") as? String
            print("tokenvalue %@", tokenVal)
        }
        
        let aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=object.get.bynetwork"
        let strofset = String(format: "%d", ofset)
        print(strofset)
        //  let networkid = UserDefaults.standard.value(forKey: "defultnetwork") as! String
        let aDictParam: [String: Any]!
        aDictParam = ["network_id": filterdnetworid,
            "limit": "30",
            "offset": strofset
        ]


        let heder = appdelegate.getAPIEncodedHeader(withParameters: aStrApi)

        let headervalue = ["X_PFTP_API_TOKEN": heder]

        print(aStrApi)
        print(headervalue)
        print(aDictParam)
        
        self.isLoadingList = true
        APIManager.requestPOSTURL(aStrApi, params: aDictParam as [String: AnyObject]?, headers: headervalue as? [String: String], success: { (usrDetail) in
                print(usrDetail)
                if usrDetail["status_code"].rawString() == "0" {
                    let usrDetailVal = usrDetail["result"].rawValue as! [String: Any]
                    print(usrDetailVal)
                    self.flagnoresult = 0

                    let Arr = usrDetailVal["objects"] as! [Any]
                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                    self.HideProgress()
                    self.refreshControl.endRefreshing()
                    self.isLoadingList = false
                    if self.flag == 1 {
                        self.refreshControl.endRefreshing()
                        self.Arrhomedata = []
                        self.flag = 0
                    }


                    if self.Arrhomedata.count > 0 {
                        self.Arrhomedata.append(contentsOf: Arr)
                        self.tblhome.reloadData()


                    } else {
                        self.Arrhomedata.append(contentsOf: Arr)
                        self.tblhome.reloadData(
                            with: .simple(duration: 1.5, direction: .rotation3D(type: .doctorStrange),
                                constantDelay: 0))

                    }

                } else {
                    self.flagnoresult = 1
                    self.refreshControl.endRefreshing()
                    // self.view.makeToast(usrDetail["message"].rawString())
                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                    self.HideProgress()
                    self.isLoadingList = false
                    self.tblhome.reloadData()

                }

            },

            failure: { (error) in
                print(error)
                // self.view.makeToast(error.localizedDescription)
                self.flagnoresult = 0
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                self.HideProgress()
                self.isLoadingList = false
            self.tblhome.reloadData()
            self.refreshControl.endRefreshing()

            })
    }
    
    
    func getyourupload() {

                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

        if flag == 0 {
            self.ShowProgress()
        }

        

     



        var tokenVal: String!
        if UserDefaults.standard.value(forKey: "deviceTokenString") == nil {
            tokenVal = "C649C67E66101C370344466BE9B2388C86DEF9641F2F655B5D526653EEF1BBFA"
        } else {
            tokenVal = UserDefaults.standard.value(forKey: "deviceTokenString") as? String
            print("tokenvalue %@", tokenVal)
        }

        let aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=object.get.byuser"
        let strofset = String(format: "%d", ofset)
        print(strofset)
        
        
        let aDictParam: [String: Any]!
        aDictParam = ["limit": "30",
            "offset": strofset
        ]
        
        


        let heder = appdelegate.getAPIEncodedHeader(withParameters: aStrApi)

        let headervalue = ["X_PFTP_API_TOKEN": heder]


        self.isLoadingList = true
        APIManager.requestPOSTURL(aStrApi, params: aDictParam as [String: AnyObject]?, headers: headervalue as? [String: String], success: { (usrDetail) in
                print(usrDetail)
                if usrDetail["status_code"].rawString() == "0" {
                    let usrDetailVal = usrDetail["result"].rawValue as! [String: Any]
                    print(usrDetailVal)
                    self.flagnoresult = 0

                    let Arr = usrDetailVal["objects"] as! [Any]
                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                    self.HideProgress()
                    self.refreshControl.endRefreshing()
                    self.isLoadingList = false
                    if self.flag == 1 {
                        self.refreshControl.endRefreshing()
                        self.Arrhomedata = []
                        self.flag = 0
                    }


                    if self.Arrhomedata.count > 0 {
                        self.Arrhomedata.append(contentsOf: Arr)
                        self.tblhome.reloadData()


                    } else {
                        self.Arrhomedata.append(contentsOf: Arr)
                        self.tblhome.reloadData(
                            with: .simple(duration: 1.5, direction: .rotation3D(type: .doctorStrange),
                                constantDelay: 0))

                    }

                } else {
                    self.flagnoresult = 1

                    // self.view.makeToast(usrDetail["message"].rawString())
                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                    self.HideProgress()
                    self.isLoadingList = false

                }

            },

            failure: { (error) in
                print(error)
                // self.view.makeToast(error.localizedDescription)
                self.flagnoresult = 0
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                self.HideProgress()
                self.isLoadingList = false
            })
    }
    
    
    
    
//    func Servicegettopic() {
//
//
//        let headers = ["X-Api-Key":"JXBhcmFuZ2F0X3RlY2hub2xvZ2llc18tdHJpYWwtc3ZwZm1pYm8WcGFyYW5nYXQgdGVjaG5vbG9naWVzIC5wYXJhbmdhdF90ZWNobm9sb2dpZXNfLXRyaWFsLXN2cGZtaWJvLWRldnJlY29nAAABd9zeWFKOhl3B"]
//        let aStrApi = "https://service.fing.io/2/devicetypelist"
//     print(aStrApi)
//        print(headers)
//
//        let encoding = URLEncoding.httpBody
//        Alamofire.request(aStrApi, method: .get, parameters: nil, encoding: encoding, headers: headers).responseJSON { (responseObject) -> Void in
//
//            if responseObject.result.isSuccess {
//                let resJson = JSON(responseObject.result.value!)
//                let usrDetail = resJson
//                print(usrDetail)
//
//
//            }
//            if responseObject.result.isFailure {
//                self.appdelegate.HideProgress()
//
//
//            }
//
//
//        }
//
//
//
//
//
//    }
    
    

    //MARK: - table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if flagnoresult == 1 {
            return 1
        } else {
            return Arrhomedata.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         if flagnoresult == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! homecell
        let dict = Arrhomedata[indexPath.row] as! [String:Any]
        
        print(dict)
        cell.imguser.sd_setImage(with: URL(string: dict["uploaderIcon"] as! String), placeholderImage: UIImage(named: ""))
        let unixTimestamp = Double(dict["unix_created"] as! String)
        let date = Date(timeIntervalSince1970: unixTimestamp ?? 0)
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "EEEE, d MMM yyyy HH:mm:ss"
        cell.lbldate.text =  dateFormatter1.string(from:date)
        cell.lblnetwork.text = self.netWorkName(fromID: dict["network"] as? String)
        cell.lblusername.text = dict["uploader"] as? String
        let file = dict["filename"] as? String // path to some file
        let fileExtension = URL(fileURLWithPath: file ?? "").pathExtension as CFString
        cell.title.text = dict["title"] as? String
        cell.lblcount.layer.cornerRadius = cell.lblcount.frame.size.width/2
        cell.lblcount.layer.borderColor = UIColor.white.cgColor
        cell.lblcount.layer.borderWidth = 1
        cell.lblcount.layer.masksToBounds = true
            cell.lblcount.isHidden = true

        let unmanagedFileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, nil)
        let fileUTI = unmanagedFileUTI!.takeRetainedValue()

        if UTTypeConformsTo(fileUTI , kUTTypeImage) {

            cell.imgpost.sd_setImage(with: URL(string: dict["thumbnail"] as! String), placeholderImage:appdelegate.jeremy)
             cell.smallthumb.sd_setImage(with: URL(string: dict["thumbnail"] as! String), placeholderImage: UIImage(named: ""))
           var count = 0


            if dict["video_group_pieces_total"] is NSNull {
              count = 0

            }else{
                let countnew =  dict["video_group_pieces_total"] as? String
                count = Int(countnew!) ?? 0
                cell.title.text = dict["video_group_filename"] as? String
            }


            print("count value is => \(count)")

            if count > 0 {
                cell.lblcount.isHidden = false
                cell.group.isHidden = false
                cell.smallthumb.isHidden = false
                cell.imgwhiteboc.isHidden = true
                cell.imgpost.isHidden = true
                cell.lblcount.text = "\(count)"


            }else{
                cell.lblcount.isHidden = true
                cell.group.isHidden = true
                cell.smallthumb.isHidden = true
                cell.imgwhiteboc.isHidden = false
                cell.imgpost.isHidden = false
            }





            print("It's image")

        } else if UTTypeConformsTo(fileUTI , kUTTypeMovie) {
            print(dict)

            var count = 0

            cell.imgpost.sd_setImage(with: URL(string: dict["thumbnail"] as! String), placeholderImage:appdelegate.jeremy)
            cell.smallthumb.sd_setImage(with: URL(string: dict["thumbnail"] as! String), placeholderImage: UIImage(named: ""))

            if dict["original_folder"] is NSNull {

            cell.lblcount.isHidden = true
            cell.group.isHidden = true
            cell.smallthumb.isHidden = true
            cell.imgwhiteboc.isHidden = false
            cell.imgpost.isHidden = false


            print("It's video")
            }else{
                if dict["video_group_pieces_total"] is NSNull {
                          count = 0
                          }else{
                      let countnew =  dict["video_group_pieces_total"] as? String
                          count = Int(countnew!) ?? 0
                          cell.title.text = dict["video_group_filename"] as? String
                      cell.lblcount.isHidden = false
                        cell.group.isHidden = false
                        cell.smallthumb.isHidden = false
                        cell.imgwhiteboc.isHidden = true
                        cell.imgpost.isHidden = true
                        cell.lblcount.text = "\(count)"

                          }





            }


        } else if UTTypeConformsTo(fileUTI , kUTTypeAudio) {
            var count = 0


            if dict["original_folder"] is NSNull {


            cell.lblcount.isHidden = true
            cell.group.isHidden = true
            cell.smallthumb.isHidden = true
            cell.imgwhiteboc.isHidden = false
            cell.imgpost.isHidden = false
            }else{

        if dict["video_group_pieces_total"] is NSNull {
            count = 0
           }else{
            let countnew =  dict["video_group_pieces_total"] as? String
            count = Int(countnew!) ?? 0
            cell.title.text = dict["video_group_filename"] as? String
            cell.lblcount.isHidden = false
            cell.group.isHidden = false
            cell.smallthumb.isHidden = false
            cell.imgwhiteboc.isHidden = true
            cell.imgpost.isHidden = true
            cell.lblcount.text = "\(count)"
            }


            }


            cell.imgpost.image = UIImage.init(named:"placeaudio")
            cell.smallthumb.image = UIImage.init(named:"placeaudio")


            print("It's audio")
        }else {
            var count = 0

            print(dict)

            if dict["original_folder"] is NSNull {


            cell.lblcount.isHidden = true
            cell.group.isHidden = true
            cell.smallthumb.isHidden = true
            cell.imgwhiteboc.isHidden = false
            cell.imgpost.isHidden = false
            }else{

        if dict["video_group_pieces_total"] is NSNull {
            count = 0
           }else{
            let countnew =  dict["video_group_pieces_total"] as? String
            count = Int(countnew!) ?? 0
            cell.title.text = dict["video_group_filename"] as? String
           cell.lblcount.isHidden = false
                         cell.group.isHidden = false
                         cell.smallthumb.isHidden = false
                         cell.imgwhiteboc.isHidden = true
                         cell.imgpost.isHidden = true
                         cell.lblcount.text = "\(count)"

    }


            }


            cell.imgpost.sd_setImage(with: URL(string: dict["thumbnail"] as! String), placeholderImage:appdelegate.jeremy)
            cell.smallthumb.sd_setImage(with: URL(string: dict["thumbnail"] as! String), placeholderImage: UIImage(named: ""))


        }
        
         
        return cell
         }else{
       let cell = tableView.dequeueReusableCell(withIdentifier: "NoResult") as! homecell
        return cell

        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if flagnoresult == 0 {

            let dict = Arrhomedata[indexPath.row] as! [String: Any]

            
            print(dict)
            
            
            let file = dict["filename"] as! String // path to some file
            let fileExtension = URL(fileURLWithPath: file).pathExtension as CFString

            let unmanagedFileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, nil)
            let fileUTI = unmanagedFileUTI!.takeRetainedValue()

            if UTTypeConformsTo(fileUTI, kUTTypeImage) {


                var count = 0


                if dict["video_group_pieces_total"] is NSNull {
                    count = 0

                } else {
                    count = Int(dict["video_group_pieces_total"] as! String)!
                }


                print("count value is => \(count)")

                if count > 0 {

                    var story: UIStoryboard?

                    if IS_IPHONE {
                        story = UIStoryboard.init(name: "Main", bundle: nil)
                    } else {
                        story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                    }

                    let PilotobjectVC = story?.instantiateViewController(withIdentifier: "PilotobjectVC") as! PilotobjectVC
                    PilotobjectVC.dictbact = dict
                    self.navigationController?.pushViewController(PilotobjectVC, animated: true)

                } else {


                    var story: UIStoryboard?

                    if IS_IPHONE {
                        story = UIStoryboard.init(name: "Main", bundle: nil)
                    } else {
                        story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                    }
                    let PilotimagedetailVC = story?.instantiateViewController(withIdentifier: "PilotimagedetailVC") as! PilotimagedetailVC
                    PilotimagedetailVC.dictback = Arrhomedata[indexPath.row] as! [String: Any]
                    PilotimagedetailVC.groupid = ""

                    PilotimagedetailVC.isimage = "image"
                    self.navigationController?.pushViewController(PilotimagedetailVC, animated: true)
                }


            } else if UTTypeConformsTo(fileUTI, kUTTypeMovie) {




                var story: UIStoryboard?

                if IS_IPHONE {
                    story = UIStoryboard.init(name: "Main", bundle: nil)
                } else {
                    story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                }


                if dict["original_folder"] is NSNull  || dict["original_folder"] as? String == ""{
                    let PilotimagedetailVC = story?.instantiateViewController(withIdentifier: "PilotimagedetailVC") as! PilotimagedetailVC
                    PilotimagedetailVC.dictback = Arrhomedata[indexPath.row] as! [String: Any]
                    PilotimagedetailVC.groupid = ""
                    PilotimagedetailVC.isimage = "video"
                    self.navigationController?.pushViewController(PilotimagedetailVC, animated: true)
                } else {

                    let GroupVCViewController = story?.instantiateViewController(withIdentifier: "GroupVCViewController") as! GroupVCViewController
                    GroupVCViewController.dictbact = Arrhomedata[indexPath.row] as! [String: Any]
                    self.navigationController?.pushViewController(GroupVCViewController, animated: true)

                }



            } else if UTTypeConformsTo(fileUTI, kUTTypeAudio) {

                var story: UIStoryboard?
                if IS_IPHONE {
                    story = UIStoryboard.init(name: "Main", bundle: nil)
                } else {
                    story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                }

                if dict["original_folder"] is NSNull  || dict["original_folder"] as? String == ""{
                    let PilotimagedetailVC = story?.instantiateViewController(withIdentifier: "PilotimagedetailVC") as! PilotimagedetailVC
                    PilotimagedetailVC.dictback = Arrhomedata[indexPath.row] as! [String: Any]
                    PilotimagedetailVC.groupid = ""
                    PilotimagedetailVC.isimage = "audio"
                    self.navigationController?.pushViewController(PilotimagedetailVC, animated: true)
                } else {
                    let GroupVCViewController = story?.instantiateViewController(withIdentifier: "GroupVCViewController") as! GroupVCViewController
                    GroupVCViewController.dictbact = Arrhomedata[indexPath.row] as! [String: Any]
                    self.navigationController?.pushViewController(GroupVCViewController, animated: true)

                }



            } else if UTTypeConformsTo(fileUTI, kUTTypePDF) {

                var story: UIStoryboard?
                if IS_IPHONE {
                    story = UIStoryboard.init(name: "Main", bundle: nil)
                } else {
                    story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                }

                if dict["original_folder"] is NSNull  || dict["original_folder"] as? String == ""{
                    let PilotimagedetailVC = story?.instantiateViewController(withIdentifier: "PilotimagedetailVC") as! PilotimagedetailVC
                    PilotimagedetailVC.dictback = Arrhomedata[indexPath.row] as! [String: Any]
                    PilotimagedetailVC.groupid = ""
                    PilotimagedetailVC.isimage = "pdf"
                    self.navigationController?.pushViewController(PilotimagedetailVC, animated: true)
                } else {
                    let GroupVCViewController = story?.instantiateViewController(withIdentifier: "GroupVCViewController") as! GroupVCViewController
                    GroupVCViewController.dictbact = Arrhomedata[indexPath.row] as! [String: Any]
                    self.navigationController?.pushViewController(GroupVCViewController, animated: true)

                }



            } else {

                var story: UIStoryboard?
                if IS_IPHONE {
                    story = UIStoryboard.init(name: "Main", bundle: nil)
                } else {
                    story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                }

                if dict["original_folder"] is NSNull  || dict["original_folder"] as? String == ""{
                    let PilotimagedetailVC = story?.instantiateViewController(withIdentifier: "PilotimagedetailVC") as! PilotimagedetailVC
                    PilotimagedetailVC.dictback = Arrhomedata[indexPath.row] as! [String: Any]
                    PilotimagedetailVC.groupid = ""
                    PilotimagedetailVC.isimage = "other"
                    self.navigationController?.pushViewController(PilotimagedetailVC, animated: true)
                } else {
                    let GroupVCViewController = story?.instantiateViewController(withIdentifier: "GroupVCViewController") as! GroupVCViewController
                    GroupVCViewController.dictbact = Arrhomedata[indexPath.row] as! [String: Any]
                    self.navigationController?.pushViewController(GroupVCViewController, animated: true)

                }



            }



        }

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if IS_IPHONE {
            return 138
        } else {
            return 177
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height) && !isLoadingList {

            print(tblhome.frame.origin.x)
            
            if self.issearching == true {
                isLoadingList = true
                searchDataOfset =  Arrhomedata.count
                self.searchbykeyword()
            } else {
                if tblhome.frame.origin.x == 0 {
                    isLoadingList = true
                    ofset = Arrhomedata.count
                    getHomedata()
                }
            }
            
//            if tblhome.frame.origin.x == 0 {
//                isLoadingList = true
//                ofset = ofset + Arrhomedata.count
//                getHomedata()
//            }
            
        }
    }

    @IBAction func filterbutton(_ sender: Any) {

        var story: UIStoryboard?

        if IS_IPHONE {
            story = UIStoryboard.init(name: "Main", bundle: nil)
        } else {
            story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
        }
        let FilterVC = story?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        FilterVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(FilterVC, animated: true)


    }

    //MARK: - Fetch network from coredata
    func netWorkName(fromID networkID: String?) -> String? {


        let networksArray = UserDefaults.standard.value(forKey: "AvailableNetworks") as? [Any]


        for dict in networksArray ?? [] {
            let dictnetwork = dict as! [String: Any]
            print(dictnetwork)
            let networkid = dictnetwork["company_id"] as! String

            if networkid == networkID {
                return dictnetwork["company_name"] as? String
            }
        }
        return ""
    }

    @IBAction func searchbutton(_ sender: Any) {

//
        tblhome.tableHeaderView = searchbar
        tblhome.contentOffset = CGPoint.zero




    }

    @IBAction func crossbutton(_ sender: Any) {
        searchheight.constant = 0
        whitebox.isHidden = true
        btncross.isHidden = true
        btnsearch.isHidden = true
        txtsearch.isHidden = true





    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        // self.searchbykeyword()
        searchheight.constant = 0
        whitebox.isHidden = true
        btncross.isHidden = true
        btnsearch.isHidden = true
        txtsearch.isHidden = true
        txtsearch.text = ""



    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.searchbykeyword()
        searchheight.constant = 0
        whitebox.isHidden = true
        btncross.isHidden = true
        btnsearch.isHidden = true
        txtsearch.isHidden = true
        txtsearch.resignFirstResponder()
        txtsearch.text = ""

        return true
    }

    func searchbykeyword()
    {

        issearching = true
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }
        self.ShowProgress()
        var aStrApi = ("\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=object.search&query=\(searchbar.text ?? "")" as NSString)
        // aStrApi = aStrApi.trimmingCharacters(in: .whitespaces) as (NSString)
        aStrApi = (aStrApi as NSString).addingPercentEscapes(using: String.Encoding.utf8.rawValue) as NSString? ?? "" as (NSString)


        print(aStrApi)
        let strofset = String(format: "%d", searchDataOfset)
        let aDictParam = [
            "limit": "30",
            "offset": strofset
        ] as [String: Any]

        self.isLoadingList = true
        APIManager.requestPOSTURL(aStrApi as String, params: aDictParam as [String: AnyObject], headers: nil, success: { (usrDetail) in
                print(usrDetail)

            
            if usrDetail["status_code"].rawString() == "0" {
                let usrDetailVal = usrDetail["result"].rawValue as! [String: Any]
                print(usrDetailVal)
                self.flagnoresult = 0

                let Arr = usrDetailVal["objects"] as! [Any]
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                self.HideProgress()
                self.refreshControl.endRefreshing()
                self.isLoadingList = false
                if self.flag == 1 {
                    self.refreshControl.endRefreshing()
                    self.Arrhomedata = []
                    self.flag = 0
                }


                if self.Arrhomedata.count > 0 {
                    self.Arrhomedata.append(contentsOf: Arr)
                    self.tblhome.reloadData()


                } else {
                    self.Arrhomedata.append(contentsOf: Arr)
                    self.tblhome.reloadData(
                        with: .simple(duration: 1.5, direction: .rotation3D(type: .doctorStrange),
                            constantDelay: 0))

                }

            } else {
                self.flagnoresult = 1

                // self.view.makeToast(usrDetail["message"].rawString())
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                self.HideProgress()
                self.isLoadingList = false

            }

            },

            failure: { (error) in
                print(error)
                self.HideProgress()
                self.issearching = false

                self.flagnoresult = 1
                self.tblhome.reloadData()


            })
    }
    
    func subscribefornetwork() {


        let notifynetwork: [Any] = UserDefaults.standard.value(forKey: "notification_preferences") as! [Any]
        let AvailableNetworks: [Any] = UserDefaults.standard.value(forKey: "AvailableNetworks") as? [Any] ?? []







        print("notification_preferences = \n \(notifynetwork)")
        print("AvailableNetworks = \n \(AvailableNetworks)")
        for index in 0..<notifynetwork.count {
            let dictnotify = notifynetwork[index] as! [String: Any]
            let company_id = "\(dictnotify["id"] as! String)"
            let notify = dictnotify["notify"] as! Bool

            print(company_id)
            print(notify)

            if notify == true {
                for dict in AvailableNetworks {
                    let dictnew = dict as! [String: Any]
                    print(dictnew)
                    let companyidd = "\(dictnew["company_id"] as! String)"
                    if companyidd == company_id {

                        let topicarn = dictnew["topic_arn"] as? String
                        print(topicarn ?? "")
                        DispatchQueue.global(qos: .background).async {
                            self.subscribefortop(topic: topicarn ?? "")
                        }
                        break;
                    }


                }
            }

        }
    }

    func subscribefortop(topic: String) {

        let plateformarn = appdelegate.SNSPlatformApplicationArn
        print(plateformarn)
        let sns = AWSSNS.default()
        let endpointRequest = AWSSNSCreatePlatformEndpointInput()
        endpointRequest?.platformApplicationArn = plateformarn
        let deviceTokenString = UserDefaults.standard.value(forKey: "DeviceToken")
        endpointRequest?.token = deviceTokenString as? String



        sns.createPlatformEndpoint(endpointRequest!).continueWith { (task) -> AnyObject? in

            if task.error != nil {
                print("Error creating platform endpoint: \(String(describing: task.error))")
                return nil
            }
            let result = task.result!
            let subscribeInput = AWSSNSSubscribeInput()
            subscribeInput?.topicArn = topic
            subscribeInput?.endpoint = result.endpointArn
            print("Enfdpoint created")
            print("Endpoint arn: \(result.endpointArn!)")
            subscribeInput?.protocols = "application"
            sns.subscribe(subscribeInput!).continueWith { (task) -> AnyObject? in

                if task.error != nil {
                    print("Error subscribing: \(String(describing: task.error))")
                    return nil
                }
                print("Subscribed succesfully")
                print("Subscription arn: \(String(describing: task.result?.subscriptionArn!))")
                let subscriptionConfirmInput = AWSSNSConfirmSubscriptionInput()


                subscriptionConfirmInput?.topicArn = topic
                sns.confirmSubscription(subscriptionConfirmInput!).continueWith { (task) -> AnyObject? in

                    if task.error != nil {

                    }

                    return nil
                }
                return nil
            }
            return nil
        }


    }

    @IBAction func search(_ sender: Any) {
        // tblhome.tableHeaderView = searchbar
        //  tblhome.contentOffset = CGPoint.zero
    }

    @IBAction func filter(_ sender: Any) {

        var story: UIStoryboard?

        if IS_IPHONE {
            story = UIStoryboard.init(name: "Main", bundle: nil)
        } else {
            story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
        }
        let FilterVC = story?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        self.navigationController?.pushViewController(FilterVC, animated: true)

    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.Arrhomedata = []
        self.flag = 0
        self.searchbykeyword()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.issearching = false
        self.tblhome.tableHeaderView = nil
    }
    
    func getimages() {
        appdelegate.image = []

        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)

        assets.enumerateObjects { (obj, idx, bool) -> Void in
            //  self.image.append(obj)
            self.appdelegate.image.insert(obj, at: 0)
        }

        print("all image done")

    }
    func getvideo() {
        appdelegate.video = []
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.video, options: fetchOptions)

        assets.enumerateObjects { (obj, idx, bool) -> Void in
            // self.video.append(obj)
            self.appdelegate.video.insert(obj, at: 0)

        }

        print("all video done")

    }

    func getall() {

        appdelegate.all = []
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d || mediaType = %d", PHAssetMediaType.image.rawValue, PHAssetMediaType.video.rawValue)
        let assets = PHAsset.fetchAssets(with: fetchOptions)

        assets.enumerateObjects { (obj, idx, bool) -> Void in
            self.appdelegate.all.append(obj)
            // self.all.insert(obj, at: 0)
        }
        print("all done")

    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if !IS_IPHONE {
            self.perform(#selector(changeloader), with: nil, afterDelay: 0.2)
        }
    }
    @objc func changeloader() {
        appdelegate.initializeLoader()
    }

    func subscibefortopic() {
        let plateformarn = appdelegate.SNSPlatformApplicationArn
        print(plateformarn)
        let sns = AWSSNS.default()
        let endpointRequest = AWSSNSCreatePlatformEndpointInput()
        endpointRequest?.platformApplicationArn = plateformarn
        let deviceTokenString = UserDefaults.standard.value(forKey: "DeviceToken")
        endpointRequest?.token = deviceTokenString as? String
        sns.createPlatformEndpoint(endpointRequest!).continueWith { (task) -> AnyObject? in
            if task.error != nil {
                return nil
            }
            let result = task.result!
            let subscribeInput = AWSSNSSubscribeInput()
            let endpoint: String = "\(result.endpointArn!)"
            self.updatetoken(EndPointArn: endpoint)
            UserDefaults.standard.set(endpoint, forKey: "EndpointArn")

            sns.subscribe(subscribeInput!).continueWith { (task) -> AnyObject? in
                if task.error != nil {
                    return nil
                }

                let subscriptionConfirmInput = AWSSNSConfirmSubscriptionInput()
                sns.confirmSubscription(subscriptionConfirmInput!).continueWith { (task) -> AnyObject? in
                    if task.error != nil {
                    }

                    return nil
                }
                return nil
            }
            return nil
        }
    }

    func unableEndPoint() {
        let deviceTokenString = UserDefaults.standard.object(forKey: "DeviceToken") as? String
        let manager = AWSSNS.default()
        let EndPointArn = UserDefaults.standard.value(forKey: "EndpointArn") as? String
        let attributes = [
            "Token": deviceTokenString ?? "",
            "Enabled": "true"
        ]
        let seai = AWSSNSSetEndpointAttributesInput()
        seai?.attributes = attributes
        seai?.endpointArn = EndPointArn
        let setEndpointAttributesTask: AWSTask = manager.setEndpointAttributes(seai!)
        setEndpointAttributesTask.continueWith(block: { task in
            if let result = task.result, let error1 = task.error {
            }

            if task.error != nil {
                if let error1 = task.error {
                }
            } else {
                if let result = task.result {
                }
            }
            return nil
        })
    }

    func updatetoken(EndPointArn: String) {
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }


        let headers = ["Authorization":
                "Bearer \(UserDefaults.standard.value(forKey: "Authtoken") ?? "")"]
        let aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)devices"

        var token: String = "4314343432432"
        if UserDefaults.standard.value(forKey: "DeviceToken") != nil {
            token = (UserDefaults.standard.value(forKey: "DeviceToken") as? String)!
        }

        let param = ["token": token,
            "os_type": "IOS",
            "os_version": "IOS13",
            "app_version": "3.0",
            "endpoint_arn": EndPointArn

        ]
        print(aStrApi)
        print(headers)
        print(param)

        let encoding = Alamofire.JSONEncoding.default
        Alamofire.request(aStrApi, method: .post, parameters: param as Parameters, encoding: encoding, headers: headers).responseJSON { (responseObject) -> Void in

            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                let usrDetail = resJson
                print(usrDetail)

            }
            if responseObject.result.isFailure {

            }

        }


    }
}
