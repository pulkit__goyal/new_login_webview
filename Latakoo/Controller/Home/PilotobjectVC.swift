//
//  PilotobjectVC.swift
//  Latakoo
//
//  Created by Mac on 09/07/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import UIKit
import MobileCoreServices

class pilotcell : UICollectionViewCell {
    
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var backgroundimage: UIImageView!

}

class PilotobjectVC:  UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet var lblheader: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    var dictbact : [String:Any] = [:]
    var videoId : String = ""
    var Arrimagedata : [Any] = []
    @IBOutlet weak var currentindex: UILabel!
    let App = UIApplication.shared.delegate as! AppDelegate

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print(dictbact)
        
        
        lblheader.text = dictbact["video_group_filename"] as? String
       videoId = (dictbact["original_folder"] as? String)!
        videoId = AJNotificationView.changetoencodedstring(videoId)
        print(videoId)

//        let customAllowedSet =  NSCharacterSet(charactersIn:"=\"#%/<>?@\\^`{|}& ").inverted
//        let urlString = (dictbact["original_folder"] as? String)!.addingPercentEncoding(withAllowedCharacters: customAllowedSet)!
//        print("urlString: \(urlString)")
//        videoId = urlString
        self.getimagadata()
        
        collectionView.collectionViewLayout = CardsCollectionViewLayout()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
    }
    
    var colors: [UIColor]  = [
        UIColor(red: 237, green: 37, blue: 78),
        UIColor(red: 249, green: 220, blue: 92),
        UIColor(red: 194, green: 234, blue: 189),
        UIColor(red: 1, green: 25, blue: 54),
        UIColor(red: 255, green: 184, blue: 209)
    ]
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! pilotcell
        let dict = Arrimagedata[indexPath.item] as! [String : Any]
        cell.layer.cornerRadius = 7.0
        cell.imageview.layer.cornerRadius = 7.0
        cell.imageview.layer.masksToBounds = true
        cell.backgroundimage.layer.cornerRadius = 7.0
        cell.backgroundimage.layer.masksToBounds = true
       cell.backgroundimage.layer.masksToBounds = true

        cell.imageview.sd_setImage(with: URL(string: dict["stream_url"] as! String), placeholderImage: App.jeremy)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Arrimagedata.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        var story : UIStoryboard?
             
        if IS_IPHONE {
             story = UIStoryboard.init(name: "Main", bundle: nil)
            }else{
             story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
        }
        
        
        let dict = Arrimagedata.last as! [String:Any]
        let PilotimagedetailVC = story?.instantiateViewController(withIdentifier: "PilotimagedetailVC") as! PilotimagedetailVC
        PilotimagedetailVC.dictback = Arrimagedata[indexPath.item] as! [String : Any]
        PilotimagedetailVC.groupid = dict["id"] as! String
        PilotimagedetailVC.isimage = "image"
        self.navigationController?.pushViewController(PilotimagedetailVC, animated: false)
    }

  

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentIndex = Int(collectionView.contentOffset.x / collectionView.frame.size.width)
        
               print(currentIndex)
                self.currentindex.text = "\(currentIndex+1) OF \(self.Arrimagedata.count)"

    }

    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
func getimagadata(){
    
    
                        if !Reachability.isConnectedToNetwork() {
        print("Internet is Not available.")
        AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
        return
    }
      let appdelegate = UIApplication.shared.delegate as! AppDelegate
      appdelegate.ShowProgress()
    
    print(videoId)

      let networkIDString = dictbact["network"] as! String
          print("networkIDString: \(networkIDString)")
          let aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=cloudvideo.get.byfolder&folder=\(videoId)&network_id=\(networkIDString)"
      //    print(aStrApi)
    
    //        aStrApi = AJNotificationView.changetoencodedstring(aStrApi)
    //        print(aStrApi)
   
    APIManager.requestPOSTURL(aStrApi, params:nil as [String : AnyObject]?, headers: nil, success: { [self](usrDetail) in
        print(usrDetail)
        if usrDetail["status_code"].rawString() == "0"{
            let usrDetailVal = usrDetail["result"].rawValue as! [Any]
            self.Arrimagedata   = usrDetailVal
            print(self.Arrimagedata)
            
            var isGroupHasOnlyPhotos = false
            for i in 0 ..< self.Arrimagedata.count {

                let feedData = self.Arrimagedata[i] as! NSDictionary
                print("feedData: \(feedData)")
                let titleString = feedData["title"] as! String
                print("titleString: \(titleString)")
                let stream_url = URL(string: feedData["stream_url"] as! String)
                print("stream_url: \(stream_url!)")

                let extensionn = stream_url!.pathExtension as CFString
                print("extensionn: \(extensionn)")
                let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extensionn as CFString, nil)

                let retainedValue = uti!.takeRetainedValue()
                print("retainedValue: \(retainedValue)")
                
                if UTTypeConformsTo(retainedValue, kUTTypeImage) {
                    isGroupHasOnlyPhotos = true
                }else{
                    isGroupHasOnlyPhotos = false
                    break
                }
            }

            if !isGroupHasOnlyPhotos {
                var story : UIStoryboard?

                if IS_IPHONE {
                    story = UIStoryboard.init(name: "Main", bundle: nil)
                }else{
                    story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                }
                let GroupVCViewController = story?.instantiateViewController(withIdentifier: "GroupVCViewController") as! GroupVCViewController
                GroupVCViewController.dictbact = self.dictbact
                self.navigationController?.pushViewController(GroupVCViewController, animated: true)
                return
            }
            
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.HideProgress()
            self.collectionView.reloadData()
            self.currentindex.text = "1 OF \(self.Arrimagedata.count)"
         
        }else{
            self.collectionView.reloadData()
            self.view.makeToast("Internal Server Eroor")
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.HideProgress()
        }
        
    },
                              
                              failure: {(error) in
                                print(error)
                                appdelegate.HideProgress()
                                
                                
    })
}
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
                  if !IS_IPHONE{
                      self.perform(#selector(changeloader), with: nil, afterDelay: 0.2)

             }
         }
         
      @objc func changeloader(){
            App.initializeLoader()
        }

}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        self.init(red: CGFloat(red)/255 ,
                  green: CGFloat(green)/255,
                  blue: CGFloat(blue)/255,
                  alpha: 1.0)
    }
}


