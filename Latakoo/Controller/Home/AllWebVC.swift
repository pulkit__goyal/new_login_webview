//
//  AllWebVC.swift
//  Latakoo
//
//  Created by Mayur Sardana on 27/08/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//

import UIKit

class AllWebVC: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var webview: UIWebView!
       var weburl: String?
       var headerstring: String?
        @IBOutlet weak var header: UILabel!
    let appdelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        
        appdelegate.ShowProgress()
          
            header.text = headerstring
              let targetURL = URL(string:weburl!)
              
              var request: URLRequest? = nil
              if let targetURL = targetURL {
                  request = URLRequest(url: targetURL)
              }
              if let request = request {
                  webview.loadRequest(request)
              }

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btback(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
       
       func webViewDidFinishLoad(_ webView: UIWebView) {
              appdelegate.HideProgress()
          }
          
          func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
            return true
          }
          
          func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
              appdelegate.HideProgress()
          }
       override var prefersStatusBarHidden: Bool {
              return true
          }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
