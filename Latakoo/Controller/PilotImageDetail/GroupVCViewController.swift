//
//  GroupVCViewController.swift
//  Latakoo
//
//  Created by ABC on 12/10/20.
//  Copyright © 2020 parangat technology. All rights reserved.
//

import UIKit
import MobileCoreServices

class groupcell : UICollectionViewCell {
    
    @IBOutlet weak var imagepost: UIImageView!
    @IBOutlet weak var video: UIImageView!

}

class GroupVCViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Arrimagedata.count

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! groupcell
      let dict = Arrimagedata[indexPath.item] as! [String : Any]
        print(dict)
        let file = dict["filename"] as! String // path to some file
              let fileExtension = URL(fileURLWithPath: file).pathExtension as CFString
              
        let unmanagedFileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, nil)
    let fileUTI = unmanagedFileUTI!.takeRetainedValue()
        if UTTypeConformsTo(fileUTI , kUTTypeImage) {

            cell.imagepost.sd_setImage(with: URL(string: dict["thumbnail"] as! String), placeholderImage: App.jeremy)
            cell.video.isHidden = true

            
        }else if UTTypeConformsTo(fileUTI , kUTTypeMovie) {
            cell.imagepost.sd_setImage(with: URL(string: dict["thumbnail"] as! String), placeholderImage: App.jeremy)
            cell.video.isHidden = false

            
        }else if UTTypeConformsTo(fileUTI , kUTTypeAudio) {
            cell.imagepost.image = UIImage.init(named:"placeaudio")
            cell.video.isHidden = true

        }else{
            cell.imagepost.sd_setImage(with: URL(string: dict["thumbnail"] as! String), placeholderImage: App.jeremy)
            cell.video.isHidden = true
        }
        

        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
          if IS_IPHONE{
              return CGSize(width:collectionView.frame.size.width/3, height: 110)
          }else{
              return CGSize(width:collectionView.frame.size.width/3, height: 200)
          }
          
      }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = Arrimagedata[indexPath.item] as! [String : Any]
               print(dict)
               let file = dict["filename"] as! String // path to some file
                     let fileExtension = URL(fileURLWithPath: file).pathExtension as CFString
                     
               let unmanagedFileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, nil)
           let fileUTI = unmanagedFileUTI!.takeRetainedValue()
               if UTTypeConformsTo(fileUTI , kUTTypeImage) {

                var story : UIStoryboard?
                                          
                                     if IS_IPHONE {
                                          story = UIStoryboard.init(name: "Main", bundle: nil)
                                         }else{
                                          story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                                     }
                
                  let dict = Arrimagedata.last as! [String:Any]
                         let PilotimagedetailVC = story?.instantiateViewController(withIdentifier: "PilotimagedetailVC") as! PilotimagedetailVC
                         PilotimagedetailVC.dictback = Arrimagedata[indexPath.item] as! [String : Any]
                         PilotimagedetailVC.groupid = dict["id"] as! String
                         PilotimagedetailVC.isimage = "image"
                         self.navigationController?.pushViewController(PilotimagedetailVC, animated: false)

                   
               }else if UTTypeConformsTo(fileUTI , kUTTypeMovie) {
                 
                var story : UIStoryboard?
                           
                      if IS_IPHONE {
                           story = UIStoryboard.init(name: "Main", bundle: nil)
                          }else{
                           story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                      }
                
            let PilotimagedetailVC = story?.instantiateViewController(withIdentifier: "PilotimagedetailVC") as! PilotimagedetailVC
                PilotimagedetailVC.dictback = Arrimagedata[indexPath.row] as! [String : Any]
                PilotimagedetailVC.groupid = dict["id"] as! String
                PilotimagedetailVC.isimage = "video"
            self.navigationController?.pushViewController(PilotimagedetailVC, animated: true)
                
               
                
               }else if UTTypeConformsTo(fileUTI , kUTTypeAudio) {
                
                var story : UIStoryboard?
                if IS_IPHONE {
                    story = UIStoryboard.init(name: "Main", bundle: nil)
                }else{
                    story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                }
                let PilotimagedetailVC = story?.instantiateViewController(withIdentifier: "PilotimagedetailVC") as! PilotimagedetailVC
                PilotimagedetailVC.dictback = Arrimagedata[indexPath.row] as! [String : Any]
                PilotimagedetailVC.groupid = dict["id"] as! String
                PilotimagedetailVC.isimage = "audio"
                self.navigationController?.pushViewController(PilotimagedetailVC, animated: true)
                
                
                
               }else if UTTypeConformsTo(fileUTI , kUTTypePDF) {
                
                var story : UIStoryboard?
                if IS_IPHONE {
                    story = UIStoryboard.init(name: "Main", bundle: nil)
                }else{
                    story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                }
                let PilotimagedetailVC = story?.instantiateViewController(withIdentifier: "PilotimagedetailVC") as! PilotimagedetailVC
                PilotimagedetailVC.dictback = Arrimagedata[indexPath.row] as! [String : Any]
                PilotimagedetailVC.groupid = dict["id"] as! String
                PilotimagedetailVC.isimage = "pdf"
                self.navigationController?.pushViewController(PilotimagedetailVC, animated: true)
                
                
                
               }
                    
               else{
            
                var story : UIStoryboard?
                               
                          if IS_IPHONE {
                               story = UIStoryboard.init(name: "Main", bundle: nil)
                              }else{
                               story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
                          }
                    
                let PilotimagedetailVC = story?.instantiateViewController(withIdentifier: "PilotimagedetailVC") as! PilotimagedetailVC
                    PilotimagedetailVC.dictback = Arrimagedata[indexPath.row] as! [String : Any]
                    PilotimagedetailVC.groupid = dict["id"] as! String
                    PilotimagedetailVC.isimage = "other"
                self.navigationController?.pushViewController(PilotimagedetailVC, animated: true)

               }
    }
      
    

    
    @IBOutlet var lblheader: UILabel!
    @IBOutlet var collectionView: UICollectionView!
       var dictbact : [String:Any] = [:]
       var videoId : String = ""
       var Arrimagedata : [Any] = []
       let App = UIApplication.shared.delegate as! AppDelegate
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print(dictbact)
        
        lblheader.text = dictbact["video_group_filename"] as? String
////        videoId = (dictbact["original_folder"] as? String)!
//        let customAllowedSet =  NSCharacterSet(charactersIn:"=\"#%/<>?@\\^`{|}& ").inverted
//        let urlString = (dictbact["original_folder"] as? String)!.addingPercentEncoding(withAllowedCharacters: customAllowedSet)!
//        print("urlString: \(urlString)")
//        videoId = urlString
        
        videoId = (dictbact["original_folder"] as? String)!
        videoId = AJNotificationView.changetoencodedstring(videoId)
        print(videoId)
        self.getfiles()
        
        
    }
    
    
    func getfiles(){
        
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }
          let appdelegate = UIApplication.shared.delegate as! AppDelegate
          appdelegate.ShowProgress()
        

//        let aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=cloudvideo.get.byfolder&folder=\(videoId)"
        
        let networkIDString = dictbact["network"] as! String
        print("networkIDString: \(networkIDString)")
        let aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=cloudvideo.get.byfolder&folder=\(videoId)&network_id=\(networkIDString)"
        print(aStrApi)
//        aStrApi = AJNotificationView.changetoencodedstring(aStrApi)
//        print(aStrApi)
       
        APIManager.requestPOSTURL(aStrApi, params:nil as [String : AnyObject]?, headers: nil, success: {(usrDetail) in
            print(usrDetail)
            if usrDetail["status_code"].rawString() == "0"{
                let usrDetailVal = usrDetail["result"].rawValue as! [Any]
                self.Arrimagedata   = usrDetailVal
                print(self.Arrimagedata)
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.HideProgress()
                self.collectionView.reloadData()
             
            }else{
                self.collectionView.reloadData()
                self.view.makeToast("Internal Server Eroor")
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.HideProgress()
            }
            
        },
                                  
                                  failure: {(error) in
                                    print(error)
                                    appdelegate.HideProgress()
                                    self.getfiles()
                                    
                                    
        })
    }
    
    
//    @IBAction func back(_ sender: Any) {
//
//
//        self.navigationController?.popViewController(animated: true)
//
//
//
//    }
    
    @IBAction func back(_ sender: Any) {
            tabBarController?.selectedIndex = 1
            let navCtrl = tabBarController?.selectedViewController as? UINavigationController
            navCtrl?.popToRootViewController(animated: false)
        }

    override var prefersStatusBarHidden: Bool {
           return true
       }
       
       override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
                     if !IS_IPHONE{
                         self.perform(#selector(changeloader), with: nil, afterDelay: 0.2)

                }
            }
            
         @objc func changeloader(){
               App.initializeLoader()
           }

}
