//  PDfviewerVC.swift
//  Latakoo
//  Created by Shruti Aggarwal on 20/08/19.
//  Copyright © 2019 parangat technology. All rights reserved.


import PDFKit
import UIKit

class PDfviewerVC: UIViewController {

    @IBOutlet weak var viewpdf: UIView!
    @IBOutlet weak var lblheader: UILabel!
    
    @IBOutlet weak var thumbnail: UIView!
    
    var pdfurl : String!
    var headerstring : String!
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    private var pdfView = PDFView()
    private var thumbnailView = PDFThumbnailView()

    override func viewDidLoad() {
        super.viewDidLoad()
        appdelegate.ShowProgress()
        perform(#selector(self.readytogo), with: nil, afterDelay: 1.0)
        perform(#selector(self.readytogo1), with: nil, afterDelay: 8.0)
        lblheader.text = headerstring
        self.viewpdf.backgroundColor = UIColor.clear
    }
    
    
    
    
    
    @objc func readytogo1(){
        appdelegate.HideProgress()
   //     setupThumbnailView()

        
    }

    



@objc func readytogo(){
     pdfView = PDFView(frame: self.viewpdf.bounds)
    self.viewpdf.addSubview(pdfView)
    self.viewpdf.bringSubview(toFront: pdfView)
    pdfView.displayDirection = .horizontal
    pdfView.usePageViewController(true)
    pdfView.pageBreakMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    pdfView.autoScales = true
    let fileURL = URL.init(string:pdfurl)
    pdfView.document = PDFDocument(url: fileURL!)
   
}


    
    @IBAction func backbutton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override var prefersStatusBarHidden: Bool {
        return false
    }

}
