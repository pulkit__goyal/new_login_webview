

import AVFoundation
import AVKit
import CoreData
import MobileCoreServices
import UIKit
import Alamofire

import AWSCore
import AWSS3
import SwiftyJSON
import Photos

class commentcell: UITableViewCell {
    @IBOutlet var imguser: UIImageView!
    @IBOutlet var lblcomment: UILabel!
    @IBOutlet var lblusername: UILabel!
    @IBOutlet var lbldate: UILabel!
}

class PilotimagedetailVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UIWebViewDelegate,URLSessionDownloadDelegate {
  
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        
    }
    
    func urlSession(_ session: URLSession, downloadTask:
        URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        
        let arr  = [location]
        
        let newUrls = arr.compactMap { (url: URL) -> URL? in
            // Create file URL to temporary folder
            var tempURL = URL(fileURLWithPath: NSTemporaryDirectory())
            // Apend filename (name+extension) to URL
            tempURL.appendPathComponent(url.lastPathComponent)
            do {
                // If file with same name exists remove it (replace file with new one)
                if FileManager.default.fileExists(atPath: tempURL.path) {
                    try FileManager.default.removeItem(atPath: tempURL.path)
                }
                // Move file from app_id-Inbox to tmp/filename
                try FileManager.default.moveItem(atPath: url.path, toPath: tempURL.path)
                return tempURL
            } catch {
                print(error.localizedDescription)
                return nil
            }
        }
        
        print(newUrls)

        
        
        


        DispatchQueue.main.async {

        
            let audioData = NSData(contentsOf: arr[0])
            
            



        
        do {
            print(self.dictback)
            
            
            let documentsURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
               
            
            
           
            
            
            
            let url = URL(string: (self.dictback["stream_url"] as? String)!)

            var tempURL = documentsURL.appendingPathComponent(url!.lastPathComponent)

            print(self.isimage)

            if self.isimage == "video" {
                
                self.app.checkdownload = "Video"
                let EXT = URL(fileURLWithPath: (self.dictback["title"] as? String)!).pathExtension

                if EXT == "" {
                    tempURL = documentsURL.appendingPathComponent("\(self.dictback["title"] as? String ?? "video").mp4")
                    print(tempURL)

                }else{
                    if self.isstream == true {
                        var name = self.dictback["title"] as? String ?? "video"
                        name = name.fileName()
                        tempURL = documentsURL.appendingPathComponent("\(name).mp4")
                        print(tempURL)

                    }else{
                        tempURL = documentsURL.appendingPathComponent("\(self.dictback["title"] as? String ?? "video")")
                        print(tempURL)

                    }
                }
                
                print(tempURL)
                print(audioData?.count ?? 0)
                
                
                FileManager.default.createFile(atPath: tempURL.path,
                                               contents: audioData as Data?,
                                                             attributes: nil)

                              if FileManager.default.fileExists(atPath: tempURL.path) {
                                  print("video file present!") // Confirm that the file is here!
                              }else{
                                print("video file not present!") // Confirm that the file is here!

                }

             //   try audioData?.write(to:tempURL, options: .atomic)
                
                
                
                sleep(UInt32(1.0))
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: tempURL)
                }) { saved, error in
                    if saved {
                        
                        if FileManager.default.fileExists(atPath: tempURL.path) {
                                                
                                               do {
                                                try FileManager.default.removeItem(atPath: tempURL.path)

                                               } catch {
                                                   
                                               }
                            
                        }
                        
                        
                        self.getvideo()
                    }else{
                        UISaveVideoAtPathToSavedPhotosAlbum((tempURL.path), self, #selector(PilotimagedetailVC.video(videoPath:didFinishSavingWithError:contextInfo:)), nil)

                    }
                    
                    
                    
                }
                
                

            } else if self.isimage == "image" {
                
                self.app.checkdownload = "Image"
                let imageee = UIImage.sd_image(with: audioData as Data?)
                UIImageWriteToSavedPhotosAlbum(imageee!, self, #selector(PilotimagedetailVC.image(imagepath:didFinishSavingWithError:contextInfo:)), nil)

            } else if self.isimage == "audio"{
                
                self.app.checkdownload = "Audio"

                var newURL: URL!

                let EXT = URL(fileURLWithPath: (self.dictback["title"] as? String)!).pathExtension

                if EXT == "" {
                    newURL = self.getDocumentsDirector().appendingPathComponent("\(self.dictback["title"] as! String).m4a")

                } else {
                    let list = self.dictback["title"] as? String
                    let listItems = list!.components(separatedBy: ".")
                    let filename = listItems[0]

                    newURL = self.getDocumentsDirector().appendingPathComponent("\(filename).m4a")
                }

                print(newURL!)
                
                
                FileManager.default.createFile(atPath: tempURL.path,
                                               contents: audioData as Data?,
                                                             attributes: nil)

                              if FileManager.default.fileExists(atPath: tempURL.path) {
                                  print("audio file present!") // Confirm that the file is here!
                              }else{
                                print("audio file not present!") // Confirm that the file is here!

                }

               // try audioData?.write(to: newURL!, options: .atomic)
                   sleep(1)
                 self.movetogalary()
                print("audio saved to device successfully")
            }else{
                let EXT = URL(string: (self.dictback["stream_url"] as? String)!)!.pathExtension
                   print(EXT)
                
                var newURL: URL!


                let list = self.dictback["title"] as? String
                    let listItems = list!.components(separatedBy: ".")
                        let filename = listItems[0]
                newURL = self.getDocumentsDirector().appendingPathComponent("\(filename).\(EXT)")
                               print(newURL!)
                
                if FileManager.default.fileExists(atPath: newURL.path) {

                    newURL = self.getDocumentsDirector().appendingPathComponent("\(filename)_\(self.randomString(length: 3)).\(EXT)")
                                                  print(newURL!)
                }
                
                
                FileManager.default.createFile(atPath: newURL.path,
                                               contents: audioData as Data?,
                                                             attributes: nil)

                              if FileManager.default.fileExists(atPath: tempURL.path) {
                                  print("other file present!") // Confirm that the file is here!
                              }else{
                                print("other file not present!") // Confirm that the file is here!

                }
                
              //  try audioData?.write(to: newURL!, options: .atomic)
             //   KGModal.sharedInstance()?.hide()
                let appearance = SCLAlertView.SCLAppearance(
                    kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                    kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                    kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                    showCloseButton: false,
                    dynamicAnimatorActive: true,
                    buttonsLayout: .horizontal
                )
                let alert = SCLAlertView(appearance: appearance)
                _ = alert.addButton("Ok") {
              KGModal.sharedInstance()?.hide()

                
                }
                
                    let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
                    _ = alert.showCustom("Alert", subTitle: "This file type is not supported by the iOS library. This file will be saved in latakoo folder in your Files.", color: color, circleIconImage:nil)
                
            }


            self.app.HideProgress()

        } catch {
            print(error)

            self.app.HideProgress()
        }

        }
    }
    
    
    func randomString(length: Int) -> String {
       let letters = "1234567890"
       return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
    let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        print(progress)
        viewCircularProgressBar.setProgress(to: Double(progress), withAnimation: true)
progressToSet = downloadingProgress
      
    
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {
  
        
 
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrcmnt.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! commentcell
        let dict = arrcmnt[indexPath.row] as! [String: Any]
        cell.imguser.sd_setImage(with: URL(string: dict["owner_icon"] as! String), placeholderImage: UIImage(named: "latakoobird"))
        let unixTimestamp = Double(dict["created"] as! String)
        let date = Date(timeIntervalSince1970: unixTimestamp ?? 0)
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "d MMM yyyy HH:mm:ss"
        cell.lbldate.text = dateFormatter1.string(from: date)
        cell.lblusername.text = dict["owner_name"] as? String

        let attachment = dict["attachmentFilename"] as? String
        if attachment == "" {
            cell.lblcomment.text = dict["body"] as? String
            if #available(iOS 13.0, *) {
                cell.lblcomment.textColor = UIColor.label
            } else {
                // Fallback on earlier versions
                cell.lblcomment.textColor = UIColor.black
            }
        } else {
            cell.lblcomment.text = dict["attachmentFilename"] as? String
            cell.lblcomment.textColor = UIColor.blue
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dict = arrcmnt[indexPath.row] as! [String: Any]
        let heightOfRow = calculateHeight(inString: (dict["body"] as? String)!)

        var height: Int = 44
        if IS_IPHONE {
            height = 44
        } else {
            height = 55
        }

        return (heightOfRow + CGFloat(height))
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrcmnt[indexPath.row] as! [String: Any]
        let attachment = dict["attachmentFilename"] as? String
        if attachment == "" {
        } else {
            let fileExtension = URL(fileURLWithPath: (dict["attachmentFilename"] as? String)!).pathExtension as CFString

            let unmanagedFileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, nil)
            let fileUTI = unmanagedFileUTI!.takeRetainedValue()

            if UTTypeConformsTo(fileUTI, kUTTypeImage) {
                cmntimaga.sd_setImage(with: URL(string: dict["attachmentURL"] as! String), placeholderImage: app.jeremy)
                KGModal.sharedInstance()?.show(withContentView: cmntimaga)

            } else if UTTypeConformsTo(fileUTI, kUTTypeMovie) {
                DispatchQueue.main.async {
                    let videoURL = URL(string: dict["attachmentURL"] as! String)
                    let player = AVPlayer(url: videoURL!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    player.playImmediately(atRate: 1.0)
                    player.automaticallyWaitsToMinimizeStalling = false

                    do {
                        try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                    } catch {
                    }

                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }
            } else {
                var story: UIStoryboard?
                story = UIStoryboard(name: "Main", bundle: nil)

                let vc = story?.instantiateViewController(withIdentifier: "AllWebVC") as! AllWebVC
                vc.headerstring = dict["attachmentFilename"] as? String
                vc.weburl = dict["attachmentURL"] as? String
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }

    func calculateHeight(inString: String) -> CGFloat {
        let messageString = inString
        let attributes: [NSAttributedStringKey: Any] = [NSAttributedString.Key(rawValue: NSAttributedStringKey.font.rawValue): UIFont.systemFont(ofSize: 10.0)]
        let attributedString: NSAttributedString = NSAttributedString(string: messageString, attributes: attributes)
        let rect: CGRect = attributedString.boundingRect(with: CGSize(width: 302.0, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        let requredSize: CGRect = rect
        return requredSize.height
    }

    @IBOutlet var btnzoom: UIButton!
    @IBOutlet var btnmenu: UIButton!

    @IBOutlet var usersmall: UIImageView!
    @IBOutlet var imguser: UIImageView!
    @IBOutlet var lbldate: UILabel!
    @IBOutlet var imgpost: UIImageView!
    @IBOutlet var lblusername: UILabel!
    @IBOutlet var lblnetwork: UILabel!
    @IBOutlet var btnnote: UIButton!
    @IBOutlet var btncomment: UIButton!
    @IBOutlet var tblcmnt: UITableView!
    @IBOutlet var txtcmnt: UITextField!
    @IBOutlet var lblheader: UILabel!
    @IBOutlet var cmntimaga: UIImageView!

    @IBOutlet var viewaudio: UIView!
    @IBOutlet var audioslider: UISlider!
    @IBOutlet var lblaudio: UILabel!

    @IBOutlet var indigatior: UIActivityIndicatorView!

    @IBOutlet var webindigaor: UIActivityIndicatorView!

    @IBOutlet var btnplayaudio: UIButton!
    @IBOutlet var metadataview: UIWebView!
    var isstream : Bool = false
    
    
    
    
    var bgTask: BackgroundTask?
    /// Overall progress
    var progressToSet: Double = 0
    @IBOutlet var viewCircularProgressBar: CircularProgressBar!
    var downloadingProgress: Double = 0

    var flagfortable = 0
    var flagforAudio = 0

    var dictback: [String: Any] = [:]
    @IBOutlet var txtNOtes: UITextView!
    var arrcmnt: [Any] = []
    var arrnotes: [Any] = []
    var groupid: String = ""
    var isimage: String = ""
    var playbackTimer1: Timer?
    var alert: SCLAlertViewResponder?

    var ArrNetwork_name: [String] = []
    var ArrNetwork_id: [String] = []
    var networkid: String = ""
    var network: [[String: Any]] = []
    var ismetadataload: Bool = false

    var audioPlayer: AVAudioPlayer?
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?

    var isReadproperty: Bool = false
    var isWriteProperty: Bool = false
    var isCopyMoveproperty: Bool = false
    var isDownloadproperty: Bool = false
    var isadmin: Bool = false
    let app = UIApplication.shared.delegate as! AppDelegate
  var iftempfile = false
 var tempfilepath = ""

    
    var audiopath : String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print("totalDiskSpaceInBytes: \(UIDevice.current.totalDiskSpaceInBytes)")
        print("freeDiskSpace: \(UIDevice.current.freeDiskSpaceInBytes)")
        print("usedDiskSpace: \(UIDevice.current.usedDiskSpaceInBytes)")
        

        tabBarController?.tabBar.isHidden = false
        indigatior.startAnimating()

        tblcmnt.estimatedRowHeight = 44.0
        tblcmnt.rowHeight = UITableViewAutomaticDimension
        flagfortable = 0
        let dict = dictback

        print(dict)

        isadmin = (dict["can_edit"] as? Bool)!

        print(isadmin)

        if isimage == "video" {
            imgpost.sd_setImage(with: URL(string: dict["thumbnail"] as! String), placeholderImage: app.jeremy)
            if dict["video_group_pieces_total"] is NSNull {
                lblheader.text = dict["title"] as? String

            } else {
                lblheader.text = dict["video_group_filename"] as? String
            }
            btnzoom.setImage(UIImage(named: "play.png"), for: .normal)

        } else if isimage == "image" || isimage == "other" {
            if dict["video_group_pieces_total"] is NSNull {
                lblheader.text = dict["title"] as? String

            } else {
                lblheader.text = dict["video_group_filename"] as? String
            }
            imgpost.sd_setImage(with: URL(string: dict["thumbnail"] as! String), placeholderImage: app.jeremy)

        } else if isimage == "pdf" {
            imgpost.sd_setImage(with: URL(string: dict["thumbnail"] as! String), placeholderImage: app.jeremy)
            if dict["video_group_pieces_total"] is NSNull {
                lblheader.text = dict["title"] as? String

            } else {
                lblheader.text = dict["video_group_filename"] as? String
            }
        } else {
            if dict["video_group_pieces_total"] is NSNull {
                lblheader.text = dict["title"] as? String
            } else {
                lblheader.text = dict["video_group_filename"] as? String
            }
            viewaudio.isHidden = false
        }

        if isimage == "image" {
            imgpost.sd_setImage(with: URL(string: dict["stream_url"] as! String), placeholderImage: app.jeremy)
        }

        imguser.sd_setImage(with: URL(string: dict["uploaderIcon"] as! String), placeholderImage: UIImage(named: "latakoobird"))

        let dictt = UserDefaults.standard.value(forKey: "userdetails") as? [String: Any]
        print(dictt!)
        usersmall.sd_setImage(with: URL(string: dictt?["profile_image"] as? String ?? ""), placeholderImage: UIImage(named: "latakoobird"))

        let unixTimestamp = Double(dict["unix_created"] as! String)
        let date = Date(timeIntervalSince1970: unixTimestamp ?? 0)
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "EEEE, d MMM yyyy HH:mm:ss"
        lbldate.text = dateFormatter1.string(from: date)
        lblnetwork.text = netWorkName(fromID: dict["network"] as? String)
        lblusername.text = dict["uploader"] as? String

        DispatchQueue.global(qos: .background).async {
            self.getcmntdata()
        }

        network = UserDefaults.standard.value(forKey: "AvailableNetworks") as! [[String: Any]]
        print(network)

        network = network.sorted { ($0["company_name"] as! String).localizedCaseInsensitiveCompare($1["company_name"] as! String) == ComparisonResult.orderedAscending }
        var dicttop: [String: Any]!

        for dict in network {
            print(dict)
            let dict1 = dict as? [String: Any]
            let ArrPermission = dict1!["permissions"] as? [String]
            if ArrPermission!.contains("write") || ArrPermission!.contains("move_copy") {
                ArrNetwork_name.append((dict1?["company_name"] as? String)!)
                ArrNetwork_id.append((dict1?["company_id"] as? String)!)
            }
            if dict1?["company_name"] as? String == lblnetwork.text {
                dicttop = dict1
            }
        }

        print(ArrNetwork_name)
        print(ArrNetwork_id)

        print(dicttop)

        if dicttop == nil {
        } else {
            let ArrPermission = dicttop!["permissions"] as? [String]

            for i in 0 ..< ArrPermission!.count {
                if ArrPermission![i] == "read" {
                    isReadproperty = true
                }
                if ArrPermission![i] == "write" {
                    isWriteProperty = true
                }
                if ArrPermission![i] == "download" {
                    isDownloadproperty = true
                }

                if ArrPermission![i] == "move_copy" {
                    isCopyMoveproperty = true
                }
            }
        }

        //   app.ShowProgress()

        DispatchQueue.main.async {
            let weburl: String = "\(UserDefaults.standard.value(forKey: "server") as! String)ajax/manage/getMetadataForm.php?id=\(self.dictback["id"] as? String ?? "")"
            let targetURL = URL(string: weburl)
            var request: URLRequest?
            if let targetURL = targetURL {
                request = URLRequest(url: targetURL)
            }

            self.metadataview.scalesPageToFit = true

            if let request = request {
                self.metadataview.loadRequest(request)
            }
            self.metadataview.isHidden = true
            self.webindigaor.isHidden = true
            self.webindigaor.startAnimating()
        }
    }

    func netWorkName(fromID networkID: String?) -> String? {
       // let networksArray = UserDefaults.standard.value(forKey: "AvailableNetworks") as! [Any]
        let networksArray = UserDefaults.standard.value(forKey: "AvailableNetworks") as? [Any] ?? []


        for dict in networksArray {
            let dictnetwork = dict as! [String: Any]
            print(dictnetwork)
            let networkid = dictnetwork["company_id"] as! String

            if networkid == networkID {
                return dictnetwork["company_name"] as? String
            }
        }
        return ""
    }

//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }

    @IBAction func btnback(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func btnsend(_ sender: Any) {
        if flagfortable == 0 {
            if txtcmnt.text == "" {
            } else {
                indigatior.isHidden = false
                indigatior.startAnimating()
                addcomment()
            }

        } else {
            if txtcmnt.text == "" {
            } else {
                addnote()
            }
        }
    }

    @IBAction func btnnote(_ sender: Any) {
        metadataview.isHidden = false
        if ismetadataload == false {
            webindigaor.isHidden = false
            webindigaor.startAnimating()
        }
        changecolor()
        flagfortable = 1
        txtcmnt.placeholder = "Type notes here.."
        btnnote.setBackgroundImage(UIImage(named: "purpule_box.png"), for: .normal)
        btnnote.setTitleColor(UIColor.white, for: .normal)
    }

    @IBAction func btncomment(_ sender: Any) {
        webindigaor.stopAnimating()
        webindigaor.isHidden = true
        metadataview.isHidden = true
        changecolor()
        txtcmnt.placeholder = "Type comment here."
        flagfortable = 0
        tblcmnt.reloadData()
        btncomment.setBackgroundImage(UIImage(named: "purpule_box.png"), for: .normal)
        btncomment.setTitleColor(UIColor.white, for: .normal)
    }

    func changecolor() {
        btnnote.setBackgroundImage(UIImage(named: "white_box_button"), for: .normal)
        btnnote.setTitleColor(UIColor(named: "pilotcolor"), for: .normal)
        btncomment.setBackgroundImage(UIImage(named: "white_box_button"), for: .normal)
        btncomment.setTitleColor(UIColor(named: "pilotcolor"), for: .normal)
    }

    func getcmntdata() {
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }
//        let appdelegate = UIApplication.shared.delegate as! AppDelegate
//        appdelegate.ShowProgress()
        var aStrApi = ""

        if groupid == "" {
            aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=object.get&classes[0]=CloudComment&namevaluepairs[video_id]=\(dictback["id"] as! String)"
        } else {
            aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=object.get&classes[0]=CloudComment&namevaluepairs[video_id]=\(groupid)"
        }

        APIManager.requestGETURL(aStrApi, success: { usrDetail in
            print(usrDetail)
            if usrDetail["status_code"].rawString() == "0" {
                let usrDetailVal = usrDetail["result"].rawValue as! [String: Any]
                print(usrDetailVal)

                self.app.HideProgress()

                let Arr = usrDetailVal["objects"] as! [Any]
                self.arrcmnt = Arr
//                let appdelegate = UIApplication.shared.delegate as! AppDelegate
//                appdelegate.HideProgress()
                print(Arr)

                self.indigatior.stopAnimating()
                self.indigatior.isHidden = true

                self.tblcmnt.reloadData(
                    with: .simple(duration: 1.5, direction: .rotation3D(type: .doctorStrange),
                                  constantDelay: 0))
            } else {
                //  self.view.makeToast(usrDetail["message"].rawString())
//                let appdelegate = UIApplication.shared.delegate as! AppDelegate
//                appdelegate.HideProgress()
                self.app.HideProgress()

                self.arrcmnt = []
                self.tblcmnt.reloadData()

                self.indigatior.stopAnimating()
                self.indigatior.isHidden = true
            }

        }, failure: { error in
            print(error)
            //  appdelegate.HideProgress()
            self.app.HideProgress()

            self.indigatior.stopAnimating()
            self.indigatior.isHidden = true
            self.arrcmnt = []
            self.tblcmnt.reloadData()

        })
    }
    
    
    
    
      
    
    
    
    

    func addcomment() {
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.ShowProgress()
        let aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=cloudcomment.add"
        let aDictParam: [String: Any]!
        if groupid == "" {
            aDictParam = [
                "video_id": dictback["id"] as? String ?? "",
                "body": txtcmnt.text ?? "",
            ]
        } else {
            aDictParam = [
                "video_id": groupid,
                "body": txtcmnt.text ?? "",
            ]
        }

        let heder = appdelegate.getAPIEncodedHeader(withParameters: aStrApi)
        let headervalue = ["X_PFTP_API_TOKEN": heder]
        APIManager.requestPOSTURL(aStrApi, params: aDictParam as [String: AnyObject]?, headers: headervalue as? [String: String], success: { usrDetail in
            print(usrDetail)
            if usrDetail["status_code"].rawString() == "0" {
                self.getcmntdata()
                self.txtcmnt.text = ""
                //   self.tblhome.reloadData()
            } else {
                self.view.makeToast(usrDetail["message"].rawString())
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.HideProgress()
            }

        },

        failure: { error in
            print(error)
            appdelegate.HideProgress()

        })
    }

    func addnote() {
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.ShowProgress()
        let aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=cloudvideo.set.notes"

        let aDictParam: [String: Any]!
        aDictParam = [
            "id": dictback["id"] as? String ?? "",
            "notes": txtcmnt.text ?? "",
        ]

        let heder = appdelegate.getAPIEncodedHeader(withParameters: aStrApi)
        let headervalue = ["X_PFTP_API_TOKEN": heder]
        APIManager.requestPOSTURL(aStrApi, params: aDictParam as [String: AnyObject]?, headers: headervalue as? [String: String], success: { usrDetail in
            print(usrDetail)
            if usrDetail["status_code"].rawString() == "0" {
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.HideProgress()
                self.txtcmnt.text = ""

                //   self.tblhome.reloadData()
            } else {
                self.view.makeToast(usrDetail["message"].rawString())
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.HideProgress()
            }

        },

        failure: { error in
            print(error)
            appdelegate.HideProgress()

        })
    }

    func videoplayer() {
        DispatchQueue.main.async {
            let videoURL = URL(string: self.dictback["stream_url"] as! String)

            //  let videoURL = URL(string:"https://dev.latakoo.com:1935/panasonic/stream.stream/playlist.m3u8")

            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            player.playImmediately(atRate: 1.0)
            player.automaticallyWaitsToMinimizeStalling = false

            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            } catch {
                // report for an error
            }

            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }

    @IBAction func zoompinchimage(_ sender: Any) {
        if isimage == "video" {
            videoplayer()

        } else if isimage == "image" {
            var story: UIStoryboard?

            if IS_IPHONE {
                story = UIStoryboard(name: "Main", bundle: nil)
            } else {
                story = UIStoryboard(name: "MainIPAD", bundle: nil)
            }
            let Bigimagevc = story?.instantiateViewController(withIdentifier: "Bigimagevc") as! Bigimagevc
            Bigimagevc.bigimage = imgpost.image
            navigationController?.pushViewController(Bigimagevc, animated: false)
        } else if isimage == "pdf" {
            var story: UIStoryboard?

            if IS_IPHONE {
                story = UIStoryboard(name: "Main", bundle: nil)
            } else {
                story = UIStoryboard(name: "MainIPAD", bundle: nil)
            }
            let PDfviewerVC = story?.instantiateViewController(withIdentifier: "PDfviewerVC") as! PDfviewerVC
            PDfviewerVC.headerstring = dictback["filename"] as? String
            PDfviewerVC.pdfurl = dictback["stream_url"] as? String
            navigationController?.pushViewController(PDfviewerVC, animated: false)

//            let pdfViewController = PDFViewController(pdfUrl: URL.init(string: (dictback["stream_url"] as? String)! )!)
//            present(pdfViewController, animated: true, completion: nil)

        } else if isimage == "other" {
            var story: UIStoryboard?
            story = UIStoryboard(name: "Main", bundle: nil)

            let vc = story?.instantiateViewController(withIdentifier: "AllWebVC") as! AllWebVC
            vc.headerstring = lblheader.text
            vc.weburl = dictback["stream_url"] as? String
            navigationController?.pushViewController(vc, animated: true)
        }
    }

    @IBAction func slider(_ sender: Any) {
 
     if player != nil {
        player!.seek(to: CMTimeMakeWithSeconds(Float64(audioslider!.value), 60000))
              let curTime = player?.currentTime()
              let seconds = CMTimeGetSeconds(curTime!)
              print(seconds)
            print(audioslider!.value)

        }
    }

    @IBAction func playaudio(_ sender: Any) {
        
        if audiopath != ""{
           playAudio(audiopath)
        }else{
        
        
        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        var filePath = ""
        var fileName: String?
        fileName = "\(documentsDirectory)/\(dictback["id"] as? String ?? "")_\("playaudio.m4a")"

        filePath = fileName ?? ""
        if FileManager.default.fileExists(atPath: filePath) {
            try! FileManager.default.removeItem(atPath: filePath)
        }

        if !FileManager.default.fileExists(atPath: filePath) {
            let app = UIApplication.shared.delegate as! AppDelegate
            app.ShowProgress()
            var req: URLRequest?
            if let url = URL(string: (dictback["stream_url"] as? String)!) {
                req = URLRequest(url: url)
            }
            if let req = req {
                NSURLConnection.sendAsynchronousRequest(req, queue: OperationQueue.main, completionHandler: { _, audioData, connectionError in
                    if connectionError != nil {
                    } else {
                        do {
                            try audioData?.write(to: URL(fileURLWithPath: fileName!), options: .atomic)
                            
                            self.audiopath = filePath
                            self.playAudio(filePath)

                            app.HideProgress()

                        } catch {
                            print(error)

                            app.HideProgress()
                        }
                    }
                })
            }
        }
        }
        
    }

    func playAudio(_ tempFilePath: String?) {
        if player == nil {
            let fileURL = URL(fileURLWithPath: tempFilePath!)
            player = AVPlayer(url: fileURL)
            playerLayer = AVPlayerLayer()
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.frame = view.bounds
            playerLayer?.videoGravity = .resizeAspectFill
            view.layer.addSublayer(playerLayer!)
            player!.actionAtItemEnd = .none
            
            
            let audioAsset = AVURLAsset(url:URL.init(fileURLWithPath: tempFilePath!), options: nil)
            let audioDuration = audioAsset.duration
             let audioDurationSeconds = CGFloat(CMTimeGetSeconds(audioDuration))
            audioslider.maximumValue = Float(audioDurationSeconds)
            
            print(audioDurationSeconds)

            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            } catch {
                // report for an error
            }

            NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: .AVPlayerItemDidPlayToEndTime, object: player?.currentItem)

            playbackTimer1 = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(myMethod1(_:)), userInfo: nil, repeats: true)
        }
        if flagforAudio == 0 {
            btnplayaudio.setImage(UIImage(named: "pausebutton.png"), for: .normal)
            flagforAudio = 1
            player?.play()

        } else {
            btnplayaudio.setImage(UIImage(named: "playbutton.png"), for: .normal)
            flagforAudio = 0
            player?.pause()
        }

        if playbackTimer1 != nil {
            playbackTimer1?.invalidate()
        }
        playbackTimer1 = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(myMethod1(_:)), userInfo: nil, repeats: true)
    }

    @objc func myMethod1(_ timer: Timer?) {
        let currentItem = player!.currentItem
        let currentTime = CMTimeGetSeconds((currentItem?.currentTime())!)
        let f = Float(currentTime)
        var seconds = CGFloat(Int(CMTimeGetSeconds((currentItem?.currentTime())!)))
        let hour = seconds / 3600
        seconds = CGFloat(Int(seconds) % 3600)
        let min = seconds / 60
        seconds = CGFloat(Int(seconds) % 60)
        lblaudio.text = String(format: "%02d: %02d: %02d", Int(hour), Int(min), Int(seconds))
        audioslider.setValue(f, animated: true)

       
    }

    func copyto() {
        let options = YActionSheet(title: "Copy to...", dismissButtonTitle: "Cancel", otherButtonTitles: ArrNetwork_name, dismissOnSelect: true, ismenifest: false)

        options!.show(in: self, withYActionSheetBlock: { buttonIndex, isCancel in
            if isCancel {
                self.networkid = ""
            } else {
                self.networkid = self.ArrNetwork_id[buttonIndex]

                let appearance = SCLAlertView.SCLAppearance(
                    kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                    kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                    kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                    showCloseButton: false,
                    dynamicAnimatorActive: true,
                    buttonsLayout: .horizontal
                )
                let alert = SCLAlertView(appearance: appearance)
                _ = alert.addButton("Cancel") {
                }
                _ = alert.addButton("Yes") {
                    self.copytonetwork()
                }

                let icon = UIImage(named: "copywhite.png")
                let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)

                _ = alert.showCustom("Alert", subTitle: "Are you sure you want to copy this file to \(self.ArrNetwork_name[buttonIndex]) ?", color: color, circleIconImage: icon!)
            }
        })
    }

    func moveto() {
        let options = YActionSheet(title: "Move to...", dismissButtonTitle: "Cancel", otherButtonTitles: ArrNetwork_name, dismissOnSelect: true, ismenifest: false)
        options!.show(in: self, withYActionSheetBlock: { buttonIndex, isCancel in
            if isCancel {
                self.networkid = ""
            } else {
                self.networkid = self.ArrNetwork_id[buttonIndex]

                let appearance = SCLAlertView.SCLAppearance(
                    kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                    kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                    kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                    showCloseButton: false,
                    dynamicAnimatorActive: true,
                    buttonsLayout: .horizontal
                )
                let alert = SCLAlertView(appearance: appearance)
                _ = alert.addButton("Cancel") {
                }
                _ = alert.addButton("Yes") {
                    self.movetonetwork()
                }

                let icon = UIImage(named: "movewhite.png")
                let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)

                _ = alert.showCustom("Alert", subTitle: "Are you sure you want to move this file to \(self.ArrNetwork_name[buttonIndex]) ?", color: color, circleIconImage: icon!)
            }
        })
    }

    override func viewWillDisappear(_ animated: Bool) {
        if flagforAudio == 1 {
            flagforAudio = 0
            player?.pause()
            flagforAudio = 0
            audioslider.value = 0
            lblaudio.text = "00:00:00"
            player = nil
            playbackTimer1?.invalidate()
            btnplayaudio.setImage(UIImage(named: "playbutton.png"), for: .normal)
        }
    }

    @IBAction func btnmenu(_ sender: UIButton) {
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "Copy to...", style: .default) { _ -> Void in

            print("First Action pressed")

            self.copyto()
        }

        let secondAction: UIAlertAction = UIAlertAction(title: "Move to...", style: .default) { _ -> Void in

            print("Second Action pressed")

            self.moveto()
        }

        let thirdaction: UIAlertAction = UIAlertAction(title: "Share", style: .default) { _ -> Void in

            var story: UIStoryboard?

            if IS_IPHONE {
                story = UIStoryboard(name: "Main", bundle: nil)
            } else {
                story = UIStoryboard(name: "MainIPAD", bundle: nil)
            }

            let vc = story?.instantiateViewController(withIdentifier: "ShareVC") as! ShareVC
            vc.videoid = self.dictback["id"] as? String
            self.navigationController?.pushViewController(vc, animated: true)
        }

        let fourthaction: UIAlertAction = UIAlertAction(title: "Download...", style: .default) { _ -> Void in

           // self.downloadata()
            self.ServicegetdownloadURL()
        }

        let fifthaction: UIAlertAction = UIAlertAction(title: "Delete", style: .destructive) { _ -> Void in

            let appearance = SCLAlertView.SCLAppearance(
                kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                showCloseButton: false,
                dynamicAnimatorActive: true,
                buttonsLayout: .horizontal
            )
            let alert = SCLAlertView(appearance: appearance)
            _ = alert.addButton("Cancel") {
            }
            _ = alert.addButton("Yes") {
                self.deletepost()
            }

            let icon = UIImage(named: "deleteWhite.png")
            let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)

            _ = alert.showCustom("Alert", subTitle: "Are you sure you want to delete this?", color: color, circleIconImage: icon!)
        }

        let sixthaction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { _ -> Void in
        }

        // add actions
        if isadmin {
            actionSheetController.addAction(firstAction)
            actionSheetController.addAction(secondAction)
            actionSheetController.addAction(thirdaction)
            actionSheetController.addAction(fourthaction)
            actionSheetController.addAction(fifthaction)

        } else {
            if isCopyMoveproperty {
                actionSheetController.addAction(firstAction)
                actionSheetController.addAction(secondAction)
                actionSheetController.addAction(thirdaction)
            }
            if isDownloadproperty {
                actionSheetController.addAction(fourthaction)
            }
        }

        actionSheetController.addAction(sixthaction)

        if IS_IPHONE {
            actionSheetController.popoverPresentationController?.sourceView = view

        } else {
            actionSheetController.popoverPresentationController?.sourceView = sender
        }

        present(actionSheetController, animated: true) {
            print("option menu presented")
        }
    }

    func deletepost() {
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.ShowProgress()
        var aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=object.delete&id=\(dictback["id"] as! String)"

        APIManager.requestDeleteURL(aStrApi, params: nil, headers: nil, success: { JSON in

            print(JSON)

            appdelegate.HideProgress()

            let appearance = SCLAlertView.SCLAppearance(
                kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                showCloseButton: false,
                dynamicAnimatorActive: true,
                buttonsLayout: .horizontal
            )

            let alert = SCLAlertView(appearance: appearance)
            _ = alert.addButton("OK") {
                self.navigationController?.popViewController(animated: true)
            }
            _ = alert.showSuccess("LATAKOO", subTitle: "Successfully Deleted.")

        }) { Error in

            print(Error)
            appdelegate.HideProgress()
        }
    }

    func copytonetwork() {
                    if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.ShowProgress()
        let aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=object.action"
//        let parameter : [String:Any] = ["copy":"action",
//                                        dictback["id"] as! String:"id_list",
//                                        networkid:"target_network_id",
//                                        ]

        let parameter: [String: Any] = ["action": "copy",
                                        "id_list": dictback["id"] as! String,
                                        "target_network_id": networkid,
        ]
        print(parameter)

        APIManager.requestPOSTURL(aStrApi, params: parameter as [String: AnyObject], headers: nil, success: { JSON in

            print(JSON)

            appdelegate.HideProgress()

            let appearance = SCLAlertView.SCLAppearance(
                kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                showCloseButton: false,
                dynamicAnimatorActive: true,
                buttonsLayout: .horizontal
            )

            let alert = SCLAlertView(appearance: appearance)
            _ = alert.addButton("OK") {
                self.navigationController?.popViewController(animated: true)
            }
            _ = alert.showSuccess("LATAKOO", subTitle: "Copied successfully.")

        }) { _ in

            appdelegate.HideProgress()

            let appearance = SCLAlertView.SCLAppearance(
                kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                showCloseButton: false,
                dynamicAnimatorActive: true,
                buttonsLayout: .horizontal
            )

            let alert = SCLAlertView(appearance: appearance)
            _ = alert.addButton("OK") {
                self.navigationController?.popViewController(animated: true)
            }
            _ = alert.showSuccess("LATAKOO", subTitle: "Copied successfully.")
            appdelegate.HideProgress()
        }
    }

    func movetonetwork() {
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.ShowProgress()
        let aStrApi = "\(UserDefaults.standard.value(forKey: "server") as! String)-/api2/?method=object.action"
        let parameter: [String: Any] = ["action": "move",
                                        "id_list": dictback["id"] as! String,
                                        "target_network_id": networkid,
        ]

        print(parameter)

        APIManager.requestPOSTURL(aStrApi, params: parameter as [String: AnyObject], headers: nil, success: { JSON in

            print(JSON)

            appdelegate.HideProgress()

            let appearance = SCLAlertView.SCLAppearance(
                kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                showCloseButton: false,
                dynamicAnimatorActive: true,
                buttonsLayout: .horizontal
            )

            let alert = SCLAlertView(appearance: appearance)
            _ = alert.addButton("OK") {
                self.navigationController?.popViewController(animated: true)
            }
            _ = alert.showSuccess("LATAKOO", subTitle: "Moved successfully.")

        }) { Error in

            print(Error)
            appdelegate.HideProgress()
            let appearance = SCLAlertView.SCLAppearance(
                kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                showCloseButton: false,
                dynamicAnimatorActive: true,
                buttonsLayout: .horizontal
            )

            let alert = SCLAlertView(appearance: appearance)
            _ = alert.addButton("OK") {
                self.navigationController?.popViewController(animated: true)
            }
            _ = alert.showSuccess("LATAKOO", subTitle: "Moved successfully.")
        }
    }

    @objc func backgroundCallback(_ info: Any?) {
        //  print("bg task running")
    }

    func downloadata(url:String) {
        
        
                bgTask = BackgroundTask()
                DispatchQueue.global(qos: .default).async(execute: {
                    self.bgTask?.startBackgroundTasks(2, target: self, selector: #selector(self.backgroundCallback(_:)))
                })

        
         self.viewCircularProgressBar.labelSize = 30
         self.viewCircularProgressBar.safePercent = 100
         self.viewCircularProgressBar.setProgress(to: self.progressToSet, withAnimation: true)
         self.viewCircularProgressBar.backgroundColor = UIColor.clear
        KGModal.sharedInstance()?.show(withContentView: self.viewCircularProgressBar)
        KGModal.sharedInstance()?.tapOutsideToDismiss = false
              
        
        
           //    if let url = URL(string: url) {
                
          print(url)
                
                ALDownloadManager.shared.download(url: url)?.downloadProgress({ (progress) in
                    // update UI
                    print(progress)
                    let progresss = Float(progress.completedUnitCount) / Float(progress.totalUnitCount)

                    self.viewCircularProgressBar.setProgress(to:Double(progresss), withAnimation: true)
                    self.progressToSet = self.downloadingProgress
                                   
                }).downloadResponse({ (response) in

                 
                    
                    if response.destinationURL != nil{
                    self.tempfilepath = response.destinationURL!.path
                    }
                    
                    
                    if response.destinationURL != nil{
                                       self.tempfilepath = response.destinationURL!.path
                                       }
//                    let dataA = NSData(contentsOf:response.temporaryURL! )
                    let dataA = NSData(contentsOf:response.destinationURL!)


                    if dataA == nil{
                        print(response.destinationURL!)
                        let dataB = NSData(contentsOf:response.destinationURL!)
                        self.savefiles(audiodata: dataB!, iftemp: false)

                    }else{

                        self.savefiles(audiodata: dataA!, iftemp: true)
                    }

                })
                
//
//                let session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
//                let downloadTask = session.downloadTask(with: url)
//                  downloadTask.resume()
       // }

        return

//        var req: URLRequest?
//        if let url = URL(string: url) {
//            req = URLRequest(url: url)
//        }
//        if let req = req {
//            NSURLConnection.sendAsynchronousRequest(req, queue: OperationQueue.main, completionHandler: { _, audioData, connectionError in
//                if connectionError != nil {
//                    _ = SCLAlertView().showError("LATAKOO", subTitle: "download failed.", closeButtonTitle: "OK")
//                } else {
//
//
//                    do {
//                        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//                        let url = URL(string: (self.dictback["stream_url"] as? String)!)
//
//                        var tempURL = documentsURL?.appendingPathComponent(url!.lastPathComponent)
//
//                        print(self.isimage)
//
//                        if self.isimage == "video" {
//
//
//                            try audioData?.write(to:tempURL!, options: .atomic)
//                            UISaveVideoAtPathToSavedPhotosAlbum((tempURL?.path)!, self, #selector(PilotimagedetailVC.video(videoPath:didFinishSavingWithError:contextInfo:)), nil)
//
//                        } else if self.isimage == "image" {
//
//                            try audioData?.write(to:tempURL!, options: .atomic)
//
//
//                            let imageee = UIImage(contentsOfFile: (tempURL?.path)!)
//
//                            UIImageWriteToSavedPhotosAlbum(imageee!, self, #selector(PilotimagedetailVC.image(imagepath:didFinishSavingWithError:contextInfo:)), nil)
//
//                        } else {
//                            var newURL: URL!
//
//                            let EXT = URL(fileURLWithPath: (self.dictback["title"] as? String)!).pathExtension
//
//                            if EXT == "" {
//                                newURL = self.getDocumentsDirector().appendingPathComponent("\(self.dictback["title"] as? String)).m4a")
//
//                            } else {
//                                let list = self.dictback["title"] as? String
//                                let listItems = list!.components(separatedBy: ".")
//                                let filename = listItems[0]
//
//                                newURL = self.getDocumentsDirector().appendingPathComponent("\(filename).m4a")
//                            }
//
//                            print(newURL)
//
//                            try audioData?.write(to: newURL, options: .atomic)
//
//                            self.perform(#selector(self.movetogalary), with: nil, afterDelay: 1.0)
//                            print("audio saved to device successfully")
//                        }
//
//                        //  try audioData?.write(to: URL(fileURLWithPath: fileName!), options: .atomic)
//
//                        self.app.HideProgress()
//
//                    } catch {
//                        print(error)
//
//                        self.app.HideProgress()
//                    }
//                }
//            })
//        }
    }
    
    
    
    
    
    
    
    func savefiles(audiodata : NSData , iftemp : Bool){

        iftempfile = iftemp
        
        DispatchQueue.global(qos: .background).async {
            self.bgTask?.stop()
       }
    
        let audioData = audiodata
        
        



    
    do {
        print(self.dictback)
        
        
        let documentsURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
           
        
        
       
        
        
        
        let url = URL(string: (self.dictback["stream_url"] as? String)!)

        var tempURL = documentsURL.appendingPathComponent(url!.lastPathComponent)

        print(self.isimage)

        if self.isimage == "video" {
            
            self.app.checkdownload = "Video"
            let EXT = URL(fileURLWithPath: (self.dictback["title"] as? String)!).pathExtension

            if EXT == "" {
                tempURL = documentsURL.appendingPathComponent("\(self.dictback["title"] as? String ?? "video").mp4")
                print(tempURL)

            }else{
                if self.isstream == true {
                    var name = self.dictback["title"] as? String ?? "video"
                    name = name.fileName()
                    tempURL = documentsURL.appendingPathComponent("\(name).mp4")
                    print(tempURL)

                }else{
                    tempURL = documentsURL.appendingPathComponent("\(self.dictback["title"] as? String ?? "video")")
                    print(tempURL)

                }
            }
            
            print(tempURL)
            print(audioData.count )
            
            
            FileManager.default.createFile(atPath: tempURL.path,
                                           contents: audioData as Data?,
                                                         attributes: nil)

                          if FileManager.default.fileExists(atPath: tempURL.path) {
                              print("video file present!") // Confirm that the file is here!
                          }else{
                            print("video file not present!") // Confirm that the file is here!

            }

         //   try audioData?.write(to:tempURL, options: .atomic)
            
            
            
            sleep(UInt32(1.0))
            PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: tempURL)
            }) { saved, error in
                if saved {
                    
                    if FileManager.default.fileExists(atPath: tempURL.path) {
                                            
                                           do {
                                            try FileManager.default.removeItem(atPath: tempURL.path)

                                           } catch {
                                               
                                           }
                        
                        
                        let   newURL = self.getDocumentsDirector().appendingPathComponent("LatakooDownloads")
                             
                              print(newURL)
                              
                        if FileManager.default.fileExists(atPath: self.tempfilepath as String) {
                                                  
                                                 do {
                                                  try FileManager.default.removeItem(atPath: newURL.path as String)

                                                 } catch {
                                                     
                                                 }
                                                 
                                             }
                        
                    }
                    
                    
                    self.getvideo()
                }else{
                    UISaveVideoAtPathToSavedPhotosAlbum((tempURL.path), self, #selector(PilotimagedetailVC.video(videoPath:didFinishSavingWithError:contextInfo:)), nil)

                }
                
                
                
            }
            
            

        } else if self.isimage == "image" {
            
            self.app.checkdownload = "Image"
            let imageee = UIImage.sd_image(with: audioData as Data?)
            UIImageWriteToSavedPhotosAlbum(imageee!, self, #selector(PilotimagedetailVC.image(imagepath:didFinishSavingWithError:contextInfo:)), nil)

        } else if self.isimage == "audio"{
            
            self.app.checkdownload = "Audio"

            var newURL: URL!

            let EXT = URL(fileURLWithPath: (self.dictback["title"] as? String)!).pathExtension

            if EXT == "" {
                newURL = self.getDocumentsDirector().appendingPathComponent("\(self.dictback["title"] as! String).m4a")

            } else {
                let list = self.dictback["title"] as? String
                let listItems = list!.components(separatedBy: ".")
                let filename = listItems[0]

                newURL = self.getDocumentsDirector().appendingPathComponent("\(filename).m4a")
            }

            print(newURL!)
            
            
            FileManager.default.createFile(atPath: tempURL.path,
                                           contents: audioData as Data?,
                                                         attributes: nil)

                          if FileManager.default.fileExists(atPath: tempURL.path) {
                              print("audio file present!") // Confirm that the file is here!
                          }else{
                            print("audio file not present!") // Confirm that the file is here!

            }
            
            
            
            let   newvalue = self.getDocumentsDirector().appendingPathComponent("LatakooDownloads")
                                        
                                         print(newURL)
                                         
                                                        if FileManager.default.fileExists(atPath: tempfilepath as String) {
                                                             
                                                            do {
                                                             try FileManager.default.removeItem(atPath: newvalue.path as String)

                                                            } catch {
                                                                
                                                            }
                                                            
                                                        }
                                   

           // try audioData?.write(to: newURL!, options: .atomic)
               sleep(1)
             self.movetogalary()
            print("audio saved to device successfully")
        }else{
            let EXT = URL(string: (self.dictback["stream_url"] as? String)!)!.pathExtension
               print(EXT)
            
            var newURL: URL!


            let list = self.dictback["title"] as? String
                let listItems = list!.components(separatedBy: ".")
                    let filename = listItems[0]
            newURL = self.getDocumentsDirector().appendingPathComponent("\(filename).\(EXT)")
                           print(newURL!)
            
            if FileManager.default.fileExists(atPath: newURL.path) {

                newURL = self.getDocumentsDirector().appendingPathComponent("\(filename)_\(self.randomString(length: 3)).\(EXT)")
                                              print(newURL!)
            }
            
            
            FileManager.default.createFile(atPath: newURL.path,
                                           contents: audioData as Data?,
                                                         attributes: nil)

                          if FileManager.default.fileExists(atPath: tempURL.path) {
                              print("other file present!") // Confirm that the file is here!
                          }else{
                            print("other file not present!") // Confirm that the file is here!

            }
            
          //  try audioData?.write(to: newURL!, options: .atomic)
         //   KGModal.sharedInstance()?.hide()
            let appearance = SCLAlertView.SCLAppearance(
                kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                showCloseButton: false,
                dynamicAnimatorActive: true,
                buttonsLayout: .horizontal
            )
            let alert = SCLAlertView(appearance: appearance)
            _ = alert.addButton("Ok") {
          KGModal.sharedInstance()?.hide()

            
            }
            
                let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
                _ = alert.showCustom("Alert", subTitle: "This file type is not supported by the iOS library. This file will be saved in latakoo folder in your Files.", color: color, circleIconImage:nil)
            
            
            let   newvalue = self.getDocumentsDirector().appendingPathComponent("LatakooDownloads")
                                        
                                         print(newURL)
                                         
                                                        if FileManager.default.fileExists(atPath: tempfilepath as String) {
                                                             
                                                            do {
                                                             try FileManager.default.removeItem(atPath: newvalue.path as String)

                                                            } catch {
                                                                
                                                            }
                                                            
                                                        }
                                   
            
        }


        self.app.HideProgress()

    } catch {
        print(error)

        self.app.HideProgress()
    }

    }
    
    
    
    

//    func downloadata() {
//        app.ShowProgress()
//
//        bgTask = BackgroundTask()
//        DispatchQueue.global(qos: .default).async(execute: {
//            self.bgTask?.startBackgroundTasks(2, target: self, selector: #selector(self.backgroundCallback(_:)))
//        })
//
//        getCredentialThroughMediaUploadAPI()
//        return
//    }

    @objc func updateProgress() {
        viewCircularProgressBar.setProgress(to: progressToSet, withAnimation: true)
        progressToSet = downloadingProgress
    }

//    func getCredentialThroughMediaUploadAPI() {
//                            if !Reachability.isConnectedToNetwork() {
//            print("Internet is Not available.")
//            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
//
//            DispatchQueue.global(qos: .background).async {
//                self.bgTask?.stop()
//            }
//
//            return
//        } else {
//            AJNotificationView.hideCurrentNotificationViewAndClearQueue()
//            AJNotificationView.hideCurrentNotificationView()
//        }
//
//        print(dictback)
//
//        let headers = ["Authorization":
//            "Bearer \(UserDefaults.standard.value(forKey: "Authtoken") ?? "")"]
//        var aStrApi = ""
//        var downloadFileID = ""
//            aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)media/download/\(dictback["id"] as! String)"
//            downloadFileID = dictback["id"] as! String
//
//
//        aStrApi = AJNotificationView.changetoencodedstring(aStrApi)
//        print("media upload api => \(aStrApi)")
//
//        APIManager.requestGETURLNEW(aStrApi, params: nil, headers: headers as? [String: String], success: { usrDetail in
//            print(usrDetail)
//
//            let tempDict = usrDetail.dictionaryObject!
//            print("tempDict: \(tempDict)")
//            if tempDict["result"] != nil {
//                let dictcredential = tempDict["result"] as! [String: Any]
//                print("dictcredential: \(dictcredential)")
//
//                let dictnew = dictcredential["credentials"] as! [String: Any]
//                let dictbucket = dictcredential["bucket"] as! [String: Any]
//                print(dictnew)
//                let accesskey = dictnew["accessKeyId"] as! String
//                let secretekey = dictnew["secretAccessKey"] as! String
//                let bucketname = dictbucket["name"] as! String
//                let sessiontoken = dictnew["sessionToken"] as! String
//                let expiration = dictnew["expiration"] as! String
//                let fileNam = dictbucket["fileName"] as! String
//                appdelegate.filenameStr = fileNam
//                let bucketp = dictbucket["key"] as! String
//                //                print("Key to upload: \(bucketp)")
//                //                let bucketpa = bucketp.dropLast()
//                //                print("bucketpa remove last: \(bucketpa)")
//                //                let bucketpath = "\(bucketpa)dat"
//                let bucketpath = bucketp
//                print("bucketpath final: \(bucketpath)")
//                print("Access key = > \(accesskey)")
//                print("secret key = > \(secretekey)")
//                print("bucket name = > \(bucketname)")
//                print("bucket path = > \(bucketpath)")
//                print("sessiontoken path = > \(sessiontoken)")
//                print("expiration = > \(expiration)")
//                appdelegate.bucketpTest = bucketp
//                appdelegate.secretKey = secretekey
//                appdelegate.accessKey = accesskey
//                appdelegate.bucketname = bucketname
//                appdelegate.bucketpath = bucketpath
//                appdelegate.sessionToken = sessiontoken
//                appdelegate.expiration = expiration
//                appdelegate.fileNameFromServer = "\(fileNam).dat"
//                appdelegate.thumbnailBucketpath = "\(appdelegate.bucketpath).jpg"
//                print("expirself.appdelegate.thumbnailBucketpathation = > \(appdelegate.thumbnailBucketpath)")
//
//                let credentialsProvider = AWSBasicSessionCredentialsProvider(accessKey: accesskey, secretKey: secretekey, sessionToken: sessiontoken)
//                let configuration = AWSServiceConfiguration(region: AWSRegionType.USEast1, credentialsProvider: credentialsProvider)
//                AWSServiceManager.default().defaultServiceConfiguration = configuration
//
//                let transferUtilityConfigurationWithRetry = AWSS3TransferUtilityConfiguration()
//                transferUtilityConfigurationWithRetry.retryLimit = 5
//                transferUtilityConfigurationWithRetry.timeoutIntervalForResource = 15 * 60 // 15 minutes
//                transferUtilityConfigurationWithRetry.bucket = bucketname
//                AWSS3TransferUtility.register(with: configuration!, transferUtilityConfiguration: transferUtilityConfigurationWithRetry, forKey: appdelegate.bucketpTest)
//
//                do {
//                    let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//                    let url = URL(string: (self.dictback["stream_url"] as? String)!)
//
//                    var tempURL = documentsURL?.appendingPathComponent(url!.lastPathComponent)
//                    print("tempURL: \(tempURL!)")
//                    print(self.isimage)
//
//                    if self.isimage == "video" {
//                    } else if self.isimage == "image" {
//                    } else {
//                        var newURL: URL!
//
//                        let EXT = URL(fileURLWithPath: (self.dictback["title"] as? String)!).pathExtension
//
//                        if EXT == "" {
//                            newURL = self.getDocumentsDirector().appendingPathComponent("\(self.dictback["title"] as! String).m4a")
//                        } else {
//                            let list = self.dictback["title"] as? String
//                            let listItems = list!.components(separatedBy: ".")
//                            let filename = listItems[0]
//                            newURL = self.getDocumentsDirector().appendingPathComponent("\(filename).m4a")
//                        }
//
//                        print(newURL)
//                        tempURL = newURL
//                    }
//
//                    DispatchQueue.main.async {
//                        self.app.HideProgress()
//
//                        self.viewCircularProgressBar.labelSize = 30
//                        self.viewCircularProgressBar.safePercent = 100
//                        self.viewCircularProgressBar.setProgress(to: self.progressToSet, withAnimation: true)
//
//                        let timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateProgress), userInfo: nil, repeats: true)
//                        // timer.fire()
//                        RunLoop.main.add(timer, forMode: .commonModes)
//
//                        self.viewCircularProgressBar.backgroundColor = UIColor.clear
//                        KGModal.sharedInstance()?.show(withContentView: self.viewCircularProgressBar)
//                        KGModal.sharedInstance()?.tapOutsideToDismiss = false
//
//                        let fileTitleString = (self.dictback["title"] as? String)!
//                        AWSS3Manager.shared.downloadFile(fileID: downloadFileID, downloadURL: tempURL!, fileName: fileTitleString, fileType: self.isimage, progress: { [weak self] progress in
//                            guard self != nil else { return }
//                            print("Downloading Percentage: \(progress)")
//                            let twodecimal = String(format: "%.2f", progress)
//                            //                            print("twodecimal: \(twodecimal)")
//                            //                            print("Printing in double: \(Double(twodecimal)!)")
//                            self!.downloadingProgress = Double(twodecimal)!
//                        }) { [weak self] uploadedFileUrl, error in
//                            guard self != nil else { return }
//                            if let finalPath = uploadedFileUrl as? String {
//                                AWSS3TransferUtility.remove(forKey: appdelegate.bucketpTest)
//
//                                print("downloaded file from url: " + finalPath)
//                                print("File downloaded at url:" + tempURL!.path)
//
//                                if self!.isimage == "video" {
//                                    UISaveVideoAtPathToSavedPhotosAlbum((tempURL?.path)!, self, #selector(PilotimagedetailVC.video(videoPath:didFinishSavingWithError:contextInfo:)), nil)
//
//                                } else if self!.isimage == "image" {
//                                    let imageee = UIImage(contentsOfFile: (tempURL?.path)!)
//
//                                    UIImageWriteToSavedPhotosAlbum(imageee!, self, #selector(PilotimagedetailVC.image(imagepath:didFinishSavingWithError:contextInfo:)), nil)
//
//                                } else {
//                                    self!.perform(#selector(self!.movetogalary), with: nil, afterDelay: 1.0)
//                                    print("audio saved to device successfully")
//                                }
//
//                            } else {
//                                KGModal.sharedInstance()?.hide()
//                                print("\(String(describing: error?.localizedDescription))")
//                                let appearance = SCLAlertView.SCLAppearance(
//                                    kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
//                                    kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
//                                    kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
//                                    showCloseButton: false,
//                                    dynamicAnimatorActive: true,
//                                    buttonsLayout: .horizontal
//                                )
//
//                                let alert = SCLAlertView(appearance: appearance)
//                                _ = alert.addButton("OK") {
//                                    self?.navigationController?.popViewController(animated: true)
//                                }
//                                _ = alert.showSuccess("ERROR", subTitle: error?.localizedDescription)
//
//                                DispatchQueue.global(qos: .background).async {
//                                    self!.bgTask?.stop()
//                                }
//                            }
//                        }
//                    }

                    //                    let fileTitleString = (self.dictback["title"] as? String)!
                    //                    AWSS3Manager.shared.downloadFile(fileID: downloadFileID, downloadURL: tempURL!, fileName: fileTitleString, fileType: self.isimage, progress: { [weak self] progress in
                    //                        guard self != nil else { return }
                    //                        print("Downloading Percentage: \(progress)")
                    //                    }) { [weak self] uploadedFileUrl, error in
                    //                        guard self != nil else { return }
                    //                        if let finalPath = uploadedFileUrl as? String {
                    //                            AWSS3TransferUtility.remove(forKey: appdelegate.bucketpTest)
                    //
                    //                            print("downloaded file from url: " + finalPath)
                    //                            print("File downloaded at url:" + tempURL!.path)
                    //
                    //                            if self!.isimage == "video" {
                    //                                UISaveVideoAtPathToSavedPhotosAlbum((tempURL?.path)!, self, #selector(PilotimagedetailVC.video(videoPath:didFinishSavingWithError:contextInfo:)), nil)
                    //
                    //                            } else if self!.isimage == "image" {
                    //                                let imageee = UIImage(contentsOfFile: (tempURL?.path)!)
                    //
                    //                                UIImageWriteToSavedPhotosAlbum(imageee!, self, #selector(PilotimagedetailVC.image(imagepath:didFinishSavingWithError:contextInfo:)), nil)
                    //
                    //                            } else {
                    //                                self!.perform(#selector(self!.movetogalary), with: nil, afterDelay: 1.0)
                    //                                print("audio saved to device successfully")
                    //                            }
                    //
                    //                        } else {
                    //                            print("\(String(describing: error?.localizedDescription))")
                    //                            let appearance = SCLAlertView.SCLAppearance(
                    //                                kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                    //                                kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                    //                                kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                    //                                showCloseButton: false,
                    //                                dynamicAnimatorActive: true,
                    //                                buttonsLayout: .horizontal
                    //                            )
                    //
                    //                            let alert = SCLAlertView(appearance: appearance)
                    //                            _ = alert.addButton("OK") {
                    //                                self?.navigationController?.popViewController(animated: true)
                    //                            }
                    //                            _ = alert.showSuccess("ERROR", subTitle: error?.localizedDescription)
                    //                        }
                    //                    }

//                } catch {
//                    print(error)
//                    self.app.HideProgress()
//                    DispatchQueue.global(qos: .background).async {
//                        self.bgTask?.stop()
//                    }
//                }
//
//            } else {
//                self.refreshToken()
//            }
//        },
//
//        failure: { error in
//
//            self.refreshToken()
//            print(error)
//            appdelegate.HideProgress()
//
//            DispatchQueue.global(qos: .background).async {
//                self.bgTask?.stop()
//            }
//        })
//    }
    
    
    
    func ServicegetdownloadURL() {
                            if !Reachability.isConnectedToNetwork() {
            print("Internet is Not available.")
            AJNotificationView.showNotice(in: (UIApplication.shared.delegate?.window)!, type: AJNotificationTypeRed, title: "Please check your internet connection", hideAfter: 1.0)
            return
        }

        appdelegate.ShowProgress()

        let headers = ["Authorization":
                "Bearer \(UserDefaults.standard.value(forKey: "Authtoken") ?? "")"]
        var aStrApi = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)media/presigned-url/\((dictback["id"] as! String))?include=lq,filesize"





        aStrApi = (aStrApi as NSString).addingPercentEscapes(using: String.Encoding.utf8.rawValue) ?? ""
        print(aStrApi)
      
        print(headers)

        let encoding = URLEncoding.httpBody
        Alamofire.request(aStrApi, method: .get, parameters: nil, encoding: encoding, headers: headers).responseJSON { (responseObject) -> Void in

            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                let usrDetail = resJson
                print(usrDetail)
                if usrDetail["result"].exists() {
              
                    let dictresult = usrDetail["result"].rawValue as! [String:Any]
                    print(dictresult)
                    
                  //  print(dictresult["url"] as? String ?? "")
                   // self.downloadata(url: dictresult["url"] as? String ?? "")
                    
                    if self.isimage == "video" {
                    appdelegate.HideProgress()
                    self.Addactionsheet(dict: dictresult)
                    }else{
                  
                        self.downloadata(url: dictresult["url"] as? String ?? "")
                  
                    }
                }else{
                    
                    appdelegate.HideProgress()

                    self.refreshToken()

                }

            }else{
                self.refreshToken()
            }
            if responseObject.result.isFailure {

                self.refreshToken()


                
            }


        }


    }
    
    
    
    
    func Addactionsheet(dict : [String:Any])  {
        
        
        
        print(dict)
             
        let strem  = dict["streaming_filesize"] as? Int ?? 0
        let filesize  = dict["filesize"] as? Int ?? 0

        let stremsize = self.getsize(Size: strem)
        let orgsize = self.getsize(Size: filesize)
      
        print(stremsize)

        print(orgsize)
        
        var arr = ["Full file (\(orgsize))","Streaming version (\(stremsize))"]
       
        print(dict)
        
        if dictback["video_group"] is NSNull {
          arr = ["Full file (\(orgsize))",
            "Streaming version (\(stremsize))"]
            
            print(arr)
            
        }else{
            
           arr = ["Full file (\(orgsize))"]
        
        }
        
        
        let options = YActionSheet(title: "Download...", dismissButtonTitle: "Cancel", otherButtonTitles:arr, dismissOnSelect: true, ismenifest: false)

        options!.show(in: self, withYActionSheetBlock: { buttonIndex, isCancel in
            if isCancel {
                self.networkid = ""
            } else {
                
                if buttonIndex == 0 {
                    
                    self.isstream = false
                    
                    let appearance = SCLAlertView.SCLAppearance(
                                                      kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                                                      kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                                                      kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                                                      showCloseButton: false,
                                                      dynamicAnimatorActive: true,
                                                      buttonsLayout: .horizontal
                                                  )
                                                  let alert = SCLAlertView(appearance: appearance)
                                                  _ = alert.addButton("Cancel") {
                                                  }
                                                  _ = alert.addButton("Yes") {
                                                    
                                                    if filesize < UIDevice.current.freeDiskSpaceInBytes{
                                                  self.downloadata(url: dict["url"] as? String ?? "")
                                                        
                                                    }else{
                                                        _ = SCLAlertView().showError("Error", subTitle:"There is not enough available space to continue the file download. Make room by deleting existing videos or photos.", closeButtonTitle:"OK")
                                                        
                                                    }
                                                
                                                  
                                                  }

                                                  let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)

                                                  _ = alert.showCustom("Alert", subTitle: "Are you sure you have enough storage and wish to continue?", color: color, circleIconImage: nil)
                }else{
                    
                    self.isstream = true

                    
                    if strem < UIDevice.current.freeDiskSpaceInBytes {
                        
                        if strem > 0 {

                
                        self.downloadata(url: dict["streaming_url"] as? String ?? "")
                        }else{
                            self.app.HideProgress()
                        }
                        
                        
                                                                     
                    }else{
                                                                           
                        
                        _ = SCLAlertView().showError("Error", subTitle:"There is not enough available space to continue the file download. Make room by deleting existing videos or photos.", closeButtonTitle:"OK")
                                                                           
                        
                    
                    }
                    
                                           

                    
                }


            }
        })
        
        
        
        

        
        

    }
    
    
    func getsize(Size:Int) -> String  {
          let suffixKB = "KB"
                  let suffixMB = "MB"
                  let suffixGB = "GB"
              var formattedSize = Size / 1024
                  var formattedExt = suffixKB
                  if formattedSize > 1024 {
                      formattedSize /= 1024
                      formattedExt = suffixMB
                  }
                  if formattedSize > 1024 {
                      formattedSize /= 1024
                      formattedExt = suffixGB
                  }
        let result = String(format: "%li %@", formattedSize, formattedExt)
               
                print(result)
        return result
    }
    
    

    func refreshToken() {
        let urlString = "\(UserDefaults.standard.value(forKey: "serverManifest") as! String)auth/refresh-token"
        let request = NSMutableURLRequest()
        request.url = URL(string: urlString)
        request.httpMethod = "PUT"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        var Parameter: [AnyHashable: Any] = [:]
        Parameter["token"] = UserDefaults.standard.object(forKey: "Authtoken")
        Parameter["refreshToken"] = UserDefaults.standard.object(forKey: "RefreshToken")
        var myData: Data?
        do {
            myData = try JSONSerialization.data(withJSONObject: Parameter, options: .prettyPrinted)
        } catch {
        }
        var putData = Data()
        if let myData = myData {
            putData.append(Data(myData))
        }
        request.httpBody = putData
        var response: URLResponse?

        var responseData: Data?
        do {
            responseData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
        } catch let err {
            print(err)
        }

        do {
            let json = try JSONSerialization.jsonObject(with: responseData!, options: []) as? [String: Any]
            print(json!)
            if (json?["result"]) != nil {
                let dict = json!["result"] as? [String: Any]
                let AuthToken = dict!["token"] as? String
                UserDefaults.standard.setValue(AuthToken, forKey: "Authtoken")
                UserDefaults.standard.synchronize()
                self.ServicegetdownloadURL()
            }

        } catch {
            print(error)
        }
    }

    @objc func video(videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        
        
        let   newURL = self.getDocumentsDirector().appendingPathComponent("LatakooDownloads")
       
        print(newURL)
        
                       if FileManager.default.fileExists(atPath: tempfilepath as String) {
                            
                           do {
                            try FileManager.default.removeItem(atPath: newURL.path as String)

                           } catch {
                               
                           }
                           
                       }
                   
        
        
        if let _ = error {
            print("Error,Video failed to save")
            print(error)
            print(error?.localizedDescription)

           // KGModal.sharedInstance()?.hide()
           
            
            
            
            
            let appearance = SCLAlertView.SCLAppearance(
                                   kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                                   kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                                   kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                                   showCloseButton: false,
                                   dynamicAnimatorActive: true,
                                   buttonsLayout: .horizontal
                               )
                               let alert = SCLAlertView(appearance: appearance)
                               _ = alert.addButton("Ok") {
                              KGModal.sharedInstance()?.hide()

            
            }
                               
                                   let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
                                   _ = alert.showCustom("Alert", subTitle: "This file type is not supported by the iOS library. This file will be saved in latakoo folder in your Files.", color: color, circleIconImage:nil)
            
        } else {
            
            
            print(videoPath)
                   let EXT = URL(fileURLWithPath:videoPath as String).pathExtension

                   if EXT == "mxf" || EXT == "mpg" || EXT == "MXF" || EXT == "MPG" {
                    print("Successfully,Video was saved")


                    let appearance = SCLAlertView.SCLAppearance(
                        kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                        kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                        kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                        showCloseButton: false,
                        dynamicAnimatorActive: true,
                        buttonsLayout: .horizontal
                    )
                    let alert = SCLAlertView(appearance: appearance)
                    _ = alert.addButton("Ok") {
                     KGModal.sharedInstance()?.hide()

                    
                    }
                    
                        let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
                        _ = alert.showCustom("Alert", subTitle: "This file type is not supported by the iOS library. This file will be saved in latakoo folder in your Files.", color: color, circleIconImage:nil)
                       
                   
                   }else{
                    
                    if FileManager.default.fileExists(atPath: videoPath as String) {
                         
                        do {
                            try FileManager.default.removeItem(atPath: videoPath as String)

                        } catch {
                            
                        }
                        
                    }
                    
                    print("Successfully,Video was saved")
                    getvideo()
                   perform(#selector(movetogalary),
                                        with: nil, afterDelay: 1.0)
                   }
            
         
        }
    }

    @objc func image(imagepath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        if let _ = error {
            print("Error,image failed to save")
        } else {
            
            
          
            
            print("Successfully,image was saved")
            getimage()
            perform(#selector(movetogalary), with: nil, afterDelay: 1.0)
        }
    }

     func getvideo() {
        app.getvideo()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
       KGModal.sharedInstance()?.hide()
        self.tabBarController?.selectedIndex = 2
    let navCtrl = self.tabBarController?.selectedViewController as? UINavigationController
     navCtrl?.popToRootViewController(animated: false)
        }

    }

    @objc func getimage() {
        app.getimage()
    }

    func getDocumentsDirector() -> URL {
        // Grab CWD and set to paths
//        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
//        return paths[0]
        
        let documentsURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
          return documentsURL
    }

    
    
    @objc func movetogalary() {
         //   alert!.close()
            KGModal.sharedInstance()?.hide()
            self.tabBarController?.selectedIndex = 2
            let navCtrl = self.tabBarController?.selectedViewController as? UINavigationController
            navCtrl?.popToRootViewController(animated: false)
           
        }

    @objc func playerItemDidReachEnd() {
        player?.pause()
        flagforAudio = 0
        audioslider.value = 0
        lblaudio.text = "00:00:00"
        player = nil
        playbackTimer1?.invalidate()
        btnplayaudio.setImage(UIImage(named: "playbutton.png"), for: .normal)
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        webindigaor.stopAnimating()
        webindigaor.isHidden = true
        ismetadataload = true
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print(request)
//                app.ShowProgress()
//               let weburl : String = "https://\(UserDefaults.standard.value(forKey: "server") as! String)/ajax/manage/getMetadataForm.php?id=\(dictback["id"] as? String ?? "")"
//               let targetURL = URL(string:weburl)
//               var request: URLRequest? = nil
//               if let targetURL = targetURL {
//               request = URLRequest(url: targetURL)
//               }
//              if let request = request {
//               metadataview.loadRequest(request)
//               }
//               metadataview.isHidden = true
        return true
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webindigaor.stopAnimating()
        webindigaor.isHidden = true
        ismetadataload = true
    }
}
extension UIDevice {
    func MBFormatter(_ bytes: Int64) -> String {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = ByteCountFormatter.Units.useMB
        formatter.countStyle = ByteCountFormatter.CountStyle.decimal
        formatter.includesUnit = false
        return formatter.string(fromByteCount: bytes) as String
    }
    
    //MARK: Get String Value
    var totalDiskSpaceInGB:String {
       return ByteCountFormatter.string(fromByteCount: totalDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.decimal)
    }
    
    var freeDiskSpaceInGB:String {
        return ByteCountFormatter.string(fromByteCount: freeDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.decimal)
    }
    
    var usedDiskSpaceInGB:String {
        return ByteCountFormatter.string(fromByteCount: usedDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.decimal)
    }
    
    var totalDiskSpaceInMB:String {
        return MBFormatter(totalDiskSpaceInBytes)
    }
    
    var freeDiskSpaceInMB:String {
        return MBFormatter(freeDiskSpaceInBytes)
    }
    
    var usedDiskSpaceInMB:String {
        return MBFormatter(usedDiskSpaceInBytes)
    }
    
    //MARK: Get raw value
    var totalDiskSpaceInBytes:Int64 {
        guard let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String),
            let space = (systemAttributes[FileAttributeKey.systemSize] as? NSNumber)?.int64Value else { return 0 }
        return space
    }
    
  
    var freeDiskSpaceInBytes:Int64 {
        if #available(iOS 11.0, *) {
            if let space = try? URL(fileURLWithPath: NSHomeDirectory() as String).resourceValues(forKeys: [URLResourceKey.volumeAvailableCapacityForImportantUsageKey]).volumeAvailableCapacityForImportantUsage {
                return space ?? 0
            } else {
                return 0
            }
        } else {
            if let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String),
            let freeSpace = (systemAttributes[FileAttributeKey.systemFreeSize] as? NSNumber)?.int64Value {
                return freeSpace
            } else {
                return 0
            }
        }
    }
    
    var usedDiskSpaceInBytes:Int64 {
       return totalDiskSpaceInBytes - freeDiskSpaceInBytes
    }

}
