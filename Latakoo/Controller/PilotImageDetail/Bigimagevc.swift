//
//  Bigimagevc.swift
//  Latakoo
//
//  Created by Mac on 16/07/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import UIKit

class Bigimagevc: UIViewController,EFImageViewZoomDelegate {

    @IBOutlet weak var bigimageview: EFImageViewZoom!
    var bigimage : UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async{

            self.bigimageview.image = self.bigimage
            self.bigimageview.contentMode = .left
            self.bigimageview._delegate = self
            
        }

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func cross(_ sender: Any) {
        DispatchQueue.main.async{

        self.navigationController?.popViewController(animated: false)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
}
