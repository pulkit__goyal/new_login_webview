//
//  MovieplayerVC.swift
//  Latakoo
//
//  Created by Mac on 30/07/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import UIKit
import Photos
import AVFoundation
import AVKit
import MobileCoreServices
import SVProgressHUD

class bottomcollectioncell: UICollectionViewCell {
    @IBOutlet weak var imgsmall: UIImageView!

}



class MovieplayerVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,ICGVideoTrimmerDelegate {
    
    
    func trimmerView(_ trimmerView: ICGVideoTrimmerView, didChangeLeftPosition startTime: CGFloat, rightPosition endTime: CGFloat) {
        stopPlaybackTimeChecker()
               player?.pause()
       
     
        
        if (startTime != self.startTime || startTime == 0) {
            self.seekVideo(toPos: startTime)
             }
             else{
            self.seekVideo(toPos: endTime)
             }
        self.startTime = startTime
        self.stopTime = endTime
               
    }
    
    func seekVideo(toPos pos: CGFloat) {
           videoPlaybackPosition = pos
        let time = CMTimeMakeWithSeconds(Float64(videoPlaybackPosition), (player?.currentTime().timescale)!)
        player?.seek(to: time, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
      //  player?.seek(to: time, toleranceBefore: ., toleranceAfter: .zero)
       }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return video.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"cell", for: indexPath) as! bottomcollectioncell
        
        cell.imgsmall.clipsToBounds = true
        cell.imgsmall.contentMode = .scaleAspectFill
        let asset = video[indexPath.item]
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        option.isNetworkAccessAllowed = true
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            let img = UIImage.init(named:"imagesthumb")
                    thumbnail = result ?? img!
            cell.imgsmall.image = thumbnail
            
        })
        
        return cell
        }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        currentindex = indexPath.item
        App.ShowProgress()
        self.perform(#selector(loadimage), with: nil, afterDelay: 0.2)
    }
    
    @IBOutlet weak var btnplay1: UIButton!
    @IBOutlet weak var trimmerView1: ICGVideoTrimmerView!

    
    let playnew = UIButton(type: .custom)

    @IBOutlet var viewCircularProgressBar: CircularProgressBar!

    @IBOutlet weak var btnsend: UIButton!
    @IBOutlet weak var playerview: UIView!
    @IBOutlet weak var btnplay: UIButton!
    @IBOutlet weak var viewedit: UIView!
    @IBOutlet weak var viewsavecancel: UIView!
    @IBOutlet weak var lblheader: UILabel!
    @IBOutlet weak var mycoolectionview: UICollectionView!
    var video:[PHAsset] = []
    var currentindex : Int!
    var player: AVPlayer?
    var controller: AVPlayerViewController?
    var Myasset : AVAsset?
    var playbackTimeCheckerTimer: Timer?
    var trimmerPositionChangedTimer: Timer?
    var assetvalue : PHAsset?
    let App = UIApplication.shared.delegate as! AppDelegate
    var startTime: CGFloat = 0.0
    var stopTime: CGFloat = 0.0
    var videoPlaybackPosition: CGFloat = 0.0

    @IBOutlet var viewprogress: UIView!
    @IBOutlet var lblcountnew: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewsavecancel.isHidden = true
        self.perform(#selector(loadimage), with: nil, afterDelay: 0.2)

    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(true)
       if IS_IPHONE {
                  UIAppDelegate?.restrictRotation = .portrait
                   }else{
                  UIAppDelegate?.restrictRotation = .all
                   }
    }
    
    
    
        
    
    
    @objc func loadimage(){
        let asset = video[currentindex] 
               assetvalue = asset

             print( asset.localIdentifier )
        
        let resourceArray = PHAssetResource.assetResources(for: asset)
                                                         let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false
                                                          if  bIsLocallayAvailable == false {
        
        DispatchQueue.main.async {

        self.viewCircularProgressBar.labelSize = 30
            self.viewCircularProgressBar.safePercent = 100
        self.viewCircularProgressBar.setProgress(to: 0, withAnimation: true)
        KGModal.sharedInstance()?.show(withContentView: self.viewprogress)
        KGModal.sharedInstance()?.tapOutsideToDismiss = false
            self.viewCircularProgressBar.backgroundColor = UIColor.clear

        
        }
                                                            
        }
        
        
        
                let option1 = PHVideoRequestOptions()
                option1.isNetworkAccessAllowed = true
                option1.deliveryMode = .highQualityFormat
                option1.version = .original
                option1.progressHandler = {  (progress, error, stop, info) in
                    DispatchQueue.main.async{
                        
                        let resourceArray = PHAssetResource.assetResources(for: asset)
                                let bIsLocallayAvailable = (resourceArray.first?.value(forKey: "locallyAvailable") as? NSNumber)?.boolValue ?? false
                                 if bIsLocallayAvailable {
                                 }else{
                                  self.viewCircularProgressBar.setProgress(to: progress, withAnimation: true)

                                 }
                        
                    }
                }

                var resultAsset: AVAsset?
                PHCachingImageManager.default().requestAVAsset(forVideo: asset, options: option1, resultHandler: { avasset, audioMix, info in
                    resultAsset = avasset
                    self.Myasset = avasset
                    let duration = resultAsset?.duration
                    let durationTime = CMTimeGetSeconds(duration ??  CMTimeMake(1, 1))
                    print(durationTime)
                    DispatchQueue.main.async{
                        KGModal.sharedInstance()?.hide()
                       self.playmovieatpath(resultAsset)
                    }
                })
            
            
            let resources = PHAssetResource.assetResources(for: asset)
            let orgFilename = (resources[0]).originalFilename
            lblheader.text = orgFilename
            if (UserDefaults.standard.value(forKey: "renamedict") != nil){
                
                let  dict = UserDefaults.standard.value(forKey:"renamedict") as! [String : Any]
                
                if dict[asset.localIdentifier] != nil
                {
                     lblheader.text = (dict[asset.localIdentifier] as! String)
                }
            }
    }
    

    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func btnback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
//        let story =  UIStoryboard(name: "Main", bundle: nil)
//        let captureVC = story.instantiateViewController(withIdentifier: "captureVC") as! captureVC
//              captureVC.hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(captureVC, animated: false)
    }
    
    
    @IBAction func btnsend(_ sender: Any) {
        
        var  arr : [PHAsset] = []
        arr.append(video[currentindex] )
        
        var story : UIStoryboard?
                   
              if IS_IPHONE {
                   story = UIStoryboard.init(name: "Main", bundle: nil)
                  }else{
                   story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
              }
        appdelegate.isUploadingPause = false
        let SelectedVideo = story?.instantiateViewController(withIdentifier: "SelectedVideo") as! SelectedVC
        SelectedVideo.isfromview = "video"
        SelectedVideo.assestarray = arr
        self.navigationController?.pushViewController(SelectedVideo, animated: true)
    }
    
    @IBAction func btnplay(_ sender: Any) {
        player?.play()
        btnplay.isHidden = true
    }
    
//    func positionBarStoppedMoving(_ playerTime: CMTime) {
//        player?.seek(to: playerTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
//        player?.play()
//        startPlaybackTimeChecker()
//    }
    
//    func didChangePositionBar(_ playerTime: CMTime) {
//
//    }
    
    func startPlaybackTimeChecker() {
        
        stopPlaybackTimeChecker()
        playbackTimeCheckerTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                                        selector:
            #selector(MovieplayerVC.onPlaybackTimeChecker), userInfo: nil, repeats: true)
    }
    
    func stopPlaybackTimeChecker() {
        
        playbackTimeCheckerTimer?.invalidate()
        playbackTimeCheckerTimer = nil
    }
    
    @objc func onPlaybackTimeChecker() {
        
        
             let curTime = player?.currentTime()
            var seconds = CMTimeGetSeconds(curTime!)
               if seconds < 0 {
                   seconds = Float64(0) // this happens! dont know why.
               }
        videoPlaybackPosition = CGFloat(seconds)

          //     trimmerView.seek(to: seconds)
        trimmerView1.seek(toTime: CGFloat(seconds))


               if videoPlaybackPosition >= stopTime - 0.1 {
                   videoPlaybackPosition = startTime
                   seekVideo(toPos: startTime)
                trimmerView1.seek(toTime: startTime)
               }
        
        
//        guard let startTime = trimmerView.startTime, let endTime = trimmerView.endTime, let player = player else {
//            return
//        }
//
//        let playBackTime = player.currentTime()
//        trimmerView.seek(to: playBackTime)
//
//        if playBackTime >= endTime {
//            player.seek(to: startTime, toleranceBefore:kCMTimeZero, toleranceAfter: kCMTimeZero)
//
//            trimmerView.seek(to: startTime)
//        }
    }
    



    
    @IBAction func btnedit(_ sender: Any) {
        
        if (player?.isPlaying)!{
            btnplay1.setImage(UIImage.init(named: "pausebutton.png"), for: .normal)

        }else{
            btnplay1.setImage(UIImage.init(named: "playbutton.png"), for: .normal)
        }
        
        
        controller?.showsPlaybackControls = false

        viewsavecancel.isHidden = false
        viewedit.isHidden = true
        trimmerView1.isHidden = false
        player?.pause()
       btnplay1.setImage(UIImage.init(named: "playbutton.png"), for: .normal)

    }
    
    @IBAction func btnrename(_ sender: Any) {
        
         player?.pause()
        
        
        let appearance = SCLAlertView.SCLAppearance(
                             kTextFieldHeight: 50,
                             showCloseButton: false,
                             dynamicAnimatorActive: true,
                             buttonsLayout: .horizontal
                             
                           )
              
              let alert = SCLAlertView(appearance: appearance)
                          let txt = alert.addTextField("Video Name")
                    
                          _ = alert.addButton("Cancel") {

                                 }
                    
                          _ = alert.addButton("Save") {
                            
                            DispatchQueue.main.async {

                            
                            var  dict : [String:Any] = [:]
                            if (UserDefaults.standard.value(forKey: "renamedict") != nil){
                                dict = UserDefaults.standard.value(forKey:"renamedict") as! [String : Any]
                                print(dict)
                                self.lblheader.text = txt.text
                                dict[self.assetvalue!.localIdentifier] = txt.text
                            }else{
                                
                                self.lblheader.text = txt.text
                                dict[self.assetvalue!.localIdentifier] = txt.text
                            }
                            
                            print(dict)
                            
                              UserDefaults.standard.set(dict, forKey:"renamedict")
                            UserDefaults.standard.synchronize()
                            let appearance = SCLAlertView.SCLAppearance(
                                                                 kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                                                                 kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                                                                 kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                                                                 showCloseButton: false,
                                                                 dynamicAnimatorActive: true,
                                                                 buttonsLayout: .horizontal
                                       )

                                       let alert = SCLAlertView(appearance: appearance)
                                       _ = alert.addButton("OK"){
                                           
                                       
                                       }
                                       _ = alert.showSuccess("LATAKOO", subTitle:"Successfully Renamed.")
                            

        }
                            
                            
                          }
                          _ = alert.showEdit("Video name", subTitle:"Save video as:")
        
        
        
      
    
    }
    
    
    
    
    func savevideo(name:String)
    
    {
  
    
        App.ShowProgress()

    let asset = Myasset
        let length = Float(asset!.duration.value) / Float(asset!.duration.timescale)
    print("video length: \(length) seconds")
    
  
        
        var tmpDirectory: [String]? = nil
        do {
            tmpDirectory = try FileManager.default.contentsOfDirectory(atPath: NSTemporaryDirectory())
        } catch {
        }
        for file in tmpDirectory ?? [] {
            do {
                try FileManager.default.removeItem(atPath: "\(NSTemporaryDirectory())\(file)")
            } catch {
            }
        }
        let tempDir = NSTemporaryDirectory()
        let outputURL = URL(fileURLWithPath: tempDir).appendingPathComponent("\(name).mp4")
        
        print(outputURL)
    
        guard let exportSession = AVAssetExportSession(asset: asset!, presetName: AVAssetExportPresetPassthrough) else { return }
    exportSession.outputURL = outputURL
    exportSession.outputFileType = .mp4
    
        let timeRange = CMTimeRange(start: CMTime(seconds: (Double(startTime)), preferredTimescale: 1000),
                                    end: CMTime(seconds: (Double(stopTime)), preferredTimescale: 1000))
    
    exportSession.timeRange = timeRange
    exportSession.exportAsynchronously {
    switch exportSession.status {
    case .completed:
       // DispatchQueue.main.async {
            self.savetrimed(url: outputURL)
     // }
    case .failed:
    print("failed \(exportSession.error.debugDescription)")
    case .cancelled:
    print("cancelled \(exportSession.error.debugDescription)")
    default: break
    }
    }
}
    

 
    
    @objc func savetrimed(url : URL) {
     
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL:url)
        }) { saved, error in
            if saved {
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                    self.savefinalvideo(url: url)
                })
            }else{
                print(error?.localizedDescription ?? "")
                self.App.HideProgress()
                
                let alert = UIAlertController(
                                  title: "Low Storage Warning",
                                  message: "You may experience isssue due to low storage. Consider removing some videos, photos, songs, or apps from your device before proceeding.",
                                  preferredStyle: .alert)


                           let yesButton = UIAlertAction(
                                  title: "OK",
                                  style: .default,
                                  handler: { action in
                                      self.navigationController?.popViewController(animated: true)

                                  })
              

                             alert.addAction(yesButton)
                           self.present(alert, animated: true)
            }
        }
     }
    
    
    
    @objc func savefinalvideo(url : URL){
      
        let fetchOptions = PHFetchOptions()
               fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
            let assets = PHAsset.fetchAssets(with: PHAssetMediaType.video, options: fetchOptions)
           let lastPHAsset =  assets.lastObject
         if lastPHAsset != nil {
           self.video.insert(lastPHAsset!, at: 0)
           self.App.video.insert(lastPHAsset!, at: 0)
          self.App.all.insert(lastPHAsset!, at: 0)
         self.currentindex = 0
        var  dict : [String:Any] = [:]
        if (UserDefaults.standard.value(forKey: "renamedict") != nil){
        dict = UserDefaults.standard.value(forKey:"renamedict") as! [String : Any]
        print(dict)
            DispatchQueue.main.async {
        dict[lastPHAsset!.localIdentifier] = self.lblheader.text
            }
                       }else{
            DispatchQueue.main.async {
                       self.lblheader.text = self.lblheader.text
            }
                   dict[lastPHAsset!.localIdentifier] = self.lblheader.text
                               }
               print(dict)
               UserDefaults.standard.set(dict, forKey:"renamedict")
               UserDefaults.standard.synchronize()
        print(self.video)
        DispatchQueue.main.async {
        self.viewsavecancel.isHidden = true
        self.trimmerView1.isHidden = true
        }
        self.App.HideProgress()
        self.Myasset = AVAsset(url: url)
        DispatchQueue.main.async {
        self.playmovieatpath(self.Myasset)
        }
        }
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    
    func handleCancel(alertView: UIAlertAction!)
    {
     
    }
    
    @IBAction func btncancel(_ sender: Any) {
        controller?.showsPlaybackControls = true

        viewsavecancel.isHidden = true
        viewedit.isHidden = false
        trimmerView1.isHidden = true
        self.playmovieatpath(self.Myasset)

    }
    
    @IBAction func btnsave(_ sender: Any) {
        
        player?.pause()
        
      let appearance = SCLAlertView.SCLAppearance(
                       kTextFieldHeight: 50,
                       showCloseButton: false,
                       dynamicAnimatorActive: true,
                       buttonsLayout: .horizontal
                       
                     )
        
        let alert = SCLAlertView(appearance: appearance)
                    let txt = alert.addTextField("Video Name")
              
                    _ = alert.addButton("Cancel") {

                           }
              
                    _ = alert.addButton("Save") {
                        print("Text value: \(txt.text ?? "NA")")
                        self.lblheader.text = txt.text ?? ""
                            self.savevideo(name: txt.text ?? "")

                    }
                    _ = alert.showEdit("Video name", subTitle:"Save video as:")
        
           
        
    }
    
    @IBAction func btnplaytrimmed(_ sender: Any) {
        
        if (player?.isPlaying)!{
            player?.pause()
            btnplay1.setImage(UIImage.init(named: "playbutton.png"), for: .normal)

        }else{
            player?.play()
            btnplay1.setImage(UIImage.init(named: "pausebutton.png"), for: .normal)
            self.startPlaybackTimeChecker()
        }
        
        
    }
   
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if !IS_IPHONE{
            self.perform(#selector(resetplayer), with: nil, afterDelay: 0.1)
        }
    }
    
    
    @objc func resetplayer(){
        self.playmovieatpath(self.Myasset)
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        player?.pause()
    }
    
    func playmovieatpath(_ MoviePath: AVAsset?) {
       /*
        if let player = GPVideoPlayer.initialize(with: self.playerview.bounds) {
                  player.isToShowPlaybackControls = true
                  
                  self.playerview.addSubview(player)
                  if let urlAsset = MoviePath as? AVURLAsset {
                  let localVideoUrl: URL = urlAsset.url as URL
                  player.loadVideo(with: localVideoUrl)
                  player.isToShowPlaybackControls = true
                  player.isMuted = true
                  player.playVideo()
                  }
              }
 
 */
        
        
        
        if player == nil {
        }else{
            player!.pause()
            player = nil
            controller!.removeFromParentViewController()
            controller!.view.removeFromSuperview()

        }
        
        if MoviePath == nil {
            return
        }
        let playerItem = AVPlayerItem(asset: MoviePath!)
        if MoviePath != nil {
         player = AVPlayer(playerItem: playerItem)
        }
        
        let curTime = player?.currentTime()
        let seconds = CMTimeGetSeconds(curTime!)
        
        trimmerView1.asset = MoviePath
           trimmerView1.themeColor = UIColor.lightGray
           trimmerView1.asset = MoviePath
           trimmerView1.showsRulerView = false
            trimmerView1.maxLength = 2000
           trimmerView1.rulerLabelInterval = 10
           trimmerView1.trackerColor = UIColor.cyan
           trimmerView1.delegate = self
           // important: reset subviews
           trimmerView1.resetSubviews()
           trimmerView1.showsRulerView = false
           trimmerView1.hideTracker(false)
//          self.startTime = 0
//        self.stopTime = CGFloat(seconds)
        self.videoPlaybackPosition = 0
          playerview.backgroundColor = UIColor.clear
        controller = AVPlayerViewController()
        
        
        
        let width = UIScreen.main.bounds.size.width
        if IS_IPHONE{
            controller?.view.frame = CGRect(x: playerview.frame.origin.x, y: playerview.frame.origin.y, width: width, height: width * 9.0 / 16.0)
            
            playnew.frame = CGRect(x: playerview.frame.origin.x, y: playerview.frame.origin.y, width: width, height: width * 9.0 / 16.0)

        }else{
            controller?.view.frame = CGRect(x: playerview.frame.origin.x, y: playerview.frame.origin.y, width: width, height: 287)
            
            playnew.frame = CGRect(x: playerview.frame.origin.x, y: playerview.frame.origin.y, width: width, height: 287)

        }
        
        addChildViewController(controller!)
               view.addSubview(controller!.view)
        playnew.isHidden = false
        playnew.setImage(UIImage.init(named: "play"), for: .normal)
        playnew.backgroundColor = .clear
        playnew.addTarget(self, action: #selector(playnewmethod), for: .touchUpInside)
        playnew.tintColor = UIColor.clear
        self.view.addSubview(playnew)
        self.view.bringSubview(toFront: playnew)
        controller!.player = player
             do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryMultiRoute)




               }
             catch {
        }
        
        player!.pause()

        NotificationCenter.default.addObserver(self, selector: #selector(MovieplayerVC.itemDidFinishPlaying(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)

       
        
    }
    
    
    @objc func playnewmethod(sender : UIButton){
        playnew.isHidden = true
        player?.play()
     
    }

@objc func itemDidFinishPlaying(_ notification: Notification) {
     let startTimer = CMTimeMake(Int64(startTime), 1)
        player?.seek(to: startTimer)
//        playnew.isHidden = false
//       self.view.bringSubview(toFront: playnew)
}

    
}

    

