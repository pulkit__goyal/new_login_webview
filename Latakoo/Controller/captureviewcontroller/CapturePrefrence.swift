//
//  CapturePrefrence.swift
//  Latakoo
//
//  Created by Shruti Aggarwal on 21/08/19.
//  Copyright © 2019 parangat technology. All rights reserved.
//

import UIKit

class CapturePrefrence: UIViewController {
    @IBOutlet weak var switchbtn: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
     let value = UserDefaults.standard.value(forKey: "capturetype") as! String
        
        
        if value == "0" {
            switchbtn.setOn(false, animated: true)
        } else {
            switchbtn.setOn(true, animated: true)
        }

        // Do any additional setup after loading the view.
    }
    

    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func capturebutton(_ sender: Any) {
        if switchbtn.isOn {
            UserDefaults.standard.set("1", forKey: "capturetype")
        } else {
            UserDefaults.standard.set("0", forKey: "capturetype")
        }
        
        UserDefaults.standard.synchronize()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override var prefersStatusBarHidden: Bool {
        return false
    }

}
