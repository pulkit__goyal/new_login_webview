
import UIKit
import Photos
import CoreServices
import AVFoundation
import AVKit
import SoundWave

class QualitySettingsListCell: UITableViewCell {
    @IBOutlet weak var qualityTitleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

class CaptureDeviceDiscovery {
@available(iOS 10.0, *)
func availableDeviceTypes() -> [AVCaptureDevice.DeviceType] {
    var deviceTypes: [AVCaptureDevice.DeviceType] = [.builtInTelephotoCamera, .builtInWideAngleCamera]
        deviceTypes.append(.builtInDualCamera)
        if #available(iOS 11.1, *) {
            deviceTypes.append(.builtInTrueDepthCamera)
        }
    
    return deviceTypes
}

func videoCaptureDevice(at position: AVCaptureDevice.Position) -> AVCaptureDevice? {
    var device: AVCaptureDevice? = nil
    if #available(iOS 10, *) {
        let deviceTypes = availableDeviceTypes()
        let session = AVCaptureDevice.DiscoverySession(deviceTypes: deviceTypes, mediaType: .video, position: position)
        device = session.devices.first
    } else {
        device = AVCaptureDevice.devices(for: AVMediaType.video).filter( { $0.position == position }).first
    }
    return device
}
}
let IS_IPHONE = UIDevice.current.userInterfaceIdiom == .phone
let UIAppDelegate = UIApplication.shared.delegate as? AppDelegate
private var audioSession:AVAudioSession = AVAudioSession.sharedInstance()

let documentDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
enum encodingTypes : Int {
    case enc_AAC = 1
    case enc_ALAC = 2
    case enc_IMA4 = 3
    case enc_ILBC = 4
    case enc_ULAW = 5
    case enc_PCM = 6
}

enum MEDIA_TYPE : Int {
    case _VIDEO = 0
    case _PHOTO = 1
    case _AUDIO = 2
}

    
@discardableResult
   func isAuth() -> Bool {
       
       var result:Bool = false
       
       AVAudioSession.sharedInstance().requestRecordPermission { (res) in
           result = res == true ? true : false
       }
       
       return result
   }

//class captureVC: UIViewController{
class captureVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate, AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    @IBOutlet weak var imagewave: UIImageView!
    @IBOutlet weak var imagewave1: UIImageView!
    @IBOutlet weak var btnsend: UIButton!
   @IBOutlet private var audioVisualizationView: AudioVisualizationView!
    @IBOutlet private var audioVisualizationView1: AudioVisualizationView!

   // var audioVisualizationView = AudioVisualizationView()

    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var rotateCameraView: UIView!
    @IBOutlet weak var rotatedcameraViewBack: UIImageView!
    var selectedMedia: Any?
    var selectedMedia1: Any?
    @IBOutlet weak var userThumbImage: UIImageView!
    @IBOutlet weak var captureBtn: UIButton!
    @IBOutlet weak var viewmeter: UIView!
    @IBOutlet weak var viewmeter1: UIView!

    var imageFilePath = ""
    var pathString = ""
    var isfrontCameraOpen = false
   
    var iscrossbuttontaped = false
    var xvalue : CGFloat = 0

    var orientationLast: UIInterfaceOrientation!
    var orientationAfterProcess: UIInterfaceOrientation!
    var pickedImage: UIImage?
    var pickedVideoURL: URL?
    var isMovieRecording = false
    var recordEncoding = 0
    var hasRecording = false
    var isRecording = false
    var isPlaying = false
    var isvideosave = false
    var rotateCameraViewXpos: CGFloat = 0.0
    var rotateCameraViewYpos: CGFloat = 0.0
    var saveName = ""
    var updateTimer: Timer?
    var recordingTime: CGFloat = 0.0
    var videoTimer: Timer?
    var RecordTimer: Timer?
    var laveltimer: Timer?

    var bgTask: BackgroundTask?


    var mediaModeFramePosition: CGFloat = 0.0
    var mediaType: MEDIA_TYPE?
    var selectedMediaColor: UIColor?
    var orientation = ""
    var App: AppDelegate?
    var isItIpad = false
    var recordingOn = false
    var swipeCounter = 0

    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer!
    var fileName : String = "audio.m4a"

    @IBOutlet weak var photoModeLabel: UILabel!
    @IBOutlet weak var videoModeLabel: UILabel!
    @IBOutlet weak var audioModeLabel: UILabel!
    @IBOutlet weak var changeMediaMode: UIView!
    @IBOutlet weak var recordingIndicator: UIImageView!
    @IBOutlet weak var switchCameraButton: UIButton!
    @IBOutlet weak var recordindTime: UILabel!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var flashImageView: UIImageView!
    @IBOutlet weak var flashImgV: UIImageView!
    @IBOutlet weak var switchCameraBtnV: UIButton!
    @IBOutlet weak var swipeView: UIView!


    @IBOutlet weak var QualitySettingsView: UIView!
    @IBOutlet var qualityLabel: UILabel!
   let cameraManager = CameraManager()
    @IBOutlet var qualitySettingsTable: UITableView!
    var QualitySettingsList : [String] = []
    var framerate : Int32 = 30
    var fps = ""
    var resolution = ""

    deinit {
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        }
   

    override func viewDidLoad() {
        super.viewDidLoad()
        isItIpad = UI_USER_INTERFACE_IDIOM() == .pad
        
        if !IS_IPHONE {
       viewmeter1.isHidden = true
             }

        NotificationCenter.default.addObserver(self, selector: #selector(self.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)

        
        if isItIpad {
            viewmeter1.transform = CGAffineTransform(rotationAngle: 3.14 / 2)

        }

        let frame = changeMediaMode.frame
        xvalue = frame.origin.x
       // isAuth()
        modalPresentationCapturesStatusBarAppearance = true


        self.tabBarController?.tabBar.isHidden = true

        let status = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            // Access has been granted.
        } else if status == .denied {
            // Access has been denied.
        } else if status == .notDetermined {

            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ status in

                if status == .authorized {
                    // Access has been granted.
                } else {
                    // Access has been denied.
                }
            })
        } else if status == .restricted {
            // Restricted access - normally won't happen.
        }

        recordingOn = false

        orientation = "Portrait"


        selectedMediaColor = videoModeLabel.textColor
        recordEncoding = 1
        navigationController?.isNavigationBarHidden = true
        print("\(NSStringFromCGRect(rotateCameraView.frame))")
        rotateCameraViewXpos = 0
        rotateCameraViewYpos = 0
        videoModeLabel.autoresizingMask = .flexibleHeight
        if IS_IPHONE {
            flashButton.transform = CGAffineTransform(rotationAngle: 3.14 / 2)
            flashImageView.transform = CGAffineTransform(rotationAngle: 3.14 / 2)
            recordindTime.transform = CGAffineTransform(rotationAngle: 3.14 / 2)
            recordingIndicator.transform = CGAffineTransform(rotationAngle: 3.14 / 2)
            switchCameraButton.transform = CGAffineTransform(rotationAngle: 3.14 / 2)
        }

        mediaType = MEDIA_TYPE(rawValue: 0)







        qualityLabel.isUserInteractionEnabled = true
               let qualityGesture = UITapGestureRecognizer(target: self, action: #selector(changeCameraQuality))
               qualityLabel.addGestureRecognizer(qualityGesture)



        cameraManager.askUserForCameraPermission { permissionGranted in

            if permissionGranted {
               self.setupCameraManager()
              self.addCameraToView()
            }
        }




       self.setupresolution(position: .front)




    }
    private func recordSetup() {
            let settings = [AVFormatIDKey: Int(kAudioFormatMPEG4AAC), AVSampleRateKey: 44100, AVNumberOfChannelsKey: 2, AVLinearPCMBitDepthKey: 16]

            let filemanager = FileManager.default
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString

            let destinationPath = documentsPath.appendingPathComponent("audio.m4a")

            print(destinationPath)
            if filemanager.fileExists(atPath: destinationPath) {
                try! filemanager.removeItem(atPath: destinationPath)
            }

            let audioFileName = getDocumentsDirector().appendingPathComponent("audio.m4a")
            print(audioFileName)

            do {
                if audioRecorder != nil {
                    audioRecorder = nil
                }

                audioRecorder = try AVAudioRecorder(url: audioFileName, settings: settings)
                audioRecorder.delegate = self as AVAudioRecorderDelegate
                audioRecorder.isMeteringEnabled = true
                audioRecorder.prepareToRecord()

                if audioRecorder.prepareToRecord() == true {
                    audioSession = AVAudioSession.sharedInstance()
    //                try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord, with: [.mixWithOthers, .duckOthers, .allowBluetooth, .defaultToSpeaker, .interruptSpokenAudioAndMixWithOthers, .allowBluetoothA2DP, .allowAirPlay])
                    // try audioSession.overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
                    
    //                try audioSession.setCategory(AVAudioSessionCategoryMultiRoute, mode: AVAudioSessionModeDefault)
                    
             //       try audioSession.setCategory(AVAudioSessionCategorySoloAmbient)
                    
                   try audioSession.setCategory(AVAudioSessionCategoryMultiRoute, with: AVAudioSessionCategoryOptions.interruptSpokenAudioAndMixWithOthers)
                    
         //          try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord, with: AVAudioSessionCategoryOptions.mixWithOthers)

                    
                  //
                    
                    do {
                        try AVAudioSession.sharedInstance().setActive(false)
                                     
                        } catch {
               print("stop all audio seesion => ",error.localizedDescription)
                        }
                    
                    
                    do {
                    
                        try audioSession.setActive(true)
                   
                    } catch {
                print("start recording seesion => ",error.localizedDescription)
                    
                    }

                    
                    
                    if !isRecording {
                        sleep(UInt32(0.2))
                        self.RecordTimer?.invalidate()
                        self.RecordTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(recordingTimeUpdating1), userInfo: nil, repeats: true)
                        
                        self.laveltimer?.invalidate()
                        self.laveltimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(recordinglavelupdate), userInfo: nil, repeats: true)
                        
                        audioRecorder.record()
                        isRecording = true
                    }
                } else {
   
              
                }

            } catch {
                print("Recording update error:", error.localizedDescription)
            }
        }
    
    

    
    

    
    
     func record() {
                
       

                 recordSetup()


             }



    func stop() {
         
        
        let isPlayingWithOthers = AVAudioSession.sharedInstance().isOtherAudioPlaying
        
        print("isPlayingWithOthers => \(isPlayingWithOthers)")
        
        if isRecording{
        isRecording = false
        recordingOn = false
        audioRecorder.stop()
        audioRecorder = nil
        audioVisualizationView.reset()
            if isItIpad{
        audioVisualizationView1.reset()
            }
            
       imagewave.isHidden = false
            if isItIpad{

        imagewave1.isHidden = false

            }
       
            

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        do {
            try audioSession.setActive(false)
        } catch {
            print("stop()",error.localizedDescription)
        }
            }
    }
    }









     func checkFPS() {
            let videoCaptureDevice = AVCaptureDevice.default(for: .video)
            var maxFrameRate: Float = 0.0
            var maxIndex = 0
            for i in 0..<videoCaptureDevice!.formats.count {
                let format = videoCaptureDevice!.formats[i] as? AVCaptureDevice.Format
                let frameRateRange = format?.videoSupportedFrameRateRanges.first
                //You can add hard coded checking for specific format like if(frameRateRange.maxFrameRate == 240) etc I am using the max one


                if ((format?.description.contains(fps))! && (format?.description.contains(resolution))!){
                    maxIndex = i
                    break
                }else{
                    if maxFrameRate < Float(frameRateRange?.maxFrameRate ?? 0.0) {
                    maxFrameRate = Float(frameRateRange?.maxFrameRate ?? 0.0)
                        maxIndex = i
                    }

                }

            }

            let format = videoCaptureDevice!.formats[maxIndex] as? AVCaptureDevice.Format
        _ = format?.videoSupportedFrameRateRanges.first
            do {
                try videoCaptureDevice!.lockForConfiguration()
            } catch {
            }
            if let format = format {
                videoCaptureDevice!.activeFormat = format
            }


      //  videoCaptureDevice!.activeVideoMinFrameDuration = CMTimeMake(1,1)
        videoCaptureDevice!.activeVideoMaxFrameDuration = CMTimeMake(1, framerate)
        videoCaptureDevice!.focusMode = .autoFocus
        videoCaptureDevice!.unlockForConfiguration()
        }
    
    @IBAction func changeCameraQuality() {
        KGModal.sharedInstance()?.show(withContentView: QualitySettingsView)
        KGModal.sharedInstance()?.tapOutsideToDismiss = false
    }

        func getDocumentsDirector() -> URL {
            // Grab CWD and set to paths
    //        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    //        return paths[0]
            
            let documentsURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            
              return documentsURL
        }



    @IBAction func nextbutton(_ sender: Any) {

        if mediaType?.rawValue == 0 {

            self.getvideo()

        }else if mediaType?.rawValue == 1 {

        self.getimages()

        }
    }










    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {

        if iscrossbuttontaped == false{
        print(flag)
        sleep(1)
        let audioFileName = getDocumentsDirector().appendingPathComponent(fileName)
        print(audioFileName)

        var story : UIStoryboard?

              if IS_IPHONE {
                   story = UIStoryboard.init(name: "Main", bundle: nil)
                  }else{
                   story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
              }
        let LibraryAudioVC = story?.instantiateViewController(withIdentifier: "LibraryAudioVC") as! LibraryAudioVC
        LibraryAudioVC.Audiopath = audioFileName
        LibraryAudioVC.hidesBottomBarWhenPushed = false
        self.navigationController?.pushViewController(LibraryAudioVC, animated: false)

        }else{
            iscrossbuttontaped = false
        }
    }


    func getimages(){
        var image : [Any] = []

        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)

        assets.enumerateObjects { (obj, idx, bool) -> Void in
            image.insert(obj, at: 0)
        }
        print(image)

        var story : UIStoryboard?

              if IS_IPHONE {
                   story = UIStoryboard.init(name: "Main", bundle: nil)
                  }else{
                   story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
              }
        let LibraryImagedetailsVC = story?.instantiateViewController(withIdentifier: "LibraryImagedetailsVC") as! LibraryImagedetailsVC
        LibraryImagedetailsVC.image = image as! [PHAsset]
        LibraryImagedetailsVC.currentindex = 0
        self.navigationController?.pushViewController(LibraryImagedetailsVC, animated: true)

    }
    func getvideo(){
        var video : [Any] = []
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.video, options: fetchOptions)

        assets.enumerateObjects { (obj, idx, bool) -> Void in
           // video.append(obj)
            video.insert(obj, at: 0)
        }
        print(video)

        var story : UIStoryboard?

              if IS_IPHONE {
                   story = UIStoryboard.init(name: "Main", bundle: nil)
                  }else{
                   story = UIStoryboard.init(name: "MainIPAD", bundle: nil)
              }
        let MovieplayerVC = story?.instantiateViewController(withIdentifier: "MovieplayerVC") as! MovieplayerVC
        MovieplayerVC.video = video as! [PHAsset]
        MovieplayerVC.currentindex = 0
        self.navigationController?.pushViewController(MovieplayerVC, animated: true)


    }









    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillResignActive, object: nil)

        if mediaType?.rawValue == 0{
              if isMovieRecording {
                captureBtn.setImage(UIImage.init(named:"stop_btn"), for:.normal)
                self.swipeView.isUserInteractionEnabled = true
                cameraManager.stopVideoRecording { (videoURL: URL?, error) -> Void in
                }
                isMovieRecording = false
                recordingTime  = 0.0
               videoTimer?.invalidate()
                


            }
            
        }
        
        
        if mediaType?.rawValue == 1 || mediaType?.rawValue == 0{
            cameraManager.stopCaptureSessionNew()

        }
        
           
            
        
        
    }


    @IBAction func crossButtonAction(_ sender: Any) {
     
        if mediaType?.rawValue == 0{
              if isMovieRecording {
                
                
                    
                    let appearance = SCLAlertView.SCLAppearance(
                                                     kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                                                     kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                                                     kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                                                     showCloseButton: false,
                                                     dynamicAnimatorActive: true,
                                                     buttonsLayout: .horizontal
                                                 )
                                                 let alert = SCLAlertView(appearance: appearance)
                                                 _ = alert.addButton("NO"){
                                                 }
                                                 _ = alert.addButton("YES")
                                                 {
                                                  
                                                    self.captureBtn.setImage(UIImage.init(named:"stop_btn"), for:.normal)
                                                              self.swipeView.isUserInteractionEnabled = true
                                                    self.cameraManager.stopVideoRecording { (videoURL: URL?, error) -> Void in
                                                                  }
                                                    self.isMovieRecording = false
                                                    self.recordingTime  = 0.0
                                                    self.videoTimer?.invalidate()
                                                    self.tabBarController?.selectedIndex = 1
                                                           self.tabBarController?.hidesBottomBarWhenPushed = false
                            
                              
                                             }
                                                 
                                                 let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
                                                 
                                                 _ = alert.showCustom("Alert", subTitle: "Are you sure you want to cancel the Video recording?", color: color, circleIconImage:nil)
                    
                   
                
                
                
                
               
              }else{
                self.tabBarController?.selectedIndex = 1
                self.tabBarController?.hidesBottomBarWhenPushed = false
                
            }
            
        }else if mediaType?.rawValue == 2{
            if isRecording {
                
                let appearance = SCLAlertView.SCLAppearance(
                                                 kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                                                 kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                                                 kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                                                 showCloseButton: false,
                                                 dynamicAnimatorActive: true,
                                                 buttonsLayout: .horizontal
                                             )
                                             let alert = SCLAlertView(appearance: appearance)
                                             _ = alert.addButton("NO"){
                                             }
                                             _ = alert.addButton("YES")
                                             {
                                              
                                                self.iscrossbuttontaped = true
                                                self.captureBtn.setImage(UIImage.init(named:"stop_btn"), for:.normal)
                                               self.swipeView.isUserInteractionEnabled = true
                                               self.recordingTime  = 0.0
                                              self.RecordTimer?.invalidate()
                                               self.laveltimer?.invalidate()
                                               self.stop()
                                    self.tabBarController?.selectedIndex = 1
                                self.tabBarController?.hidesBottomBarWhenPushed = false
                        
                          
                                         }
                                             
                                             let color = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
                                             
                                             _ = alert.showCustom("Alert", subTitle: "Are you sure you want to cancel the Audio recording?", color: color, circleIconImage:nil)
                
               
            }else{
                self.tabBarController?.selectedIndex = 1
                self.tabBarController?.hidesBottomBarWhenPushed = false
            }
        }else{
            self.tabBarController?.selectedIndex = 1
            self.tabBarController?.hidesBottomBarWhenPushed = false
        }

       
    }


    @IBAction func switchCameraAction(_ sender: Any) {


        if cameraManager.cameraDevice == CameraDevice.front {
            isfrontCameraOpen = false
            cameraManager.cameraDevice = .back
            self.setupresolution(position: .back)
        } else {
            isfrontCameraOpen = true
            cameraManager.cameraDevice = .front
            self.setupresolution(position: .front)
        }


    }



    @IBAction func captureMedia(_ sender: UIButton) {

        if mediaType?.rawValue == 1{

            print(mediaType?.rawValue)

            if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                 print("Authorised")
                self.btnsend.isUserInteractionEnabled = false

                self.takephoto()
            }

          else {

                let alert = UIAlertController(title: "Attention", message:"Please give this app permission to access your camera  in your settings app!", preferredStyle: UIAlertController.Style.alert)

                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))

                self.present(alert, animated: true, completion: nil)



            }



        }else if mediaType?.rawValue == 0{

            print(mediaType?.rawValue)


                if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {

            if UserDefaults.standard.value(forKey: "capturetype") as! String == "0"{

                    print("Authorised")


                    if isMovieRecording {
                        sender.setImage(UIImage.init(named:"stop_btn"), for:.normal)
                        self.swipeView.isUserInteractionEnabled = true
                        self.stopVideoCapture()
                        isMovieRecording = false
                        recordingTime  = 0.0
                        videoTimer?.invalidate()



                    }else{



                        sender.setImage(UIImage.init(named:"btn"), for:.normal)
                        self.swipeView.isUserInteractionEnabled = false
                        isMovieRecording = true
                        recordingTime  = 0.0
                        if framerate>=30 {
                            self.checkFPS()
                        }
                        self.btnsend.isUserInteractionEnabled = false

                        self.startVideoCapture()
                        UIApplication.shared.isIdleTimerDisabled = true
                        videoTimer?.invalidate()
                        videoTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(recordingTimeUpdating), userInfo: nil, repeats: true)
                    }



            }else{




                let deviceorientation = UIDevice.current.orientation
                if deviceorientation == .landscapeLeft {







                    print("Authorised")


                    if isMovieRecording {
                        sender.setImage(UIImage.init(named:"stop_btn"), for:.normal)
                        self.swipeView.isUserInteractionEnabled = true
                        self.stopVideoCapture()
                        recordingTime  = 0.0
                        videoTimer?.invalidate()

                        isMovieRecording = false

                    }else{

                        sender.setImage(UIImage.init(named:"btn"), for:.normal)
                        self.swipeView.isUserInteractionEnabled = false
                        isMovieRecording = true
                        recordingTime  = 0.0
                        if framerate>=30 {
                            self.checkFPS()
                        }

                        self.btnsend.isUserInteractionEnabled = false

                        self.startVideoCapture()

                        videoTimer?.invalidate()
                        videoTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(recordingTimeUpdating), userInfo: nil, repeats: true)
                    }
                }
                else{


                    let alert = UIAlertController(title: "Warning", message:"To record video, please change your camera rotation. Make sure your record button is on your right.", preferredStyle: UIAlertController.Style.alert)

                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))

                    self.present(alert, animated: true, completion: nil)


                }

            }

            }



            else {

                let alert = UIAlertController(title: "Attention", message:"Please give this app permission to access your camera  in your settings app!", preferredStyle: UIAlertController.Style.alert)

                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))


                self.present(alert, animated: true, completion: nil)



            }



        }
        else{


            
            
            
            if isRecording {
                sender.setImage(UIImage.init(named:"stop_btn"), for:.normal)
                self.swipeView.isUserInteractionEnabled = true
                self.recordingTime  = 0.0
                self.RecordTimer?.invalidate()
                self.laveltimer?.invalidate()

                
                self.stop()

            }else{

                if isAuth() == false {
               
                    let alert = UIAlertController(title: "Error", message: "Please allow microphone usage from settings", preferredStyle: .alert)
                        
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    present(alert, animated: true, completion: nil)
                    
                }else{
                sender.setImage(UIImage.init(named:"btn"), for:.normal)
                self.swipeView.isUserInteractionEnabled = false
                self.recordingTime  = 0.0
                self.record()
                }


            }

        }


    }





    @objc func getvideonew(){
    let fetchOptions = PHFetchOptions()
    fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
    let assets = PHAsset.fetchAssets(with: PHAssetMediaType.video, options: fetchOptions)
    let lastPHAsset = assets.lastObject
        App?.all.insert(lastPHAsset!, at: 0)
        App?.video.insert(lastPHAsset!, at: 0)
        self.btnsend.isUserInteractionEnabled = true

    }

    @objc func getimagenew(){
    let fetchOptions = PHFetchOptions()
    fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
    let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
    let lastPHAsset = assets.lastObject
     App?.all.insert(lastPHAsset!, at: 0)
      App?.image.insert(lastPHAsset!, at: 0)
    self.btnsend.isUserInteractionEnabled = true

    }



    @objc func recordingTimeUpdating() {
        var seconds = recordingTime
        let hour = seconds / 3600
        seconds = CGFloat(Int(seconds) % 3600)
        let min = seconds / 60
        seconds = CGFloat(Int(seconds) % 60)
        recordindTime.text = String(format: "%02d:%02d:%02d", Int(hour), Int(min), Int(seconds))
        recordingTime += 1
        
       
    }

    
    @objc func recordinglavelupdate() {

        if isRecording{
               imagewave.isHidden = true
            if isItIpad{

             imagewave1.isHidden = true
            }
             self.audioRecorder!.updateMeters()
              let averagePower = audioRecorder!.averagePower(forChannel: 0)
              let percentage: Float = pow(10, (0.05 * averagePower))
            
               audioVisualizationView.add(meteringLevel: percentage)
            if isItIpad{
            audioVisualizationView1.add(meteringLevel: percentage)
                }

              }
        
        
    }

    @objc func recordingTimeUpdating1() {
        var seconds = recordingTime
        let hour = seconds / 3600
        seconds = CGFloat(Int(seconds) % 3600)
        let min = seconds / 60
        seconds = CGFloat(Int(seconds) % 60)
        recordindTime.text = String(format: "%02d:%02d:%02d", Int(hour), Int(min), Int(seconds))
        recordingTime += 1
      
    }


    @IBAction func changeFlashTypeAction(_ sender: UIButton) {


        if sender.currentImage?.isEqual(UIImage(named: "FlashOff.png")) ?? false {
            cameraManager.flashMode = .on
            sender.setImage(UIImage(named: "FlashOn.png"), for: .normal)
        } else if sender.currentImage?.isEqual(UIImage(named: "FlashOn.png")) ?? false {

            cameraManager.flashMode = .auto
            sender.setImage(UIImage(named: "FlashAuto.png"), for: .normal)
        } else if sender.currentImage?.isEqual(UIImage(named: "FlashAuto.png")) ?? false {

            cameraManager.flashMode = .off
            sender.setImage(UIImage(named: "FlashOff.png"), for: .normal)
        }


    }

    @objc func closeActivityController()  {

        if mediaType?.rawValue == 0{
              if isMovieRecording {
                captureBtn.setImage(UIImage.init(named:"stop_btn"), for:.normal)
                self.swipeView.isUserInteractionEnabled = true
                cameraManager.stopVideoRecording { (videoURL: URL?, error) -> Void in
                }
                isMovieRecording = false
                recordingTime  = 0.0
               videoTimer?.invalidate()
              recordindTime.text = "00:00:00"



            }
            
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        imagewave.isHidden = false
        if isItIpad{
        imagewave1.isHidden = false
        }
        audioVisualizationView.meteringLevelBarWidth = 5.0
        audioVisualizationView.meteringLevelBarInterItem = 1.0
        audioVisualizationView.meteringLevelBarCornerRadius = 0.0
        audioVisualizationView.gradientStartColor = .white
        audioVisualizationView.gradientEndColor = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
        audioVisualizationView.audioVisualizationMode = .write
        
        if isItIpad{
        audioVisualizationView1.meteringLevelBarWidth = 5.0
        audioVisualizationView1.meteringLevelBarInterItem = 1.0
        audioVisualizationView1.meteringLevelBarCornerRadius = 0.0
        audioVisualizationView1.gradientStartColor = .white
        audioVisualizationView1.gradientEndColor = UIColor(red: 70.0 / 255.0, green: 16.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
        audioVisualizationView1.audioVisualizationMode = .write
        }
        
        
    let permission =    isAuth()

    NotificationCenter.default.addObserver(self, selector: #selector(self.closeActivityController), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)

        iscrossbuttontaped = false
        self.btnsend.isUserInteractionEnabled = false

         swipeCounter = 0

        UIApplication.shared.isStatusBarHidden = true

        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            App = delegate
        }

        App!.ifgotocapture = "YES"


        UserDefaults.standard.removeObject(forKey: "Wrequest")

        isfrontCameraOpen = false



            mediaType = MEDIA_TYPE(rawValue: 0)




        if !recordingOn {
            userThumbImage.image = nil

            if IS_IPHONE {
                var frame = changeMediaMode.frame
                frame.origin.x = 58.5

                if frame.origin.x < 50 {
                    frame.origin.x += 60
                }


                changeMediaMode.frame = frame

                audioModeLabel.textColor = UIColor.white
                videoModeLabel.textColor = UIColor.white
                photoModeLabel.textColor = UIColor.white



        if frame.origin.x <= -11.0 {
            photoModeLabel.textColor = selectedMediaColor
        } else {
            videoModeLabel.textColor = selectedMediaColor
        }

        if frame.origin.x <= -11.0 {
            mediaType = MEDIA_TYPE(rawValue: 1)
        }
        if frame.origin.x > 0 {
            mediaType = MEDIA_TYPE(rawValue: 0)
        }
                changeMediaTypeAction(mediaType!)
            }

            else{



                       var frame = changeMediaMode.frame
                   if self.view.frame.size.width > 1000.0 {
                        
                    frame.origin.x =  450

                   }else{
                    frame.origin.x =  335

                }
                        changeMediaMode.frame = frame

                               audioModeLabel.textColor = UIColor.white
                               videoModeLabel.textColor = self.selectedMediaColor
                               photoModeLabel.textColor = UIColor.white
                             changeMediaTypeAction(mediaType!)
                                   rotateCameraViewXpos = 0
                                   rotateCameraViewYpos = 0
                                tabBarController?.tabBar.isHidden = true
                                recordindTime.text = "00:00:00"
                                navigationController?.isNavigationBarHidden = true
                                isItIpad = UI_USER_INTERFACE_IDIOM() == .pad
               }
          }



        rotateCameraViewXpos = 0
        rotateCameraViewYpos = 0
        tabBarController?.tabBar.isHidden = true
       // tabBarController?.tabBar.backgroundColor = UIColor.black
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            if !(mediaType != nil) {
            }
        }
        recordindTime.text = "00:00:00"


        
        self.perform(#selector(self.changeorintation), with: nil, afterDelay: 0.2)
        UIAppDelegate?.restrictRotation = .portrait
        
        imagewave.image = UIImage.init(named:"placeaudio copy.png")
        if isItIpad{

        imagewave1.image = UIImage.init(named:"placeaudio copy.png")
        }
    }

    @objc func changeorintation(){

        UIDevice.current.setValue(
                   NSNumber(value: UIInterfaceOrientation.portrait.rawValue),
                   forKey: "orientation")
    }






     func changeMediaTypeAction(_ modeType: MEDIA_TYPE) {
        qualityLabel.isHidden = true

        
        if IS_IPHONE {
            recordindTime.transform = CGAffineTransform.identity
            recordindTime.frame = CGRect(x: 0, y: 22, width:self.view.frame.size.width, height: recordindTime.frame.size.height)
            recordingIndicator.transform = CGAffineTransform.identity
            rotateCameraView.transform = CGAffineTransform.identity
        }


        rotateCameraView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: rotateCameraView.frame.size.height)
        print("\(NSStringFromCGRect(rotateCameraView.frame))")
        rotateCameraView.isHidden = false
        flashButton.isHidden = true
        flashImageView.isHidden = true
        recordindTime.isHidden = false

        switchCameraButton.isHidden = true
        recordingIndicator.isHidden = true
        rotatedcameraViewBack.isHidden = true
        rotateCameraView.backgroundColor = UIColor.black

        if IS_IPHONE {
                 flashButton.transform = CGAffineTransform(rotationAngle: 3.14 / 2)
                   flashImageView.transform = CGAffineTransform(rotationAngle: 3.14 / 2)
                   flashImgV.transform = CGAffineTransform(rotationAngle: 3.14 / 2)
                   recordindTime.transform = CGAffineTransform(rotationAngle: 3.14 / 2)
                   recordingIndicator.transform = CGAffineTransform(rotationAngle: 3.14 / 2)
                   switchCameraButton.transform = CGAffineTransform(rotationAngle: 3.14 / 2)
                   switchCameraBtnV.transform = CGAffineTransform(rotationAngle: 3.14 / 2)
        }



        recordindTime.isHidden = false
        recordingTime = 0.0
         viewmeter.isHidden = true
         
         if isItIpad{

             viewmeter1.isHidden = true

         }
        switchCameraButton.isHidden = false
        recordingIndicator.isHidden = false
        rotateCameraView.isHidden = true
        flashButton.isHidden = false
        flashImageView.isHidden = true
        videoTimer?.invalidate()
        self.recordindTime.text = "00:00:00"
        self.userThumbImage.isHidden = false
        cameraView.isHidden = false
        if modeType.rawValue == 2 {
            cameraManager.stopCaptureSessionNew()

            self.userThumbImage.isHidden = true

            if IS_IPHONE {
        recordindTime.transform = CGAffineTransform.identity
                recordindTime.frame = CGRect(x: 0, y: 22, width:self.view.frame.size.width, height: recordindTime.frame.size.height)
                           print("\(NSStringFromCGRect(recordindTime.frame))")
        recordingIndicator.transform = CGAffineTransform.identity
            }



            rotateCameraView.isHidden = false
            flashButton.isHidden = true
            flashImageView.isHidden = true
            recordindTime.isHidden = false
            viewmeter.isHidden = false

            if isItIpad{

            viewmeter1.isHidden = false
            }
            
            switchCameraButton.isHidden = true
            recordingIndicator.isHidden = true
            rotatedcameraViewBack.isHidden = true
            rotateCameraView.backgroundColor = UIColor.black
            cameraView.isHidden = true
            
            if UIDevice.current.orientation.isLandscape {
                
                if !IS_IPHONE {
              viewmeter1.isHidden = false
               }
                
            } else {
                if !IS_IPHONE {
            viewmeter1.isHidden = true
                     }
            }

        }else{


            if cameraManager.captureSession?.isRunning == false{
                cameraManager.StartCaptureSessionNew()
            }


            
            self.userThumbImage.isHidden = false

            rotateCameraView.isHidden = false
            cameraManager.flashMode = .auto
            flashButton.setImage(UIImage(named: "FlashAuto.png"), for: .normal)


           if (modeType.rawValue == 0) {
               cameraManager.cameraOutputMode = .videoWithMic
                    qualityLabel.isHidden = false

                if !isItIpad {
                    rotateCameraView.transform = CGAffineTransform(rotationAngle: -3.14 / 2)
                    recordindTime.transform = CGAffineTransform.identity
                    recordingIndicator.transform = CGAffineTransform.identity
                    flashButton.transform = CGAffineTransform(rotationAngle: 3.14)
                    flashImageView.transform = CGAffineTransform.identity
                    recordindTime.transform = CGAffineTransform.identity
                    recordingIndicator.transform = CGAffineTransform.identity
                    switchCameraButton.transform = CGAffineTransform(rotationAngle: 3.14)
                    recordindTime.transform = CGAffineTransform(rotationAngle: 3.14)
                    rotateCameraView.frame = CGRect(x: rotateCameraView.frame.origin.x + 160, y: rotateCameraView.frame.origin.y + 225, width: rotateCameraView.frame.size.width, height: rotateCameraView.frame.size.height)

                    print("\(NSStringFromCGRect(recordindTime.frame))")

                    recordindTime.frame = CGRect(x: 0, y: 22, width: recordindTime.frame.size.width, height: recordindTime.frame.size.height)

                    recordingIndicator.isHidden = true
                print("\(NSStringFromCGRect(recordindTime.frame))")
                    rotatedcameraViewBack.isHidden = false
                rotateCameraView.backgroundColor = UIColor.clear

            }
           }else {

                    if (orientation == "Portrait") {

                        UIView.animate(withDuration: 0.3, animations: {
                                self.flashButton.transform = CGAffineTransform.identity
                                self.switchCameraButton.transform = CGAffineTransform.identity
                               // self.cameraView.transform = CGAffineTransform (rotationAngle: 3.14/2);
                            self.view.layoutIfNeeded()

                        })
                    }else{

                        UIView.animate(withDuration: 0.3, animations: {
                                self.flashButton.transform = CGAffineTransform(rotationAngle: 3.14 / 2)
                                self.switchCameraButton.transform = CGAffineTransform(rotationAngle: 3.14 / 2)


                        })


                    }


                    recordingIndicator.isHidden = true
                    recordindTime.isHidden = true
                    cameraManager.cameraOutputMode = .stillImage


                   if IS_IPHONE {
                rotateCameraView.transform = CGAffineTransform.identity
                                   recordindTime.frame = CGRect(x: 138, y: 18, width: recordindTime.frame.size.width, height: recordindTime.frame.size.height)
                                   rotateCameraView.frame = CGRect(x: rotateCameraViewXpos, y: rotateCameraViewXpos, width: rotateCameraView.frame.size.width, height: rotateCameraView.frame.size.height)
               }


                    rotatedcameraViewBack.isHidden = true
                    rotateCameraView.backgroundColor = UIColor.black

            }


      //  cameraView.addSubview(rotateview1)

    }
}

    @IBAction func swipeLeftAction(_ sender: Any) {

            swipeCounter = swipeCounter + 1
            print(swipeCounter)

            if IS_IPHONE {


            if swipeCounter == 3{
                swipeCounter = 2
            }else{

            }

            var frame = changeMediaMode.frame

            if(frame.origin.x > -70){
                frame.origin.x -= 70
            }


            UIView.animate(withDuration: 0.5, animations: {
                self.changeMediaMode.frame = frame

            }) { finished in
                self.audioModeLabel.textColor = UIColor.white
                self.videoModeLabel.textColor = UIColor.white
                self.photoModeLabel.textColor = UIColor.white

                if self.swipeCounter == 1{
                    self.photoModeLabel.textColor = self.selectedMediaColor
                }else{
                    self.audioModeLabel.textColor = self.selectedMediaColor
                }

                if self.swipeCounter == 1 {
                    self.mediaType = MEDIA_TYPE(rawValue: 1)

                    self.audioModeLabel.textColor = UIColor.white
                    self.videoModeLabel.textColor = UIColor.white
                    self.photoModeLabel.textColor = UIColor.white
                    self.photoModeLabel.textColor = self.selectedMediaColor
                    self.changeMediaTypeAction(self.mediaType!)

                }
                if self.swipeCounter == 2 {
                    self.audioModeLabel.textColor = UIColor.white
                    self.videoModeLabel.textColor = UIColor.white
                    self.photoModeLabel.textColor = UIColor.white
                    self.audioModeLabel.textColor = self.selectedMediaColor
                    self.mediaType = MEDIA_TYPE(rawValue: 2)
                    self.changeMediaTypeAction(self.mediaType!)

                }

            }
            }else{

             if swipeCounter == 3{
                 swipeCounter = 2
             }else{

             }

             var frame = changeMediaMode.frame

            

                
                if self.view.frame.size.width > 1000.0 {
                            if(frame.origin.x > 160){
                                frame.origin.x -= 150
                                        }

                                 }else{

                             if(frame.origin.x > 35){
                                frame.origin.x -= 150
                                    }
                
                }

             UIView.animate(withDuration: 0.5, animations: {
                 self.changeMediaMode.frame = frame

             }) { finished in
                 self.audioModeLabel.textColor = UIColor.white
                 self.videoModeLabel.textColor = UIColor.white
                 self.photoModeLabel.textColor = UIColor.white

                 if self.swipeCounter == 1{
                     self.photoModeLabel.textColor = self.selectedMediaColor
                 }else{
                     self.audioModeLabel.textColor = self.selectedMediaColor
                 }

                 if self.swipeCounter == 1 {
                     self.mediaType = MEDIA_TYPE(rawValue: 1)

                     self.audioModeLabel.textColor = UIColor.white
                     self.videoModeLabel.textColor = UIColor.white
                     self.photoModeLabel.textColor = UIColor.white
                     self.photoModeLabel.textColor = self.selectedMediaColor
                     self.changeMediaTypeAction(self.mediaType!)

                 }
                 if self.swipeCounter == 2 {
                     self.audioModeLabel.textColor = UIColor.white
                     self.videoModeLabel.textColor = UIColor.white
                     self.photoModeLabel.textColor = UIColor.white
                     self.audioModeLabel.textColor = self.selectedMediaColor
                     self.mediaType = MEDIA_TYPE(rawValue: 2)
                     self.changeMediaTypeAction(self.mediaType!)
                 }
             }
         }
    }



    @IBAction func swipeRightAction(_ sender: Any) {
         swipeCounter = swipeCounter-1


        if IS_IPHONE {


        if swipeCounter < 0{
            swipeCounter = 0
        }

        var frame = changeMediaMode.frame
        if(frame.origin.x < 55){
        frame.origin.x += 70;
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.changeMediaMode.frame = frame

        }) { finished in
            self.audioModeLabel.textColor = UIColor.white
            self.videoModeLabel.textColor = UIColor.white
            self.photoModeLabel.textColor = UIColor.white

            if self.swipeCounter == 1{
                self.photoModeLabel.textColor = self.selectedMediaColor
            }else{
                self.audioModeLabel.textColor = self.selectedMediaColor
            }

            if self.swipeCounter == 0 {
                self.mediaType = MEDIA_TYPE(rawValue: 0)

                self.audioModeLabel.textColor = UIColor.white
                self.videoModeLabel.textColor = UIColor.white
                self.photoModeLabel.textColor = UIColor.white
                self.videoModeLabel.textColor = self.selectedMediaColor
                self.changeMediaTypeAction(self.mediaType!)

            }
            if self.swipeCounter == 1 {
                self.audioModeLabel.textColor = UIColor.white
                self.videoModeLabel.textColor = UIColor.white
                self.photoModeLabel.textColor = UIColor.white
                self.photoModeLabel.textColor = self.selectedMediaColor
                self.mediaType = MEDIA_TYPE(rawValue: 1)
                self.changeMediaTypeAction(self.mediaType!)

            }

        }


        }else{


        if swipeCounter < 0{
            swipeCounter = 0
        }

        var frame = changeMediaMode.frame
            
            
       
            
            if self.view.frame.size.width > 1000.0 {
                        if(frame.origin.x < 310){
                            frame.origin.x += 150;
                                }

                             }else{

                          if(frame.origin.x < 190){
                                 frame.origin.x += 150;
                                 }
            
            }
            
        UIView.animate(withDuration: 0.5, animations: {
            self.changeMediaMode.frame = frame

        }) { finished in
            self.audioModeLabel.textColor = UIColor.white
            self.videoModeLabel.textColor = UIColor.white
            self.photoModeLabel.textColor = UIColor.white

            if self.swipeCounter == 1{
                self.photoModeLabel.textColor = self.selectedMediaColor
            }else{
                self.audioModeLabel.textColor = self.selectedMediaColor
            }

            if self.swipeCounter == 0 {
                self.mediaType = MEDIA_TYPE(rawValue: 0)

                self.audioModeLabel.textColor = UIColor.white
                self.videoModeLabel.textColor = UIColor.white
                self.photoModeLabel.textColor = UIColor.white
                self.videoModeLabel.textColor = self.selectedMediaColor
                self.changeMediaTypeAction(self.mediaType!)

            }
            if self.swipeCounter == 1 {
                self.audioModeLabel.textColor = UIColor.white
                self.videoModeLabel.textColor = UIColor.white
                self.photoModeLabel.textColor = UIColor.white
                self.photoModeLabel.textColor = self.selectedMediaColor
                self.mediaType = MEDIA_TYPE(rawValue: 1)
                self.changeMediaTypeAction(self.mediaType!)

            }

        }


        }

    }

////// new camera

    fileprivate func setupCameraManager() {


           cameraManager.shouldEnableExposure = true
           cameraManager.writeFilesToPhoneLibrary = false
           cameraManager.shouldFlipFrontCameraImage = false
           cameraManager.showAccessPermissionPopupAutomatically = false

       }


     
    
    
    

    fileprivate func addCameraToView() {
           cameraManager.addPreviewLayerToView(cameraView, newCameraOutputMode: CameraOutputMode.videoWithMic)
           cameraManager.showErrorBlock = { [weak self] (erTitle: String, erMessage: String) -> Void in

               let alertController = UIAlertController(title: erTitle, message: erMessage, preferredStyle: .alert)
               alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) -> Void in }))

               self?.present(alertController, animated: true, completion: nil)
           }
       }

    func setupresolution(position : AVCaptureDevice.Position){

        QualitySettingsList = []


        let tool = CaptureDeviceDiscovery()




    if ((tool.videoCaptureDevice(at: position)?.supportsSessionPreset(AVCaptureSession.Preset.hd1280x720))!){
        QualitySettingsList.append("720p HD at 30 fps")
        }



    if ((tool.videoCaptureDevice(at: position)?.supportsSessionPreset(AVCaptureSession.Preset.hd1920x1080))!){
        QualitySettingsList.append("1080 HD at 30 fps")
        QualitySettingsList.append("1080 HD at 60 fps")

        }








        if ((tool.videoCaptureDevice(at: position)?.supportsSessionPreset(AVCaptureSession.Preset.hd4K3840x2160))!) {

            QualitySettingsList.append("4K at 30 fps")
            QualitySettingsList.append("4K at 60 fps")


        }else{
            let modelName = UIDevice.modelName

         //   let modelName = UIDevice.modelName
               print(modelName)
                let device : [String] = ["iPhone 6s","iPhone 6s Plus","iPhone 7","iPhone 7 Plus","iPhone SE","iPhone 8","iPhone 8 Plus","iPhone X","iPhone XS","iPhone XS Max","iPhone XR","iPhone 11","iPhone 11 Pro","iPhone 11 Pro Max"]

            if device.contains(modelName){
                QualitySettingsList.append("4K at 30 fps")
                QualitySettingsList.append("4K at 60 fps")
            }
        }
        
        

        
        if ((tool.videoCaptureDevice(at: position)?.supportsSessionPreset(AVCaptureSession.Preset.iFrame960x540))!){
                  QualitySettingsList.append("iFrame 960x540")
                 }
        
        if ((tool.videoCaptureDevice(at: position)?.supportsSessionPreset(AVCaptureSession.Preset.vga640x480))!){
                       QualitySettingsList.append("vga 640x480")
                   }
        
        
        if ((tool.videoCaptureDevice(at: position)?.supportsSessionPreset(AVCaptureSession.Preset.cif352x288))!){
              QualitySettingsList.append("cif 352x288")
            }


           


        



      



        cameraManager.cameraOutputQuality = .hd1280x720

        if (UserDefaults.standard.value(forKey:"cameraquality") != nil){

            var str = UserDefaults.standard.value(forKey:"cameraquality") as! String
            qualityLabel.text = str
           
            if str == "High" {
              str = "720p HD at 30 fps"
                cameraManager.cameraOutputQuality = .hd1280x720
                framerate = 30
                fps = "30 fps"
                resolution = "1280x"
            }
          
         if str == "cif 352x288" {
        cameraManager.cameraOutputQuality = .cif352x288
        framerate = 25
        fps = ""
        resolution = ""
        } else if str == "vga 640x480" {
        cameraManager.cameraOutputQuality = .vga640x480
        framerate = 25
        fps = ""
        resolution = ""
        }
        else if str == "iFrame 960x540" {
        cameraManager.cameraOutputQuality = .iFrame960x540
        framerate = 25
        fps = ""
        resolution = ""
        }

        else if str == "720p HD at 30 fps" {
        cameraManager.cameraOutputQuality = .hd1280x720
        framerate = 30
        fps = "30 fps"
        resolution = "1280x"
        }

        else if str == "1080 HD at 30 fps" {
        cameraManager.cameraOutputQuality = .hd1920x1080
        framerate = 30
        fps = "30 fps"
        resolution = "1920x1080"
        }
        else if str == "1080 HD at 60 fps" {
        cameraManager.cameraOutputQuality = .hd1920x1080
        framerate = 60
        fps = "60 fps"
        resolution = "1920x1080"
        }
       else if str  == "4K at 30 fps" {
       cameraManager.cameraOutputQuality = .hd4K3840x2160
        framerate = 30
        fps = "30 fps"
        resolution = "3840x2160"
        }
        else if str == "4K at 60 fps" {
        cameraManager.cameraOutputQuality = .hd4K3840x2160
        framerate = 60
        fps = "60 fps"
        resolution = "3840x2160"
        }
            
        }else{
        UserDefaults.standard.set("720p HD at 30 fps", forKey: "cameraquality")
     //  cameraManager.cameraOutputQuality = .high
       cameraManager.cameraOutputQuality = .hd1280x720
            qualityLabel.text = "720p HD at 30 fps"
        framerate = 30
        fps = "30 fps"
        resolution = "1280x"
        }

        qualitySettingsTable.reloadData()



}


    @available(iOS 10.0, *)
    func availableDeviceTypes() -> [AVCaptureDevice.DeviceType] {
        var deviceTypes: [AVCaptureDevice.DeviceType] = [.builtInTelephotoCamera, .builtInWideAngleCamera]
            deviceTypes.append(.builtInDualCamera)
            if #available(iOS 11.1, *) {
                deviceTypes.append(.builtInTrueDepthCamera)
            }

        return deviceTypes
    }

    func videoCaptureDevice(at position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        var device: AVCaptureDevice? = nil
        if #available(iOS 10, *) {
            let deviceTypes = availableDeviceTypes()
            let session = AVCaptureDevice.DiscoverySession(deviceTypes: deviceTypes, mediaType: .video, position: position)
            device = session.devices.first
        } else {
            device = AVCaptureDevice.devices(for: AVMediaType.video).filter( { $0.position == position }).first
        }
        return device
    }


    func startVideoCapture() {

        cameraManager.startRecordingVideo()
       }
       func stopVideoCapture() {


           cameraManager.stopVideoRecording { (videoURL: URL?, error) -> Void in
               if error != nil {
                   self.cameraManager.showErrorBlock("Error occurred", "Cannot save video.")
               }
               else {
                self.pickedVideoURL = videoURL
                print(self.pickedVideoURL ?? "")
                let asset = AVAsset(url: self.pickedVideoURL!)
                let imageGenerator = AVAssetImageGenerator(asset: asset)
                 imageGenerator.appliesPreferredTrackTransform=true
                let time: CMTime = CMTimeMake(1, 1)
                let imageRef = try? imageGenerator.copyCGImage(at: time, actualTime: nil)

                var thumbnail: UIImage? = nil
                if let imageRef = imageRef {
                thumbnail = UIImage(cgImage: imageRef)
                }
                self.userThumbImage.image = thumbnail

                UISaveVideoAtPathToSavedPhotosAlbum((self.pickedVideoURL?.path)!, self, #selector(captureVC.video(videoPath:didFinishSavingWithError:contextInfo:)), nil)



          //  self.perform(#selector(self.getvideonew), with: nil, afterDelay: 05)

               }
           }

       }


    @objc func video(videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject)
     {
        if let _ = error {
           print("Error,Video failed to save")
        }else{
           print("Successfully,Video was saved")
            self.getvideonew()
        }
    }

    func takephoto(){



        self.btnsend.isUserInteractionEnabled = true

        cameraManager.capturePictureWithCompletion { result in
            switch result {
                case .failure:
                    self.cameraManager.showErrorBlock("Error occurred", "Cannot save picture.")
                case .success(let content):
                    let capturedData = content.asData
                    print(capturedData!.printExifData())
                    self.pickedImage = UIImage(data: capturedData!)!

                    self.userThumbImage.image = self.pickedImage
                    UIImageWriteToSavedPhotosAlbum(self.pickedImage!, self, nil
                        ,nil)

                    print("image saved to device successfully")
                    self.perform(#selector(self.getimagenew), with: nil, afterDelay: 05)
            }
        }

    }
    
    @objc func rotated() {
             
            if mediaType?.rawValue == 2  {
            if UIDevice.current.orientation.isLandscape {
                
                if !IS_IPHONE {
              viewmeter1.isHidden = false
               }
                
            } else {
                if !IS_IPHONE {
            viewmeter1.isHidden = true
                     }
            }
      }
         
    }

}
extension captureVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return QualitySettingsList.count
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        qualitySettingsTable.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")as! QualitySettingsListCell
        tableView.tableFooterView = UIView()
        let cellData = QualitySettingsList[indexPath.row]
        cell.qualityTitleLbl.text = cellData
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {


        DispatchQueue.main.async{
        KGModal.sharedInstance()?.hide()
        }

            print("Selected Indexpath.row: \(indexPath.row) & quality Setting is: \(self.QualitySettingsList[indexPath.row])")
            self.qualityLabel.text = QualitySettingsList[indexPath.row]



            UserDefaults.standard.set(self.QualitySettingsList[indexPath.row], forKey: "cameraquality")



         if QualitySettingsList[indexPath.row] == "cif 352x288" {
            cameraManager.cameraOutputQuality = .cif352x288
            framerate = 25
            fps = ""
            resolution = ""

        }  else if QualitySettingsList[indexPath.row] == "vga 640x480" {
            cameraManager.cameraOutputQuality = .vga640x480
            framerate = 25
            fps = ""
            resolution = ""

        }
        else if QualitySettingsList[indexPath.row] == "iFrame 960x540" {
            cameraManager.cameraOutputQuality = .iFrame960x540
            framerate = 25
            fps = ""
            resolution = ""

        }

        else if QualitySettingsList[indexPath.row] == "720p HD at 30 fps" {
            cameraManager.cameraOutputQuality = .hd1280x720
            framerate = 30
            fps = "30 fps"
            resolution = "1280x"

        }

        else if QualitySettingsList[indexPath.row] == "1080 HD at 30 fps" {
           cameraManager.cameraOutputQuality = .hd1920x1080
            framerate = 30
             fps = "30 fps"
            resolution = "1920x1080"

        }

        else if QualitySettingsList[indexPath.row] == "1080 HD at 60 fps" {
            cameraManager.cameraOutputQuality = .hd1920x1080
            framerate = 60
            fps = "60 fps"
            resolution = "1920x1080"



        }

        else if QualitySettingsList[indexPath.row] == "4K at 30 fps" {
         cameraManager.cameraOutputQuality = .hd4K3840x2160
            framerate = 30
            fps = "30 fps"
            resolution = "3840x2160"



        }
        else if QualitySettingsList[indexPath.row] == "4K at 60 fps" {

        cameraManager.cameraOutputQuality = .hd4K3840x2160
            framerate = 60
            fps = "60 fps"
            resolution = "3840x2160"
        }

        else {
//            cameraManager.cameraOutputQuality = .high
//            framerate = 30
//            fps = ""
//            resolution = ""
            
            cameraManager.cameraOutputQuality = .hd1280x720
            framerate = 30
            fps = "30 fps"
            resolution = "1280x"
        }

    }

}

public extension Data {
    func printExifData() {
        let cfdata: CFData = self as CFData
        let imageSourceRef = CGImageSourceCreateWithData(cfdata, nil)
        let imageProperties = CGImageSourceCopyMetadataAtIndex(imageSourceRef!, 0, nil)!
        let mutableMetadata = CGImageMetadataCreateMutableCopy(imageProperties)!
        CGImageMetadataEnumerateTagsUsingBlock(mutableMetadata, nil, nil) { _, tag in
            print(CGImageMetadataTagCopyName(tag)!, ":", CGImageMetadataTagCopyValue(tag)!)
            return true
        }
    }
}


public extension UIDevice {

    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                       return "iPod touch (5th generation)"
            case "iPod7,1":                                       return "iPod touch (6th generation)"
            case "iPod9,1":                                       return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":           return "iPhone 4"
            case "iPhone4,1":                                     return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                        return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                        return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                        return "iPhone 5s"
            case "iPhone7,2":                                     return "iPhone 6"
            case "iPhone7,1":                                     return "iPhone 6 Plus"
            case "iPhone8,1":                                     return "iPhone 6s"
            case "iPhone8,2":                                     return "iPhone 6s Plus"
            case "iPhone8,4":                                     return "iPhone SE"
            case "iPhone9,1", "iPhone9,3":                        return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                        return "iPhone 7 Plus"
            case "iPhone10,1", "iPhone10,4":                      return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                      return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                      return "iPhone X"
            case "iPhone11,2":                                    return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                      return "iPhone XS Max"
            case "iPhone11,8":                                    return "iPhone XR"
            case "iPhone12,1":                                    return "iPhone 11"
            case "iPhone12,3":                                    return "iPhone 11 Pro"
            case "iPhone12,5":                                    return "iPhone 11 Pro Max"
            case "iPhone12,8":                                    return "iPhone SE (2nd generation)"
            case "iPhone13,1":                                    return "iPhone 12 mini"
            case "iPhone13,2":                                    return "iPhone 12"
            case "iPhone13,3":                                    return "iPhone 12 Pro"
            case "iPhone13,4":                                    return "iPhone 12 Pro Max"
            case "iPhone14,4":                                    return "iPhone 13 mini"
            case "iPhone14,5":                                    return "iPhone 13"
            case "iPhone14,2":                                    return "iPhone 13 Pro"
            case "iPhone14,3":                                    return "iPhone 13 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":      return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":                 return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":                 return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                          return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                            return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                          return "iPad (7th generation)"
            case "iPad11,6", "iPad11,7":                          return "iPad (8th generation)"
            case "iPad12,1", "iPad12,2":                          return "iPad (9th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":                 return "iPad Air"
            case "iPad5,3", "iPad5,4":                            return "iPad Air 2"
            case "iPad11,3", "iPad11,4":                          return "iPad Air (3rd generation)"
            case "iPad13,1", "iPad13,2":                          return "iPad Air (4th generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":                 return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":                 return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":                 return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                            return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                          return "iPad mini (5th generation)"
            case "iPad14,1", "iPad14,2":                          return "iPad mini (6th generation)"
            case "iPad6,3", "iPad6,4":                            return "iPad Pro (9.7-inch)"
            case "iPad7,3", "iPad7,4":                            return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":      return "iPad Pro (11-inch) (1st generation)"
            case "iPad8,9", "iPad8,10":                           return "iPad Pro (11-inch) (2nd generation)"
            case "iPad13,4", "iPad13,5", "iPad13,6", "iPad13,7":  return "iPad Pro (11-inch) (3rd generation)"
            case "iPad6,7", "iPad6,8":                            return "iPad Pro (12.9-inch) (1st generation)"
            case "iPad7,1", "iPad7,2":                            return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":      return "iPad Pro (12.9-inch) (3rd generation)"
            case "iPad8,11", "iPad8,12":                          return "iPad Pro (12.9-inch) (4th generation)"
            case "iPad13,8", "iPad13,9", "iPad13,10", "iPad13,11":return "iPad Pro (12.9-inch) (5th generation)"
            case "AppleTV5,3":                                    return "Apple TV"
            case "AppleTV6,2":                                    return "Apple TV 4K"
            case "AudioAccessory1,1":                             return "HomePod"
            case "AudioAccessory5,1":                             return "HomePod mini"
            case "i386", "x86_64", "arm64":                                return "\(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                              return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "\(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }

        return mapToDevice(identifier: identifier)
    }()

}

//public extension UIDevice {
//
//    var modelName: String {
//
//        #if targetEnvironment(simulator)
//            let identifier = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"]!
//        #else
//            var systemInfo = utsname()
//            uname(&systemInfo)
//            let machineMirror = Mirror(reflecting: systemInfo.machine)
//            let identifier = machineMirror.children.reduce("") { identifier, element in
//                guard let value = element.value as? Int8 , value != 0 else { return identifier }
//                return identifier + String(UnicodeScalar(UInt8(value)))
//            }
//        #endif
//
//        switch identifier {
//        case "iPod5,1":                                 return "iPod Touch 5"
//        case "iPod7,1":                                 return "iPod Touch 6"
//        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
//        case "iPhone4,1":                               return "iPhone 4s"
//        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
//        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
//        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
//        case "iPhone7,2":                               return "iPhone 6"
//        case "iPhone7,1":                               return "iPhone 6 Plus"
//        case "iPhone8,1":                               return "iPhone 6s"
//        case "iPhone8,2":                               return "iPhone 6s Plus"
//        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
//        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
//        case "iPhone8,4":                               return "iPhone SE"
//        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
//        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
//        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
//        case "iPhone11,2":                              return "iPhone XS"
//        case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
//        case "iPhone11,8":                              return "iPhone XR"
//        case "iPhone12,1":                              return "iPhone 11"
//        case "iPhone12,3":                              return "iPhone 11 Pro"
//        case "iPhone12,5":                              return "iPhone 11 Pro Max"
//        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
//        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
//        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
//        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
//        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
//        case "iPad6,11", "iPad6,12":                    return "iPad 5"
//        case "iPad7,5", "iPad7,6":                      return "iPad 6"
//        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
//        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
//        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
//        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
//        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
//        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
//        case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
//        case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
//        case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
//        case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
//        case "AppleTV5,3":                              return "Apple TV"
//        case "AppleTV6,2":                              return "Apple TV 4K"
//        case "AudioAccessory1,1":                       return "HomePod"
//        default:                                        return identifier
//        }
//    }
//
//}




